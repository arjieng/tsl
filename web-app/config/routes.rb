require "resque_web"
Rails.application.routes.draw do

  get 'purchases/shop'

  mount ResqueWeb::Engine => "/resque_web"

	get 'customer_users/edit_personal_information'
	get 'customer_users/edit_billing_information'

	# User Accessible Site

	# Shop
	get 	'shop',							to: 'purchases#shop'
	get 	'add-to-cart/:id',				to: 'purchases#add_to_cart'
	post	'add-to-cart',					to: 'purchases#create_order'
	get		'cart-overview',				to: 'purchases#cart_overview',			as: 'cart_overview'
	delete  'cancel-order/:id',				to: 'purchases#delete_order',			as: 'delete_order'
	post 	'orders/purchase',				to: 'purchases#create',					as: 'purchase_orders'
	get		'successful-transaction/:id', 	to: 'purchases#successful_purchase',	as: 'successful_transaction'

	devise_for :customer_users, controllers: { registrations: 'registrations', passwords: 'api/v1/customer_user_passwords' }
	authenticated :customer_user do
		root 'dashboard#index', as: "authenticated_root"
	end
	devise_scope :customer_user do
		root "devise/sessions#new"
	end
	resources :customers, only: [:create, :update]
	resources :invoices, only: [:show, :index] do
		post 'submit_payment', on: :collection
	end
	resources :receipts, only: [:index, :show] do
		get 'summative_receipt', as: :summative, on: :collection
	end	

	resources :customer_users do
		get :edit_personal_information, on: :collection
		get :edit_billing_information, on: :collection
		post :update_card, on: :collection
		post :add_card, on: :collection
		post :unlink_card, on: :collection
		post :update_auto_pay, on: :collection
	end

	resources :locations do
		get 'get_address', on: :collection
		get 'get_minimum_age', on: :collection
		get 'before_and_after_school_availability', on: :collection
	end
	resources :programs do
		get 'get_capacity', on: :collection
		get 'get_single_day_dates', on: :collection
		get 'get_program', 		on: :collection
		get 'get_summer_camp_weeks', on: :collection
		get 'get_vacation_camp_schedules', on: :collection
	end

	

	get 'registration_information' => 'dashboard#registration_information'
	get '/registration_information/:id', to: 'dashboard#registration_information_show', as: 'registration_information_show'

	resources :cit_records, only: :create
	get 'cit_registration' => 'cit_records#registration'
	get 'cit-registration' => 'cit_records#registration'

	resources :registration_records, only: :create
	get 'single_day' => 'registration_records#single_day'
	get 'single-day' => 'registration_records#single_day'
	get 'single-day-terms-of-service' => 'static_pages#single_day_terms_of_service'

	get 'request_day_care_tour' => 'registration_records#request_day_care_tour'
	get 'request-day-care-tour' => 'registration_records#request_day_care_tour'
	get 'request-day-care-tour-terms-of-service' => 'static_pages#request_day_care_tour_terms_of_service'

	get 'day_care' => 'registration_records#day_care'
	get 'day-care' => 'registration_records#day_care'

	get 'before_and_afterschool' => 'registration_records#before_and_afterschool'
	get 'before-and-afterschool' => 'registration_records#before_and_afterschool'
	get 'before-and-afterschool-terms-of-service' => 'static_pages#before_and_afterschool_terms_of_service'
	get 'before-and-afterschool-rates' => 'static_pages#before_and_after_school_rates'
	get 'no-deposit-before-and-afterschool' => 'registration_records#no_deposit_before_and_afterschool'

	get 'groupon-summer-camp' => 'registration_records#groupon_summer_camp'
	get 'summer_camp' => 'registration_records#summer_camp'
	get 'summer-camp' => 'registration_records#summer_camp'
	get 'summer-camp-terms-of-service' => 'static_pages#summer_camp_terms_of_service'

	get 'vacation_camp' => 'registration_records#vacation_camp'
	get 'vacation-camp' => 'registration_records#vacation_camp'
	get 'vacation-camp-terms-of-service' => 'static_pages#vacation_camp_terms_of_service'

	get 'success' => 'registration_records#success'
	get 'cit_success' => 'cit_records#success'

	

	# Admin Panel
	namespace :admin do
		get '' => 'registration_records#index'
		get 'home' => 'registration_records#index'
		get 'dashboard' => 'dashboard#dashboard'
		get 'payroll' => 'payrolls#home'

		# shop
		get 'shop-purchases', 		to: 'purchases#index',		as: 'shop_purchases'
		get 'purchase-details/:id',	to: 'purchases#details',	as: 'purchase_details'

		# products
		get 'products',				to:	'products#index'

		devise_for :users, controllers: { omniauth_callbacks: "admin/omniauth_callbacks", registrations: "admin/registrations", sessions: "admin/sessions" }
		authenticated :user do
			root 'registration_records#index', as: "authenticated_root"
		end
		devise_scope :user do
			root "devise/sessions#new"
		end
        get  'new-invoices/:id'     => 'registration_records#new_invoices'   , as: 'new_invoices'
        patch 'create-invoices/:id'  => 'registration_records#create_invoices', as: 'create_invoices'

		get '/payment_records/:id' => 'registration_records#show' # makes old routes work still
		resources :registration_records do
			put 'approve', on: :member
			put 'email', on: :member
		end
		resources :cit_records
		resources :locations
		resources :programs
		resources :discounts
		resources :invoices
		resources :customers

		# new vacation_camp_schedule
		get		'programs/:id/new_vacation_camp_schedule',		to: 'programs#new_vacation_camp_schedule', as: 'new_camp_schedule'
		post	'programs/create_new_vacation_camp_schedule',	to: 'programs#create_vacation_camp_schedule', as: 'create_camp_schedule'
        
        # update vacation camp enrollment
		post    'update_vacation_camp_enrollment',              to: 'registration_records#update_vacation_camp_enrollment', as: 'update_vacation_record'
		
		scope :api do
			get 	'monthly-summary',			to: 'api/dashboard#monthly_summary'
			get 	'yearly-summary',			to: 'api/dashboard#yearly_summary'
			get 	'search-customer',			to: 'api/dashboard#search_customer'
			get 	'customer-information',		to: 'api/dashboard#customer_information'
			get 	'program-breakdown', 		to: 'api/dashboard#program_breakdown'
			get 	'programs-summary',			to: 'api/dashboard#programs_summary'
			get		'receipt-information/:id',	to: 'api/dashboard#receipt_information'
			post 	'update-receipt',			to: 'api/dashboard#receipt_update'
		end

		# payroll routes
		scope :payroll do
			get		'accessible-locations/:id',									to: 'payrolls#retrieve_accessible_payroll_locations'
			post	'add-new-payroll-employee', 								to: 'payrolls#add_new_payroll_employee'
			get		'retrieve-payroll-location-information/:location_id/:date',	to: 'payrolls#retrieve_payroll_location_information'
			post	'delete-payroll-employee',									to: 'payrolls#delete_payroll_employee'
			post	'update-payroll-employee-information',						to: 'payrolls#update_payroll_employee_information'
			post	'create-payroll-hour-for-new-employee',						to: 'payrolls#create_payroll_hour_for_new_employee'
			post	'update-payroll-employee-logged-hours',						to: 'payrolls#update_payroll_employee_logged_hours'
			get		'retrieve-all-payroll-locations',							to: 'payrolls#retrieve_all_payroll_locations'
			post	'add-new-payroll-location',									to: 'payrolls#add_new_payroll_location'
			get		'retrieve-all-administrators',								to: 'payrolls#retrieve_all_administrators'
			get		'retrieve-all-authorized-administrators/:location_id',		to: 'payrolls#retrieve_all_authorized_administrators'
			post	'revoke-access-to-payroll-location',						to: 'payrolls#revoke_access_to_payroll_location'
			post	'authorize-access-to-payroll-location',						to: 'payrolls#authorize_access_to_payroll_location'
			post	'update-payroll-location-name',								to: 'payrolls#update_payroll_location_name'
			get		'retrieve-all-payroll-locations-breakdown/:year/:month',	to: 'payrolls#retrieve_all_payroll_locations_breakdown'
			get		'retrieve-all-employees-of-payroll-location/:location_id',	to: 'payrolls#retrieve_all_employees_of_payroll_location'
			post	'log-employee-absence',										to: 'payrolls#log_employee_absence'
			get		'payroll-trend-analysis/:year/:month/:location_id',			to: 'payrolls#retrieve_payroll_location_information_for_trend_analysis'
		end
	end

	# API
	namespace :api, default: { format: :json } do
		scope :v1 do
			post '/test' 							=> 'v1/payments#test'
			post '/payments' 						=> 'v1/payments#create'
			post '/invoices' 						=> 'v1/invoices#generate' # callback api for temporize to generate invoices
			post '/invoices/overdue/end-of-month-afterschool-notification'		=> 'v1/invoices#end_of_month_afterschool_ultimatum' # callback for overdue invoices every end of month for families
			post '/invoices/overdue/fee'			=> 'v1/invoices#generate_late_fees' # generate overdue fees
			post '/invoices/autopay'				=> 'v1/invoices#autopay_invoices' # autopay invoices
			post '/invoices/overdue/one-week'		=> 'v1/invoices#overdue_by_one_week' # send email notification for customer and Tom every 5th of the month
			post '/invoices/unpaid/daycare'			=> 'v1/invoices#notify_tom_for_unpaid_daycare_invoices'
			post '/records/obsolete/afterschool'	=> 'v1/obsolete_records#update_obsolete_before_and_afterschool_records' # update obsolete after school registrations
			post '/records/obsolete/single-day'		=> 'v1/obsolete_records#update_obsolete_single_day_registrations' # obsolete single day registrations
			post '/records/obsolete/summer-camp'	=> 'v1/obsolete_records#update_obsolete_summer_camp_registrations' # obsolete summer camp registrations
			post '/records/obsolete/vacation-camp'	=> 'v1/obsolete_records#update_obsolete_vacation_camp_registrations' # obsolete vacation camp records
			post '/invoices/overdue/last-month'		=> 'v1/invoices#unpaid_invoices_last_month'
			post '/invoices/unpaid/after-school'	=> 'v1/invoices#unpaid_afterschool_invoices'
            post '/invoices/summer-camp/due'        => 'v1/invoices#due_summer_camp_invoices'
			post '/invoices/summer-camp/overdue'    => 'v1/invoices#overdue_summer_camp_invoices'
			post '/invoices/summer-camp/autopay'	=> 'v1/invoices#autopay_summer_camp_invoices'
            post '/invoices/background-generate'    => 'v1/invoices#background_generate'
			post '/invoices/test'                   => 'v1/invoices#test'
			post '/overdue/end-of-month-after-school'			=> 'v1/invoices#end_of_month_unpaid_after_school_notification' # end of month before/after school notification for Tom and Kyle
		end

		namespace :v1 do
			devise_scope :customer_user do
				post 'sign-in' => 'customer_user_sessions#create'
				post 'register' => 'customer_user_registrations#create'
				post 'forgot_password' => 'customer_user_passwords#create'
				get 'forgot_password/edit' => 'customer_user_passwords#edit'
				patch 'forgot_password' => 'customer_user_passwords#update'
				post 'check_password' => 'customer_user_passwords#check_password'
				put 'change_password' => 'customer_user_passwords#change_password'
			end

			resources :customers, only: [:create, :update] do
				collection do
					get :fetch_contact_informations
				end
			end

			resources :registration_records, only: [:create]
			resources :data, only: [:index] do
				collection do
					get :get_locations
					get :get_publishable_key
					get :before_and_after_school_availability
					get :get_single_day_dates
					get :get_summer_camp_weeks
					get :get_vacation_camp_schedules
					get :get_invoices
				end
			end

			resources :dashboard, except: [:index, :create, :update, :show, :new, :edit, :destroy] do
				collection do
					get :registration_information
					get :registration_information_show
				end
			end

			resources :invoices, only: [:show]
			resources :receipts, only: [:index, :show]
			resources :cit_records, only: [:create]
			resources :customer_users, only: [:index] do
				collection do
					put :update_card
					post :add_card
					put :unlink_card
					put :update_auto_pay
				end
			end
		end
	end

end
