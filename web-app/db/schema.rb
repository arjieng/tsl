# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190704073226) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "annual_single_day_promo_records", force: :cascade do |t|
    t.integer  "registration_record_id"
    t.string   "promo_name"
    t.string   "year_cycle"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "annual_single_day_promo_records", ["registration_record_id"], name: "index_annual_single_day_promo_records_on_registration_record_id", using: :btree

  create_table "before_and_after_school_program_configurations", force: :cascade do |t|
    t.boolean  "before_school", default: false
    t.boolean  "after_school",  default: false
    t.integer  "program_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "before_and_after_school_program_configurations", ["program_id"], name: "index_before_and_after_school_program_config_on_program_id", using: :btree

  create_table "before_and_afterschool_records", force: :cascade do |t|
    t.date     "start_date"
    t.boolean  "before_school_monday",    default: false
    t.boolean  "before_school_tuesday",   default: false
    t.boolean  "before_school_wednesday", default: false
    t.boolean  "before_school_thursday",  default: false
    t.boolean  "before_school_friday",    default: false
    t.boolean  "after_school_monday",     default: false
    t.boolean  "after_school_tuesday",    default: false
    t.boolean  "after_school_wednesday",  default: false
    t.boolean  "after_school_thursday",   default: false
    t.boolean  "after_school_friday",     default: false
    t.integer  "registration_record_id"
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.string   "registration_type"
    t.string   "year_cycle"
    t.boolean  "no_deposit_required",     default: false
  end

  create_table "child_records", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "dob"
    t.string   "child_classification"
    t.string   "school_attending"
    t.integer  "registration_record_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "cit_records", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "dob"
    t.string   "phone_number"
    t.string   "email"
    t.string   "address_line_one"
    t.string   "address_line_two"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "how_did_you_hear_about_us"
    t.string   "location"
    t.boolean  "week_one",                      default: false
    t.boolean  "week_two",                      default: false
    t.boolean  "week_three",                    default: false
    t.boolean  "week_four",                     default: false
    t.boolean  "week_five",                     default: false
    t.boolean  "week_six",                      default: false
    t.boolean  "week_seven",                    default: false
    t.boolean  "week_eight",                    default: false
    t.boolean  "week_nine",                     default: false
    t.boolean  "week_ten",                      default: false
    t.boolean  "attended_tsl_before"
    t.string   "attended_location"
    t.boolean  "cit_at_tsl_before"
    t.string   "cit_at_tsl_location"
    t.text     "previous_experience"
    t.text     "why_are_you_interested"
    t.boolean  "willing_to_follow_directions"
    t.text     "action_when_completed_tasks"
    t.text     "arguing_action"
    t.text     "transition_help"
    t.text     "interests_and_talents"
    t.text     "proven_responsibility"
    t.text     "difficult_problem"
    t.text     "what_you_will_bring"
    t.boolean  "willing_to_meet_vision"
    t.boolean  "agree_to_behavior_policy",      default: false
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.text     "water_play_action"
    t.text     "four_square_action"
    t.text     "uninteresting_activity_action"
    t.text     "made_a_difference"
  end

  create_table "contact_informations", force: :cascade do |t|
    t.string   "email"
    t.integer  "customer_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  add_index "contact_informations", ["customer_id"], name: "index_contact_informations_on_customer_id", using: :btree

  create_table "customer_users", force: :cascade do |t|
    t.integer  "customer_id"
    t.integer  "integer"
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.boolean  "confirmed_information",  default: false
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "access_token"
  end

  add_index "customer_users", ["email"], name: "index_customer_users_on_email", unique: true, using: :btree
  add_index "customer_users", ["reset_password_token"], name: "index_customer_users_on_reset_password_token", unique: true, using: :btree

  create_table "customers", force: :cascade do |t|
    t.string   "email"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "phone_number"
    t.string   "emergency_phone_number"
    t.string   "address_line_one"
    t.string   "address_line_two"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "how_did_you_hear_about_us"
    t.string   "stripe_customer_identifier"
    t.string   "customer_url"
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
    t.string   "card_last_four"
    t.string   "card_brand"
    t.string   "stripe_bank_identifier"
    t.boolean  "bank_verified",              default: false
    t.string   "bank_name"
    t.boolean  "autopay_enabled",            default: false
    t.boolean  "is_deleted",                 default: false
  end

  create_table "day_care_records", force: :cascade do |t|
    t.date     "start_date"
    t.integer  "registration_record_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "discounts", force: :cascade do |t|
    t.integer  "registration_record_id"
    t.integer  "invoice_id"
    t.decimal  "amount",                 default: 0.0
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "program_id"
  end

  create_table "emergency_contacts", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "phone_number"
    t.integer  "cit_record_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "fees", force: :cascade do |t|
    t.integer  "invoice_id"
    t.decimal  "amount",     default: 0.0
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "deleted",    default: false
  end

  create_table "invoices", force: :cascade do |t|
    t.integer  "registration_record_id"
    t.integer  "amount",                 default: 0
    t.boolean  "paid",                   default: false
    t.string   "memo"
    t.date     "due_date"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "amount_paid",            default: 0
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.string   "address"
    t.integer  "director_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "minimum_age", default: 5
  end

  create_table "orders", force: :cascade do |t|
    t.integer  "product_id"
    t.integer  "size_id"
    t.integer  "quantity"
    t.boolean  "purchased",                            default: false
    t.decimal  "total_amount", precision: 8, scale: 2
    t.integer  "customer_id"
    t.datetime "created_at",                                           null: false
    t.datetime "updated_at",                                           null: false
  end

  add_index "orders", ["customer_id"], name: "index_orders_on_customer_id", using: :btree
  add_index "orders", ["product_id"], name: "index_orders_on_product_id", using: :btree
  add_index "orders", ["size_id"], name: "index_orders_on_size_id", using: :btree

  create_table "payroll_employee_abscences", force: :cascade do |t|
    t.integer  "payroll_location_id"
    t.integer  "payroll_employee_id"
    t.string   "abscence_type"
    t.date     "abscence_date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "payroll_employee_abscences", ["payroll_employee_id"], name: "index_payroll_employee_abscences_on_payroll_employee_id", using: :btree
  add_index "payroll_employee_abscences", ["payroll_location_id"], name: "index_payroll_employee_abscences_on_payroll_location_id", using: :btree

  create_table "payroll_employee_hours", force: :cascade do |t|
    t.integer  "payroll_employee_id"
    t.integer  "payroll_location_id"
    t.integer  "logged_hours"
    t.date     "logged_hours_date"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "payroll_employee_hours", ["payroll_employee_id"], name: "index_payroll_employee_hours_on_payroll_employee_id", using: :btree
  add_index "payroll_employee_hours", ["payroll_location_id"], name: "index_payroll_employee_hours_on_payroll_location_id", using: :btree

  create_table "payroll_employees", force: :cascade do |t|
    t.string   "name"
    t.integer  "payroll_location_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "payroll_employees", ["payroll_location_id", "name"], name: "index_payroll_employees_on_payroll_location_id_and_name", unique: true, using: :btree
  add_index "payroll_employees", ["payroll_location_id"], name: "index_payroll_employees_on_payroll_location_id", using: :btree

  create_table "payroll_location_accesses", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "payroll_location_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  add_index "payroll_location_accesses", ["payroll_location_id"], name: "index_payroll_location_accesses_on_payroll_location_id", using: :btree
  add_index "payroll_location_accesses", ["user_id"], name: "index_payroll_location_accesses_on_user_id", using: :btree

  create_table "payroll_locations", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "products", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.string   "image"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "programs", force: :cascade do |t|
    t.string   "name"
    t.integer  "director_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "location_id"
    t.boolean  "active",            default: true
    t.string   "name_suffix"
    t.integer  "capacity",          default: 0
    t.boolean  "is_package_active", default: true
  end

  create_table "purchases", force: :cascade do |t|
    t.integer  "customer_id"
    t.string   "orders"
    t.decimal  "total_amount"
    t.string   "payment_url"
    t.string   "transaction_identifier"
    t.integer  "location_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "purchases", ["customer_id"], name: "index_purchases_on_customer_id", using: :btree
  add_index "purchases", ["location_id"], name: "index_purchases_on_location_id", using: :btree

  create_table "receipts", force: :cascade do |t|
    t.integer  "registration_record_id"
    t.integer  "invoice_id"
    t.integer  "customer_id"
    t.decimal  "amount"
    t.string   "program_name"
    t.string   "payment_for"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "purchase_id"
  end

  add_index "receipts", ["customer_id"], name: "index_receipts_on_customer_id", using: :btree
  add_index "receipts", ["purchase_id"], name: "index_receipts_on_purchase_id", using: :btree

  create_table "registration_records", force: :cascade do |t|
    t.string   "email"
    t.string   "old_program"
    t.boolean  "approved",                      default: false
    t.datetime "approved_at"
    t.boolean  "emailed",                       default: false
    t.datetime "emailed_at"
    t.datetime "created_at",                                    null: false
    t.datetime "updated_at",                                    null: false
    t.string   "transaction_identifier"
    t.string   "parent_first_name"
    t.string   "parent_last_name"
    t.string   "phone_number"
    t.string   "emergency_phone_number"
    t.string   "address_line_one"
    t.string   "address_line_two"
    t.string   "city"
    t.string   "state"
    t.string   "zip_code"
    t.string   "how_did_you_hear_about_us"
    t.string   "location"
    t.string   "payment_platform"
    t.string   "payment_url"
    t.string   "customer_url"
    t.decimal  "amount_paid",                   default: 0.0
    t.string   "customer_identifier"
    t.string   "coupon_code"
    t.boolean  "agree_to_tos",                  default: false
    t.integer  "program_id"
    t.integer  "customer_id"
    t.boolean  "enrolled",                      default: true
    t.boolean  "bill_afterschool_end_of_month", default: false
    t.boolean  "deleted",                       default: false
  end

  create_table "single_day_dates", force: :cascade do |t|
    t.string   "name"
    t.date     "date"
    t.integer  "capacity",   default: 0
    t.integer  "program_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "single_day_records", force: :cascade do |t|
    t.integer  "registration_record_id"
    t.string   "day"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "date"
    t.integer  "single_day_date_id"
    t.string   "registration_type"
  end

  create_table "sizes", force: :cascade do |t|
    t.string   "name"
    t.integer  "quantity"
    t.decimal  "price",      precision: 8, scale: 2
    t.integer  "product_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "color"
  end

  add_index "sizes", ["product_id"], name: "index_sizes_on_product_id", using: :btree

  create_table "summer_camp_records", force: :cascade do |t|
    t.boolean  "week_one",               default: false
    t.boolean  "week_two",               default: false
    t.boolean  "week_three",             default: false
    t.boolean  "week_four",              default: false
    t.boolean  "week_five",              default: false
    t.boolean  "week_six",               default: false
    t.boolean  "week_seven",             default: false
    t.boolean  "week_eight",             default: false
    t.boolean  "week_nine",              default: false
    t.boolean  "week_ten",               default: false
    t.integer  "registration_record_id"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.string   "promo_name"
  end

  create_table "summer_camp_weeks", force: :cascade do |t|
    t.string   "name"
    t.date     "start_date"
    t.integer  "capacity",   default: 0
    t.text     "notes"
    t.integer  "program_id"
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.boolean  "deleted",    default: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "name"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  create_table "vacation_camp_records", force: :cascade do |t|
    t.boolean  "monday",                    default: false
    t.boolean  "tuesday",                   default: false
    t.boolean  "wednesday",                 default: false
    t.boolean  "thursday",                  default: false
    t.boolean  "friday",                    default: false
    t.integer  "registration_record_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "month"
    t.integer  "vacation_camp_schedule_id"
  end

  add_index "vacation_camp_records", ["vacation_camp_schedule_id"], name: "index_vacation_camp_records_on_vacation_camp_schedule_id", using: :btree

  create_table "vacation_camp_schedules", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.string   "month"
    t.boolean  "day_one",          default: false
    t.boolean  "day_two",          default: false
    t.boolean  "day_three",        default: false
    t.boolean  "day_four",         default: false
    t.boolean  "day_five",         default: false
    t.boolean  "day_six",          default: false
    t.boolean  "day_seven",        default: false
    t.boolean  "day_eight",        default: false
    t.boolean  "day_nine",         default: false
    t.boolean  "day_ten",          default: false
    t.boolean  "day_eleven",       default: false
    t.boolean  "day_twelve",       default: false
    t.boolean  "day_thirteen",     default: false
    t.boolean  "day_fourteen",     default: false
    t.boolean  "day_fifteen",      default: false
    t.boolean  "day_sixteen",      default: false
    t.boolean  "day_seventeen",    default: false
    t.boolean  "day_eighteen",     default: false
    t.boolean  "day_nineteen",     default: false
    t.boolean  "day_twenty",       default: false
    t.boolean  "day_twenty_one",   default: false
    t.boolean  "day_twenty_two",   default: false
    t.boolean  "day_twenty_three", default: false
    t.boolean  "day_twenty_four",  default: false
    t.boolean  "day_twenty_five",  default: false
    t.boolean  "day_twenty_six",   default: false
    t.boolean  "day_twenty_seven", default: false
    t.boolean  "day_twenty_eight", default: false
    t.boolean  "day_twenty_nine",  default: false
    t.boolean  "day_thirty",       default: false
    t.boolean  "day_thirty_one",   default: false
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "capacity",         default: 0
    t.integer  "program_id"
    t.boolean  "deleted",          default: false
  end

  add_index "vacation_camp_schedules", ["program_id"], name: "index_vacation_camp_schedules_on_program_id", using: :btree

  add_foreign_key "annual_single_day_promo_records", "registration_records"
  add_foreign_key "before_and_after_school_program_configurations", "programs"
  add_foreign_key "contact_informations", "customers"
  add_foreign_key "orders", "customers"
  add_foreign_key "orders", "products"
  add_foreign_key "orders", "sizes"
  add_foreign_key "payroll_employee_abscences", "payroll_employees"
  add_foreign_key "payroll_employee_abscences", "payroll_locations"
  add_foreign_key "payroll_employee_hours", "payroll_employees"
  add_foreign_key "payroll_employee_hours", "payroll_locations"
  add_foreign_key "payroll_employees", "payroll_locations"
  add_foreign_key "payroll_location_accesses", "payroll_locations"
  add_foreign_key "payroll_location_accesses", "users"
  add_foreign_key "purchases", "customers"
  add_foreign_key "purchases", "locations"
  add_foreign_key "receipts", "purchases"
  add_foreign_key "sizes", "products"
end
