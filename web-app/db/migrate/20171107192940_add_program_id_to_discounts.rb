class AddProgramIdToDiscounts < ActiveRecord::Migration
  def change
    add_column :discounts, :program_id, :integer
  end
end
