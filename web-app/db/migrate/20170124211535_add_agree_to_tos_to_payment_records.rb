class AddAgreeToTosToPaymentRecords < ActiveRecord::Migration
  def change
    add_column :payment_records, :agree_to_tos, :boolean, default: false
  end
end
