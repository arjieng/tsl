class CreateBeforeAndAfterschoolRecords < ActiveRecord::Migration
  def change
    create_table :before_and_afterschool_records do |t|
      t.date :start_date
      t.boolean :before_school_monday, default: false
			t.boolean :before_school_tuesday, default: false
			t.boolean :before_school_wednesday, default: false
			t.boolean :before_school_thursday, default: false
			t.boolean :before_school_friday, default: false

      t.boolean :after_school_monday, default: false
			t.boolean :after_school_tuesday, default: false
			t.boolean :after_school_wednesday, default: false
			t.boolean :after_school_thursday, default: false
			t.boolean :after_school_friday, default: false

      t.integer :payment_record_id

      t.timestamps null: false
    end
  end
end
