class CreatePayrollEmployees < ActiveRecord::Migration
  def change
    create_table :payroll_employees do |t|
      t.string :name
      t.references :payroll_location, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
