class AddEnrolledToRegistrationRecords < ActiveRecord::Migration
  def change
    add_column :registration_records, :enrolled, :boolean, default: true
  end
end
