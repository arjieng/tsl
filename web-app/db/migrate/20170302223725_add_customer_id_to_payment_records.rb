class AddCustomerIdToPaymentRecords < ActiveRecord::Migration
  def change
    add_column :payment_records, :customer_id, :integer
  end
end
