class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :email
      t.string :first_name
      t.string :last_name
      t.string :phone_number
      t.string :emergency_phone_number
      t.string :address_line_one
      t.string :address_line_two
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :how_did_you_hear_about_us
      t.string :stripe_customer_identifier
			t.string :customer_url

      t.timestamps null: false
    end
  end
end
