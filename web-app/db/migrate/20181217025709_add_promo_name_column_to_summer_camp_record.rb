class AddPromoNameColumnToSummerCampRecord < ActiveRecord::Migration
    def change
        add_column :summer_camp_records, :promo_name, :string
    end
end
