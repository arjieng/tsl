class AddDeletedColumnToRegistrationRecords < ActiveRecord::Migration
  def change
    add_column :registration_records, :deleted, :boolean, default: false
  end
end
