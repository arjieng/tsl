class CreatePaymentRecords < ActiveRecord::Migration
  def change
    create_table :payment_records do |t|
      t.string :email
      t.string :program
      t.boolean :approved, default: false
      t.datetime :approved_at
      t.boolean :emailed, default: false
      t.datetime :emailed_at

      t.timestamps null: false
    end
  end
end