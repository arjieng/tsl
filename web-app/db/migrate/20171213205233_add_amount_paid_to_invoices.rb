class AddAmountPaidToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :amount_paid, :integer, default: 0
  end
end
