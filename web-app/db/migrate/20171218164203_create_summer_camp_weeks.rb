class CreateSummerCampWeeks < ActiveRecord::Migration
  def change
    create_table :summer_camp_weeks do |t|
      t.string :name
      t.date :start_date
      t.integer :capacity, default: 0
      t.text :notes
      t.integer :program_id

      t.timestamps null: false
    end
  end
end
