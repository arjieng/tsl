class AddDeletedToSummerCampWeeks < ActiveRecord::Migration
    def change
        add_column :summer_camp_weeks, :deleted, :boolean, default: false
    end
end
