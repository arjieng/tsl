class AddCouponCodeToPaymentRecords < ActiveRecord::Migration
  def change
    add_column :payment_records, :coupon_code, :string
  end
end
