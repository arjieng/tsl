class AddDeletedToVacationCampSchedule < ActiveRecord::Migration
    def change
        add_column :vacation_camp_schedules, :deleted, :boolean, default: false
    end
end
