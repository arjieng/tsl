class ChangeColumnColorDefault < ActiveRecord::Migration
  def change
    change_column :sizes, :color,:string, default: nil
  end
end
