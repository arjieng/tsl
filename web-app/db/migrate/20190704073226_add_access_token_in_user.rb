class AddAccessTokenInUser < ActiveRecord::Migration
  def change
    add_column :customer_users, :access_token, :string
  end
end
