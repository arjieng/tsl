class AddMinimumAgeToLocations < ActiveRecord::Migration
  def change
    add_column :locations, :minimum_age, :integer, default: 5
  end
end
