class CreateVacationCampRecords < ActiveRecord::Migration
  def change
    create_table :vacation_camp_records do |t|
      t.boolean :monday, default: false
      t.boolean :tuesday, default: false
      t.boolean :wednesday, default: false
      t.boolean :thursday, default: false
      t.boolean :friday, default: false

			t.integer :payment_record_id

      t.timestamps null: false
    end
  end
end
