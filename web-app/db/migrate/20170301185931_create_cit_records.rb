class CreateCitRecords < ActiveRecord::Migration
  def change
    create_table :cit_records do |t|
      t.string :first_name
      t.string :last_name
      t.string :dob
      t.string :phone_number
      t.string :email
      t.string :address_line_one
      t.string :address_line_two
      t.string :city
      t.string :state
      t.string :zip_code
      t.string :how_did_you_hear_about_us
      t.string :location
      t.boolean :week_one, default: false
      t.boolean :week_two, default: false
      t.boolean :week_three, default: false
      t.boolean :week_four, default: false
      t.boolean :week_five, default: false
      t.boolean :week_six, default: false
      t.boolean :week_seven, default: false
      t.boolean :week_eight, default: false
      t.boolean :week_nine, default: false
      t.boolean :week_ten, default: false
      t.boolean :attended_tsl_before
      t.string :attended_location
      t.boolean :cit_at_tsl_before
      t.string :cit_at_tsl_location
      t.text :previous_experience
      t.text :why_are_you_interested
      t.boolean :willing_to_follow_directions
      t.text :action_when_completed_tasks
      t.text :arguing_action
      t.text :transition_help
      t.text :interests_and_talents
      t.text :proven_responsibility
      t.text :difficult_problem
      t.text :what_you_will_bring
      t.boolean :willing_to_meet_vision
      t.boolean :agree_to_behavior_policy, default: false

      t.timestamps null: false
    end
  end
end
