class CreateVacationCampSchedules < ActiveRecord::Migration
  def change
    create_table :vacation_camp_schedules do |t|
      t.string :name 
      t.integer :year
      t.string :month
      t.boolean :day_one, default: false
      t.boolean :day_two, default: false
      t.boolean :day_three, default: false
      t.boolean :day_four, default: false
      t.boolean :day_five, default: false
      t.boolean :day_six, default: false
      t.boolean :day_seven, default: false
      t.boolean :day_eight, default: false
      t.boolean :day_nine, default: false
      t.boolean :day_ten, default: false
      t.boolean :day_eleven, default: false
      t.boolean :day_twelve, default: false
      t.boolean :day_thirteen, default: false
      t.boolean :day_fourteen, default: false
      t.boolean :day_fifteen, default: false
      t.boolean :day_sixteen, default: false
      t.boolean :day_seventeen, default: false
      t.boolean :day_eighteen, default: false
      t.boolean :day_nineteen, default: false
      t.boolean :day_twenty, default: false
      t.boolean :day_twenty_one, default: false
      t.boolean :day_twenty_two, default: false
      t.boolean :day_twenty_three, default: false
      t.boolean :day_twenty_four, default: false
      t.boolean :day_twenty_five, default: false
      t.boolean :day_twenty_six, default: false
      t.boolean :day_twenty_seven, default: false
      t.boolean :day_twenty_eight, default: false
      t.boolean :day_twenty_nine, default: false
      t.boolean :day_thirty, default: false
      t.boolean :day_thirty_one, default: false
      t.timestamps null: false
    end
  end
end
