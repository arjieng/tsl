class AddNewColumnToProgram < ActiveRecord::Migration
  def change
    add_column :programs, :is_package_active, :boolean ,default: true
  end
end
