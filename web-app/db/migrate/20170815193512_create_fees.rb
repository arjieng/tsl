class CreateFees < ActiveRecord::Migration
  def change
    create_table :fees do |t|
      t.integer :invoice_id
      t.decimal :amount, default: 0.0

      t.timestamps null: false
    end
  end
end
