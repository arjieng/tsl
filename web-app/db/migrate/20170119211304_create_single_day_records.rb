class CreateSingleDayRecords < ActiveRecord::Migration
  def change
    create_table :single_day_records do |t|
      t.integer :payment_record_id
      t.string :day

      t.timestamps null: false
    end
  end
end
