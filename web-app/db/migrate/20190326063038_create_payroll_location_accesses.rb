class CreatePayrollLocationAccesses < ActiveRecord::Migration
  def change
    create_table :payroll_location_accesses do |t|
      t.references :user, index: true, foreign_key: true
      t.references :payroll_location, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
