class AddYearCycleToBeforeAndAfterSchoolRecord < ActiveRecord::Migration
    def change
        add_column :before_and_afterschool_records, :year_cycle, :string
    end
end
