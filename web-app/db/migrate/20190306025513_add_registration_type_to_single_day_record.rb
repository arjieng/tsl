class AddRegistrationTypeToSingleDayRecord < ActiveRecord::Migration
  def change
      add_column :single_day_records, :registration_type, :string
  end
end
