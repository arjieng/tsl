class CreatePurchases < ActiveRecord::Migration
  def change
    create_table :purchases do |t|
      t.references :customer, index: true, foreign_key: true
      t.string :orders
      t.decimal :total_amount
      t.string :payment_url
      t.string :transaction_identifier
      t.references :location, index: true, foreign_key: true
      t.timestamps null: false
    end
  end
end
