class CreatePayrollLocations < ActiveRecord::Migration
  def change
    create_table :payroll_locations do |t|
      t.string :name
      t.timestamps null: false
    end
  end
end
