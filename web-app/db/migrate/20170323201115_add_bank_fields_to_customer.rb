class AddBankFieldsToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :stripe_bank_identifier, :string
    add_column :customers, :bank_verified, :boolean, default: false
		add_column :customers, :bank_name, :string
  end
end
