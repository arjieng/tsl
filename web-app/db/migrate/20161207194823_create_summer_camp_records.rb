class CreateSummerCampRecords < ActiveRecord::Migration
  def change
    create_table :summer_camp_records do |t|
      t.boolean :week_one, default: false
			t.boolean :week_two, default: false
			t.boolean :week_three, default: false
			t.boolean :week_four, default: false
			t.boolean :week_five, default: false
			t.boolean :week_six, default: false
			t.boolean :week_seven, default: false
			t.boolean :week_eight, default: false
			t.boolean :week_nine, default: false
			t.boolean :week_ten, default: false

      t.integer :payment_record_id

      t.timestamps null: false
    end
  end
end
