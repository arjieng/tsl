class AddCustomerIdentifierToPaymentRecords < ActiveRecord::Migration
  def change
    add_column :payment_records, :customer_identifier, :string
  end
end
