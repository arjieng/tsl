class CreateBeforeAndAfterSchoolProgramConfigurations < ActiveRecord::Migration
  def change
    create_table :before_and_after_school_program_configurations do |t|
      t.boolean :before_school, default: false
      t.boolean :after_school, default: false
      t.references :program, index: {:name => "index_before_and_after_school_program_config_on_program_id"}, foreign_key: true

      t.timestamps null: false
    end
  end
end
