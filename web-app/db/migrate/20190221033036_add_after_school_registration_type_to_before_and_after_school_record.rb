class AddAfterSchoolRegistrationTypeToBeforeAndAfterSchoolRecord < ActiveRecord::Migration
    def change
        add_column :before_and_afterschool_records, :registration_type, :string
    end
end
