class AddCardLastFourAndCardBrandToCustomers < ActiveRecord::Migration
  def change
    add_column :customers, :card_last_four, :string
    add_column :customers, :card_brand, :string
  end
end
