class AddForeignKeyToVacationCampSchedules < ActiveRecord::Migration
  def change
    add_column :vacation_camp_schedules, :capacity, :integer, default: 0
    add_column :vacation_camp_schedules, :program_id, :integer
    add_index :vacation_camp_schedules, :program_id
    add_column :vacation_camp_records, :vacation_camp_schedule_id, :integer
    add_index :vacation_camp_records, :vacation_camp_schedule_id
  end
end
