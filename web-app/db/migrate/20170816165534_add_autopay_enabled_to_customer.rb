class AddAutopayEnabledToCustomer < ActiveRecord::Migration
  def change
    add_column :customers, :autopay_enabled, :boolean, default: false
  end
end
