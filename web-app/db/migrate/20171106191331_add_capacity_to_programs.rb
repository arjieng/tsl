class AddCapacityToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :capacity, :integer, default: 0
  end
end
