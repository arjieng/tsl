class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.integer :payment_record_id
      t.integer :invoice_id
      t.decimal :amount, default: 0.0

      t.timestamps null: false
    end
  end
end
