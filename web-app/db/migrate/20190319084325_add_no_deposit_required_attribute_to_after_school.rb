class AddNoDepositRequiredAttributeToAfterSchool < ActiveRecord::Migration
    def change
        add_column :before_and_afterschool_records, :no_deposit_required, :boolean, default: false
    end
end
