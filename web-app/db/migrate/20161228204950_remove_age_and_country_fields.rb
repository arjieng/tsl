class RemoveAgeAndCountryFields < ActiveRecord::Migration
  def change
		remove_column :payment_records, :country, :string
		remove_column :child_records, :age, :string
  end
end
