class CreateSizes < ActiveRecord::Migration
  def change
    create_table :sizes do |t|
      t.string :name
      t.integer :quantity
      t.decimal :price, precision: 8, scale: 2
      t.references :product, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
