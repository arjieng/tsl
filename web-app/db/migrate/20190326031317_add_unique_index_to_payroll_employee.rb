class AddUniqueIndexToPayrollEmployee < ActiveRecord::Migration
    def change
        add_index :payroll_employees, [:payroll_location_id, :name], unique: true
    end
end
