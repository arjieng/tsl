class AddFieldsToCitRecords < ActiveRecord::Migration
  def change
    add_column :cit_records, :water_play_action, :text
    add_column :cit_records, :four_square_action, :text
    add_column :cit_records, :uninteresting_activity_action, :text
    add_column :cit_records, :made_a_difference, :text
  end
end
