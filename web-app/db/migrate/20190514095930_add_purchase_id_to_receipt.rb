class AddPurchaseIdToReceipt < ActiveRecord::Migration
    def change
        add_reference :receipts, :purchase, index: true, foreign_key: true
    end
end
