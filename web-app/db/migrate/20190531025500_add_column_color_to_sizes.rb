class AddColumnColorToSizes < ActiveRecord::Migration
  def change
    add_column :sizes,  :color,:string, default: ""
  end
end
