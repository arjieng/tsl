class AddMonthToVacationCampRecords < ActiveRecord::Migration
  def change
    add_column :vacation_camp_records, :month, :string
  end
end
