class AddBillAfterschoolEndOfMonthToRegistrationRecords < ActiveRecord::Migration
  def change
    add_column :registration_records, :bill_afterschool_end_of_month, :boolean, default: false
  end
end
