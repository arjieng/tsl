class CreatePayrollEmployeeAbscences < ActiveRecord::Migration
  def change
    create_table :payroll_employee_abscences do |t|
      t.references :payroll_location, index: true, foreign_key: true
      t.references :payroll_employee, index: true, foreign_key: true
      t.string :abscence_type
      t.date :abscence_date

      t.timestamps null: false
    end
  end
end
