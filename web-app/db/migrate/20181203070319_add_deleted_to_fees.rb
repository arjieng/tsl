class AddDeletedToFees < ActiveRecord::Migration
	def change
		add_column :fees, :deleted, :boolean, default: false
	end
end
