class AddDateToSingleDayRecords < ActiveRecord::Migration
  def change
    add_column :single_day_records, :date, :string
  end
end
