class CreateSingleDayDates < ActiveRecord::Migration
  def change
    create_table :single_day_dates do |t|
      t.string :name
      t.date :date
      t.integer :capacity, default: 0
      t.integer :program_id

      t.timestamps null: false
    end
  end
end
