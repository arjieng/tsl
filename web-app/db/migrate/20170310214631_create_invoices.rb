class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :payment_record_id
      t.integer :amount, default: 0
      t.boolean :paid, default: false
			t.string :memo
			t.date :due_date

      t.timestamps null: false
    end
  end
end
