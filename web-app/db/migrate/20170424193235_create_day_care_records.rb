class CreateDayCareRecords < ActiveRecord::Migration
  def change
    create_table :day_care_records do |t|
      t.date :start_date
      t.integer :payment_record_id

      t.timestamps null: false
    end
  end
end
