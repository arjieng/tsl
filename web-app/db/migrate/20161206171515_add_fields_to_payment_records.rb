class AddFieldsToPaymentRecords < ActiveRecord::Migration
  def change
    add_column :payment_records, :parent_first_name, :string
    add_column :payment_records, :parent_last_name, :string
    add_column :payment_records, :phone_number, :string
    add_column :payment_records, :emergency_phone_number, :string
    add_column :payment_records, :address_line_one, :string
    add_column :payment_records, :address_line_two, :string
    add_column :payment_records, :city, :string
    add_column :payment_records, :state, :string
    add_column :payment_records, :zip_code, :string
    add_column :payment_records, :country, :string
    add_column :payment_records, :how_did_you_hear_about_us, :string
    add_column :payment_records, :location, :string
    add_column :payment_records, :payment_platform, :string
    add_column :payment_records, :payment_url, :string
    add_column :payment_records, :customer_url, :string
    add_column :payment_records, :amount_paid, :decimal, default: 0.0

		rename_column :payment_records, :program, :old_program
  end
end
