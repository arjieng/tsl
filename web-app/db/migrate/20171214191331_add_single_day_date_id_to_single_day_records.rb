class AddSingleDayDateIdToSingleDayRecords < ActiveRecord::Migration
  def change
    add_column :single_day_records, :single_day_date_id, :integer
  end
end
