class CreateContactInformations < ActiveRecord::Migration
  def change
    create_table :contact_informations do |t|
      t.string :email
      t.references :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
