class CreatePayrollEmployeeHours < ActiveRecord::Migration
  def change
    create_table :payroll_employee_hours do |t|
      t.references :payroll_employee, index: true, foreign_key: true
      t.references :payroll_location, index: true, foreign_key: true
      t.integer :logged_hours
      t.date :logged_hours_date

      t.timestamps null: false
    end
  end
end
