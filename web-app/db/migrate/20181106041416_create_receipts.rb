class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.integer :registration_record_id
      t.integer :invoice_id
      t.integer :customer_id
      t.decimal :amount
      t.string :program_name
      t.string :payment_for
      t.timestamps null: false
    end

    add_index :receipts, :customer_id
  end
end
