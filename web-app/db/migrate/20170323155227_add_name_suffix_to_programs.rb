class AddNameSuffixToPrograms < ActiveRecord::Migration
  def change
    add_column :programs, :name_suffix, :string
  end
end
