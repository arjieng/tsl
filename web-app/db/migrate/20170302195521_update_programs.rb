class UpdatePrograms < ActiveRecord::Migration
  def change
		rename_column :programs, :program_identifier, :name
		rename_column :programs, :user_id, :director_id
		add_column :programs, :location_id, :integer
		add_column :programs, :active, :boolean, default: true
  end
end
