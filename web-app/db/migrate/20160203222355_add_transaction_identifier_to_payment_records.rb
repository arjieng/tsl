class AddTransactionIdentifierToPaymentRecords < ActiveRecord::Migration
  def change
    add_column :payment_records, :transaction_identifier, :string
  end
end
