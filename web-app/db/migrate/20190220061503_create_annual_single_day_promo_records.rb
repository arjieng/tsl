class CreateAnnualSingleDayPromoRecords < ActiveRecord::Migration
  def change
    create_table :annual_single_day_promo_records do |t|
      t.references :registration_record, index: true, foreign_key: true
      t.string :promo_name
      t.string :year_cycle

      t.timestamps null: false
    end
  end
end
