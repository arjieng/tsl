class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :product, index: true, foreign_key: true
      t.references :size, index: true, foreign_key: true
      t.integer :quantity
      t.boolean :purchased, default: false
      t.decimal :total_amount, precision: 8 ,scale: 2
      t.references :customer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
