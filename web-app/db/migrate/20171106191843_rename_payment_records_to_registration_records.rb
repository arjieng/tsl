class RenamePaymentRecordsToRegistrationRecords < ActiveRecord::Migration
	def change
		rename_table :payment_records, :registration_records

		rename_column :single_day_records, :payment_record_id, :registration_record_id
		rename_column :summer_camp_records, :payment_record_id, :registration_record_id
		rename_column :vacation_camp_records, :payment_record_id, :registration_record_id
		rename_column :before_and_afterschool_records, :payment_record_id, :registration_record_id
		rename_column :day_care_records, :payment_record_id, :registration_record_id
		rename_column :discounts, :payment_record_id, :registration_record_id
		rename_column :invoices, :payment_record_id, :registration_record_id
		rename_column :child_records, :payment_record_id, :registration_record_id
	end
end
