class CreateChildRecords < ActiveRecord::Migration
  def change
    create_table :child_records do |t|
      t.string :first_name
      t.string :last_name
      t.string :age
      t.string :dob
      t.string :child_classification
			t.string :school_attending
      t.integer :payment_record_id

      t.timestamps null: false
    end
  end
end
