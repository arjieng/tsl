# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

@user = User.find_by(email: "jasper@tsl.com")
unless @user.present?
  @user = User.create!(name: "Jasper", email: "jasper@tsl.com", password: "jasperdu")
end


locations = [
  { name: "Cebu", address: "Appstone Tech", director: @user },
  { name: "Leyte", address: "Jazzy Boi", director: @user },
  { name: "Manila", address: "Isko Moreno", director: @user },
  { name: "Davao", address: "Drodrigo Drutrertre", director: @user },
  { name: "Saitama", address: "One puuuunch!", director: @user },
]

programs = [
  { name: "Program 1", name_suffix: "P1", capacity: 10 },
  { name: "Program 2", name_suffix: "P2", capacity: 10 },
  { name: "Program 3", name_suffix: "P3", capacity: 10 },
  { name: "Program 4", name_suffix: "P4", capacity: 10 },
  { name: "Program 5", name_suffix: "P5", capacity: 10 },
]

Location.transaction do
  Program.transaction do
    locations.each do |location|
      location = Location.find_or_create_by!(location)

      programs.each do |program|
        Program.find_or_create_by!(program.merge!(director: @user, location: location))
      end
    end
  end
end8000