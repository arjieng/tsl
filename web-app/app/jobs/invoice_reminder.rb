class InvoiceReminder < ApplicationJob
    queue_as :invoice_reminder
    def perform invoice_id
        InvoicesMailer.customer_due_summer_camp_invoice(invoice_id).deliver!
    end
end