class RecordUpdate < ApplicationJob
    queue_as :record_update
    def perform registration_record, current_admin_user
        AdminMailer.notify_tom_of_record_update(registration_record, current_admin_user).deliver!
    end
end