class OverdueSummerCampInvoice < ApplicationJob
    queue_as :invoice_reminder
    def perform invoice_id
        InvoicesMailer.overdue_summer_camp_invoice(invoice_id).deliver!
    end
end