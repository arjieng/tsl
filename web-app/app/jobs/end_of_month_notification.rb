class EndOfMonthNotification < ApplicationJob
    queue_as :end_of_month_notification
    def perform
        invoices = Invoice.where("extract (year from invoices.created_at ) >= ?", 2017)
        .includes(:program)
        .where('programs.name = ?', 'After School')
		.where(paid: false).where('extract (month from due_date) = ?', (Date.today.month))
        .where('extract (year from due_date) = ?', (Date.today.year))
        .references(:program)

        invoices.each do |invoice|
            InvoicesMailer.notify_tom_end_of_month_afterschool(invoice.id).deliver!
        end
    end
end