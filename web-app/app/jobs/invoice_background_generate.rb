class InvoiceBackgroundGenerate < ApplicationJob
    queue_as :invoice_background_generate
    def perform
        RegistrationRecord.before_and_afterschool_records_from_2017_and_onwards.each do |registration_record|
            if registration_record.before_and_afterschool_record.start_date.year == 2018
                if registration_record.before_and_afterschool_record.start_date.month == 9 || registration_record.before_and_afterschool_record.start_date.month == 10 || registration_record.before_and_afterschool_record.start_date.month == 11
                    puts "Generating invoice for #{registration_record.id}"
                    unless registration_record.invoices.where("extract (month from due_date ) =? ",Time.now.to_date.month)
                            .where(memo: 'TSL Before and Afterschool').present?
                        registration_record.create_before_and_afterschool_invoice
                    end
                    puts "Finished generating invoice for #{registration_record.id}"
                else # handle 2018 before and afterschool registrations that will start on december
                    if registration_record.before_and_afterschool_record.start_date.month == 12
                        # add event here to check if it's december now
                        if Time.now.to_date.beginning_of_month >= registration_record.before_and_afterschool_record.start_date.to_date.beginning_of_month

                            puts "Generating invoice for #{registration_record.id}"
                            unless registration_record.invoices.where("extract (month from due_date ) =? ",Time.now.to_date.month)
                                    .where(memo: 'TSL Before and Afterschool').present?
                                registration_record.create_before_and_afterschool_invoice
                            end
                            puts "Finished generating invoice for #{registration_record.id}"
                        end
                    end
                end
            elsif registration_record.before_and_afterschool_record.start_date.year >= 2019
                # handle records with 2019 start dates and beyond here
                if Time.now.to_date.beginning_of_month >= registration_record.before_and_afterschool_record.start_date.to_date.beginning_of_month
                    unless registration_record.invoices.where("extract (month from due_date ) =? ",Time.now.to_date.month)
                            .where(memo: 'TSL Before and Afterschool').present?
                        registration_record.create_before_and_afterschool_invoice
                    end
                end
            end
        end
    end
end