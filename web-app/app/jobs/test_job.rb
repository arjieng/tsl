class TestJob < ApplicationJob
    queue_as :test_job
    def perform args
         User.create!(email: "edmond.tsl@gmail.com", password: "password")
         WelcomeMailer.test_email.deliver!
    end
end