class EmailValidator < ActiveModel::EachValidator
	def validate_each(record, attribute, value)
		unless value =~ /#{%w(tsl).map{|a| Regexp.quote(a)}.join("|")}/ || value =~ /#{%w(delight.consulting).map{|a| Regexp.quote(a)}.join("|")}/ || value =~ /#{%w(prigitalsolutions.com).map{|a| Regexp.quote(a)}.join("|")}/ 
			record.errors[attribute] << (options[:message] || "not a valid TSL email address")
		end
	end
end
