class InvoicesMailer < ApplicationMailer
	include Resque::Mailer

	def create(invoice_id) # added other email contacts
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo} Invoice")
			end
		end
	end

	def summer_camp_creation(registration_record_id, invoice_ids) # added other email contacts
		@invoices = Invoice.where(id: invoice_ids)
		@registration_record = RegistrationRecord.find(registration_record_id)
		if @registration_record.deleted == false
			@customer = @registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "Summer Camp Invoices")
			end
		end
	end

	def before_and_afterschool_monthly_invoice(registration_record_id, invoice_ids) # added contact informations
		@invoices = Invoice.where(id: invoice_ids)
		@registration_record = RegistrationRecord.find(registration_record_id)
		if @registration_record.deleted == false
			@customer = @registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "Before and Afterschool Invoices")
			end
		end
	end

	def discount_applied(discount_id)
		@discount = Discount.find(discount_id)
		@invoice = @discount.invoice
		@registration_record = @discount.invoice.registration_record
		if @registration_record.deleted == false
			@customer = @discount.invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "Discount Applied to #{@invoice.memo} Invoice")
			end
		end
	end

	def invoice_autopayed(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo} Invoice - Paid")
			end
		end
	end

	def invoice_autopay_failed(invoice_id, error_message)
		@error_message = error_message
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo} Invoice - Autopay Failed")
			end
		end
	end

	def invoice_marked_paid(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo} Invoice - Marked Paid")
			end
		end
	end

	def invoice_deleted(invoice_id, invoice_memo, registration_record_id)
		@invoice_id = invoice_id
		@invoice_memo = invoice_memo
		@registration_record = RegistrationRecord.find(registration_record_id)
		@customer = @registration_record.customer
		if @customer.is_deleted == false
			recipient = @customer.contact_informations.pluck(:email)
			recipient.push(@customer.email)
			mail(to: recipient, subject: "#{@invoice_memo} Invoice - Deleted by Admin")
		end
	end

	def invoice_edited(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo} Invoice - Edited by Admin")
			end
		end
	end

  def due_today(invoice_id)
    @invoice = Invoice.find(invoice_id)
	@registration_record = @invoice.registration_record
	if @registration_record.deleted == false
		@customer = @invoice.registration_record.customer
		if @customer.is_deleted == false
			recipient = @customer.contact_informations.pluck(:email)
			recipient.push(@customer.email)
			mail(to: recipient, subject: "#{@invoice.memo} Invoice - Due Today")
		end
	end
  end

  def notify_tom_overdue_today(invoice_id)
    @invoice = Invoice.find(invoice_id)
	@registration_record = @invoice.registration_record
	if @registration_record.deleted == false
		@customer = @invoice.registration_record.customer
		if @customer.is_deleted == false
			mail(to: Rails.application.secrets.owner_emails, subject: "#{@invoice.memo} Invoice - Overdue Today")
		end
	end
  end

	def overdue_by_one_day(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "Daycare Invoice overdue")
			end
		end
	end

	def notify_tom_overdue_by_one_day(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				mail(to: Rails.application.secrets.owner_emails, subject: "Daycare Invoice overdue by one day")
			end
		end
	end

	def overdue_by_one_week(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo} is still unpaid")
			end
		end
	end

	def overdue_by_two_weeks(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo} Invoice - 2 Weeks Overdue")
			end
		end
	end

	def notify_tom_overdue_by_one_week(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				mail(to: Rails.application.secrets.owner_emails, subject: "#{@invoice.memo} Invoice - 1 Week Overdue")
			end
		end
	end

	def notify_tom_overdue_by_two_weeks(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				mail(to: Rails.application.secrets.owner_emails, subject: "#{@invoice.memo} Invoice - 2 Weeks Overdue")
			end
		end
	end

	def notify_tom_unpaid_daycare_invoice(invoice_id, recipient)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				@recipient = recipient
				send_to = recipient == 'tom' ? Rails.application.secrets.owner_emails : 'jaimer.tsl@gmail.com'
				mail(
					to: send_to,
					subject: "#{@invoice.memo}. Day Care invoice is yet unpaid."
				)
			end
		end
	end

	def notify_jaime_overdue_daycare_by_one_day(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				mail(to: "jaimer.tsl@gmail.com", subject: "#{@invoice.memo}. Invoice - 1 day Overdue")
			end
		end
	end

	def unpaid_daycare_invoice(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "#{@invoice.memo}. Day Care invoice is yet unpaid.")
			end
		end
	end

	def notify_invoice_overdue_last_month(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(
					to: recipient,
					from: 'hello@tsladventures.net',
					subject: "Unpaid Before and After School Invoice"
				)
			end
		end
	end
    
	def customer_due_summer_camp_invoice invoice_id
		@invoice = Invoice.find_by_id(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push('wrightkyle7.tsl@gmail.com')
				recipient.push(@customer.email)
				mail(
					to: recipient,
					from: 'hello@tsladventures.net',
					subject: "Weekly summer camp Invoice due today"
				)
			end
		end
	end

	def overdue_summer_camp_invoice invoice_id
		@invoice = Invoice.find_by_id(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push('wrightkyle7.tsl@gmail.com')
				recipient.push(@customer.email)
				mail(
					to: recipient,
					from: 'hello@tsladventures.net',
								cc: 'tsladventures@gmail.com',
					subject: "Weekly summer camp Invoice due today"
				)
			end
		end
	end
		
	def end_of_month_afterschool_ultimatum invoice_id
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				recipient = @customer.contact_informations.pluck(:email)
				recipient.push(@customer.email)
				mail(to: recipient, subject: "Unpaid before/after school invoice. Unpaid invoices will result in termination of services.")
			end
		end
	end

	def notify_tom_end_of_month_afterschool(invoice_id)
		@invoice = Invoice.find(invoice_id)
		@registration_record = @invoice.registration_record
		if @registration_record.deleted == false
			@customer = @invoice.registration_record.customer
			if @customer.is_deleted == false
				mail(to: Rails.application.secrets.owner_emails, cc: 'wrightkyle7.tsl@gmail.com', subject: "End of month unpaid Before/After School Invoice notification.")
			end
		end
	end
end
