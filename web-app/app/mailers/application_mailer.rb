class ApplicationMailer < ActionMailer::Base
  default from: "hello@tsladventures.net"
  layout 'mailer'
end
