class ForgotPasswordMailer < ApplicationMailer
  include Resque::Mailer

  def forgot_password(params={})
    @user = params[:user]
    @token = params[:password_token]
    Rails.logger.debug "Example User: #{@user.inspect}"
    Rails.logger.debug "Example User Email: #{@user["email"].to_s}"
    mail(to: @user["email"].to_s,
      from: 'hello@tsladventures.net',
      subject: 'Change Password')
  end
end
