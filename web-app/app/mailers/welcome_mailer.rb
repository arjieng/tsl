class WelcomeMailer < ApplicationMailer
	add_template_helper(AdminHelper)
	helper ApplicationHelper
	def welcome_email(sender, registration_record)
		@registration_record = registration_record
		@vacation_camp_records = @registration_record.vacation_camp_records
		attachments['Enrollment_Form_2018-19.docx'] = File.read("#{Rails.root}/app/assets/images/Enrollment_Form_2018-19.docx")
		attachments['Medical and Immunization form.pdf'] = File.read("#{Rails.root}/app/assets/images/Medical and Immunization form.pdf") if registration_record.summer_camp_record.present?
		attachments['TSL_Summer_info_sheet_2019.pdf'] = File.read("#{Rails.root}/app/assets/images/TSL_Summer_info_sheet_2019.pdf") if registration_record.summer_camp_record.present?
		attachments['Final_Parent_Handbook2019.doc'] = File.read("#{Rails.root}/app/assets/images/Final_Parent_Handbook2019.doc")
		if sender
			mail(
				to: registration_record.customer.email,
				from: 'hello@tsladventures.net',
				cc: sender.email,
				subject: 'Welcome to TSL!',
			)
		else
			mail(
				to: registration_record.customer.email,
				from: 'hello@tsladventures.net',
				subject: 'Welcome to TSL!'
			)
		end
	end

	def day_care_tour_welcome_email(sender, registration_record)
		@registration_record = registration_record

		if sender
			mail(
				to: registration_record.customer.email,
				from: 'hello@tsladventures.net',
				cc: sender.email,
				subject: 'Welcome to TSL!',
			)
		else
			mail(
				to: registration_record.customer.email,
				from: 'hello@tsladventures.net',
				subject: 'Welcome to TSL!'
			)
		end
	end

	def day_care_welcome_email(sender, registration_record)
		@registration_record = registration_record
		attachments['Final_Parent_Handbook2019.doc'] = File.read("#{Rails.root}/app/assets/images/Final_Parent_Handbook2019.doc")
		attachments['Health_guidelines_.doc'] = File.read("#{Rails.root}/app/assets/images/Health_guidelines_.doc")
		attachments['infant_enrollment_packet.pdf'] = File.read("#{Rails.root}/app/assets/images/infant_enrollment_packet.pdf")
		attachments['Preschool_Enrollment_Packet.pdf'] = File.read("#{Rails.root}/app/assets/images/Preschool_Enrollment_Packet.pdf")
		attachments['Toddler_Enrollment_Packet_(Autosaved).pdf'] = File.read("#{Rails.root}/app/assets/images/Toddler_Enrollment_Packet_(Autosaved).pdf")

		if sender
			mail(
				to: registration_record.customer.email,
				from: 'hello@tsladventures.net',
				cc: sender.email,
				subject: 'Welcome to TSL!',
			)
		else
			mail(
				to: registration_record.customer.email,
				from: 'hello@tsladventures.net',
				subject: 'Welcome to TSL!'
			)
		end
	end

	def send_tom_email_notification(registration_record, registration_record_params, params = nil)
		@registration_record = registration_record
		@registration_record_params = registration_record_params
		@params = params
		@subject = registration_record.program.name
		if registration_record.program.name == 'Single Day'
			if @registration_record.annual_single_day_promo_record.present?
				@subject = 'Year-round Single Days of Care'
			end
		end
		mail(
			to: "tsladventures@gmail.com, ktbro.tsl@gmail.com, ntopaz.tsl@gmail.com",
			from: "hello@tsladventures.net",
			subject: "New #{@subject} Registration"
		)
	end

	def send_tom_cit_email_notification(cit_record)
		@cit_record = cit_record
		mail(
			to: "tsladventures@gmail.com, ktbro.tsl@gmail.com, ntopaz.tsl@gmail.com", 
			from: "hello@tsladventures.net", 
			subject: "New CIT Application for #{cit_record.location}"
		)
	end

	def test_email
		mail(
			to: 'monkkokkalis@gmail.com',
			from: 'hello@tsladventures.net',
			subject: 'Test Email with resque'
		)
	end
end
