class AdminMailer < ApplicationMailer
    add_template_helper(AdminHelper)
	helper ApplicationHelper

    def notify_tom_of_record_update registration_record, current_admin_user
        @registration_record = registration_record
        @current_admin_user = current_admin_user
        mail(
			to: "tsladventures@gmail.com", 
			from: "noreply@tsladventures.net", 
			subject: "Registration Record Updated"
		)
    end

    def notify_tom_of_shop_purchase(purchase)
        @purchase = purchase
        mail(
            to: "tsladventures@gmail.com", 
            from: "noreply@tsladventures.net",
            subject: "New Purchase from online Store"
        )
    end
end