class RegistrationRecordsController < ApplicationController
	def single_day
		@registration_record = RegistrationRecord.new
		@registration_record.build_single_day_record
		@registration_record.child_records.build
		@registration_record.build_annual_single_day_promo_record
	end

	def request_day_care_tour
		@registration_record = RegistrationRecord.new
		@registration_record.child_records.build
	end

	def day_care
		@registration_record = RegistrationRecord.new
		@registration_record.build_day_care_record
		@registration_record.child_records.build
	end

	def before_and_afterschool
		@registration_record = RegistrationRecord.new
		@registration_record.build_before_and_afterschool_record
		@registration_record.child_records.build
		@registration_record.build_annual_single_day_promo_record
	end

	def no_deposit_before_and_afterschool
		@registration_record = RegistrationRecord.new
		@registration_record.build_before_and_afterschool_record
		@registration_record.child_records.build
		@registration_record.build_annual_single_day_promo_record
	end

	def groupon_summer_camp
		@registration_record = RegistrationRecord.new
		@registration_record.build_summer_camp_record
		@registration_record.child_records.build
	end

	def summer_camp
		@registration_record = RegistrationRecord.new
		@registration_record.build_summer_camp_record
		@registration_record.child_records.build
	end

	def vacation_camp
		@registration_record = RegistrationRecord.new
		@registration_record.vacation_camp_records.build
		@registration_record.child_records.build
	end

	def create
		if registration_record_params['referrer'] == "single_day"
				pp = registration_record_params
				if pp[:single_day_date_ids].present?
					RegistrationRecord.transaction do
						pp[:single_day_date_ids].each do |date_id|
							pp[:single_day_record_attributes][:single_day_date_id] = date_id
							@registration_record = RegistrationRecord.new(pp)
							if registration_record_params[:location].present?
								program = registration_record_params[:referrer].split("_").map{|word| word.capitalize}.join(" ")
								@registration_record.program_id = Location.find_by(name: registration_record_params[:location]).programs.find_by(name: program).id
							end
							begin
								if @registration_record.valid?
									@registration_record = @registration_record.build_stripe_info(params, registration_record_params) unless registration_record_params['referrer'] == 'day_care_tour' || registration_record_params['referrer'] == 'day_care' || registration_record_params['referrer'] == 'groupon_summer_camp' || @registration_record.location == 'Counselors in Training' || params[:registration_record][:no_deposit_after_school_not_chargeable].present? 
									if @registration_record.save
										@registration_record.approve_and_send if @registration_record.emailed == false
										@registration_record.send_tom_email_notification(registration_record_params, params)
										# redirect_to success_path
									else
										@registration_record.child_records.build if (registration_record_params[:child_records_attributes] == 0 || @registration_record.child_records.empty?)
										build_associations(registration_record_params['referrer'])
										redirect_error(registration_record_params['referrer']) and return
									end
								else
									@registration_record.child_records.build if (registration_record_params[:child_records_attributes] == 0 || @registration_record.child_records.empty?)
									build_associations(registration_record_params['referrer'])
									redirect_error(registration_record_params['referrer']) and return
								end
							rescue StandardError => e # @registration_record.build_stripe_info fails if the bank is fraudulent
								@registration_record.errors.add "Your", "payment has been declined. Reason: #{e.message}"
								redirect_error(registration_record_params['referrer']) and return
							end
						end
						redirect_to success_path and return
					end
				else
					@registration_record = RegistrationRecord.new(registration_record_params)
					if registration_record_params[:location].present?
						program = registration_record_params[:referrer].split("_").map{|word| word.capitalize}.join(" ")
						@registration_record.program_id = Location.find_by(name: registration_record_params[:location]).programs.find_by(name: program).id
					end
					begin
						if @registration_record.valid?
							@registration_record = @registration_record.build_stripe_info(params, registration_record_params) unless registration_record_params['referrer'] == 'day_care_tour' || registration_record_params['referrer'] == 'day_care' || registration_record_params['referrer'] == 'groupon_summer_camp' || @registration_record.location == 'Counselors in Training' || params[:registration_record][:no_deposit_after_school_not_chargeable].present? 
							if @registration_record.save
								@registration_record.approve_and_send if @registration_record.emailed == false
								@registration_record.send_tom_email_notification(registration_record_params, params)
								# redirect_to success_path
							else
								@registration_record.child_records.build if (registration_record_params[:child_records_attributes] == 0 || @registration_record.child_records.empty?)
								build_associations(registration_record_params['referrer'])
								redirect_error(registration_record_params['referrer']) and return
							end
						else
							@registration_record.child_records.build if (registration_record_params[:child_records_attributes] == 0 || @registration_record.child_records.empty?)
							build_associations(registration_record_params['referrer'])
							redirect_error(registration_record_params['referrer']) and return
						end
					rescue StandardError => e # @registration_record.build_stripe_info fails if the bank is fraudulent
						@registration_record.errors.add "Your", "payment has been declined. Reason: #{e.message}"
						redirect_error(registration_record_params['referrer']) and return
					end
					redirect_to success_path and return
				end
		else
			@registration_record = RegistrationRecord.new(registration_record_params)
			# check for vacation camp schedules' availability
			# if any on of the selected vacation camp schedule is full, the application
			# will raise an error and redirect back to the form
			if registration_record_params[:referrer] == "vacation_camp"
				begin
					@registration_record.create_vacation_camp_records(
						params[:registration_record][:attendance],
						registration_record_params
					)
				rescue StandardError => e
					@registration_record.errors.add(
						"Vacation camp registration failed", e.message
					)
					render action: :vacation_camp and return
				end
			end
			if registration_record_params[:location].present?
				if registration_record_params[:referrer] == "groupon_summer_camp"
					program = "Summer Camp"
				else
					program = registration_record_params[:referrer].split("_").map{|word| word.capitalize}.join(" ")
				end
				@registration_record.program_id = Location.find_by(name: registration_record_params[:location]).programs.find_by(name: program).id
			end
			begin
				if @registration_record.valid?
					@registration_record = @registration_record.build_stripe_info(params, registration_record_params) unless registration_record_params['referrer'] == 'day_care_tour' || registration_record_params['referrer'] == 'day_care' || registration_record_params['referrer'] == 'groupon_summer_camp' || @registration_record.location == 'Counselors in Training' || params[:registration_record][:no_deposit_after_school_not_chargeable].present? 
					if @registration_record.save
						update_summer_camp_record if params[:summer_camp_registration_option] == "summer_camp_promo_registration"
						@registration_record.approve_and_send if @registration_record.emailed == false
						@registration_record.send_tom_email_notification(registration_record_params, params)
						redirect_to success_path
					else
						@registration_record.child_records.build if (registration_record_params[:child_records_attributes] == 0 || @registration_record.child_records.empty?)
						build_associations(registration_record_params['referrer'])
						redirect_error(registration_record_params['referrer'])
					end
				else
					@registration_record.child_records.build if (registration_record_params[:child_records_attributes] == 0 || @registration_record.child_records.empty?)
					build_associations(registration_record_params['referrer'])
					redirect_error(registration_record_params['referrer'])
				end
			rescue StandardError => e # @registration_record.build_stripe_info fails if the bank is fraudulent
				@registration_record.errors.add "Your", "payment has been declined. Reason: #{e.message}"
				redirect_error(registration_record_params['referrer'])
			end
		end
	end

	def success
	end

	private
	def registration_record_params
		params.require(:registration_record).permit(
			:location,
			:email,
			:parent_first_name,
			:parent_last_name,
			:phone_number,
			:emergency_phone_number,
			:address_line_one,
			:address_line_two,
			:city,
			:state,
			:zip_code,
			:how_did_you_hear_about_us,
			:coupon_code,
			:agree_to_tos,
			:referrer,
			:customer_id,
			single_day_date_ids: [],
			single_day_record_attributes: [
				:id,
				:registration_record_id,
				:single_day_date_id,
				:day,
				:date,
				:registration_type
			],
			before_and_afterschool_record_attributes: [
				:id,
				:registration_record_id,
				:start_date,
				:before_school_monday,
				:before_school_tuesday,
				:before_school_wednesday,
				:before_school_thursday,
				:before_school_friday,
				:after_school_monday,
				:after_school_tuesday,
				:after_school_wednesday,
				:after_school_thursday,
				:after_school_friday,
                :registration_type,
				:year_cycle,
				:no_deposit_required
			],
			summer_camp_record_attributes: [
				:id,
				:registration_record_id,
				:week_one,
				:week_two,
				:week_three,
				:week_four,
				:week_five,
				:week_six,
				:week_seven,
				:week_eight,
				:week_nine,
				:week_ten
			],
			vacation_camp_record_attributes: [
				:id,
				:month,
				:registration_record_id,
				:monday,
				:tuesday,
				:wednesday,
				:thursday,
				:friday
			],
			day_care_record_attributes: [
				:id,
				:start_date,
				:registration_record_id
			],
			child_records_attributes: [
				:id,
				:_destroy,
				:registration_record_id,
				:first_name,
				:last_name,
				:dob,
				:child_classification,
				:school_attending
			],
            annual_single_day_promo_record_attributes: [
                :id,
                :registration_record_id,
                :promo_name,
                :year_cycle
            ]
		)
	end

    def update_summer_camp_record
        summer_camp_record = @registration_record.summer_camp_record
        if params[:promo_name].include?('nine_weeks')
            summer_camp_record.attributes.each do |attr_name, attr_value|
                if attr_value.class == TrueClass || attr_value.class == FalseClass
                    summer_camp_record.update_attribute(attr_name.to_sym, true) unless attr_name == 'week_ten'
                end
            end
        end
        summer_camp_record.update_attributes(summer_camp_promo_params)
    end

    def summer_camp_promo_params
        params.permit(
            :promo_name,
            :week_one,
            :week_two,
            :week_three,
            :week_four,
            :week_five,
            :week_six,
            :week_seven,
            :week_eight,
            :week_nine,
            :week_ten
        )
    end

	def redirect_error(referrer)
		if referrer == "single_day"
			render :single_day
		elsif referrer == "day_care"
			render action: :day_care
		elsif referrer == "day_care_tour"
			render action: :request_day_care_tour
		elsif referrer == "after_school"
			if params[:registration_record][:before_and_afterschool_record_attributes][:no_deposit_required].present?
				render action: :no_deposit_before_and_afterschool
			else
				render action: :before_and_afterschool
			end
		elsif referrer == "summer_camp"
			render action: :summer_camp
		elsif referrer == "groupon_summer_camp"
			render action: :groupon_summer_camp
		elsif referrer == "vacation_camp"
			render action: :vacation_camp
		end
	end

	def build_associations referrer
		case referrer
			when 'single_day'
				@registration_record.build_single_day_record
				@registration_record.build_annual_single_day_promo_record
		end
	end
end
