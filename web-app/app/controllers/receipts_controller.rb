class ReceiptsController < ApplicationController
    def index
        @receipts = current_customer_user.customer.receipts.page(params[:page]).per(5).order("created_at DESC")
        @current_customer = current_customer_user.customer
    end

    def show
        @receipt = Receipt.find_by(id: params[:id])
    end

    def summative_receipt
        current_year = Date.today.to_date.year
        last_year = (Date.today.to_date - 1.year).year
        year = params[:summative_receipt_for_year]
        year.present? ? setup_years(year.to_i) : (@default_year, @other_year = current_year, last_year)
        @all_invoices = Invoice.all
        
        @customer = current_customer_user.customer
        # single day
        @single_day_records = @customer.single_day_records(@default_year)
        @single_day_sub_total = @single_day_records.pluck(:amount_paid).reduce(:+).to_i
        # before and after school
        @before_and_afterschool_records = @customer.before_and_afterschool_records(@default_year)
        # after_school_deposits_total = @before_and_afterschool_records.map(&:amount_paid_to_dollars).reduce(:+).to_i
        after_school_deposits_total = @before_and_afterschool_records.inject(0) do |acc, curr|
            if curr.before_and_afterschool_record.created_at.year == @default_year.to_i
                acc += curr.amount_paid.to_i
            else
                acc += 0
            end
        end
        before_and_after_school_invoices = 
            @all_invoices.where('registration_record_id in (?)', @before_and_afterschool_records.pluck(:id))
            .where('extract (year from due_date) = ?', @default_year)
        after_school_invoices_total = before_and_after_school_invoices.map(&:amount_paid_to_dollars).reduce(:+).to_i
        @before_and_afterschool_records_total = after_school_deposits_total + after_school_invoices_total
        # day care records
        @day_care_records = 
                    @customer.registration_records.includes(:program)
                    .where('programs.name = ?', "Day Care").references(:programs)
        @day_care_invoices = 
            Invoice.where('registration_record_id in (?)', @day_care_records.pluck(:id))
            .where('extract (year from created_at) = ?', @default_year)
        @day_care_records_total = @day_care_invoices.map(&:amount_paid_to_dollars).reduce(:+)
        # vacation camp records
        @vacation_camp_records = @customer.vacation_camp_records(@default_year)
        @vacation_camp_records_total = @vacation_camp_records.pluck(:amount_paid).reduce(:+).to_i
        # summer camp records
        @summer_camp_records = @customer.summer_camp_records(@default_year)
        summer_camp_records_deposits_total = @summer_camp_records.inject(0) do |acc, curr|
            if curr.created_at.year == @default_year.to_i
                acc += curr.amount_paid.to_i
            else
                acc += 0
            end
        end
        summer_camp_invoices = @all_invoices.where('registration_record_id in (?)', @summer_camp_records.pluck(:id))
            .where('extract (year from due_date) = ?', @default_year)
        summer_camp_invoices_total = summer_camp_invoices.map(&:amount_paid_to_dollars).reduce(:+).to_i

        @summer_camp_sub_total = summer_camp_records_deposits_total + summer_camp_invoices_total
        # annual single day records
        annual_single_day_recs = @customer.annual_single_day_records(@default_year)
        @annual_single_day_records = annual_single_day_recs - @before_and_afterschool_records
        @annual_single_day_records_total = @annual_single_day_records.map(&:amount_paid).reduce(:+).to_i
        # purchases
        @purchases = @customer.purchases.where('extract (year from created_at) = ?', @default_year)
            .order('created_at DESC')
        @purchases_total = @purchases.pluck(:total_amount).reduce(:+)
        # grand total
        @grand_total = @single_day_sub_total.to_i + 
            @before_and_afterschool_records_total.to_i + 
            @day_care_records_total.to_i +
            @vacation_camp_records_total.to_i +
            @summer_camp_sub_total.to_i +
            @annual_single_day_records_total.to_i + 
            @purchases_total.to_i
    end

    private

    def setup_years year
        if year < Date.today.year
            @default_year = year.to_s
            @other_year = Date.today.year
        else
            @default_year = year.to_s
            @other_year = (Date.today - 1.year).year
        end
    end

end
