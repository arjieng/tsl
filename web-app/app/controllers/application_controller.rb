class ApplicationController < ActionController::Base
	layout 'application'

	before_action :authenticate_customer_user!
	before_action :direct_to_basic_information, unless: :devise_controller?

	# Prevent CSRF attacks by raising an exception.
	# For APIs, you may want to use :null_session instead.
	protect_from_forgery with: :null_session

	# These are added because Devise Superclasses inherit from ApplicationController by default
	before_action :configure_permitted_parameters, if: :devise_controller?
	before_action :check_page!

	def configure_permitted_parameters
		devise_parameter_sanitizer.permit(:sign_up, keys: [:email, :password, :password_confirmation, :remember_me])
		devise_parameter_sanitizer.permit(:sign_in, keys: [:email, :password, :remember_me])
		devise_parameter_sanitizer.permit(:account_update, keys: [:email, :password, :password_confirmation, :name])
	end

	def direct_to_basic_information
		unless params[:controller] == "customers"
			if customer_user_signed_in? && !current_customer_user.has_basic_information? && (params[:controller] != "dashboard" || params[:action] != "index")
				redirect_to root_path, alert: "You must provide basic information before you do anything else."
			end
		end
	end
	
	protected
		def after_sign_in_path_for(resource)
			logger.debug("HELLO OI OI OI OI #{resource.class}")

			if resource.class == CustomerUser
				abc = stored_location_for(resource).to_s
				cookies[:stored_location] = abc
				#request.env['omniauth.origin'] || stored_location_for(resource) || root_path

				logger.debug "HELLO OI OI OI OI #{cookies[:stored_location]}"
				return root_path
			else
				return request.env['omniauth.origin'] || stored_location_for(resource) || admin_authenticated_root_path
			end
			
		end

		# def after_sign_up_path_for(resource)
		# 	authenticated_root_path
		# end

		def check_page!
			@trigger = false
			if controller_name.eql?("sessions") && action_name.eql?("new")
				if cookies[:from_registration].present?
					cookies.delete :from_registration
				end
			end
			

			logger.debug " controller name: #{controller_name}"			
			logger.debug " action name: #{action_name}"
			if controller_name.eql?("dashboard") && action_name.eql?("index")
				logger.debug " stored_location: #{cookies[:stored_location]}"

				if cookies[:stored_location].present?
					if !cookies[:stored_location].nil?
						if !cookies[:stored_location].to_s.eql?("")
							rt = cookies[:stored_location]
							cookies.delete :stored_location
							return redirect_to rt
						end
					end
				end
				
				if cookies[:from_registration].present?
					cookies.delete :from_registration
					cookies[:another_redirect] = "true"
					# redirect_to new_customer_user_registration_path, notice: "Welcome! You have signed up successfully."
					return redirect_to(new_customer_user_registration_path, flash: {notice: "Welcome! You have signed up successfully."} )
				end

				if cookies[:another_redirect].present?
					cookies.delete :another_redirect
					@trigger = true
				end
			end
		end
end
