class PurchasesController < ApplicationController
    before_action :set_order, only: [:add_to_cart]
    skip_before_action :authenticate_customer_user!, :only => [:shop,:cart_overview,:add_to_cart,:create_order,:create,:delete_order]
    def shop
        @products = Product.all
    end

    def create
        puts params
        if !params[:email].present? && !customer_user_signed_in?
            flash[:error] = "Email required."
            redirect_to cart_overview_path and return
        elsif !params[:name].present? && !customer_user_signed_in?
            flash[:error] = "Name Required."
            redirect_to cart_overview_path and return
        end
        @purchase = Purchase.new(Services::Purchase::Construct.invoke(purchase_params))
        begin
            if @purchase.valid?
                customer_user_signed_in? ? @purchase.create_stripe_charge(params) : @purchase.create_stripe_charge_without_customer(params)
                if @purchase.save
                    puts "-0000000000000000000000"
                    puts customer_user_signed_in?
                    customer_user_signed_in? ? "" : session.delete(:orders)
                    customer_user_signed_in? ? @purchase.update_order_status_to_purchased : @purchase.update_order_status_to_purchased_without_user
                    AdminMailer.notify_tom_of_shop_purchase(@purchase).deliver!         
                    redirect_to successful_transaction_path(@purchase)
                else
                    redirect_to cart_overview_path
                end
            else
                if @purchase.errors.full_messages.include?("Orders can't be blank").present?
                    flash[:error] = "Could not proceed with transaction. Your cart is empty."
                end
                redirect_to cart_overview_path
            end 
        rescue StandardError => error
            f = File.open('log/payment.log','a')
			f.puts "message: #{error.message}, date: #{Time.current}, purchase_id: #{@purchase}"
			f.close
            flash[:error] = error.message
            redirect_to cart_overview_path
        end
    end

    def add_to_cart
        @order = Order.new
        @locations = Location.where.not('locations.name = ?', 'Counselors in Training')
            .joins(:programs)
            .where('programs.name = ?', 'Summer Camp')
            .order('name ASC')
    end

    def create_order
        size = Size.find(order_params[:size_id])
        if size.quantity > 0
            session[:orders] ||= []
            @order = Order.new(order_params)
            if @order.save && customer_user_signed_in?
                flash[:notice] = "Successfully added item to cart"
                redirect_to cart_overview_path
            elsif !customer_user_signed_in?
                session[:orders].push(@order.id)
                puts session[:orders].inspect
                redirect_to cart_overview_path
            else
                render :add_to_cart
            end
        else
            flash[:notice] = "Sorry #{size.name},is out of stock."
            redirect_to '/add-to-cart/' + order_params[:product_id]
        end
    end

    def cart_overview
        @locations = Location.where.not('locations.name = ?', 'Counselors in Training')
            .joins(:programs)
            .where('programs.name = ?', 'Summer Camp')
            .order('name ASC')
        if customer_user_signed_in?
            @orders = current_customer_user.customer.orders.where.not(purchased: true).order('created_at DESC')
        else
            @orders = []
            
            if session[:orders].present?
                @orders = Order.where(:id => session[:orders])
            end
            puts @orders
        end
        @purchase = Purchase.new
    end

    def delete_order
        puts 
        puts params
        @order = Order.find_by(id: params[:id])
        @order.destroy if @order.present?
        session[:orders].delete(params[:id])
        flash[:notice] = "Successfully removed item from your cart"
        redirect_to cart_overview_path
    end

    def successful_purchase
        @receipt = Purchase.find_by(id: params[:id]).receipt
    end

    private

    def order_params
        params.require(:order).permit(
            :product_id,
            :size_id,
            :quantity,
            :customer_id
        )
    end

    def purchase_params
        params.require(:purchase).permit(
            :customer_id,
            :orders,
            :location_id
        )
    end

    def set_order
        @product = Product.find_by(id: params[:id])
    end
end
