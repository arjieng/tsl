class StaticPagesController < ApplicationController
	def single_day_terms_of_service
	end

	def request_day_care_tour_terms_of_service
	end

	def before_and_afterschool_terms_of_service
	end

	def summer_camp_terms_of_service
	end

	def vacation_camp_terms_of_service
	end

    def before_and_after_school_rates
    end
end
