class CustomerUsersController < ApplicationController
  def edit_personal_information
  end

	def edit_billing_information
		@customer_user = current_customer_user.customer
		@user_id = current_customer_user.customer.id
	end

	def update_card
		customer = Stripe::Customer.retrieve(current_customer_user.customer.stripe_customer_identifier)
		# retrieve the current card attached to the current customer's stripe customer account
		saved_card = customer.sources.select {|source_obj| source_obj["object"] == "card"}[0]
		saved_card.delete
		# attach the new card
		customer.sources.create({
			:source => params[:stripeToken]
		})
		# the newly added card
		new_card = customer.sources.all(:object => "card").first
		current_customer_user.customer.update_attributes(
			card_brand: new_card.brand,
			card_last_four: new_card.last4,
		)
		redirect_to :back, notice: "Card Details Updated"
	end

	def add_card
		if current_customer_user.customer.stripe_customer_identifier.present?
			# retrieve customer's unique stripe identifier from the stripe api
			customer = Stripe::Customer.retrieve(current_customer_user.customer.stripe_customer_identifier)
			customer.sources.create({
				:source => params[:stripeToken]
			})
		else
			customer = Stripe::Customer.create(
				email: params[:stripeEmail],
				source: params[:stripeToken],
				metadata: {
					"customer_id" => current_customer_user.customer.id,
					"name" => current_customer_user.customer.name
				}
			)
		end
		new_card = customer.sources.all(:object => "card").first
		current_customer_user.customer.update_attributes(
			card_brand: new_card.brand,
			card_last_four: new_card.last4,
			stripe_customer_identifier: customer.id
		)
		redirect_to :back, notice: "Successfully added Card to your account.
		 You may now use your card for your transactions normally."
	end

	def unlink_card
		if retrieve_stripe_customer_account.present?
			stripe_customer_account = retrieve_stripe_customer_account
			card = stripe_customer_account.sources.all(:object => "card").first
			unless card.nil?
				card.delete
			end
		end
		current_customer_user.customer.update_attributes(
			card_brand: nil,
			card_last_four: nil
		)	
		redirect_to :back, notice: "Successfully unlinked card from your account."
	end

	def update_auto_pay
		customer = Customer.find_by(id: params[:customer_user][:auto_pay_user_id])
		customer.update_attributes(update_auto_pay_params)
		message = "Autopay feature has been successfully #{customer.autopay_enabled ? 'enabled' : 'disabled'}."
		render json: {
			message: message,
			label_message: "#{customer.autopay_enabled ? 'Autopay enabled' : 'Autopay disabled'}."
		}, status: :ok
	end

	private

	def update_auto_pay_params
		params.require(:customer_user).permit(:autopay_enabled)
	end

	def retrieve_stripe_customer_account
		if current_customer_user.customer.stripe_customer_identifier.present?
			Stripe::Customer.retrieve(current_customer_user.customer.stripe_customer_identifier)
		else
			nil
		end
	end
end
