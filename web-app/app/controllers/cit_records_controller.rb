class CitRecordsController < ApplicationController
	skip_before_action :authenticate_customer_user!
  def registration
		@cit_record = CitRecord.new
  end

	def create
		@cit_record = CitRecord.new(cit_record_params)

		if @cit_record.save
			@cit_record.send_tom_cit_email_notification

			redirect_to cit_success_path
		else
			render action: :registration
		end
	end

	private
	def cit_record_params
		params.require(:cit_record).permit(
			:first_name,
			:last_name,
			:dob,
			:phone_number,
			:email,
			:address_line_one,
			:address_line_two,
			:city,
			:state,
			:zip_code,
			:how_did_you_hear_about_us,
			:location,
			:week_one,
			:week_two,
			:week_three,
			:week_four,
			:week_five,
			:week_six,
			:week_seven,
			:week_eight,
			:week_nine,
			:week_ten,
			:attended_tsl_before,
			:attended_location,
			:cit_at_tsl_before,
			:cit_at_tsl_location,
			:previous_experience,
			:why_are_you_interested,
			:willing_to_follow_directions,
			:action_when_completed_tasks,
			:arguing_action,
			:transition_help,
			:interests_and_talents,
			:proven_responsibility,
			:difficult_problem,
			:what_you_will_bring,
			:willing_to_meet_vision,
			:agree_to_behavior_policy,
			:water_play_action,
			:four_square_action,
			:uninteresting_activity_action,
			:made_a_difference,
			emergency_contacts_attributes: [
				:id,
				:_destroy,
				:first_name,
				:last_name,
				:email,
				:phone_number,
				:cit_record_id
			]
		)
	end
end
