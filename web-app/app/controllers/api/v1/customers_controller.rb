class Api::V1::CustomersController < Api::V1::ApiController
  skip_before_action :verify_authenticity_token
  before_action :authorization
  include JsonFormatHelper

  def create
		@customer = Customer.new(customer_params)

		if @customer.save
      stripe_customer = Stripe::Customer.create(email: @customer.email, metadata: { customer_id: @customer.id, name: "#{@customer.first_name} #{@customer.last_name}" })
      @customer.update_attributes(stripe_customer_identifier: stripe_customer.id)
      
			@customer_user.update_attributes(customer_id: @customer.id, confirmed_information: true)
			render json: {
        message: "Thanks for providing us that information!",
        customer: format_customer(@customer).merge!(format_customer_user(@customer_user)),
        status: 200,
        has_basic_information: @customer_user.has_basic_information?,
        confirmed_information: @customer_user.confirmed_information
      }, status: 201
    else
      render json: {
        messages: @customer.errors.full_messages,
        status: 300
      }, status: 400
		end
  end
  
  # TODO: Add update customer details
  def update
    @customer = @customer_user.customer
    if @customer.update_attributes(customer_params)
        render json: { messages: "Updated account!", contact_informations: @customer.contact_informations, status: 200 }, status: 201
    else
      render json: { messages: "Uh oh, something went wrong!", status: 300 }, status: 200
    end
  end

  def fetch_contact_informations
    @customer = @customer_user.customer
    render json: { contact_informations: @customer.contact_informations, status: 200 }, status: 201
  end

  private  
    def customer_params
      params.require(:customer).permit(
        :email,
        :first_name,
        :last_name,
        :phone_number,
        :emergency_phone_number,
        :address_line_one,
        :address_line_two,
        :city,
        :state,
        :zip_code,
        :how_did_you_hear_about_us,
        :stripeToken,
        :bank_name,
        :bank_submitted,
        :amount_one,
        :amount_two,
        :autopay_enabled,
        contact_informations_attributes: [
          :id,
          :email,
          :_destroy
        ]
      )
    end
end
