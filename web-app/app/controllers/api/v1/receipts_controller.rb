class Api::V1::ReceiptsController < Api::V1::ApiController
  before_action :authorization
  include JsonFormatHelper
  
  def index
    @receipts = @customer_user.customer.receipts.page(params.fetch(:page, ''))

    render json: {
        status: 200,
        receipts: @receipts,
        invoices: format_pagination(@receipts, api_v1_receipts_path(page: @receipts.next_page))
    }, status: :ok
  end

  def show
  	
  	# begin
   #      @receipt = Receipt.includes(:customer, registration_record: [:program]).find params[:id]
   #      if @receipt.registration_record.present?
   #      	if @receipt.registration_record.program.name == 'Summer Camp'
   #      		if @receipt.payment_for.include? 'Deposit'
   #      			date_of_service(@receipt).each do |week|
   #      				week.display_name
   #      			end
   #      		elsif @receipt.payment_for.include? 'Invoice'
   #      			summer_camp_week_date_of_service(@receipt.invoice.due_date)
   #      		end
   #      	else
   #      		date_of_service(@receipt)
   #      	end
   #      end

   #      if @receipt.registration_record.present?
   #      	if @receipt.registration_record.program.name == 'Summer Camp'
   #      		Services are rendered for the whole week for every indicated item.
   #      	end
   #      end

   #      return render json: { 
   #          id: @receipt.id,
   #          customer_name: @receipt.customer.name,
   #          amount: number_with_precision(@receipt.amount, :precision => 2, :delimiter => ' ,'),
   #          payment_for: @receipt.payment_for,
   #          label_for_date_of_service: label_for_date_of_service(@receipt),
   #          start_date: label_for_date_of_service(@receipt),

   #          created_at: @receipt.created_at.to_date.to_formatted_s(:long)
   #      }, start: 200

   #  rescue Exception => e
   #      return render json: {
   #          status: 300,
   #          message: "Receipt not found"
   #      }, start: 401
   #  end
    
  end
end


#         <% if @receipt.registration_record.present? %>
#             <h4 id="child-record"><%= pluralize_child_count(@receipt)%> Information</h4>
#             <div class="child-record-table">
#                 <div class="container">
#                     <% if child_record_present?(@receipt) %>
#                         <% receipt_child_records(@receipt).each do |child| %>
#                             <%= render partial: 'child', locals: {child: child} %>
#                         <% end %>
#                     <% end %>
#                 </div>
#             </div>
#         <% end %>

#         <% if @receipt.purchase.present? %>
#             <div class="purchased-items">
#                 <div class="key">
#                     Items: 
#                 </div>
#                 <div class="value">
#                     <% if false%>
#                         <ul class="item-list">
#                             <% @receipt.purchase.order_details.each do |order| %>
#                                 <li><%= order %></li>
#                             <% end %>
#                         </ul>
#                     <% end %>

#                     <table>
#                         <thead>
#                             <tr>
#                                 <th>Name</th>
#                                 <th>Size</th>
#                                 <th>Price</th>
#                                 <th>Quantity</th>
#                                 <th>Subtotal</th>
#                             </tr>
#                         </thead>
#                         <tbody>
#                             <% @receipt.purchase.order_details.each do |order| %>
#                                 <tr>
#                                     <td><%= order.product.name %></td>
#                                     <td><%= order.size.name %></td>
#                                     <td>$<%= number_with_precision(order.size.price, precision: 2) %></td>
#                                     <td><%= order.quantity %></td>
#                                     <td>$<%= number_with_precision(order.total_amount, precision: 2) %></td>
#                                 </tr>
#                             <% end %>
#                         </tbody>
#                     </table>
#                 </div>
#             </div>
#         <% end %>

#         <div class="tax">
#             <h4 class="company-tax-label">Company Tax ID Number: </h4>
#             <span>26-3268474</span>
#         </div>
        
#     </div>
# </div>

# <div class="receipt-disclaimer">
#     For inquiries about your receipt, please contact the Administrator and provide them the Receipt ID. (This message is not included when this receipt is printed)
# </div>