class Api::V1::CustomerUsersController < Api::V1::ApiController
	before_action :authorization
	before_action :retrieve_stripe_customer_account, only: [:unlink_card]
    include JsonFormatHelper
  def update_card
		customer = Stripe::Customer.retrieve(@customer_user.customer.stripe_customer_identifier)
		# retrieve the current card attached to the current customer's stripe customer account
		saved_card = customer.sources.select {|source_obj| source_obj["object"] == "card"}[0]
		saved_card.delete
		# attach the new card
		customer.sources.create({
			:source => params[:stripeToken]
		})
		# the newly added card
		new_card = customer.sources.all(:object => "card").first
		@customer_user.customer.update_attributes(
			card_brand: new_card.brand,
			card_last_four: new_card.last4,
		)
		success_response(message: "Card Details Updated")
	end

	def add_card
		if @customer_user.customer.stripe_customer_identifier.present?
			# retrieve customer's unique stripe identifier from the stripe api
			customer = Stripe::Customer.retrieve(@customer_user.customer.stripe_customer_identifier)
			customer.sources.create({
				:source => params[:stripeToken]
			})
		else
			customer = Stripe::Customer.create(
				email: params[:stripeEmail],
				source: params[:stripeToken],
				metadata: {
					"customer_id" => @customer_user.customer.id,
					"name" => @customer_user.customer.name
				}
			)
		end
		new_card = customer.sources.all(:object => "card").first
		@customer_user.customer.update_attributes(
			card_brand: new_card.brand,
			card_last_four: new_card.last4,
			stripe_customer_identifier: customer.id
    )
    success_response(message: "Successfully added Card to your account.
      You may now use your card for your transactions normally.", customer_user: format_customer(@customer_user.customer).merge!(format_customer_user(@customer_user)))
	end

	def unlink_card
		if retrieve_stripe_customer_account.present?
			stripe_customer_account = retrieve_stripe_customer_account
			card = stripe_customer_account.sources.all(:object => "card").first
			unless card.nil?
				card.delete
			end
		end
		@customer_user.customer.update_attributes(
			card_brand: nil,
			card_last_four: nil
    )
    success_response(message: "Successfully unlinked card from your account.", customer_user: format_customer(@customer_user.customer).merge!(format_customer_user(@customer_user)))
	end

	def update_auto_pay
		customer = Customer.find_by(id: params[:customer_user][:auto_pay_user_id])
		customer.update_attributes(update_auto_pay_params)
		message = "Autopay feature has been successfully #{customer.autopay_enabled ? 'enabled' : 'disabled'}."
		success_response(message: message, customer_user: format_customer(@customer_user.customer).merge!(format_customer_user(@customer_user)))
	end

	private
    def update_auto_pay_params
      params.require(:customer_user).permit(:autopay_enabled)
    end

    def retrieve_stripe_customer_account
      if @customer_user.customer.stripe_customer_identifier.present?
        Stripe::Customer.retrieve(@customer_user.customer.stripe_customer_identifier)
      else
        nil
      end
    end

    def success_response options={}
      render json: {
        status: 200,
        message: options[:message],
        customer_user: options[:customer_user]
      }, status: :ok
    end
end
