class Api::V1::CustomerUserRegistrationsController < Devise::RegistrationsController
  include JsonFormatHelper

  def create
    resource = CustomerUser.new(user_params)

    if resource.save
      sign_in resource
      user = current_customer_user
      sign_out resource

      render json: { 
        message: "Successfully registered user!", 
        user: format_customer_user(user).merge!(access_token: user.access_token),
        status: 200,
        has_basic_information: user.has_basic_information?,
        confirmed_information: user.confirmed_information
      }, status: 201
    else
      render json: errors(resource.errors.to_a), status: 400
    end
  end

  private
    def user_params
      params.require(:customer_user).permit(:email,
        :password, 
        :confirm_password)
    end

    def errors(resource_errors)
      email_errors = ""
      password_errors = ""
      phone_number_errors = ""
      name_errors = ""

      errors = []
      resource_errors.each do |error|
        errors << error
      end

      return { errors: errors }
    end
end
