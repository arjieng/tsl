class Api::V1::ObsoleteRecordsController < ApplicationController
    skip_before_action :authenticate_customer_user!
    skip_before_action :verify_authenticity_token
    before_action :verify_access_key

    def update_obsolete_before_and_afterschool_records #
        RegistrationRecord.joins(:before_and_afterschool_record)
            .where('before_and_afterschool_records.year_cycle is ? or before_and_afterschool_records.year_cycle = ?', nil, '2018-2019').each do |regis_record|
            regis_record.update_attribute(:deleted, true)
        end
        puts "Finished updating obsolete before and afterschool registrations."
        render json: {
            message: "Successfully updated obsolete before and afterschool registrations."
        }, status: 200
    end

    def update_obsolete_single_day_registrations #
        RegistrationRecord.single_day_records_from_last_year.each do |single_day_regis|
            single_day_regis.update_attribute(:deleted, true)
        end
        puts "Finished updating obsolete single day registrations."
        render json: {
            message: "Successfully updated obsolete single day registrations."
        }, status: 200
    end

    def update_obsolete_summer_camp_registrations #
        RegistrationRecord.summer_camp_records_from_last_year.each do |summer_regis|
            summer_regis.update_attribute(:deleted, true)
        end
        puts "Finished updating obsolete summer camp registrations."
        render json: {
            message: "Successfully updated obsolete summer camp registrations."
        }, status: 200
    end

    def update_obsolete_vacation_camp_registrations
        RegistrationRecord.vacation_camp_records_from_this_and_last_year.each do |vacation_regis|
            vacation_regis.update_attribute(:deleted, true)
        end
        puts "Finished updating obsolete vacation camp registrations."
        render json: {
            message: "Successfully updated obsolete vacation camp registrations."
        }, status: 200
    end


    private

    def verify_access_key
        if params[:access_key].present?
            if params[:access_key] != Rails.application.secrets.temporize_key
                render json: error("The access key is invalid."), status: 400
            end
        else
            render json: error, status: 400
        end
    end
end