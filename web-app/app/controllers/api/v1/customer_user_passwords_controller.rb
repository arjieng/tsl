class Api::V1::CustomerUserPasswordsController < Devise::PasswordsController
  skip_before_action :verify_authenticity_token
  include JsonFormatHelper

  def create
    @customer_user = CustomerUser.find_by(email: params[:customer_user][:email])

    if @customer_user.present?
      @customer_user.set_reset_password_token
      ForgotPasswordMailer.forgot_password(user: @customer_user, password_token: @customer_user.reset_password_token).deliver_now
      render json: { message: "Successfully sent an email for forgot password link.", status: 200 }, status: 200
    else
      render json: { message: "Email doesn't exist", status: 300 }, status: 200
    end
  end

  def edit
    @customer_user = CustomerUser.find_by(reset_password_token: params[:reset_password_token])

    if @customer_user.present? && @customer_user.reset_password_period_valid?
      respond_to do |format|
        format.html
      end
    else
      @message = "Token has already expired"
      render "message"
    end
  end

  def update
    @customer_user = CustomerUser.find_by(reset_password_token: user_params[:reset_password_token])

    begin
      @message = "Token has already expired"
      return render "message" unless @customer_user.reset_password_period_valid?

      @message = "Invalid token"
      return render "message" unless @customer_user.present?
    end

    if @customer_user.reset_password(user_params[:password], user_params[:password_confirmation])
      @message = "Successfully changed your password."
      render "message"
    else
      redirect_to api_v1_forgot_password_edit_path(message: "Passwords does not match", reset_password_token: user_params[:reset_password_token])
    end
  end

  def check_password
    @customer_user = CustomerUser.find_by(access_token: params[:access_token])

    if @customer_user.valid_password?(user_params[:current_password])
      render json: { message: "Password is valid.", status: 200 }, status: 200
    else
      render json: { message: "Invalid password", status: 300 }, status: 200
    end
  end
  

  def change_password
    @customer_user = CustomerUser.find_by(access_token: params[:access_token])

    unless user_params[:password].to_s == user_params[:password_confirmation].to_s
      render json: { message: "New password and confirm password field is not equal", status: 300 }, status: 400 and return
    end

    if @customer_user.update(password: user_params[:password_confirmation])
      render json: { message: "Password successfully updated!", status: 200 }, status: 400
    else
      render json: { message: "Failed to update password!", status: 300 }, status: 400
    end  
  end

  private
    def user_params
      params.require(:customer_user).permit(:reset_password_token, :password, :password_confirmation, :current_password)
    end

    def validate_reset_password_token
      @customer_user = CustomerUser.find_by(reset_password_token: params[:reset_password_token])
    end
end