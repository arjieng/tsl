class Api::V1::DataController < Api::V1::ApiController
  before_action :authorization
  include JsonFormatHelper

  def get_locations
    @locations = Location.includes(:programs)
      .where(programs: { name: params.fetch(:program_name, ''), active: true })
      .order(:name)

    render json: {
      status: 200,
      locations: format_locations(@locations)
    }
  end

  def get_program
		if params[:location_name].present?
			location = Location.find_by(name: params[:location_name])
			program = location.programs.find_by(name: params[:program])
			render json: {
				is_package_enabled: program.is_package_active
			}
			# render plain: { hello: 'world' }.to_json, content_type: 'application/json'
		end

	end

  def get_publishable_key
    render json: {
      status: 200,
      stripe_publishable_key: ENV['STRIPE_PUBLISHABLE_KEY']
    }
  end

  def get_single_day_dates
    @program = Location.find_by(name: params[:location_name]).programs.find_by(name: "Single Day")
    @single_day_dates = @program.single_day_dates

    render json: {
      status: 200,
      single_day_dates: format_single_day_dates(@single_day_dates)
    }, status: :ok
  end
  
  def get_summer_camp_weeks
    @program = Location.find_by(name: params[:location_name]).programs.find_by(name: "Summer Camp")
    @summer_camp_weeks = @program.summer_camp_weeks

    is_full_ids = []
    @summer_camp_weeks = @summer_camp_weeks.order('start_date asc').each_with_index { |summer_camp_week, index|
      if summer_camp_week.capacity < summer_camp_week.enrolled_count(index)
        is_full_ids << summer_camp_week.id
      end
    }

    @summer_camp_weeks = @summer_camp_weeks.select { |scw| !is_full_ids.include?(scw.id) }

    render json: {
      status: 200,
      summer_camp_weeks: format_summer_camp_weeks(@summer_camp_weeks)
    }, status: :ok
  end
  
  def get_vacation_camp_schedules
		@program = Location.find_by(name: params[:location_name]).programs.find_by(name: "Vacation Camp")
    @vacation_camp_schedules = @program.vacation_camp_schedules.where.not(deleted: true)
    
    render json: {
      status: 200,
      vacation_camp_schedules: @vacation_camp_schedules
    }, status: :ok
  end

  def before_and_after_school_availability
		location_name = params[:location_name]
    after_school_program = Location.where(name: location_name).first.programs.where(name: "After School").first

    if after_school_program.present?
        result = after_school_program.before_and_after_school_program_configuration
    else
        result = nil
    end
		render json: {
			before_school: result.present? ? result.before_school : false,
			after_school: result.present? ? result.after_school : false
		}, status: :ok
  end
  
  def get_invoices  
    @invoices = @customer_user.customer.invoices.page(params.fetch(:page, ''))

    render json: {
      status: 200,
      invoice: @invoices,
      invoices: format_pagination(@invoices, get_invoices_api_v1_data_path(page: @invoices.next_page))
    }, status: :ok
  end
end