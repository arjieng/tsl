class Api::V1::CustomerUserSessionsController < Devise::SessionsController
  include JsonFormatHelper

  def create
    resource = CustomerUser.find_by(email: user_params[:email])
    return invalid_credentials "Invalid Email" if resource.present? == false

    if resource.valid_password?(user_params[:password])
      sign_in resource
      user = current_customer_user
      sign_out resource

      is_customer_present = user.customer.present?
      is_stripe_present   = false
      is_bank_verified    = false

      if is_customer_present
        customer_user = format_customer(user.customer)
        is_stripe_present = user.customer.stripe_customer_identifier.present?
        is_bank_verified  = user.customer.bank_verified
      else
        customer_user = format_customer_user(user)
      end

      customer_user = customer_user.merge!(access_token: user.access_token)
      
      return render json: { status: 200, 
        customer_user: customer_user,
        message: "Successfully Logged In!",
        is_customer_present: is_customer_present,
        is_bank_verified: is_bank_verified,
        has_basic_information: user.has_basic_information?,
        confirmed_information: user.confirmed_information
      }
    end

    return invalid_credentials "Invalid Password"
  end

  private
    def user_params
      params.require(:customer_user).permit(:email, :password)
    end

    def invalid_credentials message
      render json: { 
        error: message, 
        status: 300 
      }
    end
end
