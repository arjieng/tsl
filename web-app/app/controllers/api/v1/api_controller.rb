class Api::V1::ApiController < ActionController::Base
  protect_from_forgery with: :exception, if: Proc.new { |c| c.request.format == 'application/json' }

  def authorization
    @customer_user = CustomerUser.find_by(access_token: bearer_token)
    logger.debug "Bearer Token: #{bearer_token}"

    unless @customer_user.present?
      return render json: {
        status: 300,
        message: "Unauthorized access in the API"
      }, status: 401
    end
  end

  private
    def bearer_token
      pattern = /^Bearer /
      header  = request.headers['Authorization']
      header.gsub(pattern, '') if header && header.match(pattern)
    end
end