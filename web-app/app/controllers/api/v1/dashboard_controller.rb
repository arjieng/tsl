class Api::V1::DashboardController < Api::V1::ApiController
	before_action :authorization

	def registration_information
		@cu = CustomerUser.includes(customer: [registration_records: [:program]]).find(@customer_user.id)
		ris = []
		@registration_informations = @cu.customer.registration_records.where.not(program_id: nil, deleted: true).order('created_at desc')
		@registration_informations.each do |ri|
			ris.push({ id: ri.id, name: ri.program.display_name, registration_date: ri.created_at.strftime('%D'), enrollment_active: ri.enrolled? ? "Yes" : "No" })
		end

		render json: {
	      status: 200,
	      registration_information: ris
	    }
	end

	def registration_information_show
		# child_records
		@cu = CustomerUser.includes(customer: [registration_records: [:child_records, :program, :before_and_afterschool_record, :summer_camp_record, :vacation_camp_records, single_day_record: [:single_day_date] ]]).find(@customer_user.id)
		@registration_record = @cu.customer.registration_records.find(params[:id])
		@vacation_camp_records = @registration_record.vacation_camp_records

		single_day_record_att = nil
		before_and_afterschool_record_att = nil
		summer_camp_record_att = nil
		vacation_camp_records_att = nil

		if @registration_record.single_day_record.present?
			single_day_record_att = { single_day_record_single_day_date_present: @registration_record.single_day_record.single_day_date.present? }
			if @registration_record.single_day_record.single_day_date.present?
				single_day_record_att[:single_day_record_single_day_date_present_date] = @registration_record.single_day_record.single_day_date.display_name
			else
				single_day_record_att[:single_day_record_single_day_date_not_present_day] = @registration_record.single_day_record.formatted_day
				single_day_record_att[:single_day_record_single_day_date_not_present_date] = @registration_record.single_day_record.date.present? ? @registration_record.single_day_record.date : "N/A"
			end
		end

		if @registration_record.before_and_afterschool_record.present?
			before_and_afterschool_record_att = {
				before_and_afterschool_record_start_date: @registration_record.before_and_afterschool_record.start_date.strftime('%D'),
				before_school_monday: yes_no_boolean(@registration_record.before_and_afterschool_record.before_school_monday),
				before_school_tuesday: yes_no_boolean(@registration_record.before_and_afterschool_record.before_school_tuesday),
				before_school_wednesday: yes_no_boolean(@registration_record.before_and_afterschool_record.before_school_wednesday),
				before_school_thursday: yes_no_boolean(@registration_record.before_and_afterschool_record.before_school_thursday),
				before_school_friday: yes_no_boolean(@registration_record.before_and_afterschool_record.before_school_friday),
				after_school_monday: yes_no_boolean(@registration_record.before_and_afterschool_record.after_school_monday),
				after_school_tuesday: yes_no_boolean(@registration_record.before_and_afterschool_record.after_school_tuesday),
				after_school_wednesday: yes_no_boolean(@registration_record.before_and_afterschool_record.after_school_wednesday),
				after_school_thursday: yes_no_boolean(@registration_record.before_and_afterschool_record.after_school_thursday),
				after_school_friday: yes_no_boolean(@registration_record.before_and_afterschool_record.after_school_friday)
			}
		end

		if @registration_record.summer_camp_record.present?
			summer_camp_record_att = {
				summer_camp_record_week_1: yes_no_boolean(@registration_record.summer_camp_record.week_one),
				summer_camp_record_week_2: yes_no_boolean(@registration_record.summer_camp_record.week_two),
				summer_camp_record_week_3: yes_no_boolean(@registration_record.summer_camp_record.week_three),
				summer_camp_record_week_4: yes_no_boolean(@registration_record.summer_camp_record.week_four),
				summer_camp_record_week_5: yes_no_boolean(@registration_record.summer_camp_record.week_five),
				summer_camp_record_week_6: yes_no_boolean(@registration_record.summer_camp_record.week_six),
				summer_camp_record_week_7: yes_no_boolean(@registration_record.summer_camp_record.week_seven),
				summer_camp_record_week_8: yes_no_boolean(@registration_record.summer_camp_record.week_eight),
				summer_camp_record_week_9: yes_no_boolean(@registration_record.summer_camp_record.week_nine),
				summer_camp_record_week_10: yes_no_boolean(@registration_record.summer_camp_record.week_ten)
			}
		end

		if @registration_record.vacation_camp_records.present?
			vacation_camp_records_att = []
			@vacation_camp_records.each do |vacation_camp_record|
				vcratt = { 
					month: vacation_camp_record.month.present? ? vacation_camp_record.month.capitalize : "N/A",
					monday_label: vacation_camp_date(vacation_camp_record.month, 'Mon'),
					tuesday_label: vacation_camp_date(vacation_camp_record.month, 'Tues'),
					wednesday_label: vacation_camp_date(vacation_camp_record.month, 'Wed'),
					thursday_label: vacation_camp_date(vacation_camp_record.month, 'Thurs'),
					friday_label: vacation_camp_date(vacation_camp_record.month, 'Fri'),
					monday: yes_no_boolean(vacation_camp_record.monday), 
					tuesday: yes_no_boolean(vacation_camp_record.tuesday), 
					wednesday: yes_no_boolean(vacation_camp_record.wednesday), 
					thursday: yes_no_boolean(vacation_camp_record.thursday), 
					friday: yes_no_boolean(vacation_camp_record.friday)
				}
				if vacation_camp_record.month == 'april'
					vcratt[:april_week_label] = "Fri, 19"
					vcratt[:april_week] = yes_no_boolean(vacation_camp_record.friday)
				end
				vacation_camp_records.push(vcratt)
			end
		end

		render json: {
	      status: 200,
	      registration_record: @registration_record,
	      vacation_camp_records: @vacation_camp_records,
	      child_records: @registration_record.child_records,
	      single_day_record_present: @registration_record.single_day_record.present?,
	      single_day_record_att: single_day_record_att,

	      before_and_afterschool_record_present: @registration_record.before_and_afterschool_record.present?,
	      before_and_afterschool_record_att: before_and_afterschool_record_att,

	      summer_camp_record_present: @registration_record.summer_camp_record.present?,
	      summer_camp_record_att: summer_camp_record_att,

	      vacation_camp_records_present: @registration_record.vacation_camp_records.present?,
	      vacation_camp_records_att: vacation_camp_records_att
	    }
	end

	private
	def yes_no_boolean(boolean)
		if boolean == true
			"Yes"
		else
			"No"
		end
	end
end
