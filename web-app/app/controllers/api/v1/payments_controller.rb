class Api::V1::PaymentsController < ApiController
	def test
		render json: { 'logged_in' => true }
	end

	def create
		# Convert stringified JSON to Hash
		body = JSON.parse(params[:body])
		charge = params[:charge].present? ? JSON.parse(params[:charge]) : {}

		coupon_code = params[:couponCode].present? ? params[:couponCode] : nil

		# Pull Values from Hash If It Exists
		payment_platform = (charge == {} ? nil : "Stripe")
		payment_url = (charge == {} ? nil : "#{ENV['STRIPE_DASHBOARD_BASE_URL']}/payments/#{charge['id']}")
		customer_id = (charge == {} ? nil : charge["source"]["customer"])
		customer_url = (charge == {} ? nil : "#{ENV['STRIPE_DASHBOARD_BASE_URL']}/customers/#{customer_id}")
		amount_paid = (charge == {} ? 0.0 : charge["amount"] / 100)

		registration_record = RegistrationRecord.create!(
			email: body["email"],
			customer_identifier: customer_id,
			transaction_identifier: charge["id"],
			parent_first_name: body["parentFirstName"],
			parent_last_name: body["parentLastName"],
			phone_number: body["phoneNumber"],
			emergency_phone_number: body["emergencyNumber"],
			address_line_one: body["addressLineOne"],
			address_line_two: body["addressLineTwo"],
			city: body["city"],
			state: body["state"],
			zip_code: body["zipCode"],
			how_did_you_hear_about_us: body["howDidYouHearAboutUs"],
			location: body["location"],
			payment_platform: payment_platform,
			payment_url: payment_url,
			customer_url: customer_url,
			amount_paid: amount_paid,
			coupon_code: coupon_code
		)

		body["children"].each do |child|
			ChildRecord.create!(
				registration_record_id: registration_record.id,
				first_name: child["firstName"],
				last_name: child["lastName"],
				dob: child["dob"],
				child_classification: child["childClassification"],
				school_attending: child["schoolAttending"]
			)
		end

		create_single_day(body, registration_record)
		create_before_and_afterschool(body, registration_record)
		create_summer_camp(body, registration_record)
		create_vacation_camp(body, registration_record)

		registration_record.approve_and_send if registration_record.emailed == false
		registration_record.send_tom_email_notification

		render json: {}, status: 200
	end

	private
	def create_single_day(body, registration_record)
		day = body["day"].present? ? body["day"] : false

		if day
			SingleDayRecord.create!(
				registration_record_id: registration_record.id,
				day: day
			)
		end
	end

	def create_before_and_afterschool(body, registration_record)
		before_school_monday = body["beforeSchoolMonday"].present? ? body["beforeSchoolMonday"] : false
		before_school_tuesday = body["beforeSchoolTuesday"].present? ? body["beforeSchoolTuesday"] : false
		before_school_wednesday = body["beforeSchoolWednesday"].present? ? body["beforeSchoolWednesday"] : false
		before_school_thursday = body["beforeSchoolThursday"].present? ? body["beforeSchoolThursday"] : false
		before_school_friday = body["beforeSchoolFriday"].present? ? body["beforeSchoolFriday"] : false
		after_school_monday = body["afterSchoolMonday"].present? ? body["afterSchoolMonday"] : false
		after_school_tuesday = body["afterSchoolTuesday"].present? ? body["afterSchoolTuesday"] : false
		after_school_wednesday = body["afterSchoolWednesday"].present? ? body["afterSchoolWednesday"] : false
		after_school_thursday = body["afterSchoolThursday"].present? ? body["afterSchoolThursday"] : false
		after_school_friday = body["afterSchoolFriday"].present? ? body["afterSchoolFriday"] : false

		if before_school_monday || before_school_tuesday || before_school_wednesday || before_school_thursday || before_school_friday || after_school_monday || after_school_tuesday || after_school_wednesday || after_school_thursday || after_school_friday
			BeforeAndAfterschoolRecord.create!(
				registration_record_id: registration_record.id,
				start_date: Date.parse(body["startDate"]),
				before_school_monday: before_school_monday,
				before_school_tuesday: before_school_tuesday,
				before_school_wednesday: before_school_wednesday,
				before_school_thursday: before_school_thursday,
				before_school_friday: before_school_friday,
				after_school_monday: after_school_monday,
				after_school_tuesday: after_school_tuesday,
				after_school_wednesday: after_school_wednesday,
				after_school_thursday: after_school_thursday,
				after_school_friday: after_school_friday,
			)
		end
	end

	def create_summer_camp(body, registration_record)
		week_one = body["weekOne"].present? ? body["weekOne"] : false
		week_two = body["weekTwo"].present? ? body["weekTwo"] : false
		week_three = body["weekThree"].present? ? body["weekThree"] : false
		week_four = body["weekFour"].present? ? body["weekFour"] : false
		week_five = body["weekFive"].present? ? body["weekFive"] : false
		week_six = body["weekSix"].present? ? body["weekSix"] : false
		week_seven = body["weekSeven"].present? ? body["weekSeven"] : false
		week_eight = body["weekEight"].present? ? body["weekEight"] : false
		week_nine = body["weekNine"].present? ? body["weekNine"] : false
		week_ten = body["weekTen"].present? ? body["weekTen"] : false

		if week_one || week_two || week_three || week_four || week_five || week_six || week_seven || week_eight || week_nine || week_ten
			SummerCampRecord.create!(
				registration_record_id: registration_record.id,
				week_one: week_one,
				week_two: week_two,
				week_three: week_three,
				week_four: week_four,
				week_five: week_five,
				week_six: week_six,
				week_seven: week_seven,
				week_eight: week_eight,
				week_nine: week_nine,
				week_ten: week_ten
			)
		end
	end

	def create_vacation_camp(body, registration_record)
		month = body["vacationCampMonth"].present? ? body["vacationCampMonth"].scan(/\d+|\D+/).map{|a| a.capitalize}.join(" ") : nil
		monday = body["monday"].present? ? body["monday"] : false
		tuesday = body["tuesday"].present? ? body["tuesday"] : false
		wednesday = body["wednesday"].present? ? body["wednesday"] : false
		thursday = body["thursday"].present? ? body["thursday"] : false
		friday = body["friday"].present? ? body["friday"] : false

		if monday || tuesday || wednesday || thursday || friday
			VacationCampRecord.create!(
				registration_record_id: registration_record.id,
				month: month,
				monday: monday,
				tuesday: tuesday,
				wednesday: wednesday,
				thursday: thursday,
				friday: friday
			)
		end
	end
end
