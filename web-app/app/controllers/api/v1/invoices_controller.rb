class Api::V1::InvoicesController < ApplicationController
    skip_before_action :authenticate_customer_user!
    skip_before_action :verify_authenticity_token
    before_action :verify_access_key, except: [:show]

    # only send invoices for after school programs with a program
    # start date on september first and beyond
    def generate # called back for ever 1st day of the month
        RegistrationRecord.before_and_afterschool_records_from_2017_and_onwards.each do |registration_record|
            if registration_record.before_and_afterschool_record.start_date.year == 2018
                if registration_record.before_and_afterschool_record.start_date.month == 9 || registration_record.before_and_afterschool_record.start_date.month == 10 || registration_record.before_and_afterschool_record.start_date.month == 11
                    puts "Generating invoice for #{registration_record.id}"
                    unless registration_record.invoices.where("extract (month from due_date ) =? ",Time.now.to_date.month)
                            .where(memo: 'TSL Before and Afterschool').present?
                        registration_record.create_before_and_afterschool_invoice
                    end
                    puts "Finished generating invoice for #{registration_record.id}"
                else # handle 2018 before and afterschool registrations that will start on december
                    if registration_record.before_and_afterschool_record.start_date.month == 12
                        # add event here to check if it's december now
                        if Time.now.to_date.beginning_of_month >= registration_record.before_and_afterschool_record.start_date.to_date.beginning_of_month

                            puts "Generating invoice for #{registration_record.id}"
                            unless registration_record.invoices.where("extract (month from due_date ) =? ",Time.now.to_date.month)
                                    .where(memo: 'TSL Before and Afterschool').present?
                                registration_record.create_before_and_afterschool_invoice
                            end
                            puts "Finished generating invoice for #{registration_record.id}"
                        end
                    end
                end
            elsif registration_record.before_and_afterschool_record.start_date.year >= 2019

                # handle records with 2019 start dates and beyond here
                if Time.now.to_date.beginning_of_month >= registration_record.before_and_afterschool_record.start_date.to_date.beginning_of_month
                    unless registration_record.invoices.where("extract (month from due_date ) =? ",Time.now.to_date.month)
                            .where(memo: 'TSL Before and Afterschool').present?
                        registration_record.create_before_and_afterschool_invoice
                    end
                end
            end
        end
        render json: {
            message: "Successfully generated invoices"
        }, status: :ok
    end

    def background_generate
        InvoiceBackgroundGenerate.perform_later
        render json: {
            message: 'Successfully started background generation of invoices.'
        }, status: :ok
    end

    def end_of_month_afterschool_ultimatum # called back at the once within 27th - 30th of every month for families
        if Date.today.end_of_month == Date.today
            Invoice.overdue_afterschool_this_month.each do |invoice|
                InvoicesMailer.end_of_month_afterschool_ultimatum(invoice.id).deliver!
            end
        end
        render json: success, status: success[:status]
    end

    def overdue_by_one_week
        Invoice.overdue_on_the_fifth_or_earlier.each do |invoice|
            InvoicesMailer.overdue_by_one_week(invoice.id).deliver!
        end
        render json: success("Successfully created notifictions for invoices overdue on or before the fifth"), status: success[:status]
    end

    def generate_late_fees # called back for 16th to 31st day of every month
        Invoice.overdue_this_month.includes(:program).references(:program).where('programs.name = ?', 'After School').each do |invoice|
            invoice.fees.create!(amount: 5)
        end
        render json: success("Successfully created late fees for #{Date.today}"), status: success[:status]
    end

    def autopay_invoices # called back on the 2nd of every month
        Invoice.overdue_this_month.each do |invoice|
            if invoice.registration_record.customer.autopay_enabled?
                invoice.autopay_charge_customer
            end
        end
        render json: success("Successfully autocharged customers"), status: success[:status]
    end
    def autopay_summer_camp_invoices
        Invoice.weekly_overdue_summer_camp_invoices.each do |invoice|
            if invoice.registration_record.customer.autopay_enabled?
                invoice.autopay_charge_customer
            end
        end
        render json: success("Successfully autocharged customers"), status: success[:status]
    end
    # notifies Tom for unpaid day care invoices this month
    # called back every thursday of the month
    def notify_tom_for_unpaid_daycare_invoices
        day_care_records = RegistrationRecord.includes(:program)
            .where("programs.name = ?","Day Care").references(:program)
        day_care_records.each do |day_care_rec|
            day_care_rec.invoices.where("paid = false")
                .where('extract (year from created_at) >= 2018')
                .where('extract (month from due_date) = ?',Date.today.month)
                .each do |invoice|
                InvoicesMailer.notify_tom_unpaid_daycare_invoice(invoice.id, 'tom').deliver!
                InvoicesMailer.notify_tom_unpaid_daycare_invoice(invoice.id, 'jaime').deliver!
                InvoicesMailer.unpaid_daycare_invoice(invoice.id).deliver!
            end
        end
        render json: success("Successfully created notifications for unpaid day care invoices."), status: success[:status]
    end

    # not yet scheduled for recurrence
    def unpaid_invoices_last_month
        Invoice.overdue_last_month.each do |invoice|
            if invoice.registration_record.program.name == 'After School'
                InvoicesMailer.notify_invoice_overdue_last_month(invoice.id).deliver!
            end
        end
        puts "finished sending notifications for invoices overdue last month"
        render json: success("Successfully finished sending notifications for invoices overdue last month."),
            status: success[:status]
    end

    def unpaid_afterschool_invoices
        Invoice.unpaid_afterschool_this_month.each do |invoice|
            InvoicesMailer.notify_invoice_overdue_last_month(invoice.id).deliver!
        end
        
        puts "finished sending notifications for invoices overdue last month"
        render json: success("Successfully finished sending notifications for unpaid invoices this month."),
            status: success[:status]
    end

    def due_summer_camp_invoices
        Invoice.due_summer_camp_invoices.each do |invoice|
            InvoiceReminder.perform_later(invoice.id)
        end
        render json: success("Successfully finished sending notifications for due summer camp invoices this week."),
            status: success[:status]
    end

    def overdue_summer_camp_invoices
        Invoice.overdue_summer_camp_invoices.each do |invoice|
            OverdueSummerCampInvoice.perform_later(invoice.id)
        end
        render json: success("Successfully finished sending notifications for overdue summer camp invoices this week."),
            status: success[:status]
    end

    def end_of_month_unpaid_after_school_notification
        success_message = "Forgone sending of notifications for overdue After School Invoices since today is not the end of the month."
        if Date.today == Date.today.end_of_month
            EndOfMonthNotification.perform_later
            success_message = "Successfully started sending notifications for overdue After School Invoices."
        end
        render json: success(success_message),
                status: success[:status]
    end

    def test
        TestJob.perform_later(1)
        head :ok
    end

    def show
        if bearer_token.nil?
            return render json: {
                status: 300,
                message: "Unauthorized access in the API"
              }, status: 401
        else
            @customer_user = CustomerUser.find_by(access_token: bearer_token)
            begin
                @invoice = @customer_user.customer.invoices.find params[:id]

                return render json: { 
                    invoice_amount: @invoice.amount_to_dollars, 
                    amount_paid: @invoice.amount_paid_to_dollars, 
                    discount: @invoice.discounts_total_to_dollars, 
                    fees: @invoice.fees_total_to_dollars, 
                    due: @invoice.amount_remaining_to_dollars 
                }, start: 200

            rescue Exception => e
                return render json: {
                    status: 300,
                    message: "Invoice not found"
                }, start: 401
            end
        end
    end

    private

    def verify_access_key
        if params[:access_key].present?
            if params[:access_key] != Rails.application.secrets.temporize_key
                render json: error("The access key is invalid."), status: 400
            end
        else
            render json: error, status: 400
        end
    end

    def error(message = "Your access is unauthorized.")
        {
            message: message
        }
    end

    def success(message = "Success")
        {
            message: message,
            status: 200
        }
    end

    def bearer_token
      pattern = /^Bearer /
      header  = request.headers['Authorization']
      header.gsub(pattern, '') if header && header.match(pattern)
    end
end