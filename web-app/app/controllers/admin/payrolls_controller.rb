class Admin::PayrollsController < AdminController
    def home
    end

    def retrieve_accessible_payroll_locations
        accessible_locations = current_admin_user.payroll_locations.map(&:format_self)
        render json: {message: 'success', accessible_locations: accessible_locations}, status: :ok
    end

    def add_new_payroll_employee
        result = Services::Payroll::AddEmployee.invoke(params[:payroll_location_id], params[:employee_name])
        render json: result, status: result[:status_code]
    end

    def retrieve_payroll_location_information
        result = Services::Payroll::PayrollLocationInformation.invoke(params[:location_id], params[:date])
        render json: result, status: result[:status_code]
    end

    def delete_payroll_employee
        payroll_employee = PayrollEmployee.find_by(id: params[:employee_id])
        payroll_employee.destroy if payroll_employee.present?
        render json: {message: 'The user has been successfully deleted.'}, status: :ok
    end
    
    def update_payroll_employee_information
        result = Services::Payroll::UpdateEmployee.invoke(params[:payroll_employee_id], params[:new_payroll_employee_name])
        render json: result, status: result[:status_code]
    end

    def create_payroll_hour_for_new_employee
        result = Services::Payroll::CreatePayrollHour.invoke(
            params[:payroll_location_id],
            params[:payroll_employee_id],
            params[:payroll_date_of_hours_logged]
        )
        render json: result, status: :ok
    end

    def update_payroll_employee_logged_hours
        result = Services::Payroll::UpdateEmployeeLoggedHours.invoke(
            params[:payroll_location_id],
            params[:payroll_logged_hours_date],
            params[:logged_hours].values
        )
        render json: result, status: result[:status_code]
    end

    def retrieve_all_payroll_locations
        payroll_locations = PayrollLocation.all.map(&:format_self)
        render json: {
            message: 'Successfully retrieved all payroll locations.', 
            payroll_locations: payroll_locations
        }, status: :ok
    end

    def add_new_payroll_location
        result = Services::Payroll::AddPayrollLocation.invoke(params[:payroll_location_name])
        render json: result, status: result[:status_code]
    end

    def retrieve_all_administrators
        admin_users = User.where("length(name) > 0").order('name ASC').map(&:format_self)
        render json: {message: 'Successfully retrieved all administrators', admin_users: admin_users}, status: :ok
    end

    def retrieve_all_authorized_administrators
        admin_user_ids = PayrollLocationAccess.where(payroll_location_id: params[:location_id]).pluck(:user_id)
        authorized_admin_users = User.where('id in (?)', admin_user_ids).order('name ASC').map(&:format_self)
        render json: {message: 'Successfully retrieved authorized users', authorized_administrators: authorized_admin_users}
    end

    def revoke_access_to_payroll_location
        access = PayrollLocationAccess.find_by(payroll_location_id: params[:payroll_location_id], user_id: params[:administrator_id])
        access.destroy if access.present?
        render json: {message: 'Successfully revoked access from administrator', data_admin_id: params[:administrator_id]}, status: :ok
    end

    def authorize_access_to_payroll_location
        result = Services::Payroll::AddPayrollLocationAccess.invoke(
            params[:payroll_location_id],
            params[:administrator_id]
        )
        render json: result, status: result[:status_code]
    end

    def update_payroll_location_name
        result = Services::Payroll::UpdatePayrollLocation.invoke(
            params[:payroll_location_id],
            params[:new_name]
        )
        render json: result, status: result[:status_code]
    end

    def retrieve_all_payroll_locations_breakdown
        result = Services::Payroll::RetrieveAllPayrollLocationsBreakdown.invoke(params[:year], params[:month])
        render json: result, status: result[:status_code]
    end

    def retrieve_all_employees_of_payroll_location
        location = PayrollLocation.find_by(id: params[:location_id])
        employees = location.payroll_employees.map(&:format_self)
        render json: {message: 'Successfully rettrieved payroll employees.', employees: employees}, status: :ok
    end

    def log_employee_absence
        result = Services::Payroll::LogEmployeeAbsence.invoke(
            params[:payroll_location_id],
            params[:payroll_employee_id],
            params[:date_of_absence],
            params[:absence_type]
        )
        render json: result, status: result[:status_code]
    end

    def retrieve_payroll_location_information_for_trend_analysis
        result = Services::Payroll::PayrollInformationForTrend.invoke(
            params[:year],
            params[:month],
            params[:location_id]
        )
        render json: result, status: result[:status_code]
    end
end
