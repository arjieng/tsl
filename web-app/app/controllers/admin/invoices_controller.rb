class Admin::InvoicesController < AdminController
	def index
		@q = Invoice.ransack(params[:q])
		@q.sorts = 'created_at desc' if @q.sorts.empty?
		@invoices = @q.result.page(params[:page]).per(100)
	end

	def new
		@invoice = Invoice.new
		@registration_record = RegistrationRecord.find(params[:registration_record_id])
	end

	def create
		@invoice = Invoice.new(invoice_params)
		@registration_record = @invoice.registration_record

		if @invoice.save
			# Apply Discounts
			if @invoice.registration_record.discounts.present?
				@invoice.registration_record.discounts.each do |discount|
					@invoice.discounts.create!(amount: discount.amount)
				end
			end

			if @invoice.registration_record.customer.autopay_enabled?
				# Automatically Charge Customer
				@invoice.autopay_charge_customer
			else
				# Email User To Pay Invoice
				@invoice.send_emails
			end

			redirect_to admin_registration_record_path(@invoice.registration_record)
		else
			render action: :new
		end
	end

	def edit
		@invoice = Invoice.find(params[:id])
	end

	def update
		@invoice = Invoice.find(params[:id])
		if @invoice.update_attributes(invoice_params)
			if invoice_params.include? :paid
				@invoice.update_attribute(:amount_paid, @invoice.amount)
				@invoice.create_receipt("Payment", @invoice.amount)
				InvoicesMailer.invoice_marked_paid(@invoice.id).deliver!
			end
			InvoicesMailer.invoice_edited(@invoice.id).deliver! if invoice_params[:amount].present? || invoice_params[:memo].present? || invoice_params[:due_date].present?
			redirect_to admin_registration_record_path(@invoice.registration_record), notice: "Invoice updated and customer notified"
		else
			render action: :edit
		end
	end

	def destroy
		@invoice = Invoice.find(params[:id])
		@invoice.destroy
		InvoicesMailer.invoice_deleted(@invoice.id, @invoice.memo, @invoice.registration_record.id).deliver!
		redirect_to admin_registration_record_path(@invoice.registration_record), notice: "Invoice deleted and customer notified"
	end

	private
	def invoice_params
		params.require(:invoice).permit(
			:amount,
			:memo,
			:paid,
			:due_date,
			:registration_record_id,
			fees_attributes: [
				:id,
				:amount,
				:deleted
			]
		)
	end
end
