class Admin::StaticPagesController < AdminController
  before_action :authenticate_user!

  def about
  end

  def contact
  end
end
