class Admin::RegistrationRecordsController < AdminController
	before_action :set_registration_record, only: [:show, :edit, :update, :approve, :email, :destroy, :update_vacation_camp_enrollment, :new_invoices, :create_invoices]

	def index
		if params[:summer_camp_promo_registrations].present?
			rec = RegistrationRecord.promo_summer_camp_registration
		else
			rec = RegistrationRecord.where.not(deleted: true)
		end
		@q = rec.ransack(params[:q])
		@q.sorts = 'created_at desc' if @q.sorts.empty?
		@registration_records = @q.result.page(params[:page]).per(100)
		@csv_registration_records = @q.result.order('created_at desc')
		respond_to do |format|
			format.csv { send_data @csv_registration_records.to_csv, filename: "#{DateTime.now.strftime('%D')} TSLIS Export.csv", type: 'application/csv' }
			format.html
		end
	end

	def show
		@single_day_record = @registration_record.single_day_record
		@before_and_afterschool_record = @registration_record.before_and_afterschool_record
		@summer_camp_record = @registration_record.summer_camp_record
		@vacation_camp_records = @registration_record.vacation_camp_records
		@day_care_record = @registration_record.day_care_record
	end

	def edit
	end

	def update
		if @registration_record.update_attributes(registration_record_params)
            RecordUpdate.perform_later(@registration_record, current_admin_user)
			redirect_to admin_registration_record_path(@registration_record), notice: "Registration Record Updated"
		else
			render action: 'edit'
		end
	end

	def destroy
		@registration_record.update_attribute(:deleted, true)
        redirect_to admin_root_path, notice: "Registration Record destroyed"
	end

    def new_invoices
    end

    def create_invoices
        if @registration_record.update_attributes(registration_record_params)
            RecordUpdate.perform_later(@registration_record, current_admin_user)
			redirect_to admin_registration_record_path(@registration_record), notice: "Registration Record Updated"
		else
			render :new_invoices
		end
    end

	def approve
		@registration_record.update_attributes(approved: true, approved_at: DateTime.now)
		@registration_record.send_welcome_mailer
		redirect_to admin_root_path, notice: "Registration Record approved/email sent"
	end

	def email
		@registration_record.update_attributes(emailed: true, emailed_at: DateTime.now)
		@registration_record.send_welcome_mailer
		redirect_to admin_root_path, notice: "Email sent"
	end

    def update_vacation_camp_enrollment
        if vacation_camp_record_update_params[:command] == 'Unenroll'
             @registration_record.vacation_camp_records.where(vacation_camp_schedule_id: 
                vacation_camp_record_update_params[:vacation_camp_schedule_id]
             ).first.destroy
            response_message = "Unenrollment successful."
        else
            sched = VacationCampSchedule.find(vacation_camp_record_update_params[:vacation_camp_schedule_id])
            @registration_record.vacation_camp_records.create(
                monday: true,
                tuesday: true,
                wednesday: true,
                thursday: true,
                friday: true,
                month: sched.month,
                vacation_camp_schedule_id: sched.id
            )
            response_message = "Enrollment successful."
        end

        render json: {
            message: response_message,
            camp_schedule_id: vacation_camp_record_update_params[:vacation_camp_schedule_id]
        }, status: :ok
    end

	private

    def vacation_camp_record_update_params
        params.permit(:vacation_camp_schedule_id, :command)
    end

	def set_registration_record
		@registration_record = RegistrationRecord.find(params[:id])
	end

	def registration_record_params
		params.require(:registration_record).permit(
			:email,
			:old_program,
			:approved,
			:approved_at,
			:emailed,
			:emailed_at,
			:enrolled,
			:bill_afterschool_end_of_month,
            day_care_record_attributes: [
                :start_date
            ],
			before_and_afterschool_record_attributes: [
				:start_date,
				:before_school_monday,
				:before_school_tuesday,
				:before_school_wednesday,
				:before_school_thursday,
				:before_school_friday,
				:after_school_monday,
				:after_school_tuesday,
				:after_school_wednesday,
				:after_school_thursday,
				:after_school_friday
			],
            summer_camp_record_attributes: [
                :week_one,
                :week_two,
                :week_three,
                :week_four,
                :week_five,
                :week_six,
                :week_seven,
                :week_eight,
                :week_nine,
                :week_ten
            ],
            child_records_attributes: [
				:id,
				:_destroy,
				:registration_record_id,
				:first_name,
				:last_name,
				:dob,
				:school_attending
			],
            invoices_attributes: [
                :id,
                :_destroy,
                :amount,
                :memo,
                :due_date
            ]
		)
	end
end
