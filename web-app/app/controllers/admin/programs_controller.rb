class Admin::ProgramsController < AdminController
	require 'json'
	before_action :set_program, only: [:show, :edit, :update, :destroy]

	def show
		@program = Program.find(params[:id])
		if @program.name == 'Summer Camp'
			@registration_records = @program.registration_records
				.where(deleted: false)
				.where('extract (year from registration_records.created_at) >= ?', Time.now.year - 1)
				.order('created_at DESC').page(params[:page]).per(10)
		else
			@registration_records = @program.registration_records
				.where(deleted: false).order('created_at DESC').page(params[:page]).per(10)
		end
	end

	def new
		@program = Program.new
	end

	def edit
	end

	def create
		@program = Program.new(program_params)
		if @program.save
			redirect_to admin_location_path(@program.location), notice: "Program created"
		else
			render action: 'new'
		end
	end

	def update
		# check whether the params object contains vacation_camp_schedule attributes
		# then check whether these attributes have a dates property
		check_for_vacation_camp_schedule_attributes
		if @program.update_attributes(program_params)
			redirect_to admin_location_path(@program.location), notice: "Program updated"
		else
			render action: 'edit'
		end
	end

	def destroy
		@program.destroy
		redirect_to admin_location_path(@program.location), notice: "Program deleted"
	end

	def new_vacation_camp_schedule
		@program_id = params[:id]
	end

	def create_vacation_camp_schedule
		@vacation_camp_schedule = VacationCampSchedule.new(vacation_camp_schedule_params)
		if @vacation_camp_schedule.save
			redirect_to edit_admin_program_path(vacation_camp_schedule_params[:program_id]), notice: "Vacation Camp Schedule Created"
		else
			render action: 'new_vacation_camp_schedule'
		end
	end


	private
	def set_program
		@program = Program.find(params[:id])
	end

	def program_params
		params.require(:program).permit(
			:name,
			:name_suffix,
			:location_id,
			:director_id,
			:active,
			:capacity,
			:is_package_active,
			single_day_dates_attributes: [
				:id,
				:_destroy,
				:name,
				:date,
				:capacity,
				:program_id
			],
			summer_camp_weeks_attributes: [
				:id,
                :_destroy,
				:name,
				:start_date,
				:notes,
				:capacity,
				:program_id
			],
			vacation_camp_schedules_attributes: [
				:id,
				:_destroy,
				:name,
				:capacity,
				:year,
				:month,
				:deleted
			],
			before_and_after_school_program_configuration_attributes: [
				:before_school,
				:after_school
			]
		)
	end

	def vacation_camp_schedule_params
		params.require(:vacation_camp_schedule).permit(
			:name,
			:capacity,
			:month,
			:year,
			:day_one,
			:day_two,
			:day_three,
			:day_four,
			:day_five,
			:day_six,
			:day_seven,
			:day_eight,
			:day_nine,
			:day_ten,
			:day_eleven,
			:day_twelve,
			:day_thirteen,
			:day_fourteen,
			:day_fifteen,
			:day_sixteen,
			:day_seventeen,
			:day_eighteen,
			:day_nineteen,
			:day_twenty,
			:day_twenty_one,
			:day_twenty_two,
			:day_twenty_three,
			:day_twenty_four,
			:day_twenty_five,
			:day_twenty_six,
			:day_twenty_seven,
			:day_twenty_eight,
			:day_twenty_nine,
			:day_thirty,
			:day_thirty_one,
			:program_id
		)
	end

	def check_for_vacation_camp_schedule_attributes
		if params[:program][:vacation_camp_schedules_attributes].present?
			camp_schedule_params =  params[:program][:vacation_camp_schedules_attributes]
			# key is the index of the attriubte, value is the actual value of the vacation camp attributes
			# eg. {0 => {values}, 1 => {values}}
			camp_schedule_params.to_hash.each do |key, value|
				if value["dates"].present?
					camp_schedule = VacationCampSchedule.find(value["id"])
					camp_schedule.set_all_days_to_false
					# parse stringified dates json to Hash
					dates_hash = JSON.parse(value["dates"])
					camp_schedule.update_attributes(dates_hash)
				end
			end	
		end
	end
end
