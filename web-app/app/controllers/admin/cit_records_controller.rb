class Admin::CitRecordsController < AdminController
  def index
		@q = CitRecord.ransack(params[:q])
		@q.sorts = 'created_at desc' if @q.sorts.empty?
		@cit_records = @q.result.page(params[:page]).per(25)
  end

  def show
		@cit_record = CitRecord.find(params[:id])
  end

	def destroy
		@cit_record = CitRecord.find(params[:id])

		@cit_record.destroy
		redirect_to admin_cit_records_path, notice: "CIT Application Destroyed"
	end
end
