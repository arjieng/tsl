class Admin::DashboardController < AdminController
    include AdminDashboardHelper

    before_action :check_if_current_user_is_tom

    def dashboard
        week_ranges_array = Services::AdminDashboard::DivideMonthToWeekRanges.invoke
        @week_range = Services::AdminDashboard::RetrieveWeekRange.invoke(week_ranges_array, Date.today.day)
        date_today = Date.today
        beginning_of_week = Date.new(date_today.year, date_today.month, @week_range.first).beginning_of_day
        end_of_week = Date.new(date_today.year, date_today.month, @week_range.last).end_of_day
        all_invoices = Invoice.includes(:program).references(:programs)
        @all_registration_records = RegistrationRecord.includes(:program).references(:programs)
        @weekly_records = @all_registration_records.where(created_at: beginning_of_week..end_of_week)
        @new_deposits = @weekly_records.where('programs.name IN (?)', programs_requiring_a_deposit)
        @weekly_invoices = all_invoices.where(paid: true, created_at: within_this_month, updated_at: beginning_of_week..end_of_week)
        @new_users = Customer.where(created_at: beginning_of_week..end_of_week)
        # Single Day
        @single_day = @weekly_records.joins(:single_day_record).where('programs.name = ?', 'Single Day')
        @single_day_revenue = @single_day.pluck(:amount_paid).reduce(:+).to_i
        # Year-round Single Day
        @year_round_single_day = RegistrationRecord.joins(:annual_single_day_promo_record)
        .where(created_at: beginning_of_week..end_of_week)
        @year_round_revenue = @year_round_single_day.pluck(:amount_paid).reduce(:+).to_i
        
        # After School
        @before_and_after_school = @weekly_records.where('programs.name = ?', 'After School')
        @before_and_after_school_invoices = all_invoices.where('programs.name = ?', 'After School')
            .where('extract (month from due_date) = ?', Date.today.month)
            .where(paid: true, updated_at: beginning_of_week..end_of_week)
        after_school_deposit_revenue = @before_and_after_school.pluck(:amount_paid).reduce(:+).to_i
        after_school_invoice_revenue = 
            @before_and_after_school_invoices.map(&:amount_to_dollars).reduce(:+).to_i
        @before_and_after_school_revenue = after_school_deposit_revenue + after_school_invoice_revenue
        # Day Care
        @day_care = @weekly_records.where('programs.name = ?', 'Day Care')
        @day_care_invoices = 
            all_invoices.where(created_at: within_this_month)
            .where('programs.name = ?', 'Day Care')
            .where(paid: true, updated_at: beginning_of_week..end_of_week)
        @day_care_revenue = @day_care_invoices.map(&:amount_to_dollars).reduce(:+).to_i
        # Day Care Tour
        @day_care_tour = @weekly_records.where('programs.name = ?', 'Day Care Tour')
        # Vacation Camp
        @vacation_camp = @weekly_records.where('programs.name = ?', 'Vacation Camp')
        @vacation_camp_revenue = @vacation_camp.pluck(:amount_paid).reduce(:+).to_i
        # Summer Camp
        @summer_camp = @weekly_records.where('programs.name = ?', 'Summer Camp')
        @summer_camp_invoices = 
            all_invoices.where('programs.name = ?', 'Summer Camp')
            .where(paid: true, updated_at: beginning_of_week..end_of_week)
            .where('extract (year from due_date) = ?', Date.today.year)
      
        summer_camp_deposit_revenue = @summer_camp.pluck(:amount_paid).reduce(:+).to_i
        summer_camp_invoices_revenue = @summer_camp_invoices.map(&:amount_to_dollars).reduce(:+).to_i
        @summer_camp_revenue = summer_camp_deposit_revenue + summer_camp_invoices_revenue
        # Weekly revenue
        @total_weekly_revenue = @single_day_revenue + @before_and_after_school_revenue + @day_care_revenue + 
            @vacation_camp_revenue + @summer_camp_revenue + @year_round_revenue
        # New Customers
        @new_weekly_customers = Customer.where(created_at: beginning_of_week..end_of_week)
    end

    private

    def check_if_current_user_is_tom
        unless current_admin_user.name == 'Thomas Styles' || current_admin_user.email == "tsladventures@gmail.com"
            redirect_to admin_path
        end
    end
end
