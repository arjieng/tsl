class Admin::DiscountsController < AdminController
	def new
		@discount = Discount.new
	end

	def create
		@discount = Discount.new(discount_params)

		if @discount.save
			if @discount.registration_record.present?
				redirect_to admin_registration_record_path(@discount.registration_record), notice: "Discount applied to all future invoices for this registration"
			elsif @discount.program.present?
				redirect_to admin_program_path(@discount.program), notice: "Discount applied to all future invoices for this program"
			elsif @discount.invoice.present?
				InvoicesMailer.discount_applied(@discount.id).deliver!
				redirect_to admin_registration_record_path(@discount.invoice.registration_record), notice: "Discount applied to #{@discount.invoice.id} / #{@discount.invoice.memo}"
			else
				@discount.destroy
				redirect_to :back, alert: "Something went wrong"
			end
		else
			render 'new'
		end
	end

	def destroy
		@discount = Discount.find(params[:id])
		@discount.destroy
		redirect_to :back, notice: "Discount deleted"
	end

	private
	def discount_params
		params.require(:discount).permit(
			:registration_record_id,
			:program_id,
			:invoice_id,
			:amount
		)
	end
end
