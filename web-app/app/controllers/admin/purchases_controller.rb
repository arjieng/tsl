class Admin::PurchasesController < AdminController
    def index
        @locations = Location.where.not('locations.name = ?', 'Counselors in Training')
            .joins(:programs)
            .where('programs.name = ?', 'Summer Camp')
            .order('name ASC')
        @q = Purchase.ransack(params[:q])
        @purchases = @q.result.order('created_at DESC').page(params[:page]).per(10)
    end

    def details
        @purchase = Purchase.find_by(id: params[:id])
    end
end