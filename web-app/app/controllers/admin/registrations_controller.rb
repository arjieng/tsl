class Admin::RegistrationsController < Devise::RegistrationsController
	layout 'admin'

  def update
    account_update_params = devise_parameter_sanitizer.sanitize(:account_update)

    # required for settings form to submit when password is left blank
    if account_update_params[:password].blank?
      account_update_params.delete("password")
      account_update_params.delete("password_confirmation")
    end

    @user = User.find(current_admin_user.id)
    if @user.update_attributes(account_update_params)
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case their password changed
      sign_in @user, :bypass => true
      redirect_to admin_root_path, notice: "Your account has been updated."
    else
      render "edit"
    end
  end

  protected
  def update_resource(resource, params)
    resource.update_without_password(params)
  end
end
