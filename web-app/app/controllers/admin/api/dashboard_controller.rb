class Admin::Api::DashboardController < AdminController
    before_action :check_if_user_is_tom
    def monthly_summary
        result = Services::AdminDashboard::MonthlySummary.invoke(params[:year], params[:month])
        render json: result, status: result[:status]
    end

    def yearly_summary
        result = Services::AdminDashboard::YearlySummary.invoke(params[:year])
        render json: result, status: :ok
    end

    def search_customer
        result = Services::AdminDashboard::SearchCustomer.invoke(params[:first_name], params[:last_name])
        render json: result, status: :ok
    end

    def customer_information
        result = Services::AdminDashboard::CustomerInformation.invoke(params[:customer_id])
        render json: result, status: :ok
    end

    def program_breakdown
        result = Services::AdminDashboard::ProgramBreakdown.invoke(params[:year], params[:month], params[:program])
        render json: result, status: :ok
    end

    def programs_summary
        result = Services::AdminDashboard::ProgramsSummary.invoke(params[:year])
        render json: result, status: :ok
    end
    
    def receipt_information
        result = Services::AdminDashboard::ReceiptInformation.invoke(params[:id])
        render json: result, status: result[:status_code]
    end

    def receipt_update
        result = Services::AdminDashboard::UpdateReceipt.invoke(
            params[:receipt_id],
            params[:payment_for],
            params[:child_information].values
        )
        render json: result, status: result[:status_code]
    end
    
    private

    def check_if_user_is_tom
        unless current_admin_user.name == "Thomas Styles" || current_admin_user.email == "tsladventures@gmail.com"
            return render json: {message: "You are not authorized."}, status: 403
        end
    end
end