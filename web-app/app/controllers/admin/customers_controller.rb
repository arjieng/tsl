class Admin::CustomersController < AdminController
  def index
		@customers = Customer.where("is_deleted = false").order(:email).page(params[:page]).per(100)
  end
  def destroy
    @customer = Customer.find(params[:id])
    @updated = @customer.update_attributes(is_deleted: true)
    respond_to :js
  end
  def show
		@customer = Customer.find(params[:id])
  end
end
