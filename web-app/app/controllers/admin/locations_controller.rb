class Admin::LocationsController < AdminController
	skip_before_action :authenticate_admin_user!, only: [:location_address]
	before_action :set_location, only: [:edit, :update, :destroy]

  def index
		@locations = Location.order(:name)
  end

  def show
		locations = Location.eager_load(:programs)
		@location = locations.find_by(id: params[:id])
		# @location = Location.includes(:programs).find(params[:id])
		# @location = Location.find_by(id: params[:id])
  end

  def new
		@location = Location.new
  end

  def edit
  end

	def create
		@location = Location.new(location_params)

		if @location.save
			redirect_to admin_locations_path, notice: "Location created"
		else
			render action: 'new'
		end
	end

	def update
		if @location.update_attributes(location_params)
			redirect_to admin_locations_path, notice: "Location updated"
		else
			render action: 'edit'
		end
	end

	def destroy
		@location.destroy
		redirect_to admin_locations_path, notice: "Location deleted"
	end

	private
	def set_location
		@location = Location.find(params[:id])
	end

	def location_params
		params.require(:location).permit(
			:name,
			:address,
			:director_id,
			:minimum_age
		)
	end
end
