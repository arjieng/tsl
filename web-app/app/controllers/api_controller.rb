class ApiController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  skip_before_filter  :verify_authenticity_token
	before_action :authenticate_request!

	protected
  def authenticate_request!
    unless correct_password_in_token?
      render json: { errors: ['Not Authenticated'] }, status: :unauthorized
      return
    end
  rescue JWT::VerificationError, JWT::DecodeError
    render json: { errors: ['Not Authenticated'] }, status: :unauthorized
  end

  private
  def http_token
    @http_token ||= if request.headers['Authorization'].present?
      request.headers['Authorization'].split(' ').last
    end
  end

  def auth_token
    @auth_token ||= JsonWebToken.decode(http_token)
  end

  def correct_password_in_token?
    http_token && auth_token && auth_token[:password] == ENV['JWT_PASSWORD']
  end
end
