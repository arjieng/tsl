class ProgramsController < ApplicationController
	def get_capacity
		if params[:location_name].present?
			location = Location.find_by(name: params[:location_name])
			program = location.programs.find_by(name: params[:program])

			render json: {
				capacity: program.capacity,
				enrolled: program.after_school_registrations
			}
		else
			render json: {}
		end
	end
	def get_program
		if params[:location_name].present?
			location = Location.find_by(name: params[:location_name])
			program = location.programs.find_by(name: params[:program])
			render json: {
				is_package_enabled: program.is_package_active
			}
			# render plain: { hello: 'world' }.to_json, content_type: 'application/json'
		end

	end
	def get_single_day_dates
		@program = Location.find_by(name: params[:location_name]).programs.find_by(name: "Single Day")

		respond_to do |format|
			format.js
		end
	end

	def get_summer_camp_weeks
		@program = Location.find_by(name: params[:location_name]).programs.find_by(name: "Summer Camp")

		respond_to do |format|
			format.js
		end
	end

	def get_vacation_camp_schedules
		@program = Location.find_by(name: params[:location_name]).programs.find_by(name: "Vacation Camp")

		date_september_15 = "September 15, 2019".to_date
		date_today = DateTime.now.to_date
		if date_september_15 <= date_today
			@vacation_camp_schedules = @program.vacation_camp_schedules.where.not(deleted: true, name: ["December Camp (Dec. 26-28)", "February Camp (February 18-22)", "April Camp (April 19, 22-26)", "April Camp"])
			respond_to do |format|
				format.js
			end
		else
			@vacation_camp_schedules = @program.vacation_camp_schedules.where.not(deleted: true, name: ["December Camp (Dec. 23-Jan. 3)", "February Camp (Feb. 17-21)", "April Camp (Apr. 10-17)"])
			respond_to do |format|
				format.js
			end
		end
	end
end
