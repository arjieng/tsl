class CustomersController < ApplicationController
	def create
		@customer = Customer.new(customer_params)
		if @customer.save
			stripe_customer = Stripe::Customer.create(email: @customer.email, metadata: { customer_id: @customer.id, name: "#{@customer.first_name} #{@customer.last_name}" })
			@customer.update_attributes(stripe_customer_identifier: stripe_customer.id)
			current_customer_user.update_attributes(customer_id: @customer.id, confirmed_information: true)
			redirect_to root_path, notice: "Thanks for providing us with that information!"
		else
			redirect_to :back, alert: @customer.errors.full_messages.to_sentence
		end
	end

	def update
		@customer = Customer.find(params[:id])
		if params[:public_token].present? # if this is present they're providing bank account information
			if @customer.stripe_customer_identifier.present?
				stripe_customer = Stripe::Customer.retrieve(@customer.stripe_customer_identifier)
				# stripe_customer.source = PlaidService.bank_account_token(params[:public_token], params[:account_id])
				# stripe_customer.save

				# check whether customer already has an attached bank account
				if stripe_customer.sources.all(:object => "bank_account").first.nil?
					stripe_customer.source = PlaidService.bank_account_token(params[:public_token], params[:account_id])
					stripe_customer.sources.create({
						:source => stripe_customer.source
					})
				else
					current_bank_account = stripe_customer.sources.all(:object => "bank_account").first
					current_bank_account.delete
					stripe_customer.sources.create({
						:source => PlaidService.bank_account_token(params[:public_token], params[:account_id])
					})
				end
				new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
				@customer.update_attributes(
					stripe_customer_identifier: stripe_customer.id,
					stripe_bank_identifier: new_bank_account.id,
					bank_name: new_bank_account.bank_name,
					bank_verified: true)
				redirect_to :back, notice: "Updated bank account!" and return
			else
				stripe_customer = Stripe::Customer.create(
					email: current_customer_user.email,
					source: PlaidService.bank_account_token(params[:public_token], params[:account_id]),
					metadata: {
						"customer_id" => @customer.id,
						"name" => @customer.name
					}
				)
				new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
				@customer.update_attributes(
					stripe_customer_identifier: stripe_customer.id,
					stripe_bank_identifier: new_bank_account.id,
					bank_name: new_bank_account.bank_name,
					bank_verified: true)
				redirect_to :back, notice: "Updated bank account!" and return
			end
			# @customer.update_attributes(
			# 	stripe_customer_identifier: stripe_customer.id,
			# 	stripe_bank_identifier: stripe_customer.sources.data[0].id,
			# 	bank_name: stripe_customer.sources.data[0].bank_name,
			# 	card_last_four: nil,
			# 	card_brand: nil,
			# 	bank_verified: true)
			# redirect_to :back, notice: "Updated account!" and return
		end

		if @customer.update_attributes(customer_params)
			if !@customer.has_complete_information?
				redirect_to root_path, alert: "You need to provide every piece of information."
			elsif @customer.customer_user.confirmed_information == false
				@customer.customer_user.update_attributes(confirmed_information: true)
				redirect_to root_path, notice: "Thanks for confirming!"		
			else
				redirect_to :back, notice: "Updated account!"
			end
		else
			redirect_to :back, alert: "Uh oh, something went wrong!"
		end
	end

	private
	def customer_params
		params.require(:customer).permit(
			:email,
			:first_name,
			:last_name,
			:phone_number,
			:emergency_phone_number,
			:address_line_one,
			:address_line_two,
			:city,
			:state,
			:zip_code,
			:how_did_you_hear_about_us,
			:stripeToken,
			:bank_name,
			:bank_submitted,
			:amount_one,
			:amount_two,
			:autopay_enabled,
			contact_informations_attributes: [
				:id,
				:email,
				:_destroy
			]
		)
	end
end
