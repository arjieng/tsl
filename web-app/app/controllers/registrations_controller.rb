class RegistrationsController < Devise::RegistrationsController
	before_action :check_page!
	def create
		build_resource(sign_up_params)

		resource.customer_id = Customer.where('lower(email) = ?', sign_up_params[:email].downcase).present? ? Customer.where('lower(email) = ?', sign_up_params[:email].downcase).first.id : nil

		resource.save

		yield resource if block_given?
		if resource.persisted?
			if resource.active_for_authentication?
				set_flash_message! :notice, :signed_up
				sign_up(resource_name, resource)
				respond_with resource, location: after_sign_up_path_for(resource)
			else
				set_flash_message! :notice, :"signed_up_but_#{resource.inactive_message}"
				expire_data_after_sign_in!
				respond_with resource, location: after_inactive_sign_up_path_for(resource)
			end
		else
			clean_up_passwords resource
			set_minimum_password_length
			respond_with resource
		end
	end

	def update
		self.resource = resource_class.to_adapter.get!(send(:"current_#{resource_name}").to_key)
		prev_unconfirmed_email = resource.unconfirmed_email if resource.respond_to?(:unconfirmed_email)

		resource_updated = update_resource(resource, account_update_params)
		yield resource if block_given?
		if resource_updated
			if is_flashing_format?
				flash_key = update_needs_confirmation?(resource, prev_unconfirmed_email) ?
					:update_needs_confirmation : :updated
				set_flash_message :notice, flash_key
			end

			if params[:customer_user][:email].present?
				stripe_customer = Stripe::Customer.retrieve(resource.customer.stripe_customer_identifier)
				stripe_customer.email = params[:customer_user][:email]
				stripe_customer.save
				resource.customer.update_attributes(email: params[:customer_user][:email])
			end

			bypass_sign_in resource, scope: resource_name
			respond_with resource, location: after_update_path_for(resource)
		else
			clean_up_passwords resource
			set_minimum_password_length
			respond_with resource
		end
	end

	protected
	def update_resource(resource, params)
		resource.update_without_password(params)
	end

	def check_page!
		if action_name.eql?("new")
			logger.debug "FROM REGISTRATION: #{cookies[:from_registration]}"
			if !cookies[:from_registration].present?
				cookies[:from_registration] = "true"
				logger.debug "FROM REGISTRATION: #{cookies[:from_registration]}"
			end
		end
	end
end
