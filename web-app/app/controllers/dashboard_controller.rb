class DashboardController < ApplicationController
	def index
		if current_customer_user.customer.present?
			if current_customer_user.customer.stripe_customer_identifier.present?
				stripe_customer = Stripe::Customer.retrieve(current_customer_user.customer.stripe_customer_identifier)
				@stripe_card = stripe_customer.sources.all(:object => "card").first
			else
				stripe_customer = ""
				@stripe_card = ""
			end
		end
  	end

	def registration_information
	end

	def registration_information_show
		@registration_record = current_customer_user.customer.registration_records.find(params[:id])
		@vacation_camp_records = @registration_record.vacation_camp_records
	end
end
