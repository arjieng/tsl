class LocationsController < ApplicationController
	def get_address
		address = Location.where(name: params[:location_name]).present? ? Location.find_by(name: params["location_name"]).address : ""

		render json: { address: address }
	end

	def get_minimum_age
		@location = Location.find_by(name: params[:location_name])

		respond_to do |format|
			format.js
		end
	end

	def before_and_after_school_availability
		location_name = params[:location_name]
        after_school_program = Location.where(name: location_name).first
            .programs.where(name: "After School").first
        if after_school_program.present?
            result = after_school_program.before_and_after_school_program_configuration
        else
            result = nil
        end
		render json: {
			before_school: result.present? ? result.before_school : false,
			after_school: result.present? ? result.after_school : false
		}, status: :ok
	end
end
