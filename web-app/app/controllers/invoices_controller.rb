class InvoicesController < ApplicationController
  def index
		@invoices = current_customer_user.customer.invoices
  end

  def show
		@invoice = current_customer_user.customer.invoices.find(params[:id])
  end

	def submit_payment
		@invoice = current_customer_user.customer.invoices.find(params[:invoice_id])
		
		# current_customer_user.customer
        
		begin
			# check if the user has an existing stripe customer attribute;
			# if true, implies a saved payment source
			if current_customer_user.customer && current_customer_user.customer.stripe_customer_identifier.present?
				stripe_customer = Stripe::Customer.retrieve(current_customer_user.customer.stripe_customer_identifier)
            
				if params[:stripeToken].present?
					stripe_customer.sources.create({
						:source => params[:stripeToken]
					})
					new_card = stripe_customer.sources.all(:object => "card").first
					current_customer_user.customer.update_attributes(
						card_brand: new_card.brand,
						card_last_four: new_card.last4
					)
					stripe_customer.default_source = stripe_customer.sources.all(:object => "card").data[0].id
					stripe_customer.save
				elsif params[:public_token].present?
					source = PlaidService.bank_account_token(params[:public_token], params[:account_id])
					stripe_customer.sources.create({
						:source => source
					})
					new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
					current_customer_user.customer.update_attributes(
						stripe_bank_identifier: new_bank_account.id,
						bank_name: new_bank_account.bank_name,
						bank_verified: true
					)
					stripe_customer.default_source = stripe_customer.sources.all(:object => "bank_account").data[0].id
					stripe_customer.save
				else # customer using a saved payment account and not adding a new payment source
					if params[:commit] == "Submit And Pay With Card on File"
						stripe_customer.default_source = stripe_customer.sources.all(:object => "card").data[0].id
						stripe_customer.save
					elsif params[:commit] == "Submit And Pay With Bank on File"
						stripe_customer.default_source = stripe_customer.sources.all(:object => "bank_account").data[0].id
						stripe_customer.save
					end
				end
			else
				# if customer has no stripe_customer_identifier attribute
				# create one and bind a card as a source
				if params[:stripeToken].present?
					stripe_customer = Stripe::Customer.create(
						source: params[:stripeToken],
						metadata: {
							"customer_id" => current_customer_user.customer,
							"name" => current_customer_user.customer.name
						}
					)
					current_customer_user.customer.update_attributes(
						stripe_customer_identifier: stripe_customer.id,
						card_last_four: stripe_customer.sources.first.last4,
						card_brand: stripe_customer.sources.first.brand
					)
				else
					stripe_customer = Stripe::Customer.create(
						source: PlaidService.bank_account_token(params[:public_token], params[:account_id]),
						metadata: {
							"customer_id" => current_customer_user.customer,
							"name" => current_customer_user.customer.name
						}
					)
					new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
					current_customer_user.customer.update_attributes(
						stripe_customer_identifier: stripe_customer.id,
						stripe_bank_identifier: new_bank_account.id,
						bank_name: new_bank_account.bank_name,
						bank_verified: true
					)
				end
			end
			amount = (params[:amount].to_f * 100).to_i
			charge = Stripe::Charge.create(
				customer: stripe_customer.id,
				amount: amount,
				description: @invoice.memo,
				currency: 'usd'
			)
			if amount >= @invoice.amount_remaining
				@invoice.update_attributes(paid: true, amount_paid: @invoice.amount_paid + amount)
				@invoice.create_receipt("Payment", charge.amount)
				redirect_to :back, notice: "Invoice Paid!"
			else
				@invoice.update_attributes(amount_paid: @invoice.amount_paid + amount)
				@invoice.create_receipt("Partial payment", charge.amount)
				redirect_to :back, notice: "Invoice Partially Paid!"
			end
		rescue StandardError => e # card has been declined
			return redirect_to invoice_path(@invoice), alert: "Your payment has been declined. Reason: #{e.message}"
		end
		#======== charge here
	end

	private
	def invoice_params
		params.require(:invoice).permit(
			:amount,
			:paid,
			:memo,
			:due_date
		)
	end
end
