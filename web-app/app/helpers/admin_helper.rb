module AdminHelper
	def approved_status(registration_record)
		if registration_record.approved? && registration_record.approved_at.present?
			"Approved at #{registration_record.approved_at.strftime('%D')}"
		elsif registration_record.approved?
			"Approved at Unknown Time"
		else
			"Not Approved"
		end
	end

	def emailed_status(registration_record)
		if registration_record.emailed? && registration_record.emailed_at.present?
			"#{registration_record.emailed_at.strftime('%D')}"
		elsif registration_record.emailed?
			"Unknown Time"
		else
			"Not Emailed"
		end
	end

	def yes_no_boolean(boolean)
		if boolean == true
			"Yes"
		else
			"No"
		end
	end

	def program_names
		['Day Care', 'After School', 'Summer Camp', 'Vacation Camp', 'Single Day', 'Day Care Tour']
	end
end
