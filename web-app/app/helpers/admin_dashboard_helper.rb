module AdminDashboardHelper
    def month_now
        Date.today.strftime('%B')
    end

    def programs_requiring_a_deposit
        ["Single Day", "Summer Camp", "After School", "Vacation Camp"]
    end

    def format_to_currency number
        number_with_precision(number, precision: 0, delimiter: ', ')
    end

    def within_this_month
        Date.today.beginning_of_month..Date.today.end_of_month
    end
end