module ReceiptHelper
    def date_of_service(receipt)
        if receipt.program_name == "Single Day"
            if receipt.registration_record.single_day_record.present?
                if receipt.registration_record.single_day_record.single_day_date.present?
                    receipt.registration_record.single_day_record.single_day_date.date.to_formatted_s(:long)
                else
                    receipt.registration_record.single_day_record.created_at.to_formatted_s(:long)
                end
            else
                "#{receipt.registration_record.annual_single_day_promo_record.year_cycle} (Year-round)"
            end
        elsif receipt.program_name == "Vacation Camp"
            result = ""
            if receipt.registration_record.vacation_camp_records.present?
                result = receipt.registration_record.vacation_camp_records.map do | rec |
                    "#{rec.vacation_camp_schedule.schedule_summary}"
                end
                result[result.length - 1] = result[result.length - 1].gsub("|", "")
                result = result.join(' ')
            end
            result
        elsif receipt.program_name == "After School"
            if receipt.registration_record_id.present?
                if receipt.payment_for.include? 'Deposit'
                    if receipt.registration_record.before_and_afterschool_record.present?
                        format_date(receipt.registration_record.before_and_afterschool_record.start_date)
                    else
                        format_date(receipt.registration_record.created_at.to_date)
                    end
                    
                elsif receipt.payment_for.include? 'Invoice'
                    beginning_of_month = receipt.invoice.due_date.beginning_of_month
                    end_of_month = receipt.invoice.due_date.end_of_month
                    "#{format_date(beginning_of_month)} - #{format_date(end_of_month)}"
                end
            end
        elsif receipt.program_name == "Summer Camp"
            if receipt.registration_record_id.present?
                summer_camp_record = receipt.registration_record.summer_camp_record
                program = summer_camp_record.registration_record.program
                weeks_attending = []
                summer_camp_record.attributes.each do |attr_name, attr_value|
                    if attr_value.class == TrueClass || attr_value.class == FalseClass
                        if attr_value
                            weeks_attending << "#{attr_name}"
                        end
                    end
                end
                weeks_attending = format_summer_camp_week_names(weeks_attending)
                result = program.summer_camp_weeks.to_a.keep_if do |week|
                    weeks_attending.any? { |attended_week| week.name.downcase.include? attended_week }
                end
                result.sort
            end
        elsif receipt.program_name == "Day Care"
            "#{format_date(receipt.invoice.due_date)} - #{format_date(receipt.invoice.due_date + 4.days)} "
        end
	end

    def pluralize_child_count(receipt)
        programs = ["Single Day", "Vacation Camp", "After School", 'Summer Camp']
        if programs.any? {|program| program == receipt.program_name }
			'Child'.pluralize(receipt.registration_record.child_records.count)
		end
    end
    
    def child_record_present?(receipt)
        programs = ["Single Day", "Vacation Camp", "After School", "Summer Camp", "Day Care"]
        if programs.any? {|program| program == receipt.program_name }
            receipt.registration_record.child_records.present?
        end
    end

    def receipt_child_records(receipt)
        programs = ["Single Day", "Vacation Camp", "After School", "Summer Camp", "Day Care"]
        if programs.any? {|program| program == receipt.program_name }
            receipt.registration_record.child_records
        end
    end

    def label_for_date_of_service(receipt)
        programs = ["Vacation Camp", "Summer Camp", "Day Care"]
        if programs.any? {|program| program == receipt.program_name }
            "Dates of Service: "
        else
            if receipt.program_name == "Single Day"
                "Date of Service: "
            elsif receipt.program_name == 'After School'
                if receipt.payment_for.include? 'Deposit'
                    "Service Start Date: "
                elsif receipt.payment_for.include? 'Invoice'
                    "Dates of Service: "
                end
            end 
        end
    end

    def format_date date
        date.to_formatted_s(:long)
    end

    def format_old_vacation_camp_schedule(vacation_camp_record)
        month = vacation_camp_record.month
        matched_month = ''
        %w[december february april].each do |el|
            unless month.match(el).nil?
                matched_month = el
            end
        end
        matched_month.capitalize
    end

    def format_summer_camp_week_names weeks_attending
        result = weeks_attending.map do |el|
            case el
                when 'week_one'
                    'week 1'
                when 'week_two'
                    'week 2'
                when 'week_three'
                    'week 3'
                when 'week_four'
                    'week 4'
                when 'week_five'
                    'week 5'
                when 'week_six'
                    'week 6'
                when 'week_seven'
                    'week 7'
                when 'week_eight'
                    'week 8'
                when 'week_nine'
                    'week 9'
                when 'week_ten'
                    'week 10'
                else
                    el
            end
        end
    end

    def summer_camp_week_date_of_service invoice_due_date
        "#{format_date(invoice_due_date)} - #{format_date(invoice_due_date + 4.days)}"
    end

end