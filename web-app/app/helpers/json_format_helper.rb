module JsonFormatHelper
  def format_customer_user _
    {
      email: _.email
    }
  end

  def format_customer _
    {
      customer_id: _.id,
      first_name: _.first_name,
      last_name: _.last_name,
      phone_number: _.phone_number,
      emergency_phone_number: _.emergency_phone_number,
      address_line_one: _.address_line_one,
      address_line_two: _.address_line_two,
      city: _.city,
      state: _.state,
      zip_code: _.zip_code,
      how_did_you_hear_about_us: _.how_did_you_hear_about_us,
      card_last_four: _.card_last_four,
      card_brand: _.card_brand,
      autopay_enabled: _.autopay_enabled
    }.merge!(format_customer_user(_.customer_user))
  end

  def format_location _
    {
      name: _.name,
      address: _.address,
      minimum_age: _.get_minimum_age
    }
  end
  
  def format_locations locations
    locations.map { |location|
      format_location(location)
    }
  end

  def format_single_day_dates single_day_dates
    single_day_dates.map { |single_day_date|
      { id: single_day_date.id, name: single_day_date.name, date: single_day_date.date, capacity: single_day_date.capacity }
    }
  end

  def format_summer_camp_weeks summer_camp_weeks
    summer_camp_weeks.map { |summer_camp_week|
      { id: summer_camp_week.id, name: summer_camp_week.name, start_date: summer_camp_week.start_date, capacity: summer_camp_week.capacity }
    }
  end

  def format_pagination collection, path
    {
      data: collection.total_count,
      load_more: collection.next_page.present?,
      url: path
    }
  end
end