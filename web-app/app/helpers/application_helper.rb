module ApplicationHelper
	def how_did_you_hear_about_us_options
		[['Friend', 'friend'], ['Internet Search', 'internetSearch'], ['Facebook', 'facebook'], ['Advertising', 'advertising'], ['Other', 'other']]
	end

	def single_day_locations
		[['Clifton Park', 'cliftonPark'], ['East Greenbush', 'eastGreenbush'], ['Guilderland - Christ Lutheran Church', 'guilderlandChristLutheranChurch'], ['Guilderland - School House Road', 'guilderlandSchoolHouseRoad'], ['Niskayuna', 'niskayuna'], ['Troy', 'troy']]
	end

	def single_day_days
		[['Columbus Day', 'columbusDay'], ['Rosh Hashanah', 'roshHashanah'], ['Yom Kippur', 'yomKippur'], ['Election Day', 'electionDay'], ['Veterans Day', 'veteransDay'], ['Thanksgiving Eve', 'thanksgivingEve'], ['Martin Luther King Day', 'martinLutherKingDay'], ['Conference Day', 'conferenceDay'], ['Good Friday', 'goodFriday']]
	end

	def searchable_single_day_days
		[['Columbus Day', 'columbusDay'], ['Rosh Hashanah', 'roshHashanah'], ['Yom Kippur', 'yomKippur'], ['Election Day', 'electionDay'], ['Veterans Day', 'veteransDay'], ['Thanksgiving Eve', 'thanksgivingEve'], ['Martin Luther King Day', 'martinLutherKingDay'], ['Conference Day', 'conferenceDay'], ['Good Friday', 'goodFriday']]
	end

	def cit_locations
		[['Clifton Park', 'cliftonPark'], ['Duanesburg', 'duanesburg'], ['East Greenbush', 'eastGreenbush'], ['Guilderland - Christ Lutheran Church', 'guilderlandChristLutheranChurch'], ['Guilderland - School House Road', 'guilderlandSchoolHouseRoad'], ['Niskayuna', 'niskayuna'], ['Troy', 'troy'], ['Rotterdam/Schalmont', 'rotterdamSchalmont']]
	end

	def vacation_camp_locations
		[['Clifton Park', 'cliftonPark'], ['East Greenbush', 'eastGreenbush'], ['Guilderland - Christ Lutheran Church', 'guilderlandChristLutheranChurch'], ['Guilderland - School House Road', 'guilderlandSchoolHouseRoad'], ['Niskayuna', 'niskayuna'], ['Troy', 'troy']]
	end

	def vacation_camp_months
		[['April 2018'], ['December 2018']]
	end

	def searchable_vacation_camp_months
		[['December 2018', 'december2018'], ['February 2019', 'february2019'], ['April 2019', 'april2019']]
	end

	def number_to_word(number)
		case number
		when 1
			"one"
		when 2
			"two"
		when 3
			"three"
		when 4
			"four"
		when 5
			"five"
		when 6
			"six"
		when 7
			"seven"
		when 8
			"eight"
		when 9
			"nine"
		when 10
			"ten"
		end
	end

	def months
		[
			'january', 'february', 'march', 'april',
			'may', 'june', 'july', 'august', 'september',
			'october', 'november', 'december'
		]
	end

	def vacation_camp_date(month, day_of_the_week)
		if month == 'december'
			case day_of_the_week
			when 'Wed'
				", 26"
			when 'Thurs'
				", 27"
			when 'Fri'
				", 28"
			end
		elsif month == 'february'
			case day_of_the_week
			when 'Mon'
				", 18"
			when 'Tues'
				", 19"
			when 'Wed'
				", 20"
			when 'Thurs'
				", 21"
			when 'Fri'
				", 22"
			end
		elsif month == 'april'
			case day_of_the_week
			when 'Mon'
				", 22"
			when 'Tues'
				", 23"
			when 'Wed'
				", 24"
			when 'Thurs'
				", 25"
			when 'Fri'
				", 26"
			end
		end
	end

	def parse_error_message(message)
		message
	end

	def current_year
		Date.today.year.to_s
	end

	def current_month
		Time.now.strftime("%B").downcase
	end

	def get_card_icon card_brand
		case card_brand.downcase
		when "visa"
			'/assets/cards/visa.png'
		when "mastercard"
			'/assets/cards/master-card.png'
		when 'discover'
			'/assets/cards/discover.png'
		when 'american express'
			'/assets/cards/american-express.png'
		when 'amazon'
			'/assets/cards/amazon.png'
		when 'cirrus'
			'/assets/cards/cirrus.png'
		when 'diners club'
			'/assets/cards/diners-club.png'
		when 'direct debit'
			'/assets/cards/direct-debit.png'
		when 'ebay'
			'/assets/cards/ebay.png'
		when 'eway'
			'/assets/cards/eway.png'
		when 'jcb'
			'/assets/cards/jcb.png'
		when 'maestro'
			'/assets/cards/maestro.png'
		when 'paypal'
			'/assets/cards/paypal.png'
		when 'sage'
			'/assets/cards/sage.png'
		when 'shopify'
			'/assets/cards/shopify.png'
		when 'skrill'
			'/assets/cards/skrill.png'
		when 'solo'
			'/assets/cards/solo.png'
		when 'western union'
			'/assets/cards/western-union.png'
		else
			'/assets/cards/w.png'
		end
	end

    def format_promo_name promo_name
        promo_name.gsub('_', ' ').titleize
    end

    def summer_camp_registration_type summer_camp_rec
        if summer_camp_rec.promo_name.present?
            summer_camp_rec.promo_name.gsub("_", ' ') + " promo package"
        else
            "Regular registration"
        end
    end

    def summer_camp_weeks_enrolled summer_camp_rec
        result = ""
        summer_camp_rec.attributes.each do |attr_name, attr_value|
            if attr_value.class == TrueClass || attr_value.class == FalseClass
                if attr_value == true
                    result << "<li>#{parse_summer_camp_week_name(attr_name)}</li>"
                end
            end
        end
        result.html_safe
    end

    def before_and_after_school_schedule before_and_after_school_rec
       result = []
       before_and_after_school_rec.attributes.each do |attr_name, attr_value|
            if attr_value.class == TrueClass || attr_value.class == FalseClass
                if attr_value == true
                    result << attr_name
                end
            end
       end
       result
    end

    def create_vacation_camp_schedule_button registration_rec, sched
        vacation_camp_enrollments = registration_rec.vacation_camp_records.pluck(:vacation_camp_schedule_id)
        enrolled_to_camp_schedule = false
        if vacation_camp_enrollments.any? {|enrollment_id| enrollment_id == sched.id}
            "<button class='vacation-camp-toggle-enrollment' data-regis-record-id=#{registration_rec.id} " \
                "data-schedule-id=#{sched.id} type='button' id='camp-sched-#{sched.id}'>Unenroll</button>".html_safe
        else
            "<button class='vacation-camp-toggle-enrollment' data-regis-record-id=#{registration_rec.id} " \
                "data-schedule-id=#{sched.id} type='button' id='camp-sched-#{sched.id}'>Enroll</button>".html_safe
        end
    end

    private

    def parse_summer_camp_week_name camp_week
        case camp_week
            when "week_one"
                "Week 1"
            when "week_two"
                "Week 2"
            when "week_three"
                "Week 3"
            when "week_four"
                "Week 4"
            when "week_five"
                "Week 5"
            when "week_six"
                "Week 6"
            when "week_seven"
                "Week 7"
            when "week_eight"
                "Week 8"
            when "week_nine"
                "Week 9"
            when "week_ten"
                "Week 10"
        end
    end

end
