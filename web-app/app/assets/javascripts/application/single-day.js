function singleDay() {
    bindEventsToPromoOptions()
    $("input[name='registration_record[single_day_record_attributes][registration_type]']").change(function() {
        switch($(this).val()) {
            case 'standard_single_day_registration':
                $('div.single-day-date-selection').children().show();
                $('div.year-round-single-day-promo-options').removeClass('show-package-options');
                $('#no_annual_single_day_package_selected').prop('checked', true);
                updatePaymentAmount('standard_single_day_registration');
                $('.year-round-tool-tip').removeClass('show-year-round-tool-tip');
                break;
            case 'promo_registration':
                $('div.single-day-date-selection').children().hide();
                $('div.year-round-single-day-promo-options').addClass('show-package-options');
                $('#one_child_annual_single_day').prop('checked', true);
                updatePaymentAmount('promo_registration');
                $('.year-round-tool-tip').addClass('show-year-round-tool-tip');
                break;
        }
    })
}

function bindEventsToPromoOptions() {
    $("input[name='registration_record[annual_single_day_promo_record_attributes][promo_name]']").change(function() {
        switch ($(this).val()) {
            case 'one_child_annual_single_day':
                $('p.single-day-payment-amount > span').text('$400');
            break;
            case 'two_children_annual_single_day':
                $('p.single-day-payment-amount > span').text('$700');
            break;
            case 'three_children_annual_single_day':
                $('p.single-day-payment-amount > span').text('$900');
            break
        }
    })
}

function updatePaymentAmount(registrationType) {
    switch (registrationType) {
        case 'standard_single_day_registration':
            const childrenAdded = $('#child-records .nested-fields').length;
            if (childrenAdded === 1) {
                $('p.single-day-payment-amount > span').text('$45');
            } else if (childrenAdded === 2) {
                $('p.single-day-payment-amount > span').text('$75');
            } else if (childrenAdded === 3) {
                $('p.single-day-payment-amount > span').text('$95');
            } else {
                $('#js-payment-amount').text("$45");
            }
        break;
        case 'promo_registration':
            $('p.single-day-payment-amount > span').text('$400');
        break;
    }
}