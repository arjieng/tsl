function address(location) {
	if (location) {
		switch (location) {
			case "cliftonPark":
				return '<p>4 Northcrest Drive Clifton Park, NY 12065</p>';
			case "eastGreenbush":
				return '<p>1 Gilligan Road East Greenbush, NY 12061</p>';
			case "duanesburg":
				return '<p>165 Chadwick Road Delanson, NY 12053</p>';
			case "guilderlandChristLutheranChurch":
				return '<p>1500 Western Avenue Albany, NY 12203</p>';
			case "guilderlandSchoolHouseRoad":
				return '<p>183 Schoolhouse Road Albany, NY 12203</p>';
			case "niskayuna":
				return '<p>3041 Troy Schenectady Road Niskayuna, NY, 12309</p>';
			case "troy":
				return '<p>55 North Lake Avenue Troy, NY 12180</p>';
			case "rotterdamSchalmont":
				return '<p>59 Giffords Church Road Schenectady, NY 12306</p>';
			default:
				return '';
		}
	}

	return '';
}
