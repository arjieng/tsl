function purchases() {
    if (!$('div.shopping-page').length) return false;

    $('img.product-image').click(function() {
        var product_id = $(this).closest('div').siblings('input.product-id').val()
        window.location = `/add-to-cart/${product_id}`
    })

    $('button.deduct-quantity').click(function() {
        let quantity = $('span.order-quantity-text');
        if (!(+quantity.text() - 1) <= 0 ) {
            quantity.text(+quantity.text() - 1);
            $('input#order_quantity').val(quantity.text())
        }
    })

    $('button.add-quantity').click(function() {
        let quantity = $('span.order-quantity-text');
        quantity.text(+quantity.text() + 1)
        $('input#order_quantity').val(quantity.text())
    })

    $('input.submit-purchase-button').closest('form').submit(function() {
        $('input.submit-purchase-button').attr('disabled', true);
    })
}