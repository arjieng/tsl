function afterSchool() {
    $('#before-after-school-start-date').datepicker();

    $("input[name='registration_record[before_and_afterschool_record_attributes][registration_type]']").change(function() {
        switch($(this).val()) {
            case 'standard_after_school_registration':
                $('#before-and-after-school-fields').show();
                hideAnnualSingleDayPromoOptions();
                changeTotalPaymentValues('standard_after_school_registration');
                break;
            case 'regular_with_promo_after_school_registration':
                $('.annual-single-day-promo-options').addClass('show-package-options');
                $('#before-and-after-school-fields').show();
                $('#one_child_annual_single_day').prop('checked', true);
                changeTotalPaymentValues('regular_with_promo_after_school_registration');
                break;
            case 'promo_only_registration':
                $('.annual-single-day-promo-options').addClass('show-package-options');
                $('#before-and-after-school-fields').hide();
                $('#one_child_annual_single_day').prop('checked', true);
                changeTotalPaymentValues('promo_only_registration');
                break;
        }
    })
    updateTotalPaymentSection();
    bindEventListnersToRegistrationTypeOptions();
    bindEventListnersToYearCycleOptions();
}

function bindEventListnersToRegistrationTypeOptions() {
    $('.annual-single-day-promo-option').change(function() {
        switch($("input[name='registration_record[annual_single_day_promo_record_attributes][promo_name]']:checked").val()) {
            case 'one_child_annual_single_day':
                $('#annual-package-value').html('$400');
                $('#deposit-fee-value').html() == '$0' ? $('#total-amount-value').html('$400') : $('#total-amount-value').html('$525');
                break;
            case 'two_children_annual_single_day':
                $('#annual-package-value').html('$700');
                $('#deposit-fee-value').html() == '$0' ? $('#total-amount-value').html('$700') : $('#total-amount-value').html('$825');
                break;
            case 'three_children_annual_single_day':
                $('#annual-package-value').html('$900');
                $('#deposit-fee-value').html() == '$0' ? $('#total-amount-value').html('$900') : $('#total-amount-value').html('$1025');
                break;
        }
    })
}

function updateTotalPaymentSection() {
    switch($("input[name='registration_record[annual_single_day_promo_record_attributes][promo_name]']:checked").val()) {
        case 'one_child_annual_single_day':
            $('#annual-package-value').html('$400');
            $('#deposit-fee-value').html() == '$0' ? $('#total-amount-value').html('$400') : $('#total-amount-value').html('$525');
            break;
        case 'two_children_annual_single_day':
            $('#annual-package-value').html('$700');
            $('#deposit-fee-value').html() == '$0' ? $('#total-amount-value').html('$700') : $('#total-amount-value').html('$825');
            break;
        case 'three_children_annual_single_day':
            $('#annual-package-value').html('$900');
            $('#deposit-fee-value').html() == '$0' ? $('#total-amount-value').html('$900') : $('#total-amount-value').html('$1025');
            break;
    }
}

function changeTotalPaymentValues(selectedOption) {
    if (selectedOption == 'standard_after_school_registration') {
        if ($('#registration_record_no_deposit_required').length) {
            $('#deposit-fee-value').html('$0');
            $('#registration-fee-value').html('$0');
            $('#annual-package-value').html('$0');
            $('#total-amount-value').html('$0');
            toggleNoDepositAfterSchoolPaymentSection('not_chargeable');
        } else {
            $('#deposit-fee-value').html('$100');
            $('#registration-fee-value').html('$25');
            $('#annual-package-value').html('$0');
            $('#total-amount-value').html('$125');
            toggleNoDepositAfterSchoolPaymentSection('chargeable');
        }
    } else if (selectedOption == 'regular_with_promo_after_school_registration') {
        toggleNoDepositAfterSchoolPaymentSection('chargeable');
        if ($('#registration_record_no_deposit_required').length) {
            $('#deposit-fee-value').html('$0');
            $('#registration-fee-value').html('$0');
            $('#annual-package-value').html('$400');
            $('#total-amount-value').html('$400');
        } else {
            $('#deposit-fee-value').html('$100');
            $('#registration-fee-value').html('$25');
            $('#annual-package-value').html('$400');
            $('#total-amount-value').html('$525');
        }
    } else if (selectedOption == 'promo_only_registration') {
        toggleNoDepositAfterSchoolPaymentSection('chargeable');
        $('#deposit-fee-value').html('$0');
        $('#registration-fee-value').html('$0');
        $('#annual-package-value').html('$400');
        $('#total-amount-value').html('$400');
    }
}

function hideAnnualSingleDayPromoOptions() {
    $('.annual-single-day-promo-options').removeClass('show-package-options');
    $('#no_annual_single_day_package_selected').prop('checked', true);
}

function bindEventListnersToYearCycleOptions() {
    switch ($("input[name='registration_record[before_and_afterschool_record_attributes][year_cycle]']:checked").val()) {
        case '2018-2019':
            $('div.with-promo').hide();
            $('div.promo-only-after-school').hide();
            $('.annual-single-day-promo-options').removeClass('show-package-options');
            
            break;
        case '2019-2020':
            $('div.with-promo').show();
            $('div.promo-only-after-school').show();
            $('.annual-single-day-promo-options').addClass('show-package-options');
            break;
    }
    
    $("input[name='registration_record[before_and_afterschool_record_attributes][year_cycle]']").change(function() {
        switch($(this).val()) {
            case '2018-2019':
                $('div.with-promo').hide();
                $('div.promo-only-after-school').hide();
                $('.annual-single-day-promo-options').removeClass('show-package-options');
                toggleNoDepositAfterSchoolPaymentSection('not_chargeable');
                if ($('#registration_record_no_deposit_required').length) {
                    $('#deposit-fee-value').html('$0');
                    $('#registration-fee-value').html('$0');
                    $('#annual-package-value').html('$0');
                    $('#total-amount-value').html('$0');
                } else {
                    $('#deposit-fee-value').html('$100');
                    $('#registration-fee-value').html('$25');
                    $('#annual-package-value').html('$0');
                    $('#total-amount-value').html('$125');
                }
                break;
            case '2019-2020':
                $('div.with-promo').show();
                $('div.promo-only-after-school').show();
                break;
        }
        $('#before-and-after-school-fields').show();
        $('#standard_after_school_registration').prop('checked', true);
        $('#no_annual_single_day_package_selected').prop('checked', true);
    });
}

function toggleNoDepositAfterSchoolPaymentSection(state) {
    if (state == 'chargeable') {
        $('div.no-charge-submit').css({
            display: 'none'
        })
        $('div.no-charge-submit').find('input').prop('disabled', true);

        $('div.charged-no-deposit-submit').css({
            display: 'block'
        })
        $('div.charged-no-deposit-submit').find('button').prop('disabled', false);
        $('div.charged-no-deposit-submit').find('input').prop('disabled', false);
    } else {
        $('div.no-charge-submit').css({
            display: 'block'
        })
        $('div.no-charge-submit').find('input').prop('disabled', false);

        $('div.charged-no-deposit-submit').css({
            display: 'none'
        })
        $('div.charged-no-deposit-submit').find('button').prop('disabled', true);
        $('div.charged-no-deposit-submit').find('input').prop('disabled', true);
    }
}