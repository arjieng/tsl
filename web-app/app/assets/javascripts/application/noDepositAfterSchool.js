function noDepositAfterSchool() {
    $('#no-deposit-before-afterschool-start-date').datepicker();

    $('div.charged-no-deposit-submit').css({
        display: 'none'
    })
    $('div.charged-no-deposit-submit').find('button').prop('disabled', true);
    $('div.charged-no-deposit-submit').find('input').prop('disabled', true);
    $('form#no-deposit-after-school-form').submit(function(event) {
        event.preventDefault();
        if ($('#total-amount-value').text() == '$0') {
            $('<input />').attr('type', 'hidden')
                .attr('name', 'registration_record[no_deposit_after_school_not_chargeable]')
                .attr('value', 'true')
                .appendTo('form#no-deposit-after-school-form');
        }
        this.submit();
    })
}