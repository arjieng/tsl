// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require cocoon
//= require turbolinks
//= require_tree .
$.datepicker.setDefaults({
	dateFormat: 'yy-mm-dd'
});
// function myScript(i){console.log("letse");
// 		window.addEventListener('beforeunload', (event) => {
// 		// Cancel the event as stated by the standard.
// 			// event.preventDefault();
// 			// Chrome requires returnValue to be set.
// 			// console.log("pisti yawa");
// 			// event.returnValue = 'Are you sure?';
// 			// var r = confirm("Are you sure you want to reload the page.");
// 			console.log(i);
// 			if (i == "wow"){
// 			console.log("l");

// 			event.returnValue = `Are you sure you want to leave?`;
// 			}
// 			else{
// 			console.log("g");

// 			event.returnValue = null;
			
// 			}// return "Are you sure you want to reload the page.";
// 	});
// }
$(document).on('turbolinks:load', function() {
	// var program = window.location.pathname.replace('/',"");
	// console.log($('#cit_record_location').length);

	// if ($('#cit_record_location').length) {
	// 	setCitRegistrationOnChanges(program);
	// 	getCitRegistrationData(program);
	// 	console.log($('#cit_record_location').length);
	// 	// $('body').addEventListener("beforeunload", myScript);
	// 	// window.onbeforeunload = function () { return 'Are you sure?' };
	// 	myScript("wow");
	// }
	// else {
	// 	console.log("weeee");
	// 	myScript("weeee");
	// 	localStorage.removeItem(program+ '_location');
	// }
	// if ($('#new_registration_record').length){
	// 	getLocalStorage(window.location.pathname.replace('/',""));
	// }
	check_location_input();
	// console.log($('label[for="registration_record_single_day_record_attributes_single_day_date_id"].required:after'))
	displayLocation();
	updateSummerCampWeeks();
	updateSingleDayDates();
	determineCapacity();
	hideDaycareForm();
	initializeDaycareCode();
	hideWeekTen();
	limitChildrenAndCalculatePayment();
	updateVacationCampSchedules();
	toggleContactInfoVisibility();
    attachChangeEventToSummerCampRadioButtons();
    toggleRegistrationType();
    limitWeekSelectionForFiveWeekPromos();
    preventFormMultipleSubmission();
    blinkAndYouMightMissIt();
	afterSchool();
	singleDay();
	noDepositAfterSchool();
	purchases();
	$('#registration_record_location').change(function() {
		// setLocalStorage(window.location.pathname.replace('/',""));
		// console.log(window.location.pathname.replace('/',""));
		check_location_input();
		displayLocation();
		determineMinimumAge();
		updateSummerCampWeeks();
		updateSingleDayDates();
		determineCapacity();
		hideWeekTen();
		hideOrShowBeforeAndAfterschoolOptions();
		updateVacationCampSchedules();
		hora();
	});
	
	// function setCitRegistrationOnChanges(program){
		
	// 	$('#cit_record_location').change(function(){
	// 		// localStorage["cit_registration"].setItem(program + "_location",$(this).val());
	// 		localStorage.setItem(program + "_location",$(this).val());
	// 	});
	// 	$('#cit_record_first_name').change(function(){
	// 		localStorage.setItem(program + "_first_name",$(this).val());
	// 		// localStorage["cit_registration"].setItem(program + "_first_name",$(this).val());

	// 	});
	// 	$('#cit_record_last_name').change(function(){
	// 		localStorage.setItem(program + "_last_name",$(this).val());
	// 	});
	// 	$('#cit_record_dob').change(function(){
	// 		localStorage.setItem(program + "_dob",$(this).val());
	// 	});
	// 	$('#cit_record_phone_number').change(function(){
	// 		localStorage.setItem(program + "_phone_number",$(this).val());
	// 	});
	// 	$('#cit_record_email').change(function(){
	// 		localStorage.setItem(program + "_email",$(this).val());
	// 	});
	// 	$('#cit_record_address_line_one').change(function(){
	// 		localStorage.setItem(program + "_address1",$(this).val());
	// 	});
	// 	$('#cit_record_address_line_two').change(function(){
	// 		localStorage.setItem(program + "_address2",$(this).val());
	// 	});
	// 	$('#cit_record_city').change(function(){
	// 		localStorage.setItem(program + "_city",$(this).val());
	// 	});
	// 	$('#cit_record_state').change(function(){
	// 		localStorage.setItem(program + "_state",$(this).val());
	// 	});
	// 	$('#cit_record_zip_code').change(function(){
	// 		localStorage.setItem(program + "_zipcode",$(this).val());
	// 	});
	// 	$('#cit_record_how_did_you_hear_about_us').change(function(){
	// 		localStorage.setItem(program + "_hear_about",$(this).val());
	// 	});
	// 	$("input[type=radio][name='cit_record[attended_tsl_before]']").change(function(){
	// 	localStorage.setItem(program + '_tsl_before',$("input[name='cit_record[attended_tsl_before]']:checked").val());
	// 	});

	// 	$('#cit_record_attended_location').change(function(){
	// 		localStorage.setItem(program + "_tsl_attended_location",$(this).val());
	// 	});
	// 	$("input[type=radio][name='cit_record[cit_at_tsl_before]']").change(function(){
	// 		localStorage.setItem(program + '_cit_tsl_before',$("input[name='cit_record[cit_at_tsl_before]']:checked").val());
	// 	});
	// 	$('#cit_record_cit_at_tsl_location').change(function(){
	// 		localStorage.setItem(program + "_cit_attended_location",$(this).val());
	// 	});
	// 	$('#cit_record_previous_experience').change(function(){
	// 		localStorage.setItem(program + "_experience",$(this).val());
	// 	});
	// 	$('#cit_record_why_are_you_interested').change(function(){
	// 		localStorage.setItem(program + "_interest",$(this).val());
	// 	});
	// 	$("input[type=radio][name='cit_record[willing_to_follow_directions]']").change(function(){
	// 		localStorage.setItem(program + '_willing_to_follow',$("input[name='cit_record[willing_to_follow_directions]']:checked").val());
	// 	});
	// 	$('#cit_record_action_when_completed_tasks').change(function(){
	// 		localStorage.setItem(program + "_action_when_completed_task",$(this).val());
	// 	});
	// 	$('#cit_record_water_play_action').change(function(){
	// 		localStorage.setItem(program + "_water_play_action",$(this).val());
	// 	});
	// 	$('#cit_record_four_square_action').change(function(){
	// 		localStorage.setItem(program + "_four_square_action",$(this).val());
	// 	});
	// 	$('#cit_record_arguing_action').change(function(){
	// 		localStorage.setItem(program + "_arguing_action",$(this).val());
	// 	});
	// 	$('#cit_record_transition_help').change(function(){
	// 		localStorage.setItem(program + "_transition_help",$(this).val());
	// 	});
	// 	$('#cit_record_uninteresting_activity_action').change(function(){
	// 		localStorage.setItem(program + "_uninteresting_activity",$(this).val());
	// 	});
	// 	$('#cit_record_interests_and_talents').change(function(){
	// 		localStorage.setItem(program + "_talents",$(this).val());
	// 	});
	// 	$('#cit_record_proven_responsibility').change(function(){
	// 		localStorage.setItem(program + "_proven_responsibility",$(this).val());
	// 	});
	// 	$('#cit_record_made_a_difference').change(function(){
	// 		localStorage.setItem(program + "_make_difference",$(this).val());
	// 	});
	// 	$('#cit_record_what_you_will_bring').change(function(){
	// 		localStorage.setItem(program + "_what_you_bring",$(this).val());
	// 	});
	// 	$("input[type=radio][name='cit_record[willing_to_meet_vision]']").change(function(){
	// 		localStorage.setItem(program + '_willing_to_strive',$("input[name='cit_record[willing_to_meet_vision]']:checked").val());
	// 	});
	// }
	// function getCitRegistrationData(program){
	// 	var _location = localStorage.getItem(program + "_location") || "";
	// 	var _first_name = localStorage.getItem(program + "_first_name") || "";
	// 	var _last_name = localStorage.getItem(program + "_last_name") || "";
	// 	var _dob = localStorage.getItem(program + "_dob") || "";
	// 	var _phone_number = localStorage.getItem(program + "_phone_number") || "";
	// 	var _email = localStorage.getItem(program + "_email") || "";
	// 	var _address1 = localStorage.getItem(program + "_address1") || "";
	// 	var _address2 = localStorage.getItem(program + "_address2") || "";
	// 	var _city = localStorage.getItem(program + "_city") || "";
	// 	var _state = localStorage.getItem(program + "_state") || "";
	// 	var _zipcode = localStorage.getItem(program + "_zipcode") || "";
	// 	var _hear_about = localStorage.getItem(program + "_hear_about") || "";
	// 	var _tsl_before = localStorage.getItem(program + "_tsl_before") || "";
	// 	var _tsl_attended_location = localStorage.getItem(program + "_tsl_attended_location") || "";
	// 	var _cit_tsl_before = localStorage.getItem(program + "_cit_tsl_before") || "";
	// 	var _cit_attended_location = localStorage.getItem(program + "_cit_attended_location") || "";
	// 	var _experience = localStorage.getItem(program + "_experience") || "";
	// 	var _interest = localStorage.getItem(program + "_interest") || "";
	// 	var _willing_to_follow = localStorage.getItem(program + "_willing_to_follow") || "";
	// 	var _action_when_completed_task = localStorage.getItem(program + "_action_when_completed_task") || "";
	// 	var _water_play_action = localStorage.getItem(program + "_water_play_action") || "";
	// 	var _four_square_action = localStorage.getItem(program + "_four_square_action") || "";
	// 	var _arguing_action = localStorage.getItem(program + "_arguing_action") || "";
	// 	var _transition_help = localStorage.getItem(program + "_transition_help") || "";
	// 	var _uninteresting_activity = localStorage.getItem(program + "_uninteresting_activity") || "";
	// 	var _talents = localStorage.getItem(program + "_talents") || "";
	// 	var _proven_responsibility = localStorage.getItem(program + "_proven_responsibility") || "";
	// 	var _make_difference = localStorage.getItem(program + "_make_difference") || "";
	// 	var _what_you_bring = localStorage.getItem(program + "_what_you_bring") || "";
	// 	var _willing_to_strive = localStorage.getItem(program + "_willing_to_strive") || "";
	// 	$('#cit_record_location').val(_location);
	// 	$('#cit_record_first_name').val(_first_name);
	// 	$('#cit_record_last_name').val(_last_name);
	// 	$('#cit_record_dob').val(_dob);
	// 	$('#cit_record_phone_number').val(_phone_number);
	// 	$('#cit_record_email').val(_email);
	// 	$('#cit_record_address_line_one').val(_address1);
	// 	$('#cit_record_address_line_two').val(_address2);
	// 	$('#cit_record_city').val(_city);
	// 	$('#cit_record_state').val(_state);
	// 	$('#cit_record_zip_code').val(_zipcode);
	// 	$('#cit_record_how_did_you_hear_about_us').val(_hear_about);
	// 	$('#cit_record_attended_tsl_before_' + _tsl_before).prop("checked",true);
	// 	$('#cit_record_attended_location').val(_tsl_attended_location);
	// 	$('#cit_record_cit_at_tsl_before_' + _cit_tsl_before).prop("checked",true);
	// 	$('#cit_record_cit_at_tsl_location').val(_cit_attended_location);
	// 	$('#cit_record_previous_experience').val(_experience);
	// 	$('#cit_record_why_are_you_interested').val(_interest);
	// 	$('#cit_record_willing_to_follow_directions_' + _willing_to_follow).prop("checked",true);
	// 	$('#cit_record_action_when_completed_tasks').val(_action_when_completed_task);
	// 	$('#cit_record_water_play_action').val(_water_play_action);
	// 	$('#cit_record_four_square_action').val(_four_square_action);
	// 	$('#cit_record_arguing_action').val(_arguing_action);
	// 	$('#cit_record_transition_help').val(_transition_help);
	// 	$('#cit_record_uninteresting_activity_action').val(_uninteresting_activity);
	// 	$('#cit_record_interests_and_talents').val(_talents);
	// 	$('#cit_record_proven_responsibility').val(_proven_responsibility);
	// 	$('#cit_record_made_a_difference').val(_make_difference);
	// 	$('#cit_record_what_you_will_bring').val(_what_you_bring);
	// 	$('#cit_record_willing_to_meet_vision_' + _willing_to_strive).prop("checked",true);
	// }
	// function setLocalStorage(program){
	// 	var location = $('#registration_record_location').val();
	// 	var stor = localStorage.setItem(program + "_location",location);
	// }
	// function getLocalStorage(program){
	// 	var star = localStorage.getItem(program + "_location") || "";
	// 	$('#registration_record_location').val(star);
	// }
	function check_location_input(){
		var location = $('#registration_record_location').val();
		if (location == ""){
			$('label[for="registration_record_single_day_record_attributes_single_day_date_id"]').addClass('required').attr('data-content','Select location first.');
		}else {
			$('label[for="registration_record_single_day_record_attributes_single_day_date_id"]').addClass('required').attr('data-content','');
		}
	}
	function hora() {
		var location = $('#registration_record_location').val();
		var program = $('#registration_record_location').data('program');
		$("input[id='summer_camp_regular_registration']").prop('checked', false);
		$("input[id='summer_camp_promo_registration']").prop('checked',false);
		$('.summer-camp-regular-registration-section').hide();
		var url = "/programs/get_program?location_name=" + location + "&program=" + program;
		// if (program === "After School" || program === "Day Care") {
			$.ajax({
				method: "GET",
				url: url
			}).success(function(result) {
				if (result['is_package_enabled']){
					console.log('----------------------');
					console.log("huhahaha");
					$("input[id='summer_camp_promo_registration']").show();
					$("label[for='summer_camp_promo_registration']").show();
				}
				else {
					// console.log("heheheh");
					// console.log(result['is_package_enabled']);
					$("input[id='summer_camp_promo_registration']").hide();
					$("label[for='summer_camp_promo_registration']").hide();
					$("label[for='summer_camp_regular_registration']").text('Regular Registration');
					$("input[id='summer_camp_regular_registration']").prop('checked', true);
					$('.summer-camp-regular-registration-section').show();
				}
			});
	}
	$('#registration_record_vacation_camp_record_attributes_month').change(function() {
		hideMonday();
	});

	$('.datepicker').datepicker();
	$('.date-format').mask('9999-99-99', {placeholder:"yyyy-mm-dd"});

	$('#child-records').on('cocoon:after-insert', function(e, insertedItem) {
		$('.datepicker').datepicker();
		$('.date-format').mask('9999-99-99', {placeholder:"yyyy-mm-dd"});
	});

	$('#js-submit-card-on-file').click(function(button) {
		button.preventDefault();

		$('#js-stripe-code').html("");

		$('#js-submit-card-on-file').parent().submit();
	});

	$('#js-school-submit-card-on-file').click(function(button) {
		button.preventDefault();

		$('#js-stripe-code').html("");

		$('#js-school-submit-card-on-file').parent().parent().submit();
	});

	// show before school option only for Troy, Rotterdam, and Guilderland, and Greenbush
	$("#registration_record_location").change(function() {
		var location = $('#registration_record_location').val();
		var url = "/locations/before_and_after_school_availability?location_name=" + location;
		$.ajax({
			method: "GET",
			url: url
		}).done(function(result) {
			if (result['before_school']) {
				$("#js-before-school").show();
			} else {
				$("#js-before-school").hide();

				$('#registration_record_before_and_afterschool_record_attributes_before_school_monday')
				.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_before_school_tuesday')
					.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_before_school_wednesday')
					.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_before_school_thursday')
					.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_before_school_friday')
					.attr('checked', false);
			}

			if (result['after_school']) {
				$("#js-after-school").show();
			} else {
				$("#js-after-school").hide();

				$('#registration_record_before_and_afterschool_record_attributes_after_school_monday')
				.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_after_school_tuesday')
					.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_after_school_wednesday')
					.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_after_school_thursday')
					.attr('checked', false);
				$('#registration_record_before_and_afterschool_record_attributes_after_school_friday')
					.attr('checked', false);
			}

            if(location == "Counselors in Training") {
                $("input[id='summer_camp_promo_registration']").hide();
                $("label[for='summer_camp_promo_registration']").hide();
                $('.summer-camp-promo-section').hide();

                $("input[id='summer_camp_regular_registration']").prop('checked', true);
                $('.summer-camp-regular-registration-section').show();

                $("label[for='summer_camp_regular_registration']").text('CIT Registration');
				$("h4[id='regular-regisration-header']").hide();
			}
            // } else {
            //     $("input[id='summer_camp_promo_registration']").show();
            //     $("label[for='summer_camp_promo_registration']").show();

            //     $("label[for='summer_camp_regular_registration']").text('Regular Registration');
            //     $("h4[id='regular-regisration-header']").show();
            // }
		})
	});

	$('#bars-icon').click(function() {
		$('#menu-options').toggleClass('visible');
	})

	$('#scroll-to-top').click(function() {
		$('body, html').animate({scrollTop: 0}, 700);
	})

	// hide elements when document is clicked
	$(document).click(function(event) {
		target = $(event.target)
		if (!target.closest('#bars-icon').length) {
			$('#menu-options').removeClass('visible');
		}

		if (!target.closest('#nav-links-mobile').length) {
			$('.nav-links-mobile-menu').removeClass('visible');
			$('#mobile-billing-menu').removeClass('visible');
			$('#mobile-account-menu').removeClass('visible');
		}

		if (target.is('a')) {
			$('.account-options').removeClass('visible');
			$('.account-arrow').removeClass('visible');

			$('.billing-options').removeClass('visible');
			$('.billing-arrow').removeClass('visible');

			$('.nav-links-mobile-menu').removeClass('.visible');
			$('#mobile-account-menu').removeClass('visible');
			$('#mobile-billing-menu').removeClass('visible');
		}
	})

	$('.bars-icon').click(function() {
		$('#menu-options').toggleClass('visible');
	})
	// 
	$('.billing-menu').hover(function() {
		
		$('.billing-options').addClass('visible');
		$('.billing-arrow').addClass('visible');

		$('.account-options').removeClass('visible');
		$('.account-arrow').removeClass('visible');
	}, function() {
		
		$('.billing-options').removeClass('visible');
		$('.billing-arrow').removeClass('visible');
	})

	$('.account-menu').hover(function() {
		$('.account-options').addClass('visible');
		$('.account-arrow').addClass('visible');

		$('.billing-options').removeClass('visible');
		$('.billing-arrow').removeClass('visible');
	}, function() {
		$('.account-options').removeClass('visible');
		$('.account-arrow').removeClass('visible');
	})
	//

	$('.mobile-billing-option').click(function() {
		$('#mobile-billing-menu').addClass('visible');
		$('#mobile-account-menu').removeClass('visible');
	})

	$('#mobile-account-option').click(function() {
		$('#mobile-billing-menu').removeClass('visible');
		$('#mobile-account-menu').addClass('visible');
	})

	$('#mobile-nav').click(function() {
		$('.nav-links-mobile-menu').toggleClass('visible');
		$('#mobile-billing-menu').removeClass('visible');
		$('#mobile-account-menu').removeClass('visible');
	})

	$('#enable_autopay').change(function() {
		var is_autopay_enabled = $(this).prop('checked');
		var url = "update_auto_pay"
		var auto_pay_user_id = $('#update_auto_pay_user_id').val();
		$.ajax({
			method: "POST",
			url: url,
			data: {
				customer_user: {
						autopay_enabled: is_autopay_enabled,
						auto_pay_user_id: auto_pay_user_id
				}
			}
		})
		.done(function(result) {
				$('#enable_autopay_label').text(result['label_message'])
		});
	});

	// limit adding of additional email accounts to three
	$('#contact-informations').on('cocoon:after-insert', function() {
		toggleContactInfoVisibility();
	})

	$('#contact-informations').on('cocoon:after-remove', function() {
		toggleContactInfoVisibility();
	})

	$('#unlink_button').hover(function() {
		$('.unlink-tool-tip').toggleClass('visible');
	})

	$('#edit_button').hover(function() {
		$('.edit-tool-tip').toggleClass('visible');
	})
});

$('#registration_record_before_and_afterschool_record_attributes_before_school_monday')
	.attr('checked', false);
$('#registration_record_before_and_afterschool_record_attributes_before_school_tuesday')
	.attr('checked', false);
$('#registration_record_before_and_afterschool_record_attributes_before_school_wednesday')
	.attr('checked', false);
$('#registration_record_before_and_afterschool_record_attributes_before_school_thursday')
	.attr('checked', false);
$('#registration_record_before_and_afterschool_record_attributes_before_school_friday')
	.attr('checked', false);
$("#js-before-school").hide();

function toggleRegistrationType() {
    $('.summer-camp-regular-registration-section').hide();
    $('.summer-camp-promo-section').hide();

    $("input[name='summer_camp_registration_option']").change(function() {
        selected_option = $(this).val();
        switch(selected_option) {
            case 'summer_camp_promo_registration':
                $('.summer-camp-promo-section').show();
                $('.summer-camp-regular-registration-section').hide();
                $("input[name='summer_camp_promo_registration_chosen']").prop('value', 'true');
                $("input[name='summer_camp_regular_registration_chosen']").prop('value', 'false');

				$('.regular_registration_checkbox').prop('checked', false);
				$('p.summer-camp-registration-fee').hide();
                break;
            case 'summer_camp_regular_registration':
                $('.summer-camp-regular-registration-section').show();
                $('.summer-camp-promo-section').hide();
                $("input[name='summer_camp_regular_registration_chosen']").prop('value', 'true');
				$("input[name='summer_camp_promo_registration_chosen']").prop('value', 'false');
				$('p.summer-camp-registration-fee').show();
                break;
        }
    })
}

function attachChangeEventToSummerCampRadioButtons() {
    $('.promo-checkboxes').hide();
    $("input[name='promo_name']").change(function() {
        $('.promo_week_checkbox:not(:checked)').prop('disabled', false);
        radio_button_name = $(this).val();
        $('.promo_week_checkbox').prop('checked', false)
        switch(radio_button_name) {
            case 'one_child_nine_weeks':
            case 'two_children_nine_weeks':
            case 'three_children_nine_weeks':
                $(".promo_week_checkbox").prop('checked', false);
                $('.promo-checkboxes').hide();
                break
            case 'one_child_five_weeks':
                $(".one_child_five_weeks_promo_checkboxes").show();
                $(".promo_week_checkbox").show();
                $(".two_children_five_weeks_promo_checkboxes").hide();
                $(".three_children_five_weeks_promo_checkboxes").hide();
                break;
            case 'two_children_five_weeks':
                $(".two_children_five_weeks_promo_checkboxes").show();
                $(".promo_week_checkbox").show();
                $(".one_child_five_weeks_promo_checkboxes").hide();
                $(".three_children_five_weeks_promo_checkboxes").hide();
                break;
            case 'three_children_five_weeks':
                $(".three_children_five_weeks_promo_checkboxes").show();
                $(".promo_week_checkbox").show();
                $(".one_child_five_weeks_promo_checkboxes").hide();
                $(".two_children_five_weeks_promo_checkboxes").hide();
                break;
        }
    })
}

function limitWeekSelectionForFiveWeekPromos() {
    const checkboxes = $('.promo_week_checkbox');
    checkboxes.change(function() {
        if($('.promo_week_checkbox:checked').length >= 5) {
            $('.promo_week_checkbox:not(:checked)').prop('disabled', true)
        } else {
            $('.promo_week_checkbox:not(:checked)').prop('disabled', false)
        }
    })
}

function toggleContactInfoVisibility() {
	if ($('.contact-informations-section:visible').length == 3) {
		$('.add_fields').hide();
	} else {
		$('.add_fields').show();
	}
}

function displayLocation() {
	$(".dates-here").html("");
	var location = $('#registration_record_location').val();
	var url = "/locations/get_address?location_name=" + location;

	$.ajax({
	  method: "GET",
	  url: url
	}).done(function(result) {
		$('#js-location').html("<p>" + result.address + "</p>");
	});
}

function determineMinimumAge() {
	var location = $('#registration_record_location').val();
	var url = "/locations/get_minimum_age?location_name=" + location + "&.js";

	$.ajax({
		method: "GET",
		url: url
	}).done(function(result) {
		// using unobtrusive javscript in "locations/get_minimum_age.js" to rerender minumum age
	});
}

function updateSummerCampWeeks() {
	var location = $('#registration_record_location').val();
	var program = $('#registration_record_location').data('program');
	var url = "/programs/get_summer_camp_weeks?location_name=" + location + "&program=" + program + ".js";

	if (program === "Summer Camp" && location !== "") {
		$.ajax({
			method: "GET",
			url: url,
		}).done(function(result) {
			// using unobtrusive javscript in "programs/get_summer_camp_weeks.js" to rerender dates field
		});
	}
}

function updateVacationCampSchedules() {
	var location = $('#registration_record_location').val();
	var program = $('#registration_record_location').data('program');
	var url = "/programs/get_vacation_camp_schedules?location_name=" + location + "&program=" + program;
	$('#vacation_camp_schedules').html('');
	if(program === "Vacation Camp" && location !== "") {
		$.ajax({
			method: "GET",
			url: url,
		}).done(function() {
			// mutates the dom by appending vacation camp schedule check boxes
			// the checkboxes have an 'option' class
			const checkboxes = $('.option');
			checkboxes.each(function() {
				$(this).change(function() {
					calculatePayment()
				})
			})
		}) 
	}
}	

function updateSingleDayDates() {
	var location = $('#registration_record_location').val();
	var program = $('#registration_record_location').data('program');
	var url = "/programs/get_single_day_dates?location_name=" + location + "&program=" + program + ".js";
	if (program === "Single Day" && location !== "") {
		$.ajax({
			method: "GET",
			url: url,
		}).done(function(result) {
			// using unobtrusive javscript in "programs/get_single_day_dates.js" to rerender dates field
		});
	}
}

function determineCapacity() {
	var location = $('#registration_record_location').val();
	var program = $('#registration_record_location').data('program');
	var url = "/programs/get_capacity?location_name=" + location + "&program=" + program;
	if (program === "After School" || program === "Day Care") {
		$.ajax({
			method: "GET",
			url: url
		}).done(function(result) {
			if (result.capacity <= result.enrolled) {
				$('#js-capacity-wrapper').hide();
				$('#js-capacity-message').html("<p>" + "This location is at capacity for the moment. Please select another location." + "</p>");
			} else {
				$('#js-capacity-wrapper').show();
				$('#js-capacity-message').html("");
			}
		});
	}
}

function hideDaycareForm() {
	$('#js-daycare-wrapper').hide();
}

function initializeDaycareCode() {
	$('#js-daycare-code-submit').click(function() {
		if ($('#js-daycare-code').val() === "adventure") {
			showDaycareForm();
		}
	});
}

function showDaycareForm() {
	$('#js-daycare-code-entry').hide();
	$('#js-daycare-wrapper').show();
}

function hideWeekTen() {
	var location = $('#registration_record_location').val();

	if (location === "duanesburg") {
		$("label[for='registration_record_summer_camp_record_attributes_week_ten']").hide();
		$('#registration_record_summer_camp_record_attributes_week_ten').hide();
	} else {
		$("label[for='registration_record_summer_camp_record_attributes_week_ten']").show();
		$('#registration_record_summer_camp_record_attributes_week_ten').show();
	}
}

function hideMonday() {
	var week = $('#registration_record_vacation_camp_record_attributes_month').val();

	if (week === "december2017") {
		$("label[for='registration_record_vacation_camp_record_attributes_monday']").hide();
		$('#registration_record_vacation_camp_record_attributes_monday').hide();
		$('#registration_record_vacation_camp_record_attributes_monday').attr('checked', false);
	} else {
		$("label[for='registration_record_vacation_camp_record_attributes_monday']").show();
		$('#registration_record_vacation_camp_record_attributes_monday').show();
	}
}

function limitChildrenAndCalculatePayment() {
	// limits the number of child-records
	$('#child-records').on('cocoon:after-insert', function() {
	check_to_hide_or_show_add_link();
		calculatePayment();
	});

	$('#child-records').on('cocoon:after-remove', function() {
	check_to_hide_or_show_add_link();
		calculatePayment();
	});
	check_to_hide_or_show_add_link();
	calculatePayment();
}

function hideOrShowBeforeAndAfterschoolOptions() {
	if ($('#registration_record_location').val().indexOf("Lansingburgh") != -1) {
		$('#registration_record_before_and_afterschool_record_attributes_before_school_monday').attr('checked', false);
		$('#registration_record_before_and_afterschool_record_attributes_before_school_tuesday').attr('checked', false);
		$('#registration_record_before_and_afterschool_record_attributes_before_school_wednesday').attr('checked', false);
		$('#registration_record_before_and_afterschool_record_attributes_before_school_thursday').attr('checked', false);
		$('#registration_record_before_and_afterschool_record_attributes_before_school_friday').attr('checked', false);
		$('#js-before-school').hide();
	} else {
		$('#js-before-school').show();
	}
}

function check_to_hide_or_show_add_link() {
	if ($('#child-records .nested-fields').length == 3) {
		$('.js-child-records-add').hide();
	} else {
		$('.js-child-records-add').show();
	}
}

function calculatePayment() {
	var childrenAdded = $('#child-records .nested-fields').length;

	if ($('#registration_record_referrer').val() === "single_day") {
		if ($("input[name='registration_record[single_day_record_attributes][registration_type]']:checked").val() == 'standard_single_day_registration') {
			var date = new Date();
			var sept2 = Date.parse("September 2, 2019 00:00:00");
			var current_date = Date.parse(date.getFullYear()+"-"+ (date.getMonth()+1) +"-"+date.getDate()+" 00:00:00");
			if(sept2 <= current_date){
				if (childrenAdded === 1) {
					$('p.single-day-payment-amount > span').text("$50");
				} else if (childrenAdded === 2) {
					$('p.single-day-payment-amount > span').text("$85");
				} else if (childrenAdded === 3) {
					$('p.single-day-payment-amount > span').text("$100");	
				} else {
					$('p.single-day-payment-amount > span').text("$50");
				}
			}else{
				if (childrenAdded === 1) {
					$('p.single-day-payment-amount > span').text("$45");
				} else if (childrenAdded === 2) {
					$('p.single-day-payment-amount > span').text("$75");
				} else if (childrenAdded === 3) {
					$('p.single-day-payment-amount > span').text("$95");	
				} else {
					$('p.single-day-payment-amount > span').text("$45");
				}
			}
		}
		
	} else if ($('#registration_record_referrer').val() === "vacation_camp") {
		var date = new Date();
		var sept15 = Date.parse("September 15, 2019 00:00:00");
		var current_date = Date.parse(date.getFullYear()+"-"+ (date.getMonth()+1) +"-"+date.getDate()+" 00:00:00");

		const checked_months = $('.option:checked');
		if(sept15 <= current_date){
			var found1 = checked_months.toArray().find(x => x.id === "registration_record_attendance_december");

			if (checked_months.length == 1) {
				if(childrenAdded == 1) {
					if(found1 != null){
						$('#js-payment-amount').text("$300");
					}else{
						$('#js-payment-amount').text("$190");
					}
				} else if (childrenAdded == 2) {
					if(found1 != null){
						$('#js-payment-amount').text("$500");
					}else{
						$('#js-payment-amount').text("$315");
					}
				} else if (childrenAdded == 3) {
					if(found1 != null){
						$('#js-payment-amount').text("$650");
					}else{
						$('#js-payment-amount').text("$400");
					}
				} else {
					if(found1 != null){
						$('#js-payment-amount').text("$300");
					}else{
						$('#js-payment-amount').text("$190");
					}
				}
			} else if (checked_months.length == 2) {
				if(childrenAdded == 1) {
					if(found1 != null){
						$('#js-payment-amount').text("$570");
					}else{
						$('#js-payment-amount').text("$380");
					}
					
				} else if (childrenAdded == 2) {
					if(found1 != null){
						$('#js-payment-amount').text("$945");
					}else{
						$('#js-payment-amount').text("$630");
					}
					
				} else if (childrenAdded == 3) {
					if(found1 != null){
						$('#js-payment-amount').text("$1200");
					}else{
						$('#js-payment-amount').text("$800");
					}
					
				} else {
					if(found1 != null){
						$('#js-payment-amount').text("$570");
					}else{
						$('#js-payment-amount').text("$380");
					}
					
				}
			} else if (checked_months.length == 3) {
				if(childrenAdded == 1) {
					if(found1 != null){
						$('#js-payment-amount').text("$485");
					}else{
						$('#js-payment-amount').text("$570");
					}

				} else if (childrenAdded == 2) {
					if(found1 != null){
						$('#js-payment-amount').text("$800");
					}else{
						$('#js-payment-amount').text("$945");
					}

				} else if (childrenAdded == 3) {
					if(found1 != null){
						$('#js-payment-amount').text("$999");
					}else{
						$('#js-payment-amount').text("$1200");
					}

				} else {
					if(found1 != null){
						$('#js-payment-amount').text("$485");
					}else{
						$('#js-payment-amount').text("$570");
					}
				}
			} else {
				if(found1 != null){
					$('#js-payment-amount').text("$300");
				}else{
					$('#js-payment-amount').text("$190");
				}
			}			

		}else{
			if (checked_months.length == 1) {
				if(childrenAdded == 1) {
					$('#js-payment-amount').text("$150");
				} else if (childrenAdded == 2) {
					$('#js-payment-amount').text("$200");
				} else if (childrenAdded == 3) {
					$('#js-payment-amount').text("$275");
				} else {
					$('#js-payment-amount').text("$150");
				}
			} else if (checked_months.length == 2) {
				if (childrenAdded == 1) {
					$('#js-payment-amount').text("$300");
				} else if (childrenAdded == 2) {
					$('#js-payment-amount').text("$400");
				} else if (childrenAdded == 3) {
					$('#js-payment-amount').text("$550");
				} else {
					$('#js-payment-amount').text("$300");
				}
			} else if (checked_months.length == 3) {
				if (childrenAdded == 1) {
					$('#js-payment-amount').text("$400");
				} else if (childrenAdded == 2) {
					$('#js-payment-amount').text("$550");
				} else if (childrenAdded == 3) {
					$('#js-payment-amount').text("$775");
				} else {
					$('#js-payment-amount').text("$400");
				}
			} else {
				$('#js-payment-amount').text("$150");
			}
		}
	}	
}

function preventFormMultipleSubmission() {
    $('.invoice-payment-submit-input').click(function() {
        showTransactionNotifier();
        const hidden = $("<input type='hidden' />").attr("name", 'commit').attr("value", $(this).text());
        $('#invoice-payment-form').append(hidden);
        $('#invoice-payment-form').submit();
        $('.invoice-payment-submit-input').prop('disabled', true);
    })
}

function blinkAndYouMightMissIt() {
    $('.transaction-processing-notifier').delay(200).fadeOut('slow').delay(50).fadeIn('slow', blinkAndYouMightMissIt);
};

function showTransactionNotifier() {
    $('.transaction-notifier-container').addClass('visible');
}