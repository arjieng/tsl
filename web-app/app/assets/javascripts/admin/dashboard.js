function adminDashboard() {
    monthlySummary();
    yearlyOverview();
    sideBar();
    searchCustomer();
    programsSection();
    billingSection();
    payrollSection();
}