// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery-ui
//= require cocoon
//= require Chart.min
//= require_tree .

$(document).ready(function() {
	adminDashboard();
	payroll();
	navigationEventHandler();
	$('.js-multi-button__arrow').click(function() {
		$(this).parent().toggleClass('is-open');
	});

	$(document).on('click', function(event) {
		if (!$(event.target).closest('.js-multi-button').length) {
			$('.js-multi-button').removeClass('is-open');
		}
	});

	$('.datepicker').datepicker();
	$.datepicker.setDefaults({
		dateFormat: 'yy-mm-dd'
	});

	$('.date_editor').on('click', function() {
		var camp_schedule_information = JSON.parse($(this).prev().val())
		const dates = preSelectDates(camp_schedule_information)
		
		const dates_selected = $(this).next().multiDatesPicker({
			addDates: dates,
			defaultDate: dates[0],
			dateFormat: 'dd-M-yy',
			changeMonth: false,
			changeYear: false,
			stepMonths: 0,
			onSelect: function() {
				$(this).siblings(':last').val(stringifyDates(filterDates($(this).val())));
				object = jsonifySelectedDates(filterDates($(this).val()));
				$(this).siblings(':first').val(JSON.stringify(object));
			}
		})
		$(this).siblings(':last').val(stringifyDates(filterDates(dates_selected.val())));
	})

	// returns an array of Date objects
	const preSelectDates = function(camp_information) {
		const dates_array = []
		for (key in camp_information) {
			if (camp_information.hasOwnProperty(key)) {
				if (!isNaN(+key)) {
					dates_array.push(
						new Date(camp_information['year'],camp_information['month'] - 1, +key)
					)
				} 
			}
		}
		return dates_array;
	}

	const filterDates = function(picked_dates) {
		split_dates = picked_dates.split(',')
		days_only = split_dates.map(
			function(el) { return +el.split('-')[0] }
		)

		return days_only;
	}
	const stringifyDates = function(filteredDates) {

		return filteredDates.map(function(el) {
			return " " + el
		})
	}

	const jsonifySelectedDates = function(selected_dates) {
		var result = {}
		selected_dates.forEach(function(date) {
			result = Object.assign(dateToObject(date),result);
		})
		return result;
	}

	const dateToObject = function(date) {
		switch(date) {
			case 1:	 return {"day_one": true};
			case 2:	 return {"day_two": true};
			case 3:	 return {"day_three": true};
			case 4:	 return {"day_four": true};
            case 5:  return {"day_five": true};
			case 6:  return {"day_six": true};
			case 7:  return {"day_seven": true};
            case 8:  return {"day_eight": true};
            case 9:	 return {"day_nine": true};
            case 10: return {"day_ten": true};
            case 11: return {"day_eleven": true};
			case 12: return {"day_twelve": true};
            case 13: return {"day_thirteen": true};
			case 14: return {"day_fourteen": true};
            case 15: return {"day_fifteen": true};
            case 16: return {"day_sixteen": true};
            case 17: return {"day_seventeen": true};
			case 18: return {"day_eighteen": true};
            case 19: return {"day_nineteen": true};
            case 20: return {"day_twenty": true};
            case 21: return {"day_twenty_one": true};
            case 22: return {"day_twenty_two": true};
            case 23: return {"day_twenty_three": true};
            case 24: return {"day_twenty_four": true};
            case 25: return {"day_twenty_five": true};
            case 26: return {"day_twenty_six": true};
            case 27: return {"day_twenty_seven": true};
            case 28: return {"day_twenty_eight": true};
            case 29: return {"day_twenty_nine": true};
            case 30: return	{"day_thirty": true};
            case 31: return {"day_thirty_one": true};
		}
	}
	// Vacation Camp Schedule

	var yearField = $(".camp-year");
	var monthField = $(".camp-month");
	var selectedDays = $('#selected_camp_days')
	var selectedDatesContainer = $('#selected_dates_container');
	$('.multi_date_picker').multiDatesPicker({
		dateFormat: 'dd-M-yy',
		onSelect: function() {
			selectedDays.val(stringifyDates(filterDates($(this).val())));
			selectedDatesContainer.html('');
			generateHiddenFieldsForSubmit(jsonifySelectedDates(filterDates($(this).val())));
		},
		onChangeMonthYear: function(year, month) {
			yearField.val(year);
			monthField.val(monthNumberToWord(month));
		}
	})

    enableDateForCocoon();
    
	const monthNumberToWord = function(monthNumber) {
		switch(monthNumber) {
			case 1:  return 'january';
			case 2:  return 'february';
			case 3:  return 'march';
			case 4:  return 'april';
			case 5:  return 'may';
			case 6:  return 'june';
			case 7:  return 'july';
			case 8:  return 'august';
			case 9:  return 'september';
			case 10: return 'october';
			case 11: return 'november';
			case 12: return 'december';
		}
	}

	const generateHiddenFieldsForSubmit = function(jsonified_dates) {
		for (var prop in jsonified_dates) {
			if (jsonified_dates.hasOwnProperty(prop)) {
				selectedDatesContainer.append(
					`<input type='hidden' name='vacation_camp_schedule[${prop}]' value='${jsonified_dates[prop]}'>`
				);
			}
		}
	}

	initializeEnrollmentToggleButtons();
	$('#registration_record_before_and_afterschool_record_attributes_start_date').datepicker();
});


function initializeEnrollmentToggleButtons() {
    $('.vacation-camp-toggle-enrollment').click(function() {
        url = '/admin/update_vacation_camp_enrollment'
        registration_record_id = $(this).attr('data-regis-record-id')
        $.ajax({
            method: "POST",
            url: url,
            data: {
                vacation_camp_schedule_id: $(this).attr('data-schedule-id'),
                id: registration_record_id,
                command: $(this).text()
            }
        }).done(function(data) {
            if(data.message == 'Enrollment successful.') {
                $(`#camp-sched-${data.camp_schedule_id}`).html('Unenroll');
            } else if(data.message == 'Unenrollment successful.') {
                $(`#camp-sched-${data.camp_schedule_id}`).html('Enroll');
            }
            $('.vacation-camp-record-update-alert').html(data.message);
            $('.vacation-camp-record-update-alert').fadeTo('slow', 1, function() {
                $('.vacation-camp-record-update-alert').delay(600).fadeTo('slow', 0);
            });
        })
    })
}

function enableDateForCocoon() {
    $('#invoices-section').on('cocoon:after-insert', function(e, event) {
        $('.datepicker').datepicker();
    })
}
