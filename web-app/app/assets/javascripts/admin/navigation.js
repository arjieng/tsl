function navigationEventHandler() {
    $('button.dashboard-mobile-navigation-button').click(function() {
        navigation_links = $('ul.mobile-navigation-links')
        if (navigation_links.hasClass('hidden')) {
            navigation_links.css({
                height: '400px'
            })
            navigation_links.removeClass('hidden');
        } else {
            navigation_links.css({
                height: '0px'
            })
            navigation_links.addClass('hidden');
        }
    })
}