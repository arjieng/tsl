function billingSection() {

    $('input#receipt-id-text-field').keydown(function(event) {
        if (event.keyCode === 13) {
            $('button.retrieve-receipt-information-button').click();
        }
    })

    $('button.retrieve-receipt-information-button').click(function() {
        url = `/admin/api/receipt-information/${$('input#receipt-id-text-field').val()}`
        if ($('input#receipt-id-text-field').val().length > 0) {
            $('div.search-receipt-section div.loading-ellipsis').css({
                opacity: '1'
            })
            $(this).attr('disabled', true);
            $.ajax({
                method: 'get',
                url: url,
                error: function(response) {
                    showBillingSectionEventNotification(response.responseJSON['message'], response.responseJSON['status'])
                    hideBillingSectionEllipsis();
                }
            }).done(function(response) {
                $('div.receipt-button-update-container').css({display: 'block'});
                $('button.receipt-update-button').attr('disabled', false);
                showBillingSectionEventNotification(response['message'], response['status'])
                hideBillingSectionEllipsis();
                updateReceiptInformationSection(response)
            })
        } else {
            showBillingSectionEventNotification('Please enter receipt id.', 'failure')
        }
    })

    $('button.receipt-update-button').click(function() {
        $(this).attr('disabled', true);
        child_information = $('div.receipt-child-information').map(function() {
            return {
                    id: $(this).find('input:hidden').val(),
                    first_name: $(this).find('div.receipt-child-first-name input').val(),
                    last_name: $(this).find('div.receipt-child-last-name input').val(),
                    date_of_birth: $(this).find('div.receipt-child-date-of-birth input').val()
            };
        }).get();
        data = {
            receipt_id: $('div.receipt-information-section div.receipt-id').text(),
            payment_for: $('div.receipt-information-section div.receipt-payment-for > input').val(),
            child_information: child_information
        }
        url = '/admin/api/update-receipt'
        $.ajax({
            url: url,
            method: 'post',
            data: data,
            dataType: 'json',
            error: function(response) {
                showBillingSectionEventNotification(response.responseJSON['message'], response.responseJSON['status'])
                hideBillingSectionEllipsis();
            }
        }).done(function(response) {
            setTimeout(function() {
                $('button.receipt-update-button').attr('disabled', false);
            }, 3200)
            showBillingSectionEventNotification(response['message'], response['status'])
        })
    })

}

function hideBillingSectionEllipsis() {
    $('div.search-receipt-section div.loading-ellipsis').css({ opacity: '0'})
}

function showBillingSectionEventNotification(message, type) {
    $('div.billing-section-event-notification h4').css({
        opacity: '1',
        backgroundColor: type == 'success' ? '#81C784' : '#C62828'
    })
    $('div.billing-section-event-notification h4').text(message);
    setTimeout(function() {
        $('div.billing-section-event-notification h4').css({ opacity: '0' })
    }, 1800)

    setTimeout(function() {
        $('button.retrieve-receipt-information-button').attr('disabled', false);
    }, 3000)
}

function updateReceiptInformationSection(response) {
    $('div.receipt-child-information').remove();
    $('div.receipt-information-section div.receipt-id').text(response['receipt_information']['id'])
    $('div.receipt-information-section div.receipt-amount').text(response['receipt_information']['amount'])
    $('div.receipt-information-section div.receipt-program').text(response['receipt_information']['program_name'])
    $('div.receipt-information-section div.receipt-payment-for > input').val(response['receipt_information']['payment_for'])

    response['child_information'].forEach(function(element) {
        $('h3.receipt-child-information-header').after(
            `<div class="receipt-child-information">
                    <input type="hidden" value="${element.id}">
                    <div class="row">
                        <div class="key">First Name</div>
                        <div class="value receipt-child-first-name"><input type="text" value=${element.first_name}></div>
                    </div>
                    <div class="row">
                        <div class="key">Last Name</div>
                        <div class="value receipt-child-last-name"><input type="text" value=${element.last_name}></div>
                    </div>
                    <div class="row">
                        <div class="key">Date of Birth</div>
                        <div class="value receipt-child-date-of-birth"><input type="text" value=${element.date_of_birth}></div>
                    </div>
            </div>`
        )
    })

    $('div.receipt-child-date-of-birth input').datepicker();
}