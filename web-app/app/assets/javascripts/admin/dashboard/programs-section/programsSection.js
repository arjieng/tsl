function programsSection() {
    month_now = new Date().getMonth() + 1;
    updateMonthOfPrograms();
    bindEventHandlerToSelectElements();
    updateSingleDayProgramInformation(month_now, 'single_day');
    updateYearRoundSingleDayProgramInformation(month_now, 'year_round_single_day');
    updateAfterSchoolProgramInformation(month_now, 'before_and_after_school');
    updateDayCareProgramInformation(month_now, 'day_care');
    updateVacationCampProgramInformation(month_now, 'vacation_camp');
    updateSummerCampProgramInformation(month_now, 'summer_camp');
    updateProgramsSummary();
}

function updateMonthOfPrograms() {
    month_now = new Date().getMonth() + 1;  
    $('select.program-month-select > option').each(function() {
        if ($(this).val().toString() == month_now.toString()) {
            $(this).prop('selected', true);
        }
    })
}

function bindEventHandlerToSelectElements() {
    $('select.program-month-select').change(function() {
        switch ($(this).prop('id')) {
            case 'programs-month-of-single-day':
                updateSingleDayProgramInformation($(this).val(), 'single_day');
                break;
            case 'programs-month-of-year-round-single-day':
                updateYearRoundSingleDayProgramInformation($(this).val(), 'year_round_single_day');
                break;
            case 'programs-month-of-after-school':
                updateAfterSchoolProgramInformation($(this).val(), 'before_and_after_school');
                break;
            case 'programs-month-of-day-care':
                updateDayCareProgramInformation($(this).val(), 'day_care');
                break;
            case 'programs-month-of-vacation-camp':
                updateVacationCampProgramInformation($(this).val(), 'vacation_camp')
                break;
            case 'programs-month-of-summer-camp':
                updateSummerCampProgramInformation($(this).val(), 'summer_camp');
                break;
        }
    })
}

function updateSingleDayProgramInformation(month, program) {
    year = new Date().getFullYear();
    url = `api/program-breakdown?month=${month}&year=${year}&program=${program}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(data) {
        $('div.single-day-information-container > div.monthly-breakdown div.week-pair').each(function(index) {
            if (data['breakdown'][index] != undefined) {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text(`$${data['breakdown'][index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })
            } else {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text('N/A').fadeIn(400);
                })
            }
        })
        $('div.single-day-information-container > div.program-summary div.total-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
    })
}

function updateAfterSchoolProgramInformation(month, program) {
    year = new Date().getFullYear();
    url = `api/program-breakdown?month=${month}&year=${year}&program=${program}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(data) {
        $('div.after-school-information-container > div.monthly-breakdown div.week-pair').each(function(index) {
            if (data['breakdown'][index] != undefined) {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text(`$${data['breakdown'][index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })
            } else {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text('N/A').fadeIn(400);
                })
            }
        })
        $('div.after-school-information-container > div.program-summary div.total-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
    })
}

function updateDayCareProgramInformation(month, program) {
    year = new Date().getFullYear();
    url = `api/program-breakdown?month=${month}&year=${year}&program=${program}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(data) {
        $('div.day-care-information-container > div.monthly-breakdown div.week-pair').each(function(index) {
            if (data['breakdown'][index] != undefined) {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text(`$${data['breakdown'][index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })
            } else {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text('N/A').fadeIn(400);
                })
            }
        })
        $('div.day-care-information-container > div.program-summary div.total-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
    })
}

function updateVacationCampProgramInformation(month, program) {
    year = new Date().getFullYear();
    url = `api/program-breakdown?month=${month}&year=${year}&program=${program}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(data) {
        $('div.vacation-camp-information-container > div.monthly-breakdown div.week-pair').each(function(index) {
            if (data['breakdown'][index] != undefined) {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text(`$${data['breakdown'][index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })
            } else {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text('N/A').fadeIn(400);
                })
            }
        })
        $('div.vacation-camp-information-container > div.program-summary div.total-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
    })
}

function updateSummerCampProgramInformation(month, program) {
    year = new Date().getFullYear();
    url = `api/program-breakdown?month=${month}&year=${year}&program=${program}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(data) {
        $('div.summer-camp-information-container > div.monthly-breakdown div.week-pair').each(function(index) {
            if (data['breakdown'][index] != undefined) {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text(`$${data['breakdown'][index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })
            } else {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text('N/A').fadeIn(400);
                })
            }
        })
        $('div.summer-camp-information-container > div.program-summary div.total-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
    })
}

function updateYearRoundSingleDayProgramInformation(month, program) {
    year = new Date().getFullYear();
    url = `api/program-breakdown?month=${month}&year=${year}&program=${program}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(data) {
        $('div.year-round-single-day-information-container > div.monthly-breakdown div.week-pair').each(function(index) {
            if (data['breakdown'][index] != undefined) {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text(`$${data['breakdown'][index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })
            } else {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text('N/A').fadeIn(400);
                })
            }
        })
        $('div.year-round-single-day-information-container > div.program-summary div.total-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
    })
}

function updateProgramsSummary() {
    year = new Date().getFullYear();
    url = `api/programs-summary?year=${year}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(data) {
        $('div.single-day-information-container > div.program-summary div.average-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['single_day']['monthly_average'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.single-day-information-container > div.program-summary div.year-to-date-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['single_day']['yearly_total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.after-school-information-container > div.program-summary div.average-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['after_school']['monthly_average'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.after-school-information-container > div.program-summary div.year-to-date-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['after_school']['yearly_total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.day-care-information-container > div.program-summary div.average-weekly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['day_care']['weekly_average'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.day-care-information-container > div.program-summary div.year-to-date-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['day_care']['yearly_total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.vacation-camp-information-container > div.program-summary div.average-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['vacation_camp']['monthly_average'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.vacation-camp-information-container > div.program-summary div.year-to-date-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['vacation_camp']['yearly_total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.summer-camp-information-container > div.program-summary div.year-to-date-deposits-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['summer_camp']['year_to_date_deposits_revenue'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
        
        $('div.summer-camp-information-container > div.program-summary div.year-to-date-invoices-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['summer_camp']['year_to_date_invoices_revenue'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.summer-camp-information-container > div.program-summary div.current-outstanding-balances')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['summer_camp']['outstanding_balance'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.summer-camp-information-container > div.program-summary div.total-year-to-date-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['summer_camp']['total_year_to_date_earnings'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.year-round-single-day-information-container > div.program-summary div.average-monthly-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['year_round_single_day']['monthly_average'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })

        $('div.year-round-single-day-information-container > div.program-summary div.year-to-date-revenue')
            .find('div.value')
            .fadeOut(500, function() {
                $(this).text(`$${data['year_round_single_day']['yearly_total'].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
            })
        
    })
}
