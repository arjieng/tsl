function monthlySummary() {
    updateMonthOfYearlySummary();

    const context = $('#monthly-overview-canvas');
    if (context.length) {
        const data = {
            labels: ['Week 1', 'Week 2', 'Week 3', 'Week 4', 'Week 5'],
            datasets: [
                {
                    backgroundColor: ['#42A5F5', '#7E57C2', '#66BB6A', '#FFA726', '#26C6DA'],
                    data: [0, 0, 0, 0, 0]
                }
            ]
        }
        const barChart = new Chart(
            context, {
                type: 'bar',
                data: data,
                options: {
                    legend: { display: false },
                    title: { display: false },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                    return `$${tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`
                                }
                        }
                    }
                }
            }
        )
        updateMonthlySummary(barChart);
        bindEventToSummaryButton(barChart);
    }
}

function updateMonthlySummary(barChart) {
    url = `api/monthly-summary?month=${$('#month-of-monthly-summary').val()}&year=${$('#year-of-monthly-summary').val()}`
    $.ajax({
        method: "get",
        url: url
    }).done(function(data) {
        $('div.chart-detail-box').each(function(index) {
            if (Object.values(data)[index] != undefined) {
                $(this).find('> div.text-group > div.revenue').text(Object.values(data)[index]['weekly_total'])
                $(this).find('> div.text-group > div.week-range').text(Object.values(data)[index]['week_range'])
            }
        })
        if (Object.values(data).length < 5) {
            $('div.chart-detail-box:eq(4)').hide();
        } else {
            $('div.chart-detail-box:eq(4)').show();
        }
        updateMonthlySummaryChart(barChart, data)
        updateMonthlySummaryRow(data);
        updateMonthHeader();
    })
    
}

function updateMonthlySummaryChart(barChart, data) {
    barChart.data.labels = [];
    barChart.data.datasets[0].data = [];
    barChart.update();
    Object.values(data).forEach((object, index) => {
        barChart.data.labels.push(`Week ${index + 1}`)
        barChart.data.datasets[0].data.push(object['weekly_total_numeric_form']);
        barChart.update();
    })
    barChart.update();
}

function updateMonthOfYearlySummary() {
    month_now = new Date().getMonth() + 1;  
    $('#month-of-monthly-summary > option').each(function() {
        if ($(this).val().toString() == month_now.toString()) {
            $(this).prop('selected', true);
        }
    })
    updateMonthHeader();
}

function updateMonthlySummaryRow(data) {
    single_day_monthly_total = Object.values(data).reduce((acc, curr) => acc + curr['single_day_weekly_revenue'], 0)
    year_round_single_day_monthly_total = Object.values(data).reduce((acc, curr) => acc + curr['year_round_single_day_weekly_revenue'], 0)
    after_school_monthly_total = Object.values(data).reduce((acc, curr) => acc + curr['after_school_weekly_revenue'], 0)
    day_care_school_monthly_total = Object.values(data).reduce((acc, curr) => acc + curr['day_care_weekly_revenue'], 0)
    vacation_camp_monthly_total = Object.values(data).reduce((acc, curr) => acc + curr['vacation_camp_weekly_revenue'], 0)
    summer_camp_monthly_total = Object.values(data).reduce((acc, curr) => acc + curr['summer_camp_weekly_revenue'], 0)
    monthly_total = single_day_monthly_total + year_round_single_day_monthly_total + after_school_monthly_total + day_care_school_monthly_total + vacation_camp_monthly_total + summer_camp_monthly_total;

    $('div.single-day-monthly-total').fadeOut(500, function() {
        $(this).text(`$${single_day_monthly_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
    })
    $('div.year-round-single-day-monthly-total').fadeOut(500, function() {
        $(this).text(`$${year_round_single_day_monthly_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
    })
    $('div.before-and-afterschool-monthly-total').fadeOut(500, function() {
        $(this).text(`$${after_school_monthly_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
    })
    $('div.day-care-monthly-total').fadeOut(500, function() {
        $(this).text(`$${day_care_school_monthly_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
    })
    $('div.vacation-camp-monthly-total').fadeOut(500, function() {
        $(this).text(`$${vacation_camp_monthly_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
    })
    $('div.summer-camp-monthly-total').fadeOut(500, function() {
        $(this).text(`$${summer_camp_monthly_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
    })
    $('div.total-monthly-total').fadeOut(500, function() {
        $(this).text(`$${monthly_total.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
    })
    
    // expiremintal
        $('div.single-day-summary-breakdown').find('> div').each(function(index) {
            if ((Object.values(data)[index]) != undefined) { 
                summary_data = Object.values(data)[index]['single_day_weekly_revenue']
                $(this).find('> span').text(`$${summary_data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`)
            }
            if (Object.values(data).length < 5) {
                $(this).find('> div.week-5').hide();
            } else {
                $(this).find('> div.week-5').show();
            }        
        })
        if (Object.values(data).length < 5) {
            $('div.summary-breakdown').find('> div.week-5').hide();
        } else {
            $('div.summary-breakdown').find('> div.week-5').show();
        }

        $('div.year-round-single-day-summary-breakdown').find('> div').each(function(index) {
            if ((Object.values(data)[index]) != undefined) { 
                summary_data = Object.values(data)[index]['year_round_single_day_weekly_revenue']
                $(this).find('> span').text(`$${summary_data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`)
            }
            if (Object.values(data).length < 5) {
                $(this).find('> div.week-5').hide();
            } else {
                $(this).find('> div.week-5').show();
            }        
        })
        if (Object.values(data).length < 5) {
            $('div.summary-breakdown').find('> div.week-5').hide();
        } else {
            $('div.summary-breakdown').find('> div.week-5').show();
        }

        // after-school-summary-breakdown
        $('div.after-school-summary-breakdown').find('> div').each(function(index) {
            if ((Object.values(data)[index]) != undefined) { 
                summary_data = Object.values(data)[index]['after_school_weekly_revenue']
                $(this).find('> span').text(`$${summary_data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`)
            }
            //   console.log($(this).find('div.week-5'))
            if (Object.values(data).length < 5) {
                $(this).find('> div.week-5').hide();
            } else {
                $(this).find('> div.week-5').show();
            }        
        })
        if (Object.values(data).length < 5) {
            $('div.summary-breakdown').find('> div.week-5').hide();
        } else {
            $('div.summary-breakdown').find('> div.week-5').show();
        }

        //day_care_weekly_revenue
        $('div.day-care-summary-breakdown').find('> div').each(function(index) {
            if ((Object.values(data)[index]) != undefined) { 
                summary_data = Object.values(data)[index]['day_care_weekly_revenue']
                $(this).find('> span').text(`$${summary_data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`)
            }
            if (Object.values(data).length < 5) {
                $(this).find('> div.week-5').hide();
            } else {
                $(this).find('> div.week-5').show();
            }        
        })
        if (Object.values(data).length < 5) {
            $('div.summary-breakdown').find('> div.week-5').hide();
        } else {
            $('div.summary-breakdown').find('> div.week-5').show();
        }

        //vacation_camp_weekly_revenue
        $('div.vacation-camp-summary-breakdown').find('> div').each(function(index) {
            if ((Object.values(data)[index]) != undefined) { 
                summary_data = Object.values(data)[index]['vacation_camp_weekly_revenue']
                $(this).find('> span').text(`$${summary_data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`)
            }
            //   console.log($(this).find('div.week-5'))
            if (Object.values(data).length < 5) {
                $(this).find('> div.week-5').hide();
            } else {
                $(this).find('> div.week-5').show();
            }        
        })
        if (Object.values(data).length < 5) {
            $('div.summary-breakdown').find('> div.week-5').hide();
        } else {
            $('div.summary-breakdown').find('> div.week-5').show();
        }
        //summer_camp_weekly_revenue
        $('div.summer-camp-summary-breakdown').find('> div').each(function(index) {
            if ((Object.values(data)[index]) != undefined) { 
                summary_data = Object.values(data)[index]['summer_camp_weekly_revenue']
                $(this).find('> span').text(`$${summary_data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`)
            }
            //   console.log($(this).find('div.week-5'))
            if (Object.values(data).length < 5) {
                $(this).find('> div.week-5').hide();
            } else {
                $(this).find('> div.week-5').show();
            }        
        })
        if (Object.values(data).length < 5) {
            $('div.summary-breakdown').find('> div.week-5').hide();
        } else {
            $('div.summary-breakdown').find('> div.week-5').show();
        }

    //
}

function updateMonthHeader() {
    switch ($('#month-of-monthly-summary').val()) {
        case "1":
            $('h4.header-section > span.header-month-name').text('January');
            break;
        case "2":
            $('h4.header-section > span.header-month-name').text('February');
            break;
        case "3":
            $('h4.header-section > span.header-month-name').text('March');
            break;
        case "4":
            $('h4.header-section > span.header-month-name').text('April');
            break;
        case "5":
            $('h4.header-section > span.header-month-name').text('May');
            break;
        case "6":
            $('h4.header-section > span.header-month-name').text('June');
            break;
        case "7":
            $('h4.header-section > span.header-month-name').text('July');
            break;
        case "8":
            $('h4.header-section > span.header-month-name').text('August');
            break;
        case "9":
            $('h4.header-section > span.header-month-name').text('September');
            break;
        case "10":
            $('h4.header-section > span.header-month-name').text('October');
            break;
        case "11":
            $('h4.header-section > span.header-month-name').text('November');
            break;
        case "12":
            $('h4.header-section > span.header-month-name').text('December');
            break;
    }
}

function bindEventToSummaryButton(barChart) {
    $('button#summary-refresh-button').click(function() {
        updateMonthlySummary(barChart);
    })
}