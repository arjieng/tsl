function yearlyOverview() {
    const context = $('canvas#yearly-overview-canvas');
    if (context.length) {
        const data = {
            labels: ['January','February','March', 'April', 'May', 'June', 'July','August','September','October','November','December'],
            datasets: [
                {
                    backgroundColor: '#01579B',
                    data: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
                    pointBackgroundColor: '#37474F',
                    pointHitRadius: 10,
                    pointHoverBackgroundColor: '#0288D1',
                    lineTension: 0
                }
            ]
        }
        Chart.defaults.global.maintainAspectRatio = true;
        const lineChart = new Chart(
            context, {
                type: 'line',
                data: data,
                options: {
                    responsive: true,
                    legend: { display: false },
                    title: { display: false },
                    animation: {
                        duration: 1600
                    },
                    tooltips: {
                        callbacks: {
                            label: function(tooltipItem, data) {
                                    return `$${tooltipItem.yLabel.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`
                                }
                        }
                    }
                }
            }
        )
        updateYearlySummary(lineChart);
        bindEventToYearlySummaryButton(lineChart);
    }
}

function updateYearlySummary(lineChart) {
    url = `api/yearly-summary?year=${$('#year-select-input').val()}`
    $('div.yearly-overview-loader').css({
        visibility: 'visible',
        opacity: 1
    })
    $.ajax({
        method: "get",
        url: url
    }).done(function(data) {
        updateYearlySummaryChart(lineChart, data);
        updateYearlySummaryRows(data);
        updateYearlySummaryHeader();
        updateTotalAnnualRevenue(data);
        $('div.yearly-overview-loader').css({
            opacity: 0
        })
    })
}

function updateYearlySummaryChart(lineChart, data) {
    lineChart.data.datasets[0].data = [];
    Object.values(data['yearly_breakdown']).forEach(data => lineChart.data.datasets[0].data.push(data))
    lineChart.update();
}

function updateYearlySummaryRows(data) {
    $('div.first-half-box > div.month-amount').add('div.second-half-box > div.month-amount').each(function(index) {
        if (Object.values(data['yearly_breakdown'])[index] != undefined)  {
            $(this).fadeOut(500, function() {
                $(this).text(`$${Object.values(data['yearly_breakdown'])[index].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(300);
            })
        } else {
            $(this).fadeOut(500, function() {
                $(this).text('$0').fadeIn(300);
            })
        }
    })
}

function bindEventToYearlySummaryButton(lineChart) {
    $('.year-refresh-button').click(function() {
        updateYearlySummary(lineChart)
    })
}

function updateYearlySummaryHeader() {
    $('.yearly-summary-year-text').text($('#year-select-input').val())
}

function updateTotalAnnualRevenue(data) {
    total_revenue = Object.values(data['yearly_breakdown']).reduce((acc, curr) => acc + curr);
    $('div.total-year-revenue-section > div.value').text(`$${total_revenue.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`);
}