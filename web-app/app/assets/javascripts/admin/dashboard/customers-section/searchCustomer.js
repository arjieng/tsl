function searchCustomer() {
    $('div.search-field-section > button.search-button').click(function() {
        $(this).prop('disabled', true);
        const firstName = $('div.search-field-section > input.first-name').val();
        const lastName = $('div.search-field-section > input.last-name').val();
        const url = `api/search-customer?first_name=${firstName}&last_name=${lastName}`
        $('div.loading-ellipsis').css({
            visibility: 'visible',
            opacity: 1
        });
        $.ajax({
            method: 'get',
            url: url
        }).done(function(data) {
            $('div.search-result-table-inner-container > table.result-table tr').not('.header-row').remove();
            data.forEach(customer => {
                element = 
                        `<tr data-customer-id="${customer.id}">` +
                            `<td>${customer.first_name}</td>` +
                            `<td>${customer.last_name}</td>` +
                            `<td>${customer.address}</td>` +
                        '</tr>'
                $('div.search-result-table-inner-container > table.result-table').append(element);
            })
            $('div.search-field-section > button.search-button').prop('disabled', false);
            $('div.loading-ellipsis').css({
                opacity: 0
            });
            bindCustomerInformationRows();
        })
    })
    $('div.search-field-section input').keyup(function(event) {
        if (event.keyCode === 13) {
            $('div.search-field-section > button.search-button').click();
        }
    })
}

function bindCustomerInformationRows() {
    $('div.search-result-table-inner-container > table.result-table tr').click(function () {
        url = `api/customer-information?customer_id=${$(this).data('customer-id')}`
        $.ajax({
            method: 'get',
            url: url
        }).done(function(data) {
            $('section.account-section > div.account-information > div.row').each(function(index) {
                $(this).find('div.value').fadeOut(500, function() {
                    $(this).text(Object.values(data['account_information'])[index]).fadeIn(300);
                })
            })
    
            $('div.registration-information-container > div.single-day-information')
                .find('div.registration-count').fadeOut(500, function() {
                    $(this).text(Object.values(data['registrations_information']['single_day'])[0]).fadeIn(400);
                })
            $('div.registration-information-container > div.single-day-information')
                .find('div.earnings').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['single_day'])[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.after-school-information')
                .find('div.invoices-paid').fadeOut(500, function() {
                    $(this).text(Object.values(data['registrations_information']['before_and_after_school'])[0]).fadeIn(400);
            })

            $('div.registration-information-container > div.after-school-information')
                .find('div.outstanding').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['before_and_after_school'])[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.after-school-information')
                .find('div.earnings').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['before_and_after_school'])[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.year-round-single-day-information')
                .find('div.registration-count').fadeOut(500, function() {
                    $(this).text(Object.values(data['registrations_information']['year_round_single_day'])[0]).fadeIn(400);
                })

            $('div.registration-information-container > div.year-round-single-day-information')
                .find('div.earnings').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['year_round_single_day'])[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.day-care-information')
                .find('div.monthly-due').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['day_care'])[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })
            
            $('div.registration-information-container > div.day-care-information')
                .find('div.invoices-paid').fadeOut(500, function() {
                    $(this).text(Object.values(data['registrations_information']['day_care'])[1]).fadeIn(400);
                })

            $('div.registration-information-container > div.day-care-information')
                .find('div.outstanding').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['day_care'])[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.day-care-information')
                .find('div.earnings').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['day_care'])[3].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.vacation-camp-information')
                .find('div.registration-count').fadeOut(500, function() {
                    $(this).text(Object.values(data['registrations_information']['vacation_camp'])[0]).fadeIn(400);
                })
            
            $('div.registration-information-container > div.vacation-camp-information')
                .find('div.earnings').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['vacation_camp'])[1].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.summer-camp-information')
                .find('div.deposit-revenue').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['summer_camp'])[0].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.summer-camp-information')
                .find('div.invoices-paid').fadeOut(500, function() {
                    $(this).text(Object.values(data['registrations_information']['summer_camp'])[1]).fadeIn(400);
                })

            $('div.registration-information-container > div.summer-camp-information')
                .find('div.invoices-revenue').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['summer_camp'])[2].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.summer-camp-information')
                .find('div.outstanding').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['summer_camp'])[3].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

            $('div.registration-information-container > div.summer-camp-information')
                .find('div.earnings').fadeOut(500, function() {
                    $(this).text(`$${Object.values(data['registrations_information']['summer_camp'])[4].toString().replace(/\B(?=(\d{3})+(?!\d))/g, ", ")}`).fadeIn(400);
                })

        })
    });
}