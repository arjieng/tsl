function payrollSection() {
    if ($('input#dashboard-payroll-breakdown-section').length === 0) {return;}
    retrieveAllPayrollLocations();
    retrieveAllAdministrators();
    bindEventToAdministratorSelectButton();
    delegateEventToAdminSelectList();
    delegateAuthorizedAdministratorsListEvents();
    bindEventToAddAccessButton();
    delegateEventsToEditAndSavePayrollLocationButton();
    bindEventsToPayrollLoggedHoursMonthSelect();
    retrieveAllPayrollLocationsBreakDown();
    bindEventHandlerToPayrollBreakdownButton();
    bindEventHandlerToPayrollTrendElements();
    $('ul.payroll-locations-list').on('click', 'button.payroll-location-submenu-button', function() {
        submenu = $(this).closest('div.payroll-location-button-pair').next('div.sub-menu')
        if(submenu.hasClass('hidden')) {
            $(this).find('i').css({
                transform: 'rotate(-180deg)'
            })
            submenu.css({
                height: '40px'
            })
            submenu.removeClass('hidden')
        } else {
            $(this).find('i').css({
                transform: 'rotate(0deg)'
            })
            submenu.css({
                height: '1px'
            })
            submenu.addClass('hidden')
        }
    })

    $('ul.payroll-locations-list').on('click', 'input', function() {
        $(this).closest('li').siblings().removeClass('active');
        $(this).closest('li').addClass('active');
        updateAuthorizedAdministratorsList();
    })

    $('button.add-new-payroll-location-button').click(function() {
        container = $(this).parent().prev('div.new-payroll-location-container')
        if (container.hasClass('hidden')) {
            container.css({
                height: '70px'
            })
            container.removeClass('hidden')
        } else {
            container.css({
                height: '0px'
            })
            container.addClass('hidden')
        }
    })

    $('button.confirm-adding-of-new-payroll-location').click(function() {
        url = '/admin/payroll/add-new-payroll-location'
        $.ajax({
            url: url,
            method: 'post',
            dataType: 'json',
            data: {
                payroll_location_name: $('input.new-payroll-location-input').val().trim()
            },
            error: function(response) {
                showPayrollSectionEventNotification(response['responseJSON']['message'], 'failure')
            }
        }).done(function(response) {
            $('ul.payroll-locations-list').append(
                `<li data-payroll-location-id="${response['location']['id']}">
                    <div class="payroll-location-button-pair">
                        <div class="payroll-location-item-input-container"><input type="text" value="${response['location']['name']}" readonly></div>
                        <div class="location-pair-submenu-button-container">
                            <button class="payroll-location-submenu-button"><i class="fas fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div class="sub-menu hidden">
                        <button><i class="fas fa-ban"></i></button>
                        <button class="payroll-location-edit-button"><i class="fas fa-edit"></i></button>
                        <button class="payroll-location-save-changes-button"><i class="fas fa-check"></i></button>
                    </div>
                </li>`
            )
            showPayrollSectionEventNotification(response['message'], 'success')
            $('div.new-payroll-location-container').css({
                height: '0px'
            })
            $('div.new-payroll-location-container').addClass('hidden')
            setTimeout(function(){
                $('input.new-payroll-location-input').val('');
            }, 600)
            retrieveAllPayrollLocationsBreakDown();
        })
    })
}

function retrieveAllPayrollLocations() {
    url = '/admin/payroll/retrieve-all-payroll-locations'
    $.ajax({
        method: 'get',
        url: url
    }).done(function(response) {
        response['payroll_locations'].forEach(function(location) {
            $('ul.payroll-locations-list').append(
                `<li data-payroll-location-id="${location['id']}">
                    <div class="payroll-location-button-pair">
                        <div class="payroll-location-item-input-container"><input type="text" value="${location['name']}" readonly></div>
                        <div class="location-pair-submenu-button-container">
                            <button class="payroll-location-submenu-button"><i class="fas fa-chevron-down"></i></button>
                        </div>
                    </div>
                    <div class="sub-menu hidden">
                        <button><i class="fas fa-ban"></i></button>
                        <button class="payroll-location-edit-button"><i class="fas fa-edit"></i></button>
                        <button class="payroll-location-save-changes-button"><i class="fas fa-check"></i></button>
                    </div>
                </li>`
            )

            $('ul.payroll-trend-location-options').append(
                `<li data-location-id="${location['id']}"><button>${location['name']}</button></li>`
            )
        })
        $('ul.payroll-locations-list li').first().addClass('active');
        updateAuthorizedAdministratorsList();
    })
}

function bindEventToAdministratorSelectButton() {
    $('button.administrator-select-button').click(function() {
        admin_select = $('div.administrator-select');
        admin_select_options = $('ul.administrator-select-options')
        arrow = $(this).find('i')
        if ($(this).next('ul.administrator-select-options').hasClass('hidden')) {
            admin_select.css({
                height: '210px'
            })
            admin_select_options.removeClass('hidden');
            arrow.css({transform: 'rotate(-180deg)'})
        } else {
            admin_select.css({height: '40px'})
            admin_select_options.addClass('hidden');
            arrow.css({transform: 'rotate(0deg)'})
        }
    })
}

function delegateEventToAdminSelectList() {
    $('ul.administrator-select-options').on('click', 'button.administrator-select-list-item-button',function() {
        $('input.selected-administrator-id-for-authorization').val($(this).closest('li').data('admin-id'))
        $('span.selected-payroll-location-administrator').text($(this).text());
        $('div.administrator-select').css({height: '40px'})
        $('ul.administrator-select-options').addClass('hidden');
        $('button.administrator-select-button').find('i').css({transform: 'rotate(0deg)'})
    })
}

function retrieveAllAdministrators() {
    url = '/admin/payroll/retrieve-all-administrators'
    $.ajax({
        method: 'get',
        url: url
    }).done(function(response) {
        response['admin_users'].forEach(function(admin) {
            $('ul.administrator-select-options').append(
                `<li data-admin-id=${admin['id']}><button class="administrator-select-list-item-button">${admin['name']}</button></li>`
            )
        })  
    })
}

function updateAuthorizedAdministratorsList() {
    url = `/admin/payroll/retrieve-all-authorized-administrators/${$('ul.payroll-locations-list li.active').data('payroll-location-id')}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(response) {
        $('ul.authorized-administrators-list li').remove();
        response['authorized_administrators'].forEach(function(admin) {
            $('ul.authorized-administrators-list').append(
                `<li data-admin-id=${admin['id']}><span>${admin['name']}</span><button><i class="fas fa-times-circle"></i></button></li>`
            )
        })
    })
}

function delegateAuthorizedAdministratorsListEvents() {
    $('ul.authorized-administrators-list').on('click', 'button', function() {
        url = `/admin/payroll/revoke-access-to-payroll-location`
        $.ajax({
            url: url,
            method: 'post',
            dataType: 'json',
            data: {
                administrator_id: $(this).closest('li').data('admin-id'),
                payroll_location_id: $('ul.payroll-locations-list li.active').data('payroll-location-id')
            }
        }).done(function(response) {
            $('ul.authorized-administrators-list li').each(function() {
                if ($(this).data('admin-id') == response['data_admin_id']) {
                    $(this).remove();
                }
            })
        })
    })
}

function bindEventToAddAccessButton() {
    $('button.authorize-administrator-button').click(function() {
        url = '/admin/payroll/authorize-access-to-payroll-location'
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {
                payroll_location_id: $('ul.payroll-locations-list li.active').data('payroll-location-id'),
                administrator_id: $('input.selected-administrator-id-for-authorization').val()
            },
            error: function(response) {
                showPayrollSectionEventNotification(response['responseJSON']['message'], 'failure')
            }
        }).done(function(response) {
            showPayrollSectionEventNotification(response['message'], 'success')
            $('ul.authorized-administrators-list').append(
                `<li data-admin-id=${response['administrator']['id']}>
                    <span>${response['administrator']['name']}</span><button><i class="fas fa-times-circle"></i></button>
                </li>`
            )
        })
    })
}

function showPayrollSectionEventNotification(message, status) {
    notice = $('h3.payroll-section-event-notice')
    switch(status) {
        case 'success':
            background_color = '#43A047';
            break;
        case 'failure':
            background_color = '#C62828';
            break;
    }
    notice.css({backgroundColor: background_color, opacity: '1'});
    notice.text(message);
    setTimeout(function() {
        notice.css({opacity: '0'})
    }, 2000)
}

function delegateEventsToEditAndSavePayrollLocationButton() {
    $('ul.payroll-locations-list').on('click', 'button.payroll-location-edit-button', function() {
        input = $(this).closest('div.sub-menu').prev('div.payroll-location-button-pair').find('input');
        location_id = $(this).closest('li').data('payroll-location-id')
        input.attr('readonly', false);
        input.css({cursor: 'text'});
        input.focus();
        input[0].setSelectionRange(input.val().length, input.val().length);
    })

    $('ul.payroll-locations-list').on('click', 'button.payroll-location-save-changes-button', function() {
        input = $(this).closest('div.sub-menu').prev('div.payroll-location-button-pair').find('input');
        location_id = $(this).closest('li').data('payroll-location-id')
        submenu = $(this).closest('div.sub-menu');
        $.ajax({
            method: 'post',
            url: '/admin/payroll/update-payroll-location-name',
            dataType: 'json',
            data: {
                payroll_location_id: location_id,
                new_name: input.val().trim()
            },
            error: function(response) {
                showPayrollSectionEventNotification(response['responseJSON']['message'], 'failure');
            }
        }).done(function(response) {
            showPayrollSectionEventNotification(response['message'], 'success');
            submenu.css({height: '1px'})
            submenu.addClass('hidden');
            submenu.prev('div.payroll-location-button-pair')
                .find('button.payroll-location-submenu-button i').css({transform: 'rotate(0deg)'})
            input.attr('readonly', true);
            input.css({cursor: 'pointer'});
        }) 
    })
}

function bindEventsToPayrollLoggedHoursMonthSelect() {
    month_options = $('ul.logged-hours-month-options');
    month_select = $('div.payroll-logged-hours-month-select');
    $('button.logged-hours-month-select-dropdown-button').click(function() {
        if (month_options.hasClass('hidden')) {
            month_select.css({height: '200px'});
            month_options.removeClass('hidden');
            $(this).find('i').css({transform: 'rotate(-180deg)'})
        } else {
            month_select.css({height: '40px'});
            month_options.addClass('hidden');
            $(this).find('i').css({transform: 'rotate(0deg)'})
        }
    })

    $('ul.logged-hours-month-options').find('button').click(function() {
        $('span.payroll-logged-hours-month-select-text').text($(this).text());
        $('input.payroll-logged-hours-breakdown-selected-month').val($(this).closest('li').data('month-as-number'));
        month_select.css({height: '40px'});
        month_options.addClass('hidden');
        $('button.logged-hours-month-select-dropdown-button').find('i').css({transform: 'rotate(0deg)'})
    })
}

function retrieveAllPayrollLocationsBreakDown() {
    year = $('input.year-of-payroll-logged-hours-breakdown').val();
    month = $('input.payroll-logged-hours-breakdown-selected-month').val();
    url = `/admin/payroll/retrieve-all-payroll-locations-breakdown/${year}/${month}`
    $.ajax({
        method: 'get',
        url: url,
        error: function(response) {
            showPayrollSectionEventNotification(response['responseJSON']['message'], 'failure')
        }
    }).done(function(response) {
        showPayrollSectionEventNotification(response['message'], 'success')
        $('div.payroll-logged-hours-breakdown-section').find('div.location-breakdown').remove();
        response['payroll_breakdown'].forEach(function(location) {
            $('div.payroll-logged-hours-breakdown-section').append(
            `<div class="location-breakdown">
                <h4 class="location-name">${location['payroll_location_name']}</h4>
                <div class="headers">
                    <div>Name</div>
                    <div class="one-to-fifteen">1-15</div>
                    <div class="sixteen-to-thirty-one">16-31</div>
                    <div class="total-hours">Total</div>
                    <div class="sick-days">Sick</div>
                    <div class="vacation">Vacation</div>
                </div>
                ${generateEmployeeRowNodes(location['payroll_location_breakdown'])}
            </div>`
            )
        })
    })
    
}

function generateEmployeeRowNodes(array) {
    result = "";
    array.forEach(function(employee) {
        result += `<div class="row">
                            <div>${employee['employee_name']}</div>
                            <div class="one-to-fifteen">${employee['1-15']}</div>
                            <div class="sixteen-to-thirty-one">${employee['16-31']}</div>
                            <div class="total-hours">${employee['1-15'] + employee['16-31']}</div>
                            <div class="sick-days">${employee['sick']}</div>
                            <div class="vacation">${employee['vacation']}</div>
                    </div>`
    })
    return result
}

function bindEventHandlerToPayrollBreakdownButton() {
    $('button.refresh-payroll-logged-hours-breakdown').click(function() {
        thisButton = $(this)
        retrieveAllPayrollLocationsBreakDown();
        thisButton.attr('disabled', true);
        setTimeout(function(){
            $('button.refresh-payroll-logged-hours-breakdown').attr('disabled', false);
        }, 2000)
    })
}

function bindEventHandlerToPayrollTrendElements() {
    $('button.payroll-trend-month-select-dropdown-button, button.payroll-trend-location-select-dropdown-button').click(function() {
        let month_options = $(this).next('ul');
        let month_select = $(this).closest('div');
        if (month_options.hasClass('hidden')) {
            month_select.css({height: '200px'});
            month_options.removeClass('hidden');
            $(this).find('i').css({transform: 'rotate(-180deg)'})
        } else {
            month_select.css({height: '40px'});
            month_options.addClass('hidden');
            $(this).find('i').css({transform: 'rotate(0deg)'})
        }
    })

    $('ul.payroll-trend-month-options').find('button').click(function() {
        let month_select = $(this).closest('div.payroll-trend-month-select');
        let month_options = $(this).closest('ul.payroll-trend-month-options');
        let parent_list_item = $(this).closest('li');
        $(this).closest('ul.payroll-trend-month-options').siblings('span.payroll-trend-month-text').text($(this).text());
        parent_list_item.siblings('input.payroll-trend-selected-month').val(parent_list_item.data('month-as-number'));
        month_select.css({height: '40px'});
        month_options.addClass('hidden');
        month_options.prev('button.payroll-trend-month-select-dropdown-button').find('i').css({transform: 'rotate(0deg)'})
    })

    $('ul.payroll-trend-location-options').on('click', 'button', function() {
        let location_select = $(this).closest('div.payroll-trend-location-select');
        let location_options = $(this).closest('ul.payroll-trend-location-options');
        let parent_list_item = $(this).closest('li');
        location_options.siblings('span.payroll-trend-location-text').text($(this).text());
        parent_list_item.siblings('input.payroll-trend-selected-location').val(parent_list_item.data('location-id'));
        location_select.css({height: '40px'});
        location_options.addClass('hidden');
        location_options.prev('button.payroll-trend-location-select-dropdown-button').find('i').css({transform: 'rotate(0deg)'})
    })

    $('button.payroll-trend-data-refresh-button').click(function() {
        let year_month = $(this).closest('div.refresh-button-container').prev('div.year-month-location-options');
        let year = year_month.find('input.year-of-labor-trend').val();
        let month = year_month.find('input.payroll-trend-selected-month').val()
        let location_id = year_month.find('input.payroll-trend-selected-location').val();
        let url = `/admin/payroll/payroll-trend-analysis/${year}/${month}/${location_id}`;
        let this_button = $(this);
        if (location_id == -1) {
            showPayrollSectionEventNotification('Please select a location', 'failure')
        } else {
            $.ajax({
                method: 'get',
                url: url
            }).done(function(response) {
                let table_rows = ""
                response['location_summary']['payroll_location_breakdown'].forEach(function(element) {
                    table_rows += `
                        <tr>
                            <td>${element['employee_name']}</td>
                            <td>${element['1-15']}</td>
                            <td>${element['16-31']}</td>
                            <td>${element['1-15'] + element['16-31']}</td>
                            <td>${element['sick']}</td>
                            <td>${element['vacation']}</td>
                        </tr>
                    `
                })
                year_month.siblings('div.trend-table-container').find('table').remove();
                year_month.siblings('div.trend-table-container').append(
                    `<table class="trend-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>1-15</th>
                                <th>16-31</th>
                                <th>Total</th>
                                <th>Sick</th>
                                <th>Vacation</th>
                            </tr>
                        </thead>
                        <tbody>
                            ${table_rows}
                        </tbody>
                    </table>`
                )
                showPayrollSectionEventNotification(response['message'], 'success')
            })
        }
    })
}