function sideBar() {
    $('div.side-bar-row').click(function() {
        switch ($(this).prop('id')) {
            case 'home-section-button':
                $('div.admin-dashboard-container > section').css('display', 'none');
                $('section.home').css('display', 'block');
                break;
            case 'customers-section-button':
                $('div.admin-dashboard-container > section').css('display', 'none');
                $('section.customers').css('display', 'block');
                break;
            case 'programs-section-button':
                $('div.admin-dashboard-container > section').css('display', 'none');
                $('section.programs').css('display', 'block');
                break;
            case 'billing-section-button':
                $('div.admin-dashboard-container > section').css('display', 'none');
                $('section.billing').css('display', 'block');
                break;
            case 'payroll-section-button':
                $('div.admin-dashboard-container > section').css('display', 'none');
                $('section.payroll').css('display', 'block');
                break;
        }
    })
}