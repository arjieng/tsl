function payroll() {
    if ($('#payroll-admin-id-hidden').length) {
        initializePayrollComponents();
        retrieveLocations();
        bindEventToAddNewEmployeeButton();
        bindEventToShowAddNewEmployeeField();
        bindEventToUpdateLoggedHoursButton();
        $('div#payroll-calendar').datepicker({
            maxDate: '+0d',
            onSelect: function(dateText) {
                if (dateText != $('input#payroll-date-of-logged-hours').val()) {
                    $('input#payroll-date-of-logged-hours').val(dateText)
                    updateMonthNameOfPayrollOverview(new Date(dateText).getMonth());
                    updatePayrollPageInformation();
                }
            }
        });
        bindEventHandlerToPayrollLocationSelect();
        bindEventHandlerToPayrollLocationAbscenceSelect();
        bindEventHandlerToAbsentPayrollEmployeeSelect();
        bindEventHandlersToLogAbsenceModal();
        $('input.date-of-absence-input-field').datepicker();
        bindEventHandlerToSubmitAbsenceButton();
    }
}

function retrieveLocations() {
    url = `/admin/payroll/accessible-locations/${$('#payroll-admin-id-hidden').val()}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(response) {
        response['accessible_locations'].forEach(function(element) {
            $('#payroll-location-select').append(`<option value='${element.id}'>${element.name}</option>`)
            $('div.employee-abscence-location-select').find('ul.location-list').append(`<li data-location-id="${element.id}"><button>${element.name}</button></li>`)
        })
        updatePayrollPageInformation();
    })
}

function bindEventToAddNewEmployeeButton() {
    $('button.employee-save-button').click(function() {
        employee_name = $('input.new-employee-name').val().trim();
        if ($(this).data('clicked') == false) {
            $(this).data('clicked', true);
            if (employee_name.length < 3) {
                showEventNotice('The name is too short. Try again.', 'failure')
            } else {
                $.ajax({
                    method: 'post',
                    url: '/admin/payroll/add-new-payroll-employee',
                    dataType: 'json',
                    data: {
                        payroll_location_id: $('#payroll-location-select').val(),
                        employee_name: $('input.new-employee-name').val()
                    }, 
                    error: function(response) {
                        showEventNotice(response.responseJSON['message'], 'failure');
                    }
                }).done(function(response) {
                    switch(response['status']) {
                        case 'success':
                            showEventNotice('Successfully added new employee', 'success');
                            $('ul.payroll-employee-list').append(
                                `<li>
                                    <input type="text" value="${response['employee']['name']}" readonly="true" data-employee-id="${response['employee']['id']}" />
                                    <i class="fas fa-angle-down payroll-employee-arrow"></i>
                                    <div class="edit-and-delete-employee-container hidden">
                                        <button type="button" class="edit-payroll-employee-button"><i class="fas fa-edit"></i></button>
                                        <button type="button" class="delete-payroll-employee-button"><i class="fas fa-minus-circle"></i></button>
                                    </div>
                                </li>`
                            );
                            createPayrollHourForNewEmployee(response['employee']['id']);
                            hideAddNewEmployeeField();
                            updatePayrollMonthOverview();
                            break;
                    }
                })
            }
        }
    })
}

function showEventNotice(event_message, event_type) {
    $('div.payroll-event-notice').text(event_message);
    switch (event_type) {
        case 'failure':
            $('div.payroll-event-notice').css({ 'background-color': '#E57373',opacity: '1'});
            break;
        case 'success':
            $('div.payroll-event-notice').css({ 'background-color': '#81C784',opacity: '1'});
            break;
    }
    setTimeout(function() {
        $('div.payroll-event-notice').css({opacity: '0'});
        $('button.employee-save-button').data('clicked', false);
    }, 2200);
}

function updatePayrollPageInformation() {
    url = `/admin/payroll/retrieve-payroll-location-information/${$('#payroll-location-select').val()}/${$('input#payroll-date-of-logged-hours').val()}`
    $.ajax({
        method: 'get',
        url: url
    }).done(function(response) {
        $('ul.payroll-employee-list li').remove();
        $('tbody.employees-log-table-body tr').remove();
        response['payroll_employees'].forEach((employee) => {
             $('ul.payroll-employee-list').append(
                `<li class="payroll-employee-list-item">
                    <input type="text" value="${employee.name}" readonly="true" data-employee-id="${employee.id}" class="payroll-employee-name-input" />
                    <i class="fas fa-angle-down payroll-employee-arrow"></i>
                    <div class="edit-and-delete-employee-container hidden">
                        <button type="button" class="save-edited-employee-name-button"><i class="fas fa-save"></i></button>
                        <button type="button" class="edit-payroll-employee-button"><i class="fas fa-edit"></i></button>
                        <button type="button" class="delete-payroll-employee-button"><i class="fas fa-minus-circle"></i></button>
                    </div>
                </li>`
            )
            $('table.employees-log-table tbody').append(
                `<tr>
                    <input type="hidden" value="${employee.id}" class="row-payroll-employee-id">
                    <td><span>${employee.name}</span></td>
                    <td><input type="text" value="${employee.logged_hours}" name="hours"></td>
                </tr>`
            )
        })
        updatePayrollMonthOverview(response['total_hours_for_the_month']);
    })
}

function bindEventToShowAddNewEmployeeField() {
    $('button.add-employee-button').click(function() {
        if ($('div.new-employee-section').hasClass('hidden')) {
            $('div.new-employee-section').css({
                height: '80px'
            })
            $('div.new-employee-section').removeClass('hidden');
            $(this).find('i').removeClass('fa-plus');
            $(this).find('i').addClass('fa-minus');
            $('input.new-employee-name').prop('disabled', false);
        } else {
            hideAddNewEmployeeField();
        }
    })
}

function initializePayrollComponents() {
    initializeDelegationOfPayrollEmployeeListEvents();
    $('div.payroll-modal-container').click(function(event) {
        if (event.target !== event.currentTarget) return;
        $('div.payroll-modal-container').css({
            display: 'none'
        });
        $('div.payroll-modal-window').css({
            top: '-25%'
        })
    })

    $('i.close-payroll-modal-window-icon, button.close-payroll-modal-window-button').click(function(event) {
        if ($(event.target).hasClass('close-payroll-modal-window-icon')) {
            $('div.payroll-modal-container').css({
                display: 'none'
            });
            $('div.payroll-modal-window').css({
                top: '-25%'
            })
        } else {
            $('div.payroll-modal-window').css({
                top: '-25%'
            })
            setTimeout(function() {
                $('div.payroll-modal-container').css({
                    display: 'none'
                });
            }, 700)
        }
        
    })

    $('input.new-employee-name').keydown(function(event) {
        if(event.which === 13) {
            $('button.employee-save-button').click();
        }
    })
    $('button.employee-cancel-button').click(function() {hideAddNewEmployeeField()})
    $('div.new-employee-section').addClass('hidden');
    $('button.employee-save-button').data('clicked', false);
    $('input.new-employee-name').prop('disabled', true);
    $('button.payroll-confirm-employee-deletion').click(function() {
        url = '/admin/payroll/delete-payroll-employee'
        thisButton = $(this);
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {
                employee_id: $('input#payroll-employee-id-marked-for-deletion').val()
            }
        }).done(function(response) {
            showEventNotice(response['message'], 'success');
            updatePayrollPageInformation();
            $('div.payroll-modal-window').css({
                top: '-25%'
            })
            setTimeout(function() {
                $('div.payroll-modal-container').css({
                    display: 'none'
                });
            }, 400)
        })
    })
}

function hideAddNewEmployeeField() {
    $('div.new-employee-section').css({
        height: '0'
    })
    $('div.new-employee-section').addClass('hidden');
    $('button.add-employee-button').find('i').fadeOut(200, function() {
        $('button.add-employee-button').find('i').addClass('fa-plus');
        $(this).fadeIn(400, function() {
            $('button.add-employee-button').find('i').removeClass('fa-minus');
        })
    })
    $('input.new-employee-name').prop('disabled', true);
    $('input.new-employee-name').val('');
}

function initializeDelegationOfPayrollEmployeeListEvents() {
    $('ul.payroll-employee-list').on("click", "i.payroll-employee-arrow", function() {
        if ($(this).next('div.edit-and-delete-employee-container').hasClass('hidden')) {
            $(this).css({
                transform: 'rotate(-180deg)'
            })
            $(this).next('div.edit-and-delete-employee-container').css({
                height: '30px'
            })
            $(this).next('div.edit-and-delete-employee-container').removeClass('hidden');
        } else {
            $(this).css({
                transform: 'rotate(0deg)'
            })
            $(this).next('div.edit-and-delete-employee-container').css({
                height: '0'
            })
            $(this).next('div.edit-and-delete-employee-container').addClass('hidden');
            $(this).prev('input').attr('readonly', true);
            thisButton = $(this)
            setTimeout(function() {
                thisButton.next('div.edit-and-delete-employee-container').find('button.save-edited-employee-name-button').css({
                    opacity: '0',
                    visibility: 'hidden'
                })
            }, 410)
        }
    })

    $('ul.payroll-employee-list').on('keydown', 'input.payroll-employee-name-input', function(event) {
        thisInput = $(this);
        if(event.which === 13) {
            thisInput.next().next().find('button.save-edited-employee-name-button').click();
        }
    })

    $('ul.payroll-employee-list').on("click", "button.delete-payroll-employee-button", function() {
        $('input#payroll-employee-id-marked-for-deletion').val($(this).parent().siblings('input').attr('data-employee-id'))
        $('div.payroll-modal-container').css({
            display: 'block'
        });
        setTimeout(function() {
            $('div.payroll-modal-window').css({
                top: '50%'
            })
        }, 1)
    })

    $('ul.payroll-employee-list').on("click", "button.edit-payroll-employee-button", function() {
        input = $(this).parent().siblings('input');
        input.attr('readonly', false);
        input.focus();
        input[0].setSelectionRange(input.val().length, input.val().length);
        $(this).prev('button.save-edited-employee-name-button').css({
            opacity: '1',
            visibility: 'visible'
        });
        $.ajax({

        })
    })

    $('ul.payroll-employee-list').on("click", "button.save-edited-employee-name-button", function() {
        url = '/admin/payroll/update-payroll-employee-information'
        thisButton = $(this)
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {
                payroll_employee_id: $(this).parent().siblings('input').attr('data-employee-id'),
                new_payroll_employee_name: $(this).parent().siblings('input').val()
            }, error: function(response) {
                thisButton.parent().siblings('input').focus();
                showEventNotice(response.responseJSON['message'], 'failure');
            }
        }).done(function(response) {
            thisButton.parent().css({
                height: '0'
            })
            thisButton.parent().prev('i.payroll-employee-arrow').css({
                transform: 'rotate(0deg)'
            })
            thisButton.parent().addClass('hidden');
            thisButton.parent().prev().prev().attr('readonly', true);
            showEventNotice(response['message'], 'success');
        })
    })
}

function createPayrollHourForNewEmployee(employee_id) {
    url = '/admin/payroll/create-payroll-hour-for-new-employee'
    $.ajax({
        url: url,
        method: 'post',
        dataType: 'json',
        data: {
            payroll_location_id: $('#payroll-location-select').val(),
            payroll_employee_id: employee_id,
            payroll_date_of_hours_logged: $('input#payroll-date-of-logged-hours').val()
        }
    }).done(function(employee) {
        $('table.employees-log-table tbody').append(
            `<tr>
                <input type="hidden" value="${employee.id}" class="row-payroll-employee-id">
                <td><span>${employee.name}</span></td>
                <td><input type="text" value="${employee.logged_hours}" name="hours"></td>
            </tr>`
        )
    })
}

function bindEventToUpdateLoggedHoursButton() {
    $('button#update-payroll-logged-hours').click(function() {
        $(this).attr('disabled', true);
        url = '/admin/payroll/update-payroll-employee-logged-hours'
        array = []
        $('tbody.employees-log-table-body tr').each(function() {
            array.push({
                payroll_employee_id: $(this).find('input.row-payroll-employee-id').val(),
                logged_hours: $(this).find('input[name="hours"]').val()
            })
        })
        data = {
            payroll_location_id: $('#payroll-location-select').val(),
            payroll_logged_hours_date: $('input#payroll-date-of-logged-hours').val(),
            logged_hours: array
        }
        $.ajax({
            url: url,
            method: 'post',
            dataType: 'json',
            data: data,
            error: function (response) {
                showEventNotice(response['responseJSON']['message'], 'failure')
                setTimeout(function() {
                    $('button#update-payroll-logged-hours').attr('disabled', false);
                }, 3000)
            }
        }).done(function(response) {
            showEventNotice('Successfully updated logged hours', 'success')
            updatePayrollMonthOverview(response['payroll_hours_for_the_month']);
            setTimeout(function() {
                $('button#update-payroll-logged-hours').attr('disabled', false);
            }, 3000)
        })
        $('h4.payroll-for-the-month-header').text(
            updateMonthNameOfPayrollOverview(new Date($('input#payroll-date-of-logged-hours').val()).getMonth())
        )

    })
}

function updatePayrollMonthOverview(total_hours_for_the_month) {
    $('div.payroll-location-employee-count').text($('ul.payroll-employee-list li').length)
    hours = [0]
    $('tbody.employees-log-table-body').find('input[name="hours"]').each(function() {
        hours.push($(this).val())
    })
    $('div.payroll-total-hours-of-active-date').text(hours.reduce((acc, curr) => +acc + +curr));
    $('div.payroll-total-hours-for-the-month').text(total_hours_for_the_month)
}

function updateMonthNameOfPayrollOverview(monthNumber) {
    switch (monthNumber) {
        case 0:
            month = "January"
            break;
        case 1:
            month = "February"
            break;
        case 2:
            month = "March"
            break;
        case 3:
            month = "April"
            break;
        case 4:
            month = "May"
            break;
        case 5:
            month = "June"
            break;
        case 6:
            month = "July"
            break;
        case 7:
            month = "August"
            break;
        case 8:
            month = "September"
            break;
        case 9:
            month = "October"
            break;
        case 10:
            month = "November"
            break;
        case 11:
            month = "December"
            break;
    }
    $('span.month-name-of-paroll-overview').text(month);
    $('h4.payroll-for-the-month-header').text(month);
}

function bindEventHandlerToPayrollLocationSelect() {
    $('select#payroll-location-select').change(function() {
        updatePayrollPageInformation();
    })
}

function bindEventHandlerToPayrollLocationAbscenceSelect() {
    let select = $('div.employee-abscence-location-select');
    let button = select.find('button.location-dropdown-button');
    let location_list = select.find('ul.location-list');

    let employee_select = $('div.absent-employee-select');
    let employee_list = employee_select.find('ul.employee-list');
    let employee_button = employee_select.find('button.employee-dropdown-button');
    button.click(function() {
        if (location_list.hasClass('hidden')) {
            toggleAbsenceSelectVisibility(select, location_list, button, 'visible')
            toggleAbsenceSelectVisibility(employee_select, employee_list, employee_button, 'hidden')
        } else {
            toggleAbsenceSelectVisibility(select, location_list, button, 'hidden')
        }
    })

    select.find('ul.location-list').on('click', 'button', function() {
        select.find('span.location-name').text($(this).text());
        $('input.absent-employee-location-id').val($(this).closest('li').data('location-id'));
        toggleAbsenceSelectVisibility(select, location_list, button, 'hidden');
        let url = `/admin/payroll/retrieve-all-employees-of-payroll-location/${$('input.absent-employee-location-id').val()}`
        $.ajax({
            method: 'get',
            url: url
        }).done(function(response) {
            $('div.absent-employee-select').find('ul.employee-list').find('li').remove();
            response['employees'].forEach(function(element) {
                $('div.absent-employee-select').find('ul.employee-list').append(
                    `<li data-payroll-employee-id="${element.id}"><button>${element.name}<button></li>`
                )
            })
        })
    })
}

function bindEventHandlerToAbsentPayrollEmployeeSelect() {
    let select = $('div.absent-employee-select');
    let button = select.find('button.employee-dropdown-button');
    let employee_list = select.find('ul.employee-list');

    let location_select = $('div.employee-abscence-location-select');
    let location_list = location_select.find('ul.location-list');
    let location_button = location_select.find('button.location-dropdown-button');

    button.click(function() {
        if (employee_list.hasClass('hidden')) {
            toggleAbsenceSelectVisibility(select, employee_list, button, 'visible');
            toggleAbsenceSelectVisibility(location_select, location_list, location_button, 'hidden')
        } else {
            toggleAbsenceSelectVisibility(select, employee_list, button, 'hidden');
        }
    })

    select.find('ul.employee-list').on('click', 'button', function() {
        select.find('span.employee-name').text($(this).text());
        toggleAbsenceSelectVisibility(select, employee_list, button, 'hidden');
        $('input.absent-employee-id').val($(this).closest('li').data('payroll-employee-id'));
    })
}

function toggleAbsenceSelectVisibility(select, list, button, visibility) {
    switch (visibility) {
        case 'visible':
            $(button).find('i').css({transform: 'rotate(-180deg)'});
            select.css({height: '218px'})
            list.removeClass('hidden');
            break;
        case 'hidden':
            $(button).find('i').css({transform: 'rotate(0deg)'});
            select.css({height: '40px'});
            list.addClass('hidden');
            break;
    }
}

function bindEventHandlersToLogAbsenceModal() {
    $('button.log-employee-abscence-button').click(function() {
        $('div.log-employee-abscence-modal-container').css({display: 'block', opacity: '1'});
        setTimeout(function(){
            $('div.log-employee-abscence-modal-window').css({
                visibility: 'visible',
                opacity: '1'
            })
        }, 200)
    })

    $('button.close-log-employee-abscence-modal-button, button.cancel-log-absence-button').click(function() {
        setTimeout(function(){
            $('div.log-employee-abscence-modal-container').css({display: 'none'});
        }, 410)
        $('div.log-employee-abscence-modal-container').css({opacity: '0'});
        $('div.log-employee-abscence-modal-window').css({
            visibility: 'visible',
            opacity: '0'
        })
    })
}

function bindEventHandlerToSubmitAbsenceButton() {
    $('div.absence-submit-cancel-button-container').find('button.submit-button').click(function() {
        let url = '/admin/payroll/log-employee-absence'
        $(this).attr('disabled', true);
        $.ajax({
            method: 'post',
            url: url,
            dataType: 'json',
            data: {
                payroll_location_id: $('input.absent-employee-location-id').val(),
                payroll_employee_id: $('input.absent-employee-id').val(),
                date_of_absence: $('input.date-of-absence-input-field').val(),
                absence_type: $('input[name="absence-type-option"]:checked').val()
            },
            error: function(response) {
                showAbsenceEventNotification('failure', response['responseJSON']['message'])
            }
        }).done(function(response) {
            showAbsenceEventNotification('success', response['message']);
        })
    })
}

function showAbsenceEventNotification(status, message) {
    let color = '';
    switch (status) {
        case 'success':
            color = '#2E7D32'
            break;
        case 'failure':
            color = '#B71C1C'
            break;
    }
    let header = $('div.employee-absence-event-notification').find('h4');
    header.text(message);
    header.css({backgroundColor: color, opacity: '1'});
    setTimeout(function(){
        header.css({opacity: 0});
        $('div.absence-submit-cancel-button-container').find('button.submit-button').attr('disabled', false);
    }, 3000)
}