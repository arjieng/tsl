class CustomerUser < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

	belongs_to :customer

	after_create :tokenizer

	def set_reset_password_token
    raw, enc = Devise.token_generator.generate(self.class, :reset_password_token)

    self.reset_password_token   = enc
    self.reset_password_sent_at = Time.now.utc
    save(validate: false)
    return raw
	end
	

	def has_basic_information?
		if self.customer.present? && self.customer.has_complete_information?
			true
		else
			false
		end
	end

	def has_partial_information?
		self.customer.present? ? true : false
	end

	def tokenizer
		secret = Digest::SHA1.hexdigest(SecureRandom.urlsafe_base64)
		self.access_token = CustomerUser.find_by(access_token: secret).present? ? tokenizer : secret
		self.save
	end

end
