class SummerCampWeek < ActiveRecord::Base
	belongs_to :program

	def week_display
		"#{start_date.strftime('%m/%d')} - #{(start_date + 4.days).strftime('%m/%d')}"
	end

	def display_name
		if notes.present?
			"#{name} - #{week_display} (#{notes})"
		else
			"#{name} - #{week_display}"
		end
	end

	def enrolled_count(index)
		week_number = index + 1
		beginning_of_2019 = Date.parse("2019-01-01")
		end_of_2019 = beginning_of_2019.end_of_year

		self.program.registration_records.includes(:summer_camp_record)
			.where.not('summer_camp_records.id' => nil)
			.where("summer_camp_records.week_#{number_to_word(week_number)}" => true)
			.where(enrolled: true)
			.where('registration_records.created_at > ? AND registration_records.created_at < ?', beginning_of_2019, end_of_2019).to_a.sum(&:child_records_count)
	end

	private
	def number_to_word(number)
		case number
		when 1
			"one"
		when 2
			"two"
		when 3
			"three"
		when 4
			"four"
		when 5
			"five"
		when 6
			"six"
		when 7
			"seven"
		when 8
			"eight"
		when 9
			"nine"
		when 10
			"ten"
		end
	end
end
