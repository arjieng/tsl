class PayrollLocation < ActiveRecord::Base
    before_save :downcase_attributes
    has_many :payroll_employees
    has_many :payroll_employee_hours
    def format_self
        {
            id: self.id,
            name: self.name.titleize
        }
    end

    private

    def downcase_attributes
        self.name = self.name.downcase
    end
end
