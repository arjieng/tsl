class Fee < ActiveRecord::Base
	belongs_to :invoice

	def amount_in_cents
		amount.to_f * 100.0
	end
end
