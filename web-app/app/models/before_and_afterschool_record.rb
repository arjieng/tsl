class BeforeAndAfterschoolRecord < ActiveRecord::Base
	belongs_to :registration_record

	validates_presence_of :start_date
	validate :one_day_chosen
    validate :start_date_schedule
	validate :allowed_start_dates_based_on_year_cycle
	validate :allowed_dates_for_current_year_cycles

	def before_days_per_week
		count = 0

		count += 1 if before_school_monday?
		count += 1 if before_school_tuesday?
		count += 1 if before_school_wednesday?
		count += 1 if before_school_thursday?
		count += 1 if before_school_friday?

		return count
	end

	def after_days_per_week
		count = 0

		count += 1 if after_school_monday?
		count += 1 if after_school_tuesday?
		count += 1 if after_school_wednesday?
		count += 1 if after_school_thursday?
		count += 1 if after_school_friday?

		return count
	end

	def days_per_week
		count = 0

		count += 1 if (before_school_monday? || after_school_monday?)
		count += 1 if (before_school_tuesday? || after_school_tuesday?)
		count += 1 if (before_school_wednesday? || after_school_wednesday?)
		count += 1 if (before_school_thursday? || after_school_thursday?)
		count += 1 if (before_school_friday? || after_school_friday?)

		return count
	end

	def starts_this_or_previous_month(month)
		(start_date.month == month.month) || start_date < month
	end

	private

	def one_day_chosen
		errors.add "", "one day must be chosen" if (!before_school_monday && !before_school_tuesday && !before_school_wednesday && !before_school_thursday && !before_school_friday && !after_school_monday && !after_school_tuesday && !after_school_wednesday && !after_school_thursday && !after_school_friday)
	end

	def start_date_schedule	
		if self.start_date.present?
			unless self.start_date.present?
				errors.add "", "start date must be present. Please try again."
			end
			if self.start_date < Time.now.to_date
				errors.add "", "start date may not be earlier than today. Please try again."
			end
		end
	end
	
	def allowed_start_dates_based_on_year_cycle
		if self.year_cycle == '2018-2019'
			if self.start_date.month > 6
				errors.add '', 'start date may not be beyond June for the selected year cycle.'
			elsif self.start_date.year > 2019
				errors.add '', 'start date for this year cycle may not be beyond this year.'
			end
		end
	end

	def allowed_dates_for_current_year_cycles
		if self.start_date.present?
			if self.year_cycle == '2019-2020'
				if Date.today.year == 2019
					if self.start_date < Date.new(2019, 9, 1)
						errors.add '', 'start date for year cycle 2019-2020 may not be earlier than September 1, 2019.'
					end
				end
			end
		end
	end
end
