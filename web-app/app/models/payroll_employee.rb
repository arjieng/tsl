class PayrollEmployee < ActiveRecord::Base
	belongs_to :payroll_location
	validates :name, uniqueness: {scope: :payroll_location_id}
	has_many :payroll_employee_hours, dependent: :destroy
	has_many :payroll_employee_abscences, dependent: :destroy

	def format_self
		{
			id: self.id,
			name: self.name.titleize
		}
	end

	def logged_hours
		self.payroll_employee_hours
	end

end
