class PayrollEmployeeAbscence < ActiveRecord::Base
    belongs_to :payroll_location
    belongs_to :payroll_employee
end
