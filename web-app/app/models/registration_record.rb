class RegistrationRecord < ActiveRecord::Base
	attr_accessor :referrer,:single_day_date_ids

	has_many :child_records, dependent: :destroy
	accepts_nested_attributes_for :child_records, allow_destroy: true, reject_if: :all_blank
	has_one :single_day_record, dependent: :destroy
	accepts_nested_attributes_for :single_day_record, reject_if: :year_round_single_day_registration
	has_one :before_and_afterschool_record, dependent: :destroy
	accepts_nested_attributes_for :before_and_afterschool_record, reject_if: :promo_only_registration
	has_one :summer_camp_record, dependent: :destroy
	accepts_nested_attributes_for :summer_camp_record
	has_many :vacation_camp_records, dependent: :destroy
	accepts_nested_attributes_for :vacation_camp_records
	has_one :day_care_record, dependent: :destroy
	accepts_nested_attributes_for :day_care_record
	has_one :receipt
	has_one :single_day_date, through: :single_day_record
	has_many :invoices, dependent: :destroy
    accepts_nested_attributes_for :invoices, allow_destroy: true
	has_many :discounts, dependent: :destroy
    has_one :annual_single_day_promo_record, dependent: :destroy
	accepts_nested_attributes_for :annual_single_day_promo_record, reject_if: proc { |attribs| attribs['promo_name'].blank? }
	
	belongs_to :program
	belongs_to :customer

	validates_presence_of :location, :child_records
	validate :agree_to_tos_is_true
	
	after_create :create_summer_camp_invoices, if: Proc.new { |p| p.program.name.include?("Summer Camp") }
	after_create "create_before_and_afterschool_invoice(0)", if: Proc.new { |p| p.program.name.include?("After School") }
	after_create :create_receipt_for_registration, if: :registration_chargeable?

	scope :promo_summer_camp_registration, -> {
		where.not(deleted: true)
		.joins(:summer_camp_record).where.not('summer_camp_records.promo_name is ?', nil)
	}

	scope :before_and_afterschool_records_from_2017_and_onwards, -> {
		where.not(deleted: true)
        .joins(:before_and_afterschool_record)
		.where("extract( year from registration_records.created_at ) >= ?", 2017)
		.includes(:program)
		.where("programs.name=?","After School")
		.references(:program)
	}

	scope :before_and_afterschool_records_from_this_and_last_year, -> { #
		where.not(deleted: true)
		.includes(:before_and_afterschool_record)
		.where("extract (year from before_and_afterschool_records.start_date) < ?", Time.now.to_date.year)
		.references(:before_and_afterschool_records)
	}
	
	scope :single_day_records_from_last_year, -> { #
		where.not(deleted: true)
		.joins(:single_day_record)
		.where("extract (year from single_day_records.created_at) < ?", Time.now.to_date.year)
	}

	scope :summer_camp_records_from_last_year, -> { #
		where.not(deleted: true)
		.joins(:summer_camp_record)
		.where("extract (year from summer_camp_records.created_at) < ?", Time.now.to_date.year - 1)
	}

	scope :vacation_camp_records_from_this_and_last_year, -> { #
		where.not(deleted: true)
		.joins(:vacation_camp_records)
		.joins("inner join vacation_camp_schedules on vacation_camp_records.id = vacation_camp_schedules.id")
		.where("vacation_camp_schedules.year < ?", Time.now.to_date.year)
	}

    def amount_paid_to_dollars
        self.amount_paid.to_i
    end

	def parent_name
		"#{parent_first_name} #{parent_last_name}"
	end

	def child_records_count
		Rails.cache.fetch("/registration_records/#{self.id}/child_records_count", expires_in: 12.hours) { get_child_records_count! }
	end

	def get_child_records_count!
		self.child_records.count
	end

	def address
		address_line_two = address_line_two.present? ? " #{address_line_two}" : ""

		return "#{address_line_one}#{address_line_two}, #{city} #{state}, #{zip_code}"
	end

    def total_amount_including_invoices_due_in(year)
        invoice_total = self.invoices.where('extract (year from due_date) = ?', year).map(&:amount_paid_to_dollars).reduce(:+)
        if invoice_total.nil?
			invoice_total = 0
		end
        
        if self.before_and_afterschool_record.start_date.year == year.to_i
            (self.amount_paid + invoice_total).to_i
        else
            invoice_total.to_i
        end
    end

    def summer_camp_total_amount_including_invoices_due_in(year)
        invoice_total = self.invoices.where('extract (year from due_date) = ?', year).map(&:amount_paid_to_dollars).reduce(:+)
        if invoice_total.nil?
			invoice_total = 0
		end
        
        if self.created_at.year == year.to_i
            (self.amount_paid + invoice_total).to_i
        else
            invoice_total.to_i
        end
    end

	def total_amount_including_invoices 
		invoice_total = self.invoices.map(&:amount_paid_to_dollars).reduce(:+)
		if invoice_total.nil?
			invoice_total = 0
		end
		(self.amount_paid + invoice_total).to_i
	end

	def send_welcome_mailer
		director = program.director.present? ? program.director : program.location.director
		update_attributes(emailed: true, emailed_at: DateTime.now)

		if program.name.include?("Day Care Tour")
			WelcomeMailer.day_care_tour_welcome_email(director, self).deliver!
		elsif !program.name.include?("Day Care")
			WelcomeMailer.welcome_email(director, self).deliver!
		else
			WelcomeMailer.day_care_welcome_email(director, self).deliver!
		end
	end

	def send_tom_email_notification(registration_record_params = nil, params)
		WelcomeMailer.send_tom_email_notification(self, registration_record_params, params).deliver!
	end

	def approve_and_send
		update_attributes(approved: true, approved_at: DateTime.now)
		send_welcome_mailer
	end

	def build_stripe_info(params, registration_record_params)
		if registration_record_params['referrer'] == 'single_day'
			amount = calculate_single_day_amount(registration_record_params)
			description = "Single Day Registration"
		elsif registration_record_params['referrer'] == 'after_school'
			amount = calculate_before_and_afterschool_charge_amount(params)
			description = "Before and Afterschool Registration"
		elsif registration_record_params['referrer'] == 'summer_camp'
            if params[:summer_camp_registration_option] == "summer_camp_promo_registration"
                case params[:promo_name]
                    when 'one_child_nine_weeks'
                        amount = 153900
                    when 'two_children_nine_weeks'
                        amount = 255000
                    when 'three_children_nine_weeks'
                        amount = 332000
                    when 'one_child_five_weeks'
                        amount = 85500
                    when 'two_children_five_weeks'
                        amount = 141800
                    when 'three_children_five_weeks'
                        amount = 186800
                end
            else
                amount = 17500
            end
			description = "Summer Camp Registration"
		elsif registration_record_params['referrer'] == 'vacation_camp'
			amount = calculate_vacation_camp_amount(params)
			description = "Vacation Camp Registration"
		end
		# check if customer has an existing stripe account attached;
		# this also implies that a saved payment source has been saved
		if self.customer && self.customer.stripe_customer_identifier.present?
			stripe_customer = Stripe::Customer.retrieve(self.customer.stripe_customer_identifier)
			# two possibilities are checked here. If a customer has a stripe token yet has an existing bank identifier,
			# it means they attaching a Card as a new payment source; this could be vice-versa
			if params[:stripeToken].present?
				stripe_customer.sources.create({
					:source => params[:stripeToken]
				})
				new_card = stripe_customer.sources.all(:object => "card").first
				self.customer.update_attributes(
					card_brand: new_card.brand,
					card_last_four: new_card.last4
				)
				stripe_customer.default_source = stripe_customer.sources.all(:object => "card").data[0].id
				stripe_customer.save
			elsif params[:public_token].present?
				source = PlaidService.bank_account_token(params[:public_token], params[:account_id])
				stripe_customer.sources.create({
						:source => source
				})
				new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
				self.customer.update_attributes(
					stripe_bank_identifier: new_bank_account.id,
					bank_name: new_bank_account.bank_name,
					bank_verified: true
				)
				stripe_customer.default_source = stripe_customer.sources.all(:object => "bank_account").data[0].id 
				stripe_customer.save
			else # customer using a saved payment account and not adding a new payment source
				if params[:commit] == "Submit And Pay With Card on File"
					stripe_customer.default_source = stripe_customer.sources.all(:object => "card").data[0].id
					stripe_customer.save
				elsif params[:commit] == "Submit And Pay With Bank on File"
					stripe_customer.default_source = stripe_customer.sources.all(:object => "bank_account").data[0].id 
					stripe_customer.save
				end
			end
			##
		else
			# if the customer has no stripe_customer_identifier attribute,
			# create one and bind a card as a source
			if params[:stripeToken].present?
				stripe_customer = Stripe::Customer.create(
					email: params[:stripeEmail],
					source: params[:stripeToken],
					metadata: {
						"customer_id" => self.customer.id,
						"name" => self.customer.name
					}
				)
				self.customer.update_attributes(
					stripe_customer_identifier: stripe_customer.id,
					card_last_four: stripe_customer.sources.first.last4,
					card_brand: stripe_customer.sources.first.brand
				)
			else
				stripe_customer = Stripe::Customer.create(
					email: self.customer.email,
					source: PlaidService.bank_account_token(params[:public_token], params[:account_id]),
					metadata: {
						"customer_id" => self.customer.id,
						"name" => self.customer.name
					}
				)
				new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
				self.customer.update_attributes(
					stripe_customer_identifier: stripe_customer.id,
					stripe_bank_identifier: new_bank_account.id,
					bank_name: new_bank_account.bank_name,
					bank_verified: true
				
				)
			end
		end
		
		charge = Stripe::Charge.create(
			customer: stripe_customer.id,
			amount: amount,
			description: description,
			currency: 'usd',
			metadata: {
				"registration_record_id" => self.id,
				"name": self.customer.name
			}
		)
		payment_url = "#{ENV['STRIPE_DASHBOARD_BASE_URL']}/payments/#{charge.id}"
		customer_url = "#{ENV['STRIPE_DASHBOARD_BASE_URL']}/customers/#{stripe_customer.id}"

		self.customer_identifier = stripe_customer.id
		self.transaction_identifier = charge.id
		self.payment_platform = "Stripe"
		self.payment_url = payment_url
		self.customer_url = customer_url
		self.amount_paid = (amount / 100)
	
		return self
	end

	class << self
		def to_csv
			CSV.generate do |csv|
				csv << ['Parent Name', 'Program', 'Phone Number', 'Emergency Phone Number', 'Address', 'How They Heard About TSL', 'Children']
				all.each do |registration_record|
					children = registration_record.child_records.map { |child_record| "#{child_record.first_name} #{child_record.last_name}, #{child_record.age} years old, attending #{child_record.school_attending}" }.join(" | ")
					csv << [registration_record.parent_name, registration_record.program, registration_record.phone_number, registration_record.emergency_phone_number, registration_record.address, registration_record.how_did_you_hear_about_us, children]
				end
			end
		end

		def filter_by_program(program_filter)
			ids = select { |registration_record| registration_record.program.display_name == program_filter }.map { |registration_record| registration_record.id }
			where(id: ids)
		end

		def ransackable_scopes(auth_object = nil)
			[:filter_by_program]
		end
	end

	def create_summer_camp_invoices
		if self.program.name.include?("Summer Camp") && self.coupon_code != "groupon-2019"
			@invoices_array = []
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 1", due_date: Date.parse('01-07-2019'))) if summer_camp_record.week_one?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 2", due_date: Date.parse('08-07-2019'))) if summer_camp_record.week_two?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 3", due_date: Date.parse('15-07-2019'))) if summer_camp_record.week_three?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 4", due_date: Date.parse('22-07-2019'))) if summer_camp_record.week_four?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 5", due_date: Date.parse('29-07-2019'))) if summer_camp_record.week_five?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 6", due_date: Date.parse('05-08-2019'))) if summer_camp_record.week_six?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 7", due_date: Date.parse('12-08-2019'))) if summer_camp_record.week_seven?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 8", due_date: Date.parse('19-08-2019'))) if summer_camp_record.week_eight?
			@invoices_array.push(self.invoices.create!(amount: calculate_summer_charge, memo: "TSL Summer Camp - Week 9", due_date: Date.parse('26-08-2019'))) if summer_camp_record.week_nine?

			if self.discounts.present?
				self.discounts.each do |discount|
					@invoices_array.each do |invoice|
						invoice.discounts.create!(amount: discount.amount)
					end
				end
			end
			if self.program.present? && self.program.discounts.present?
				self.program.discounts.each do |discount|
					@invoices_array.each do |invoice|
						invoice.discounts.create!(amount: discount.amount)
					end
				end
			end

            self.invoices.where('invoices.amount <= ?', 0).update_all(paid: true)
			InvoicesMailer.summer_camp_creation(self.id, @invoices_array.map{|i| i.id}).deliver! if @invoices_array.count > 0
		end
	end

	def create_yesterday_invoices
		if self.program.name.include?("Day Care")
			# Create Invoice on Monday Every Week if After Start Date
			if self.day_care_record.start_date <= Date.yesterday && Date.yesterday.monday?
				amount = calculate_day_care_charge

				unless self.invoices.where(amount: amount, memo: "TSL Day Care", due_date: Date.yesterday).present?
					@invoice = self.invoices.create!(amount: amount, memo: "TSL Day Care", due_date: Date.yesterday)

					if self.discounts.present?
						self.discounts.each do |discount|
							@invoice.discounts.create!(amount: discount.amount)
						end
					end
					if self.program.present? && self.program.discounts.present?
						self.program.discounts.each do |discount|
							@invoice.discounts.create!(amount: discount.amount)
						end
					end

					if @invoice.registration_record.customer.autopay_enabled?
						# Automatically Charge Customer
						@invoice.autopay_charge_customer
					else
						# Email User To Pay Invoice
						@invoice.send_emails
					end
				end
			end
		end
	end

	def create_daycare_invoices
		if self.program.name.include?("Day Care")
			# Create Invoice on Monday Every Week if After Start Date
            # today.monday? - input this
			if self.day_care_record.start_date <= Date.today && Date.today.monday?
				amount = calculate_day_care_charge

				unless self.invoices.where(amount: amount, memo: "TSL Day Care", due_date: Date.today).present?
					@invoice = self.invoices.create!(amount: amount, memo: "TSL Day Care", due_date: Date.today)

					if self.discounts.present?
						self.discounts.each do |discount|
							@invoice.discounts.create!(amount: discount.amount)
						end
					end
					if self.program.present? && self.program.discounts.present?
						self.program.discounts.each do |discount|
							@invoice.discounts.create!(amount: discount.amount)
						end
					end

					if @invoice.registration_record.customer.autopay_enabled?
						# Automatically Charge Customer
						@invoice.autopay_charge_customer
					else
						# Email User To Pay Invoice
						@invoice.send_emails
					end
				end
			end
		end
	end

	def create_before_and_afterschool_invoice(pa = nil)
		if self.program.name.include?("After School") && self.before_and_afterschool_record.present?
			if !(Date::MONTHNAMES[Date.today.month] == "July" || Date::MONTHNAMES[Date.today.month] == "August") || pa.present?
				if self.program.location.name.include?("East Greenbush")
                    if self.before_and_afterschool_record.year_cycle == '2018-2019' || self.before_and_afterschool_record.year_cycle == nil
                        amount = previous_year_eg_before_and_afterschool_charge
                    else
                        amount = calculate_eg_before_and_afterschool_charge
					end
					memo = "TSL Before and Afterschool"
				elsif self.program.location.name.include?("Lansingburgh")
					amount = calculate_lansingburgh_before_and_afterschool_charge
					memo = "TSL Before and Afterschool"
				elsif ["Cohoes", "Abram Lansing", "Harmony Hill", "Van Schaik", ].any? {|el| el == self.program.location.name }
                    if self.before_and_afterschool_record.year_cycle == '2018-2019' || self.before_and_afterschool_record.year_cycle == nil
                        amount = previous_year_cohoes_before_and_afterschool_charge 
                    else
                        amount = calculate_cohoes_before_and_afterschool_charge
                    end
					memo = "TSL Before and Afterschool"
				else
                    if self.before_and_afterschool_record.year_cycle == '2018-2019' || self.before_and_afterschool_record.year_cycle == nil
                        amount = previous_year_before_and_afterschool_charge
                    else
                        amount = calculate_before_and_afterschool_charge 
                    end
					memo = "TSL Before and Afterschool"
				end

				# since this function is called back after a registration_record instance is created,
				# the application must set the invoice due_date to be the 15th of the same month for days 1-15;
				# for 16-31, the due date should be set to the 30th or 31st
				# Otherwise, the scheduler will invoke this function for all invoices
				# every first of the month and have the deadline set on the fifteenth
				if self.created_at.month == DateTime.now.to_date.month && self.created_at.year ==  DateTime.now.to_date.year # check if the record is newly created
					month = self.before_and_afterschool_record.start_date.to_date
				else
					month = DateTime.now.to_date.beginning_of_month
				end

				# check whether this is a new After-School registration; if it is, then
				# generate a discount for the amount that was paid as deposit on the first invoice
				if self.created_at.month == DateTime.now.to_date.month && self.created_at.year ==  DateTime.now.to_date.year
					start_date = self.before_and_afterschool_record.start_date.to_date
					if start_date.day == start_date.beginning_of_month.day
						due_date = start_date.beginning_of_month
					else
						# due_date = start_date + 3.days
						if (start_date + 3.days).month == start_date.month
							due_date = start_date + 3.days
						else
							due_date = start_date.end_of_month
						end
					end
					# if self.before_and_afterschool_record.
					if self.before_and_afterschool_record.no_deposit_required
						deposit_amount = 0
					else
						deposit_amount = 10000
					end
					invoice = self.invoices.create!(amount: amount - deposit_amount, memo: memo, due_date: due_date)
				else
					if self.before_and_afterschool_record.year_cycle == '2018-2019' || self.before_and_afterschool_record.year_cycle == nil
						due_date = Date.today.beginning_of_month + 14.days
					else
						due_date = Date.today.beginning_of_month
					end
					invoice = self.invoices.create!(amount: amount, memo: memo, due_date: due_date)
				end

				# create discounts for the invoice if there are any

				if self.discounts.present?
					self.discounts.each do |discount|
						invoice.discounts.create!(amount: discount.amount)
					end
				end
				if self.program.present? && self.program.discounts.present?
					self.program.discounts.each do |discount|
						invoice.discounts.create!(amount: discount.amount)
					end
				end
				
				new_invoices = []
				new_invoices.push(invoice)
				InvoicesMailer.before_and_afterschool_monthly_invoice(self.id, new_invoices.map{|i| i.id}).deliver! if new_invoices.count > 0
			end
		end
	end

	def calculate_day_care_charge
		amount = 0
		dateTimeNow = DateTime.now.to_date
		dateTimeSept2Mon = "Mon, 2 September 2019".to_date

		self.child_records.each_with_index do |child_record, index|
			# Give a 10% discount on first child if more than one is enrolled
			if self.child_records.count > 1 && index == 0
				percent_multiplier = 0.9
			else
				percent_multiplier = 1
			end

			child_record_amount = (dateTimeNow >= dateTimeSept2Mon ? 27000 : 26000) * percent_multiplier

			amount += child_record_amount.to_i
		end

		return amount
	end

	def calculate_summer_charge
		# unlike normal locations, CIT applicants have a flat $150 weekly summer camp invoice per child
		if self.invoices.select{|invoice| invoice.memo.include?("Summer Camp")}.present?
			discount = 0
		else
            discount = 15000
		end
        
        unless self.location == "Counselors in Training"
            if self.child_records.count == 1
                case self.summer_camp_record.amount_of_weeks
                    when 1
                        22000 - discount
                    when 2
                        21000 - discount
                    when 3
                        20000 - discount
                    when 4
                        19500 - discount
                    else
                        19000 - discount
                end
            elsif self.child_records.count == 2
                case self.summer_camp_record.amount_of_weeks
                when 1
                    35000 - discount
                when 2
                    34000 - discount
                when 3
                    33000 - discount
                when 4
                    32000 - discount
                else
                    31500 - discount
                end
            elsif self.child_records.count >= 3
                case self.summer_camp_record.amount_of_weeks
                when 1
                    43500 - discount
                when 2
                    42500 - discount
                when 3
                    41500 - discount
                when 4
                    41000 - discount
                else
                    41000 - discount
                end
            end
        else
            if self.child_records.count == 1
                15000
            elsif self.child_records.count == 2
                30000
            elsif self.child_records.count >= 3
                45000
            end
        end
	end

	def calculate_before_and_afterschool_charge
		if self.before_and_afterschool_record.before_days_per_week > 0 && self.before_and_afterschool_record.after_days_per_week > 0
			type = "both"
		elsif self.before_and_afterschool_record.before_days_per_week > 0
			type = "before"
		elsif self.before_and_afterschool_record.after_days_per_week > 0
			type = "after"
		end

		if child_records.count == 1
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					18000 # 1 child for up to 6 days
				elsif days_a_week <= 3 
					27500 # up to 12 days
				else
					38500 # 
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					8000
				elsif days_a_week <= 3
					10000
				else
					15000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					15000
				elsif days_a_week <= 3
					21500
				else
					28000
				end
			end
		elsif child_records.count == 2
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					27000
				elsif days_a_week <= 3
					41500
				else
					57000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					12500
				elsif days_a_week <= 3
					17500
				else
					22500
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					22500
				elsif days_a_week <= 3
					32500
				else
					49000
				end
			end
		elsif child_records.count >= 3
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					35000
				elsif days_a_week <= 3
					55000
				else
					60000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					17500
				elsif days_a_week <= 3
					22000
				else
					30000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					30000
				elsif days_a_week <= 3
					43000
				else
					56000
				end
			end
		end
	end

    # standard rates for most locations
    def previous_year_before_and_afterschool_charge
		if self.before_and_afterschool_record.before_days_per_week > 0 && self.before_and_afterschool_record.after_days_per_week > 0
			type = "both"
		elsif self.before_and_afterschool_record.before_days_per_week > 0
			type = "before"
		elsif self.before_and_afterschool_record.after_days_per_week > 0
			type = "after"
		end

		if child_records.count == 1
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					14000 # 1 child for up to 6 days
				elsif days_a_week <= 3 
					28500 # up to 12 days
				else
					30500 # 
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					4000
				elsif days_a_week <= 3
					7600
				else
					9500
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					11500
				elsif days_a_week <= 3
					20000
				else
					26500
				end
			end
		elsif child_records.count == 2
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					20000
				elsif days_a_week <= 3
					40000
				else
					47500
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					5700
				elsif days_a_week <= 3
					9500
				else
					15000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					17000
				elsif days_a_week <= 3
					32500
				else
					43500
				end
			end
		elsif child_records.count >= 3
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					28500
				elsif days_a_week <= 3
					43000
				else
					49500
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					6500
				elsif days_a_week <= 3
					11400
				else
					18000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					25500
				elsif days_a_week <= 3
					38000
				else
					47500
				end
			end
		end
	end
    # end of standard rates for most locations

	def calculate_eg_before_and_afterschool_charge
		if self.before_and_afterschool_record.before_days_per_week > 0 && self.before_and_afterschool_record.after_days_per_week > 0
			type = "both"
		elsif self.before_and_afterschool_record.before_days_per_week > 0
			type = "before"
		elsif self.before_and_afterschool_record.after_days_per_week > 0
			type = "after"
		end

		if child_records.count == 1
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					20000
				elsif days_a_week <= 3
					29500
				else
					34000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					12000
				elsif days_a_week <= 3
					15000
				else
					18000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					15000
				elsif days_a_week <= 3
					19000
				else
					21000
				end
			end
		elsif child_records.count == 2
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					30000
				elsif days_a_week <= 3
					39500
				else
					45000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					19000
				elsif days_a_week <= 3
					22500
				else
					27000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					20000
				elsif days_a_week <= 3
					27000
				else
					30500
				end
			end
		elsif child_records.count >= 3
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					35000
				elsif days_a_week <= 3
					46000
				else
					53000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					25000
				elsif days_a_week <= 3
					30000
				else
					36000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					25000
				elsif days_a_week <= 3
					36000
				else
					40000
				end
			end
		end
	end

    # eg previous year rates
    def previous_year_eg_before_and_afterschool_charge
		if self.before_and_afterschool_record.before_days_per_week > 0 && self.before_and_afterschool_record.after_days_per_week > 0
			type = "both"
		elsif self.before_and_afterschool_record.before_days_per_week > 0
			type = "before"
		elsif self.before_and_afterschool_record.after_days_per_week > 0
			type = "after"
		end

		if child_records.count == 1
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					16000
				elsif days_a_week <= 3
					24500
				else
					33000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					9500
				elsif days_a_week <= 3
					14000
				else
					17000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					11000
				elsif days_a_week <= 3
					17000
				else
					19000
				end
			end
		elsif child_records.count == 2
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					28500
				elsif days_a_week <= 3
					30500
				else
					38000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					18000
				elsif days_a_week <= 3
					19000
				else
					23500
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					17000
				elsif days_a_week <= 3
					22500
				else
					28500
				end
			end
		elsif child_records.count >= 3
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					29500
				elsif days_a_week <= 3
					37000
				else
					47500
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					19000
				elsif days_a_week <= 3
					22500
				else
					28500
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					19000
				elsif days_a_week <= 3
					27500
				else
					33000
				end
			end
		end
	end
    #

	def calculate_lansingburgh_before_and_afterschool_charge
		days_a_week = self.before_and_afterschool_record.after_days_per_week

		if child_records.count == 1
			if days_a_week == 1
				8000
			elsif days_a_week <= 3
				14000
			else
				18000
			end
		elsif child_records.count == 2
			if days_a_week == 1
				14000
			elsif days_a_week <= 3
				18000
			else
				24000
			end
		elsif child_records.count == 3
			if days_a_week == 1
				16000
			elsif days_a_week <= 3
				21000
			else
				28000
			end
		end
	end

	def calculate_cohoes_before_and_afterschool_charge
		if self.before_and_afterschool_record.before_days_per_week > 0 && self.before_and_afterschool_record.after_days_per_week > 0
			type = "both"
		elsif self.before_and_afterschool_record.before_days_per_week > 0
			type = "before"
		elsif self.before_and_afterschool_record.after_days_per_week > 0
			type = "after"
		end

		if child_records.count == 1
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					20000 # 1 child for up to 6 days
				elsif days_a_week <= 3 
					29500 # up to 12 days
				else
					34000 # 
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					12000
				elsif days_a_week <= 3
					15000
				else
					18000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					15000
				elsif days_a_week <= 3
					19000
				else
					21000
				end
			end
		elsif child_records.count == 2
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					30000
				elsif days_a_week <= 3
					39500
				else
					45000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					19000
				elsif days_a_week <= 3
					22500
				else
					27000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1 
					20000
				elsif days_a_week <= 3
					27000
				else
					30500
				end
			end
		elsif child_records.count >= 3
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					35000
				elsif days_a_week <= 3
					46000
				else
					53000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					25000
				elsif days_a_week <= 3
					30000
				else
					36000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					25000
				elsif days_a_week <= 3
					36000
				else
					40000
				end
			end
		end

	end

    # previous year cohoes rates
    def previous_year_cohoes_before_and_afterschool_charge
		if self.before_and_afterschool_record.before_days_per_week > 0 && self.before_and_afterschool_record.after_days_per_week > 0
			type = "both"
		elsif self.before_and_afterschool_record.before_days_per_week > 0
			type = "before"
		elsif self.before_and_afterschool_record.after_days_per_week > 0
			type = "after"
		end

		if child_records.count == 1
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					12000 # 1 child for up to 6 days
				elsif days_a_week <= 3 
					20000 # up to 12 days
				else
					28000 # 
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					5000
				elsif days_a_week <= 3
					10000
				else
					13000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					8000
				elsif days_a_week <= 3
					14000
				else
					18000
				end
			end
		elsif child_records.count == 2
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					22000
				elsif days_a_week <= 3
					29000
				else
					35000
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					10000
				elsif days_a_week <= 3
					13000
				else
					19000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1 
					14000
				elsif days_a_week <= 3
					18000
				else
					24000
				end
			end
		elsif child_records.count >= 3
			case type
			when "both"
				days_a_week = self.before_and_afterschool_record.days_per_week

				if days_a_week == 1
					25000
				elsif days_a_week <= 3
					34000
				else
					42500
				end
			when "before"
				days_a_week = self.before_and_afterschool_record.before_days_per_week

				if days_a_week == 1
					12000
				elsif days_a_week <= 3
					16000
				else
					23000
				end
			when "after"
				days_a_week = self.before_and_afterschool_record.after_days_per_week

				if days_a_week == 1
					16000
				elsif days_a_week <= 3
					21000
				else
					28000
				end
			end
		end

	end
    # end of previous year cohoes rates

	def calculate_vacation_camp_amount(params)
		children_added =  params[:registration_record][:child_records_attributes].count
		number_of_camps = params[:registration_record][:attendance].length
		date_september_15 = "September 15, 2019".to_date
		date_today = DateTime.now.to_date
		if date_september_15 <= date_today
			if number_of_camps == 1
				if params[:registration_record][:attendance][:december].present?
					if children_added == 1
						30000
					elsif children_added == 2 
						50000
					elsif children_added == 3
						65000
					end
				else
					if children_added == 1
						19000
					elsif children_added == 2 
						31500
					elsif children_added == 3
						40000
					end
				end

			elsif number_of_camps == 2
				if params[:registration_record][:attendance][:december].present?
					if children_added == 1
						57000
					elsif children_added == 2 
						94500
					elsif children_added == 3
						120000
					end
				else
					if children_added == 1
						38000
					elsif children_added == 2 
						63000
					elsif children_added == 3
						80000
					end
				end
				
			elsif number_of_camps == 3
				if params[:registration_record][:attendance][:december].present?
					if children_added == 1
						48500
					elsif children_added == 2 
						80000
					elsif children_added == 3
						99900
					end
				else
					if children_added == 1
						57000
					elsif children_added == 2 
						94500
					elsif children_added == 3
						120000
					end
				end
			end
		else
			if number_of_camps == 1
				if children_added == 1
					15000
				elsif children_added == 2 
					20000
				elsif children_added == 3
					27500
				end
			elsif number_of_camps == 2
				if children_added == 1
					30000
				elsif children_added == 2 
					40000
				elsif children_added == 3
					55000
				end
			elsif number_of_camps == 3
				if children_added == 1
					40000
				elsif children_added == 2 
					55000
				elsif children_added == 3
					77500
				end
			end
		end
	end

	def calculate_single_day_amount(registration_record_params)
		date_september_2 = "September 2, 2019".to_date
		date_today = DateTime.now.to_date


		if registration_record_params[:single_day_record_attributes][:registration_type] == 'promo_registration'
			# if date_september_2 <= date_today
				# after
				
			# else
				# before
				case self.annual_single_day_promo_record.promo_name
					when 'one_child_annual_single_day'
						40000
					when 'two_children_annual_single_day'
						70000
					when 'three_children_annual_single_day'
						90000
				end
			# end
		else
			if date_september_2 <= date_today
				# after
				if registration_record_params[:child_records_attributes].count == 1
					5000
				elsif registration_record_params[:child_records_attributes].count == 2
					8500
				elsif registration_record_params[:child_records_attributes].count == 3
					10000
				else
					5000
				end
			else
				# before
				if registration_record_params[:child_records_attributes].count == 1
					4500
				elsif registration_record_params[:child_records_attributes].count == 2
					7500
				elsif registration_record_params[:child_records_attributes].count == 3
					9500
				else
					4500
				end
			end
		end
	end

	def create_vacation_camp_records(attendance, registration_record_params)
		check_if_schedule_not_full(attendance, registration_record_params)
		attendance.each do |vacation_camp_schedule_month, vacation_camp_schedule_id|
			if vacation_camp_schedule_month == "december"
				self.vacation_camp_records.new(
					month: "december",
					wednesday: true,
					thursday: true,
					friday: true,
					vacation_camp_schedule_id: vacation_camp_schedule_id
				)
			elsif vacation_camp_schedule_month == "february"
				self.vacation_camp_records.new(
					month: "february",
					monday: true,
					tuesday: true,
					wednesday: true,
					thursday: true,
					friday: true,
					vacation_camp_schedule_id: vacation_camp_schedule_id
				)
			elsif vacation_camp_schedule_month == "april"
				self.vacation_camp_records.new(
					month: "april",
					monday: true,
					tuesday: true,
					wednesday: true,
					thursday: true,
					friday: true,
					vacation_camp_schedule_id: vacation_camp_schedule_id
				)
			else 
				self.vacation_camp_records.new(
					month: vacation_camp_schedule_month,
					monday: true,
					tuesday: true,
					wednesday: true,
					thursday: true,
					friday: true,
					vacation_camp_schedule_id: vacation_camp_schedule_id
				)
			end
		end
	end

	private
	def agree_to_tos_is_true
		errors.add "Agree to Terms of Service", "must be checked" if self.agree_to_tos != true
	end

	def check_if_schedule_not_full(attendance, registration_record_params)
		if attendance.present? # check if at least one vacation camp schedule has been selected
			one_of_the_schedules_is_full = false
			attendance.each do |vacation_camp_schedule_month, vacation_camp_schedule_id|
				schedule = VacationCampSchedule.find_by(id: vacation_camp_schedule_id)
				capacity = schedule.capacity
				enrollment_count = schedule.enrollment_count
				child_records_count = registration_record_params[:child_records_attributes].count
				if (enrollment_count + child_records_count) > capacity
					one_of_the_schedules_is_full = true
				end
			end
			if one_of_the_schedules_is_full
				raise ". One of the vacation camp schedules you have tried to enroll to is either already full or" +
				" will exceed the maximum capacity. Please try again."
			end
		else
			raise ". You haven't selected at least one Vacation Camp Schedule. Please try again."
		end
	end

	def create_receipt_for_registration
		payment_for = ''
		if self.program.name == 'Single Day'
			if self.single_day_record.present?
				payment_for = 'Single Day Child Care Service'
			else
				payment_for = 'Year-round Single Days of Care Service Deposit'
			end
		elsif self.program.name == 'Vacation Camp'
			payment_for = 'Vacation Camp Child Care Service'
		elsif self.program.name == 'After School'
            if self.before_and_afterschool_record.present? && self.annual_single_day_promo_record == nil
                payment_for = 'Before and Afterschool Registration Deposit'
            elsif self.before_and_afterschool_record.present? && self.annual_single_day_promo_record.present?
                payment_for = 'Before and Afterschool and Annual Single Day Care Registration Deposit'
            elsif self.before_and_afterschool_record == nil && self.annual_single_day_promo_record.present?
                payment_for = 'Annual Single Day Care Registration Deposit'
            end
		elsif self.program.name == 'Summer Camp'
            if self.summer_camp_record.promo_name.present?
                payment_for = 'Summer Camp Registration Fee'
            else
                payment_for = 'Summer Camp Registration Deposit'
            end
		end
		new_receipt = Receipt.create(
			registration_record_id: self.id,
			customer_id: self.customer.id,
			amount: self.amount_paid,
			program_name: self.program.name,
			payment_for: payment_for
		)
		self.receipt = new_receipt
	end

	def registration_chargeable?
		programs = ["Single Day", "Vacation Camp", "After School", "Summer Camp"]
		if programs.any? {|prog| prog == self.program.name}
			true
		else
			false
		end
	end

    def promo_only_registration attributes
        attributes['registration_type'] == 'promo_only_registration'
    end

	def calculate_before_and_afterschool_charge_amount(params)
        return 12500 if self.annual_single_day_promo_record == nil
        case self.annual_single_day_promo_record.promo_name
            when 'one_child_annual_single_day'
                amount = 40000
            when 'two_children_annual_single_day'
                amount = 70000
            when 'three_children_annual_single_day'
                amount = 90000
		end
		deposit_amount = params[:registration_record][:no_deposit_required].present? ? 0 : 12500
        self.before_and_afterschool_record.present? ? amount += deposit_amount : amount
	end
	
	def year_round_single_day_registration attributes
		attributes['registration_type'] == 'promo_registration'
	end
end
