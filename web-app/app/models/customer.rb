class Customer < ActiveRecord::Base
	attr_accessor :account_holder_name, :account_number, :routing_number, :amount_one, :amount_two

	has_many :registration_records, dependent: :destroy
	has_many :invoices, through: :registration_records
	has_one :customer_user
	has_many :receipts
	has_many :contact_informations
	has_many :orders
	has_many :purchases

	accepts_nested_attributes_for :contact_informations, allow_destroy: true
	validates_uniqueness_of :email

	def registration_records_count
		Rails.cache.fetch("/customers/#{self.id}/registration_records_count", expires_in: 12.hours) { get_registration_records_count! }
	end

	def get_registration_records_count!
		self.registration_records.count
	end

	def name
		"#{first_name} #{last_name}"
	end

	def address
		address_line_two = address_line_two.present? ? " #{address_line_two}" : ""

		return "#{address_line_one}#{address_line_two}, #{city} #{state}, #{zip_code}"
	end

	def has_complete_information?
		if email.present? && first_name.present? && last_name.present? && phone_number.present? && emergency_phone_number.present? && address_line_one.present? && city.present? && state.present? && zip_code.present? && how_did_you_hear_about_us.present?
			true
		else
			false
		end
	end

	def single_day_records(year)
		self.registration_records.includes(:single_day_record)
			.where("extract (year from single_day_records.created_at) = ?", year)
			.order("single_day_records.created_at DESC")
	end

	def day_care_records(year)
		self.registration_records.includes(:day_care_record)
			.where("extract (year from day_care_records.start_date) = ? ", year)
			.order("day_care_records.start_date DESC")
	end

	def before_and_afterschool_records(year)
		self.registration_records.joins(:before_and_afterschool_record)
			.where("extract (year from before_and_afterschool_records.start_date) in (?) ", [year, year.to_i - 1])
			.order("before_and_afterschool_records.start_date DESC")
	end

	def vacation_camp_records(year)
		self.registration_records.joins(:vacation_camp_records)
			.where("extract (year from registration_records.created_at) = ?", year)
			.distinct
	end

	def summer_camp_records(year)
		self.registration_records.joins(:summer_camp_record)
			.where("extract (year from registration_records.created_at) in (?)", [year, year.to_i - 1])
	end

    def annual_single_day_records(year)
        self.registration_records.joins(:annual_single_day_promo_record)
            .where('extract (year from annual_single_day_promo_records.created_at) = ?', year)
            .distinct
    end
end
