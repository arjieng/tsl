class Purchase < ActiveRecord::Base

    belongs_to :customer
	has_one :receipt
	belongs_to :location

	validates :orders, presence: true

	after_create :create_purchase_receipt
	after_create :update_product_quantity
	def create_stripe_charge_without_customer(params)
		puts self.inspect
		puts "0000000000000000000"
		charge = Stripe::Charge.create({
		amount: self.total_amount_in_cents,
		currency: 'usd',
		description: "Payment for Order purchase of TSL Store items",
		source: params[:stripeToken],
		})
		puts charge
		email = params[:email]
		first_name = charge.billing_details.name.reverse.split(" ", 2).map(&:reverse).reverse[0]
		last_name = charge.billing_details.name.reverse.split(" ", 2).map(&:reverse).reverse[1]
		@customer = Customer.create(email: email, first_name: first_name, last_name: last_name)
			@customer.update_attributes(email: @customer.email + "_#{@customer.id}")
		self.customer_id = @customer.id
	end
    def create_stripe_charge(params)
        # check if customer has an existing stripe account attached;
		# this also implies that a saved payment source has been saved
		if self.customer && self.customer.stripe_customer_identifier.present?
			stripe_customer = Stripe::Customer.retrieve(self.customer.stripe_customer_identifier)
			# two possibilities are checked here. If a customer has a stripe token yet has an existing bank identifier,
			# it means they attaching a Card as a new payment source; this could be vice-versa
			if params[:stripeToken].present?
				stripe_customer.sources.create({
					:source => params[:stripeToken]
				})
				new_card = stripe_customer.sources.all(:object => "card").first
				self.customer.update_attributes(
					card_brand: new_card.brand,
					card_last_four: new_card.last4
				)
				stripe_customer.default_source = stripe_customer.sources.all(:object => "card").data[0].id
				stripe_customer.save
			elsif params[:public_token].present?
				source = PlaidService.bank_account_token(params[:public_token], params[:account_id])
				stripe_customer.sources.create({
						:source => source
				})
				new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
				self.customer.update_attributes(
					stripe_bank_identifier: new_bank_account.id,
					bank_name: new_bank_account.bank_name,
					bank_verified: true
				)
				stripe_customer.default_source = stripe_customer.sources.all(:object => "bank_account").data[0].id 
				stripe_customer.save
			else # customer using a saved payment account and not adding a new payment source
				if params[:commit] == "Submit And Pay With Card on File"
					stripe_customer.default_source = stripe_customer.sources.all(:object => "card").data[0].id
					stripe_customer.save
				elsif params[:commit] == "Submit And Pay With Bank on File"
					stripe_customer.default_source = stripe_customer.sources.all(:object => "bank_account").data[0].id 
					stripe_customer.save
				end
			end
			##
		else
			# if the customer has no stripe_customer_identifier attribute,
			# create one and bind a card as a source
			if params[:stripeToken].present?
				stripe_customer = Stripe::Customer.create(
					email: params[:stripeEmail],
					source: params[:stripeToken],
					metadata: {
						"customer_id" => self.customer.id,
						"name" => self.customer.name
					}
				)
				self.customer.update_attributes(
					stripe_customer_identifier: stripe_customer.id,
					card_last_four: stripe_customer.sources.first.last4,
					card_brand: stripe_customer.sources.first.brand
				)
			else
				stripe_customer = Stripe::Customer.create(
					email: self.customer.email,
					source: PlaidService.bank_account_token(params[:public_token], params[:account_id]),
					metadata: {
						"customer_id" => self.customer.id,
						"name" => self.customer.name
					}
				)
				new_bank_account = stripe_customer.sources.all(:object => "bank_account").first
				self.customer.update_attributes(
					stripe_customer_identifier: stripe_customer.id,
					stripe_bank_identifier: new_bank_account.id,
					bank_name: new_bank_account.bank_name,
					bank_verified: true
				)
			end
        end
        
        charge = Stripe::Charge.create(
			customer: stripe_customer.id,
			amount: self.total_amount_in_cents,
			description: "Payment for Order purchase of TSL Store items",
			currency: 'usd',
			metadata: {
				"name": self.customer.name
			}
		)

		self.payment_url = "#{ENV['STRIPE_DASHBOARD_BASE_URL']}/payments/#{charge.id}"
		self.transaction_identifier = charge.id
		return self
    end

	def update_order_status_to_purchased
        Order.where(id: self.orders.split(',')).update_all({purchased: true})
    end
	def update_order_status_to_purchased_without_user
        Order.where(id: self.orders.split(',')).update_all({purchased: true,customer_id: self.customer_id})
    end
    def total_amount_in_cents
        (self.total_amount.to_i) * 100
	end

	def order_details 
		orders = Order.where(id: self.orders.split(','))
	end
	
	private

	def create_purchase_receipt
		if self.customer.present?
		receipt = Receipt.create!(
			customer_id: self.customer.id,
			amount: self.total_amount,
			program_name: "TSL Online Store",
			payment_for: "Purchase of TSL store items"
		)
		self.receipt = receipt
		end
	end

	def update_product_quantity
		self.order_details.each do |order|
			size = order.size
			size.update_attribute(:quantity, size.quantity - order.quantity)
		end
	end
end
