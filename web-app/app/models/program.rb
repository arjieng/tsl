class Program < ActiveRecord::Base
  	belongs_to :director, class_name: "User"
	belongs_to :location
	has_many :registration_records, dependent: :destroy
	has_many :child_records, through: :registration_records
	has_many :discounts, dependent: :destroy
	has_many :single_day_dates, dependent: :destroy
	accepts_nested_attributes_for :single_day_dates, allow_destroy: true, reject_if: :all_blank
	has_many :summer_camp_weeks, dependent: :destroy
	accepts_nested_attributes_for :summer_camp_weeks, allow_destroy: true, reject_if: :all_blank
	has_many :vacation_camp_schedules, dependent: :destroy
	accepts_nested_attributes_for :vacation_camp_schedules, allow_destroy: true, reject_if: :all_blank
	has_one :before_and_after_school_program_configuration, dependent: :destroy
	accepts_nested_attributes_for :before_and_after_school_program_configuration
  	validates_presence_of :name, :location, :capacity
	validates_uniqueness_of :name, scope: :location

	after_create :create_before_and_after_school_program_config, if: :before_and_after_school_program?

	def display_name
		"#{location.name} #{name}"
	end

	def has_top_level_capacity?
		if name == "After School" || name == "Day Care"
			true
		else
			false
		end
	end

	def enrolled_count
		Rails.cache.fetch("/programs/#{self.id}/enrolled_count", expires_in: 12.hours) { get_enrolled_count! }
	end

	def get_enrolled_count!
		registration_records.where(enrolled: true).to_a.sum(&:child_records_count)
	end

	def vacation_camp_enrollment_count
		vacation_camp_enrollment_count = vacation_camp_schedules.inject(0) do |sum, schedule|
			sum += schedule.enrollment_count
		end
	end

    def after_school_registrations(request_type = 'enrollment_count')
        aft_recs = self.registration_records.includes(:before_and_afterschool_record)
            .where('extract (year from before_and_afterschool_records.start_date) >= ?', 2019)
            .references(:before_and_afterschool_records)
        result = aft_recs.inject([]) do |acc, curr|
            bafs_start_date = curr.before_and_afterschool_record.start_date
            if bafs_start_date.year == 2019
                if bafs_start_date.month >= 9
                    acc << curr
                else
                    acc
                end
            else
                acc << curr
            end
        end
        request_type == 'enrollment_count' ? result.sum(&:child_records_count) : result
	end
	
	def daycare_records
		self.registration_records.includes(:program)
		.where('programs.name = ?', 'Day Care')
		.references(:programs)
		.order('registration_records.created_at DESC')
	end

	private

	def create_before_and_after_school_program_config
		before_and_after_school_config = BeforeAndAfterSchoolProgramConfiguration.create
		self.before_and_after_school_program_configuration = before_and_after_school_config
	end

	def before_and_after_school_program?
		if self.name == "After School"
			true
		else
			false
		end
	end

end
