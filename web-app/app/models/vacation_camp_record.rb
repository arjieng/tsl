class VacationCampRecord < ActiveRecord::Base
	belongs_to :registration_record
	belongs_to :vacation_camp_schedule
	validates_presence_of :month
end
