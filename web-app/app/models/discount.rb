class Discount < ActiveRecord::Base
	belongs_to :invoice
	belongs_to :registration_record
	belongs_to :program

	def amount_in_cents
		amount.to_f * 100.0
	end
end
