class Receipt < ActiveRecord::Base
    belongs_to :registration_record
    belongs_to :invoice
    belongs_to :customer
    belongs_to :purchase

    def format_self
        {
            id: self.id,
            amount: "$#{self.amount.to_i}",
            program_name: self.program_name,
            payment_for: self.payment_for
        }
    end
    
end
