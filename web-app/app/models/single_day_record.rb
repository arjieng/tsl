class SingleDayRecord < ActiveRecord::Base
	belongs_to :registration_record
	belongs_to :single_day_date
	validates_presence_of :single_day_date_id

	def formatted_day
		day.present? ? day.underscore.split('_').collect{|c| c.capitalize}.join(' ') : nil
	end

	def day
		self.created_at.to_date.to_formatted_s(:long)
	end
end
