class Invoice < ActiveRecord::Base
	attr_accessor :enable_autopay

	belongs_to :registration_record
	has_many :discounts, dependent: :destroy
	has_many :fees, dependent: :destroy
	accepts_nested_attributes_for :fees
	has_many :receipts
	has_one :program, through: :registration_record
	has_one :customer, through: :registration_record
	validates_presence_of :amount, :memo, :due_date

	scope :due_today, -> { 
		where("extract (year from created_at ) >= ?", 2017) 
		.where(paid: false).where('due_date = ?', Date.today)
	}

	scope :overdue_on_the_fifth_or_earlier, -> {
		includes(:program)
		.references(:program)
		.where("extract (year from invoices.created_at ) >= ?", 2017)
		.where(paid: false).where('extract (month from due_date) = ?', (Date.today.month))
		.where('extract (year from due_date) = ?', (Date.today.year))
		.where('extract (day from due_date) <= ?', 5)
		.where('programs.name = ?', 'After School')
	}

	scope :overdue_by_one_day, -> {
		where("extract (year from created_at ) >= ?", 2017)
		.where(paid: false).where('due_date = ?', Date.today - 1.day)	
	}

	scope :overdue_by_two_weeks, -> { 
		where("extract (year from created_at ) >= ?", 2017)
		.where(paid: false).where('due_date = ?', (Date.today - 2.weeks)) 
	}

	scope :overdue_this_month, -> {
		where("extract (year from invoices.created_at ) >= ?", 2017)
		.where(paid: false).where('extract (month from due_date) = ?', (Date.today.month))
		.where('extract (year from due_date) = ?', (Date.today.year))
		.joins(:program)
        .where.not("programs.name = ?", "Summer Camp")
	}

	scope :overdue_last_month, -> {
		where("extract (month from due_date) = ?", Date.today.last_month.month)
		.where("extract (year from due_date) = ?", Date.today.last_month.year)
		.where(paid: false)
	}

	scope :overdue_afterschool_this_month, -> {
		where("extract (month from due_date) = ?", Date.today.month)
		.where("extract (year from due_date) = ?", Date.today.year)
		.where(paid: false).joins(:program)
		.where("programs.name = ?", "After School")
	}

	scope :unpaid_afterschool_this_month, -> {
		where("extract (month from due_date) = ?", Date.today.month)
		.where("extract (year from due_date) = ?", Date.today.year)
		.where('extract (day from due_date) <= 15')
		.where(paid: false).joins(:program)
		.where("programs.name = ?", "After School")
	}
	
    scope :due_summer_camp_invoices, -> {
        where(paid: false)
        .joins(:registration_record)
        .where("registration_records.deleted = ?", false)
        .where("extract (year from invoices.created_at ) >= ?", 2017)
        .where("extract (day from invoices.due_date) = ?", Date.today.day)
        .where("extract (month from invoices.due_date) = ?", Date.today.month)
		.where("extract (year from invoices.due_date) = ?", Date.today.year)
        .joins(:program)
        .where("programs.name = ?", "Summer Camp")
    }
	scope :weekly_overdue_summer_camp_invoices, -> {
        where(paid: false)
        .joins(:registration_record)
        .where("registration_records.deleted = ?", false)
        .where("extract (year from invoices.created_at ) >= ?", 2017)
        .where("extract (day from invoices.due_date) = ?", Date.today.day)
        .where("extract (month from invoices.due_date) = ?", Date.today.month)
		.where("extract (year from invoices.due_date) = ?", Date.today.year)
        .joins(:program)
        .where("programs.name = ?", "Summer Camp")
    }
    scope :overdue_summer_camp_invoices, -> {
        where(paid: false)
        .joins(:registration_record)
        .where("registration_records.deleted = ?", false)
        .where("extract (year from invoices.created_at ) >= ?", 2017)
        .where("extract (day from invoices.due_date) = ?", (Date.today - 2.days).day)
        .where("extract (month from invoices.due_date) = ?", Date.today.month)
		.where("extract (year from invoices.due_date) = ?", Date.today.year)
        .joins(:program)
        .where("programs.name = ?", "Summer Camp")
    }
    
	def amount_to_dollars
		amount / 100
	end

	def amount_paid_to_dollars
		amount_paid / 100
	end

	def discounts_total_to_dollars
		discounts_total / 100
	end

	def fees_total_to_dollars
		fees_total / 100
	end

	def amount_after_discounts_and_fees
		amount - discounts_total + fees_total
	end

	def amount_after_discounts_and_fees_to_dollars
		amount_after_discounts_and_fees / 100
	end

	def amount_remaining
		amount_after_discounts_and_fees - amount_paid
	end

	def amount_remaining_to_dollars
		amount_remaining / 100
	end

	def discounts_total
		self.discounts.to_a.sum(&:amount_in_cents)
	end

	def fees_total
		self.fees.where.not(deleted: true).to_a.sum(&:amount_in_cents)
	end

	def create_receipt(payment, amount)
		payment_for = "#{payment} for #{self.memo} Invoice# #{self.id}"
		if self.registration_record.program.name == "After School"
			date = self.due_date.strftime("%B %Y")
			payment_for = "#{payment} for #{self.memo} #{date} Invoice"
		end
		self.receipts.create(
			registration_record_id: self.registration_record.id,
			customer_id: self.customer.id,
			amount: amount / 100,
			program_name: self.program.name,
			payment_for: payment_for
		)
	end

	def autopay_charge_customer
		begin
			if registration_record.customer.stripe_customer_identifier.present?
				stripe_customer = Stripe::Customer.retrieve(registration_record.customer.stripe_customer_identifier)
				charge = Stripe::Charge.create(
					customer: stripe_customer.id,
					amount: amount_after_discounts_and_fees.to_i,
					description: memo,
					currency: 'usd'
				)
				update_attributes(paid: true, amount_paid: charge.amount)
				self.create_receipt("Payment", charge.amount)
				InvoicesMailer.invoice_autopayed(self.id).deliver!
			else
				InvoicesMailer.invoice_autopay_failed(self.id).deliver!
			end
		rescue StandardError => e # card has been declined
			f = File.open('log/autopay.log','a')
			f.puts "message: #{e.message}, date: #{Time.current}, invoice_id: #{self.id}"
			f.close
			InvoicesMailer.invoice_autopay_failed(self.id, e.message).deliver!
		end
	end

	def send_emails
		InvoicesMailer.create(self.id).deliver!
	end
end
