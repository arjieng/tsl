class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
   :recoverable, :rememberable, :trackable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2]

	has_many :locations, foreign_key: :director_id
  has_many :programs, foreign_key: :director_id
  has_many :payroll_location_accesses
  has_many :payroll_locations, through: :payroll_location_accesses
  validates :email, :presence => true, :email => true
  
    def self.from_omniauth(access_token)
      data = access_token.info
      user = User.where(:email => data["email"]).first

      # Uncomment the section below if you want users to be created if they don't exist
      unless user
        user = User.create(
          name: data["name"],
          email: data["email"],
          password: Devise.friendly_token[0,20]
        )
      end

      user
    end

    def format_self
        {
            id: self.id,
            name: self.name.titleize
        }
    end

end
