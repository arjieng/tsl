class SingleDayDate < ActiveRecord::Base
	belongs_to :program
	has_many :single_day_records
	has_many :registration_records, through: :single_day_records

	def display_name
		if date.present?
			"#{name} - #{date.strftime('%D')}"
		else
			"#{name}"
		end
	end

	def enrolled_count
		registration_records.where(enrolled: true).to_a.sum(&:child_records_count)
	end
end
