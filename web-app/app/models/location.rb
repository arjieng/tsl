class Location < ActiveRecord::Base
	belongs_to :director, class_name: "User"
	has_many :programs, dependent: :destroy
	has_many :registration_records, through: :programs
	has_many :customers, -> { distinct }, through: :registration_records

	validates_presence_of :name, :address, :director

	def dropdown_display_name(program_type)
		program = self.programs.find_by(name: program_type)

		if program.name_suffix.present?
			"#{self.name} - #{program.name_suffix}"
		else
			self.name
		end
	end

	def get_minimum_age
		self.minimum_age.nil? ? "" : self.minimum_age 
	end
end
