class Order < ActiveRecord::Base
    before_save :set_total_amount

    belongs_to :product
    belongs_to :size
    has_one :product, through: :size
    belongs_to :customer

    def amount_in_cents
        (self.total_amount * 100).to_i
    end

    private

    def set_total_amount
        self.total_amount = self.size.price * self.quantity
    end
end
