class PlaidService
  def self.bank_account_token(public_token, account_id)
    client = Plaid::Client.new(env: Rails.application.secrets.plaid_env.to_sym,
                           client_id: Rails.application.secrets.plaid_client_id,
                           secret: Rails.application.secrets.plaid_secret_key,
                           public_key: Rails.application.secrets.plaid_public_key)
    exchange_token_response = client.item.public_token.exchange(public_token)
    access_token = exchange_token_response['access_token']
    stripe_response = client.processor.stripe.bank_account_token.create(access_token, account_id)
    stripe_response['stripe_bank_account_token']
  end
end