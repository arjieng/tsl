class SummerCampRecord < ActiveRecord::Base
	belongs_to :registration_record

	def amount_of_weeks
		count = 0

		count += 1 if week_one?
		count += 1 if week_two?
		count += 1 if week_three?
		count += 1 if week_four?
		count += 1 if week_five?
		count += 1 if week_six?
		count += 1 if week_seven?
		count += 1 if week_eight?
		count += 1 if week_nine?
		count += 1 if week_ten?

		return count
	end
end
