class CitRecord < ActiveRecord::Base
	has_many :emergency_contacts, dependent: :destroy
	accepts_nested_attributes_for :emergency_contacts, allow_destroy: true, reject_if: :all_blank

	validates_presence_of :first_name, :last_name, :phone_number, :email, :address_line_one, :city, :state, :zip_code, :how_did_you_hear_about_us, :location, :previous_experience, :why_are_you_interested, :action_when_completed_tasks, :arguing_action, :transition_help, :interests_and_talents, :proven_responsibility, :what_you_will_bring, :dob, :water_play_action, :four_square_action, :uninteresting_activity_action, :made_a_difference
	validates :attended_tsl_before, :cit_at_tsl_before, :willing_to_follow_directions, :willing_to_meet_vision, inclusion: { in: [true, false], message: "must select yes or no" }
	validate :agree_to_behavior_policy_is_true

	def full_name
		"#{first_name} #{last_name}"
	end

	def filtered_location
		case location
		when "guilderlandChristLutheranChurch"
			"Christ Lutheran Church"
		when "guilderlandSchoolHouseRoad"
			"School House Road"
		else
			location.underscore.split('_').collect{|c| c.capitalize}.join(' ')
		end
	end

	def address
		address_line_two = address_line_two.present? ? " #{address_line_two}" : ""

		return "#{address_line_one}#{address_line_two}, #{city} #{state}, #{zip_code}"
	end

	def send_tom_cit_email_notification
		WelcomeMailer.send_tom_cit_email_notification(self).deliver!
	end

	private
	def agree_to_behavior_policy_is_true
		errors.add "Agree to Behavior Policy", "must be checked" if self.agree_to_behavior_policy != true
	end
end
