class PayrollLocationAccess < ActiveRecord::Base
    belongs_to :user
    belongs_to :payroll_location
end
