class ChildRecord < ActiveRecord::Base
	belongs_to :registration_record

	validates_presence_of :first_name, :last_name
	validate :dob_format_correct

	def age
		now = Time.now.to_date

		begin
			date_of_birth = Date.strptime(self.dob, "%Y-%m-%d")

			now.year - date_of_birth.year - ((now.month > date_of_birth.month || (now.month == date_of_birth.month && now.day >= date_of_birth.day)) ? 0 : 1)
		rescue
			0
		end
	end

	def full_name
		"#{first_name.capitalize} #{last_name.capitalize}"
	end

	def format_self
		{
			id: self.id,
			first_name: self.first_name.downcase,
			last_name: self.last_name.downcase,
			date_of_birth: self.dob
		}
	end

	private
	def dob_format_correct
		begin
			Date.parse(self.dob)
		rescue
			errors.add "Date of birth", "is not formatted correctly"
		end
	end
end
