class VacationCampSchedule < ActiveRecord::Base
    belongs_to :program
    has_many :vacation_camp_records
    has_many :registration_records, through: :vacation_camp_records 

    def enrollment_count
        registration_records.where(enrolled: true).to_a.sum(&:child_records_count)
    end

    def set_all_days_to_false
        attributes.each do |attr_name, attr_value|
            if attr_value.class == TrueClass || attr_value.class == FalseClass
                update_attribute(attr_name.to_sym, false)
            end
        end
    end

    def schedule_summary
        days = self.information.keys.map(&:to_s)
        full_info_hash = self.full_information
        year = full_info_hash["year"]
        month = Date::MONTHNAMES[full_info_hash["month"]]
        " #{year}, #{month} (#{days.first}-#{days.last}) |"
    end

    def information
        days_hash = attributes.select do |key, value|
            value == true
        end
        days_keys = days_hash.keys
        day_names_and_number(year, month, days_keys)
    end

    def full_information
        result = information
        result.store("year", year)
        result.store("month", month_word_to_number(month))
        result
    end

    def days
        days_hash = attributes.select do |key, value|
            value == true
        end
        days_keys = days_hash.keys
        result = days_keys.map do |word|
            word_to_number(word)
        end
        result.map(&:inspect).join(', ')
    end

    private
    
    def day_names_and_number(year, month, array_of_days)
        result = {}
        array_of_days = array_of_days.delete_if {|day_as_word| day_as_word == "deleted"}
        array_of_days.each do |day|
            result.store(
                word_to_number(day), Date.new(year, month_word_to_number(month), word_to_number(day))
                    .strftime('%A')
            )
        end
        result
    end

    def word_to_number(day_as_word)
        case day_as_word
            when "day_one"
                1
            when "day_two"
                2
            when "day_three"
                3
            when "day_four"
                4
            when "day_five"
                5
            when "day_six"
                6
            when "day_seven"
                7
            when "day_eight"
                8
            when "day_nine"
                9
            when "day_ten"
                10
            when "day_eleven"
                11
            when "day_twelve"
                12
            when "day_thirteen"
                13
            when "day_fourteen"
                14
            when "day_fifteen"
                15
            when "day_sixteen"
                16
            when "day_seventeen"
                17
            when "day_eighteen"
                18
            when "day_nineteen"
                19
            when "day_twenty"
                20
            when "day_twenty_one"
                21
            when "day_twenty_two"
                22
            when "day_twenty_three"
                23
            when "day_twenty_four"
                24
            when "day_twenty_five"
                25
            when "day_twenty_six"
                26
            when "day_twenty_seven"
                27
            when "day_twenty_eight"
                28
            when "day_twenty_nine"
                29
            when "day_thirty"
                30
            when "day_thirty_one"
                31
        end
    end

    def month_word_to_number(month_word)
        case month_word
            when "january"
                1
            when "february"
                2
            when "march"
                3
            when "april"
                4
            when "may"
                5
            when "june"
                6
            when "july"
                7
            when "august"
                8
            when "september"
                9
            when "october"
                10
            when "november"
                11
            when "december"
                12
        end
    end
    
end
