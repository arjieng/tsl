class PayrollEmployeeHour < ActiveRecord::Base
    belongs_to :payroll_employee
    belongs_to :payroll_location
end
