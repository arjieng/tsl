class Services::Purchase::Construct
    attr_reader :customer_id, :orders, :location_id

    def self.invoke(purchase_params)
        self.new(purchase_params).execute
    end

    def initialize(purchase_params)
        @customer_id = purchase_params[:customer_id]
        @orders = purchase_params[:orders]
        @location_id = purchase_params[:location_id]
    end

    def execute
        orders_array = Order.where(id: orders.split(','))
        total_amount = orders_array.pluck(:total_amount).reduce(:+).to_i
        {
            customer_id: customer_id,
            orders: orders,
            location_id: location_id,
            total_amount: total_amount
        }
    end
end