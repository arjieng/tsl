class Services::Payroll::PayrollInformationForTrend
    attr_reader :selected_year, :selected_month, :location_id

    def self.invoke(selected_year, selected_month, location_id)
        self.new(selected_year, selected_month, location_id).execute
    end

    def initialize(selected_year, selected_month, location_id)
        @selected_year = selected_year
        @selected_month = selected_month
        @location_id = location_id
    end

    def execute
        location_name = PayrollLocation.find_by(id: location_id).name.titleize
        breakdown = Services::Payroll::RetrieveAllPayrollLocationsBreakdown.invoke(
            selected_year, selected_month
        )
        location_summary = breakdown[:payroll_breakdown].select {|el| el[:payroll_location_name] == location_name}
        result = {
            message: 'Successfully retrieved location summary',
            location_summary: location_summary.first,
            status_code: 200
        }
    end
end