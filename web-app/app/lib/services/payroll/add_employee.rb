class Services::Payroll::AddEmployee
    attr_reader :location_id, :employee_name

    def self.invoke(location_id, employee_name)
        self.new(location_id, employee_name).execute
    end

    def initialize(location_id, employee_name)
        @location_id = location_id
        @employee_name = employee_name
    end

    def execute
        payroll_location = PayrollLocation.find_by(id: location_id)
        unless payroll_location.present?
            return {
                status: 'failure',
                status_code: 404,
                message: 'Unable to add employee. Location could not be found'
            }
        end
        begin
            employee = payroll_location.payroll_employees.create!(
                name: employee_name.downcase
            )
        rescue ActiveRecord::RecordInvalid => error
            return {
                status: 'failure',
                status_code: 400,
                message: 'An employee with the same name already exists in the database.'
            }
        end
        {
            status: 'success',
            status_code: 200,
            message: 'The employee has been successfully added.',
            employee: {
                id: employee.id,
                name: employee.name.titleize,
            }
        }
    end
end