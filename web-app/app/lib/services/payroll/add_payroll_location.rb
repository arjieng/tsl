class Services::Payroll::AddPayrollLocation
    attr_reader :payroll_location_name

    def self.invoke(payroll_location_name)
        self.new(payroll_location_name).execute
    end

    def initialize(payroll_location_name)
        @payroll_location_name = payroll_location_name.downcase
    end

    def execute
        if payroll_location = PayrollLocation.find_by(name: payroll_location_name).present?
            return {
                status: 'failure',
                status_code: 400,
                message: 'A payroll location with the same name already exists'
            }
        end

        new_payroll_location = PayrollLocation.create(name: payroll_location_name)
        {
            status: 'success',
            status_code: 200,
            message: 'Successfully created new payroll location',
            location: new_payroll_location.format_self
        }
    end
end