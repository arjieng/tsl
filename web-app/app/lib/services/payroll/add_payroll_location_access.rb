class Services::Payroll::AddPayrollLocationAccess
    attr_reader :payroll_location_id, :administrator_id
    def self.invoke(payroll_location_id, administrator_id)
        self.new(payroll_location_id, administrator_id).execute
    end

    def initialize(payroll_location_id, administrator_id)
        @payroll_location_id = payroll_location_id.to_i
        @administrator_id = administrator_id.to_i
    end

    def execute
        if administrator_id == -1
            return {
                message: 'Please select a user and try again',
                status: 'failure',
                status_code: 404
            }
        end
        unless User.find_by(id: administrator_id).present?
            return {
                message: 'The selected administrator no longer exists',
                status: 'failure',
                status_code: 404
            }
        end
        if payroll_location_id == 0
            return {
                message: 'Please select a payroll location to grant access to',
                status: 'failure',
                status_code: 400
            }
        end
        access = PayrollLocationAccess.find_by(
            user_id: administrator_id,
            payroll_location_id: payroll_location_id
        )
        if access.present?
            return {
                message: 'The selected administrator is already authorized',
                status: 'failure',
                status_code: 404
            }
        end
        PayrollLocationAccess.create(
            user_id: administrator_id,
            payroll_location_id: payroll_location_id 
        )
        {
            message: 'Successfully authorized user',
            status: 'success',
            administrator: User.find_by(id: administrator_id).format_self,
            status_code: 200
        }
    end
end