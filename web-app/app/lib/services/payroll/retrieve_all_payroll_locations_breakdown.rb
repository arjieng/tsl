class Services::Payroll::RetrieveAllPayrollLocationsBreakdown
    attr_reader :selected_year, :selected_month
    def self.invoke(selected_year, selected_month)
        self.new(selected_year, selected_month).execute
    end

    def initialize(selected_year, selected_month)
        @selected_year = selected_year
        @selected_month = selected_month
    end

    def execute
        unless selected_year.scan(/\D/).empty?
            return {
                message: 'Only numeric character are allowed for the year. Please try again',
                status_code: 400
            }
        end

        payroll_locations = PayrollLocation.includes(:payroll_employees).includes(:payroll_employee_hours)
        result = payroll_locations.inject([]) do |acc, location|
            acc << {
                payroll_location_name:  location.name.titleize,
                payroll_location_breakdown: retrieve_location_breakdown(location)
            }
        end
        {
            message: "Successfully retrieved payroll summary for #{Date.new(selected_year.to_i, selected_month.to_i, 1).strftime("%B")} #{selected_year.to_i}",
            payroll_breakdown: result,
            status_code: 200
        }
    end

    private

    def retrieve_location_breakdown(payroll_location)
        result = payroll_location.payroll_employees.order('name ASC').inject([]) do |acc, employee|
            acc << {
                "employee_name": employee.name.titleize,
                "1-15": retrieve_logged_hours_summary(1, 15, payroll_location, employee),
                "16-31": retrieve_logged_hours_summary(16, 31, payroll_location, employee),
                "sick": retrieve_absence_summary('sick', payroll_location, employee),
                "vacation": retrieve_absence_summary('vacation', payroll_location, employee)
            }
        end
    end

    def retrieve_logged_hours_summary(start_day, end_day, payroll_location, payroll_employee)
        hours = payroll_employee.logged_hours.where('extract (year from logged_hours_date) = ?', selected_year)
            .where('extract (month from logged_hours_date) = ?', selected_month)
            .where('extract (day from logged_hours_date) >= ? and extract (day from logged_hours_date) <= ?', start_day, end_day)
            .where(payroll_location_id: payroll_location.id)
            .pluck(:logged_hours).reduce(:+).to_i
    end

    def retrieve_absence_summary(absence_type, payroll_location, payroll_employee)
        absences = payroll_employee.payroll_employee_abscences.where('extract (year from abscence_date) = ?', selected_year)
            .where('extract (month from abscence_date) = ?', selected_month)
            .where(payroll_location_id: payroll_location.id)
            .where(abscence_type: absence_type)
            .count
    end
end