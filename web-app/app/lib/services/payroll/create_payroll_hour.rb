class Services::Payroll::CreatePayrollHour
    attr_reader :payroll_location_id, :payroll_employee_id, :payroll_hour_date
    def self.invoke(payroll_location_id, payroll_employee_id, payroll_hour_date)
        self.new(payroll_location_id, payroll_employee_id, payroll_hour_date).execute
    end

    def initialize(payroll_location_id, payroll_employee_id, payroll_hour_date)
        @payroll_location_id = payroll_location_id
        @payroll_employee_id = payroll_employee_id
        @payroll_hour_date = payroll_hour_date
    end

    def execute
        payroll_hour = PayrollEmployeeHour.create(
            payroll_location_id: payroll_location_id,
            payroll_employee_id: payroll_employee_id,
            logged_hours_date: payroll_hour_date,
            logged_hours: 0
        )
        {
            id: payroll_employee_id,
            name: PayrollEmployee.find_by(id: payroll_employee_id).name.titleize,
            logged_hours: payroll_hour.logged_hours
        }
    end
end