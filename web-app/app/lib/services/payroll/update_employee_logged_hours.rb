class Services::Payroll::UpdateEmployeeLoggedHours
    attr_reader :payroll_location_id, :payroll_logged_hours_date, :payroll_employees
    def self.invoke(payroll_location_id, payroll_logged_hours_date, payroll_employees)
        self.new(payroll_location_id, payroll_logged_hours_date, payroll_employees).execute        
    end

    def initialize(payroll_location_id, payroll_logged_hours_date, payroll_employees)
        @payroll_location_id = payroll_location_id
        @payroll_logged_hours_date = Date.parse(payroll_logged_hours_date)
        @payroll_employees = payroll_employees
    end

    def execute
        payroll_location = PayrollLocation.find_by(id: payroll_location_id)
        all_payroll_employee_hours = PayrollEmployeeHour.all

        unless payroll_employees.map {|employee| employee[:logged_hours]}.join('').scan(/\D/).empty?
            return {
                message: 'Update failed. Only numbers are allowed on the logged hours fields',
                status_code: 400
            }
        end

        payroll_employees.each do |employee|
            employee_hour = all_payroll_employee_hours.find_by(
                payroll_employee_id: employee[:payroll_employee_id],
                payroll_location_id: payroll_location_id,
                logged_hours_date: payroll_logged_hours_date
            )
            employee_hour.update_attribute(:logged_hours, employee[:logged_hours]) if employee_hour.present?
        end
        total_hours = PayrollEmployeeHour.where('extract (year from logged_hours_date) = ?', payroll_logged_hours_date.year)
            .where('extract (month from logged_hours_date) = ?', payroll_logged_hours_date.month)
            .where(payroll_location_id: payroll_location.id)
            .pluck(:logged_hours).reduce(:+)
        {
            message: 'Update successfull',
            payroll_hours_for_the_month: total_hours.to_i,
            status_code: 200
        }
    end
end