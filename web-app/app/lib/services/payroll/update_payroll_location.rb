class Services::Payroll::UpdatePayrollLocation
    attr_reader :payroll_location_id, :new_name
    
    def self.invoke(payroll_location_id, new_name)
        self.new(payroll_location_id, new_name).execute
    end

    def initialize(payroll_location_id, new_name)
        @payroll_location_id = payroll_location_id
        @new_name = new_name.downcase
    end

    def execute
        if PayrollLocation.find_by(name: new_name).present?
            return {
                message: 'A location with the same name already exists',
                status_code: 400
            }
        end
        payroll_location = PayrollLocation.find_by(id: payroll_location_id)
        unless payroll_location.present?
            return {
                message: "Update failed. The Location couldn't be found"
            }
        end
        payroll_location.update_attribute(:name, new_name)
        {
            message: 'Successfully updated location',
            status_code: 200
        }
    end
end