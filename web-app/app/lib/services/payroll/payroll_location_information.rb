class Services::Payroll::PayrollLocationInformation
    attr_reader :location_id, :payroll_hours_date

    def self.invoke(location_id, payroll_hours_date)
        self.new(location_id, payroll_hours_date).execute
    end

    def initialize(location_id, payroll_hours_date)
        @location_id = location_id
        @payroll_hours_date = Date.parse(payroll_hours_date)
    end

    def execute
        payroll_location = PayrollLocation.find_by(id: location_id)
        if payroll_location.present?
            result = construct_employee_information(
                payroll_location.payroll_employees.map(&:format_self),
                payroll_location.payroll_employee_hours
            )
        else
            result = nil
        end
        total_hours = PayrollEmployeeHour.where('extract (year from logged_hours_date) = ?', payroll_hours_date.year)
            .where('extract (month from logged_hours_date) = ?', payroll_hours_date.month)
            .where(payroll_location_id: payroll_location.id)
            .pluck(:logged_hours).reduce(:+)
        {
            payroll_employees: result,
            total_hours_for_the_month: total_hours.to_i
        }
    end

    private

    def construct_employee_information(payroll_employees, payroll_location_hours)
        result = payroll_employees.map do |employee|
           payroll_hour = payroll_location_hours.find_by(payroll_location_id: location_id, payroll_employee_id: employee[:id] , logged_hours_date: payroll_hours_date)
            if payroll_hour.present?
                payroll_hour_object = payroll_hour 
            else
                payroll_hour_object = PayrollEmployeeHour.create(
                    payroll_location_id: location_id,
                    payroll_employee_id: employee[:id] , 
                    logged_hours_date: payroll_hours_date,
                    logged_hours: 0
                )
            end
            {
                id: employee[:id],
                name: employee[:name].titleize,
                logged_hours: payroll_hour_object.logged_hours
            }         
        end
    end
end