class Services::Payroll::LogEmployeeAbsence
    attr_reader :payroll_location_id, :payroll_employee_id, :absence_date, :absence_type

    def self.invoke(payroll_location_id, payroll_employee_id, absence_date, absence_type)
        self.new(payroll_location_id, payroll_employee_id, absence_date, absence_type).execute
    end

    def initialize(payroll_location_id, payroll_employee_id, absence_date, absence_type)
        @payroll_location_id = payroll_location_id.to_i
        @payroll_employee_id = payroll_employee_id.to_i
        @absence_date = absence_date
        @absence_type = absence_type
    end

    def execute
        return respond_with_error_message('Please select a location and try again.', 400) if payroll_location_id == 0
        return respond_with_error_message('Please select an employee and try again.', 400) if payroll_employee_id == 0
        begin
            Date.parse(absence_date)
        rescue StandardError => e
            return respond_with_error_message('Please enter a valid date.', 400)
        end
        if PayrollEmployeeAbscence.find_by(
                payroll_location_id: payroll_location_id,
                payroll_employee_id: payroll_employee_id,
                abscence_date: absence_date
            ).present?
            return respond_with_error_message('An absence has already been logged on the specified date for the employee.', 400)
        end
        PayrollEmployeeAbscence.create(
            payroll_location_id: payroll_location_id,
            payroll_employee_id: payroll_employee_id,
            abscence_date: absence_date,
            abscence_type: absence_type
        )
        {
            message: "Successfully logged #{absence_type} day.",
            status_code: 200
        }
    end

    private

    def respond_with_error_message(message, status_code)
        {
            message: message,
            status_code: status_code
        }
    end
end