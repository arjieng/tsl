class Services::Payroll::UpdateEmployee
    attr_reader :payroll_employee_id, :new_payroll_employee_name

    def self.invoke(payroll_employee_id, new_payroll_employee_name)
        self.new(payroll_employee_id, new_payroll_employee_name).execute
    end

    def initialize(payroll_employee_id, new_payroll_employee_name)
        @payroll_employee_id = payroll_employee_id
        @new_payroll_employee_name = new_payroll_employee_name
    end

    def execute
        payroll_employee = PayrollEmployee.find_by(id: payroll_employee_id)
        unless payroll_employee.present? 
            return {
                status: 'failure',
                status_code: 400,
                message: "The employee record could not be updated. It couldn't be found."
            }
        end
        begin
            employee = payroll_employee.update_attribute(:name, new_payroll_employee_name.downcase)
        rescue ActiveRecord::RecordNotUnique => error
            return {
                status: 'failure',
                status_code: 400,
                message: 'An employee with the same name already exists in the database.'
            }
        end
        {
            status: 'success',
            status_code: 200,
            message: "The employee's information has been successfully updated.",
            employee: {
                id: payroll_employee.id,
                name: payroll_employee.name.titleize,
            }
        }
    end
end