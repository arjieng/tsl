class Services::AdminDashboard::YearlySummary
    attr_reader :year

    def self.invoke(year = Date.today.year)
        self.new(year).execute
    end

    def initialize(year)
        @year = year.to_i
    end

    def execute
        initialize_all_invoices_and_registration_records
        result = calculate_yearly_revenue
    end

    private

    def initialize_all_invoices_and_registration_records
        @all_invoices = Invoice.includes(:program).references(:programs)
        @all_registration_records = RegistrationRecord.includes(:program).references(:programs)
        @all_year_round_single_day_records = RegistrationRecord.joins(:annual_single_day_promo_record)
    end

    def calculate_yearly_revenue
        programs = {
            single_day: [],
            year_round_single_day: [],
            after_school: [],
            day_care: [],
            vacation_camp: [],
            summer_camp: []
        }
        months = []
        12.times do |month|
            placeholder_array = []

            single_day = calculate_single_day_revenue(month + 1)
            programs[:single_day] << single_day
            placeholder_array << single_day

            year_round_single_day = calculate_year_round_revenue(month + 1)
            programs[:year_round_single_day] << year_round_single_day
            placeholder_array << year_round_single_day

            after_school = calculate_afterschool_revenue(month + 1)
            programs[:after_school] << after_school
            placeholder_array << after_school

            day_care = calculate_day_care_revenue(month + 1)
            programs[:day_care] << day_care
            placeholder_array << day_care

            vacation_camp = calculate_vacation_camp_revenue(month + 1)
            programs[:vacation_camp] << vacation_camp
            placeholder_array << vacation_camp

            summer_camp = calculate_summer_camp_revenue(month + 1)
            programs[:summer_camp] << summer_camp
            placeholder_array << summer_camp

            months << placeholder_array
        end
        index = 0
        result = months.inject({}) do |acc, curr|
            index += 1
            acc.store(
                Date.new(year, index, 1).strftime('%B'),
                curr.reduce(:+).to_i
            )
            acc
        end
        {
            'yearly_breakdown': result,
            'program_breakdown': programs
        }
    end

    def calculate_single_day_revenue(month)
        single_day_records = @all_registration_records.where('programs.name = ?', 'Single Day')
            .joins(:single_day_record)
            .where(created_at: 
                Date.new(year, month, 1).beginning_of_month.beginning_of_day..Date.new(year, month, 1).end_of_month.end_of_day
            )
        revenue_for_the_month = single_day_records.pluck(:amount_paid).reduce(:+).to_i
    end

    def calculate_year_round_revenue(month)
        year_round_recs = @all_year_round_single_day_records
        .where(created_at: 
            Date.new(year, month, 1).beginning_of_month.beginning_of_day..Date.new(year, month, 1).end_of_month.end_of_day
        )
        before_and_after_school_recs = @all_registration_records.where('programs.name = ?', 'After School')
            .joins(:before_and_afterschool_record)
            .where(created_at: 
                Date.new(year, month, 1).beginning_of_month.beginning_of_day..Date.new(year, month, 1).end_of_month.end_of_day
            )
        filtered = year_round_recs - before_and_after_school_recs
        year_round_revenue = filtered.map(&:amount_paid).map(&:to_i).reduce(:+).to_i
    end

    def calculate_afterschool_revenue(month)
        before_and_after_school_recs = @all_registration_records.where('programs.name = ?', 'After School')
            .joins(:before_and_afterschool_record)
            .where(created_at: 
                Date.new(year, month, 1).beginning_of_month.beginning_of_day..Date.new(year, month, 1).end_of_month.end_of_day
            )
            after_school_deposit_revenue = before_and_after_school_recs.pluck(:amount_paid).reduce(:+).to_i
            after_school_invoice_revenue = 
                @all_invoices.where('programs.name = ?', 'After School')
                .where('extract (year from due_date) = ?', year)
                .where('extract (month from invoices.updated_at) = ?', month) 
                .where(paid: true)
                .map(&:amount_to_dollars).reduce(:+).to_i
                after_school_revenue = after_school_deposit_revenue + after_school_invoice_revenue
    end

    def calculate_day_care_revenue(month)
        day_care_revenue =
            @all_invoices.where('programs.name = ?', 'Day Care')
            .where('extract (year from due_date) = ?', year)
            .where('extract (month from invoices.updated_at) = ?', month)
            .where(paid: true)
            .map(&:amount_to_dollars).reduce(:+).to_i
    end

    def calculate_vacation_camp_revenue(month)
        vacation_camp_revenue =
            @all_registration_records.where('programs.name = ?', 'Vacation Camp')
            .where(created_at: 
                Date.new(year, month, 1).beginning_of_month.beginning_of_day..Date.new(year, month, 1).end_of_month.end_of_day
            )
            .pluck(:amount_paid).reduce(:+).to_i
        vacation_camp_revenue
    end

    def calculate_summer_camp_revenue(month)
        summer_camp_recs = 
            @all_registration_records.where('programs.name = ?', 'Summer Camp')
            .where(created_at: 
                Date.new(year, month, 1).beginning_of_month.beginning_of_day..Date.new(year, month, 1).end_of_month.end_of_day
            )
        summer_camp_deposit_revenue = summer_camp_recs.pluck(:amount_paid).reduce(:+).to_i
        summer_camp_invoice_revenue = 
            @all_invoices.where('programs.name = ?', 'Summer Camp')
            .where('extract (year from due_date) = ?', year)
            .where('extract (month from invoices.updated_at) = ?', month)
            .where(paid: true)
            .map(&:amount_to_dollars).reduce(:+).to_i
        summer_camp_revenue = summer_camp_deposit_revenue + summer_camp_invoice_revenue
    end
end