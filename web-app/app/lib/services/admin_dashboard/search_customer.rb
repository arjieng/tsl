class Services::AdminDashboard::SearchCustomer
    attr_reader :first_name, :last_name

    def self.invoke(first_name, last_name)
        self.new(first_name, last_name).execute
    end

    def initialize(first_name, last_name)
        @first_name = first_name
        @last_name = last_name
    end

    def execute
        customers = Customer.where('first_name ilike ?', '%' + first_name + '%')
            .where('last_name ilike ?', '%' + last_name + '%')
            .order(:first_name)
            .map do |element|
                {
                    id: element.id,
                    first_name: element.first_name.capitalize,
                    last_name: element.last_name.capitalize,
                    address: element.address
                }
            end
    end
end