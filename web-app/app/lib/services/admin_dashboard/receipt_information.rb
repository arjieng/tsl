class Services::AdminDashboard::ReceiptInformation
    attr_reader :receipt_id
    def self.invoke(receipt_id)
        self.new(receipt_id).execute
    end

    def initialize(receipt_id)
        @receipt_id = receipt_id
    end

    def execute
        receipt = Receipt.find_by(id: receipt_id)
         return {
            message: "A receipt with the specified ID couldn't be found.",
            status: 'failure',
            status_code: 404
        } if !receipt.present?
        {
            message: "Success",
            status: 'success',
            status_code: 200,
            receipt_information: receipt.format_self,
            child_information: receipt.registration_record.child_records.map(&:format_self)
        }
    end
end