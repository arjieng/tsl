class Services::AdminDashboard::DivideMonthToWeekRanges
    attr_reader :year, :month

    def self.invoke(year = Date.today.year, month = Date.today.month)
        self.new(year.to_i, month.to_i).execute
    end

    def initialize(year, month)
        @year = year
        @month = month
    end

    def execute
        date = Date.new(year, month, 1)
        beginning_of_month = date.beginning_of_month.day
        end_of_month = date.end_of_month.day
        week_range_array = []
        edge_counter = 0
        
        loop do
            container = []
            7.times do
                edge_counter += 1
                (container << edge_counter) if edge_counter <= end_of_month
            end
            week_range_array << container unless container.empty?
            break if edge_counter > end_of_month
        end
        week_range_array
    end
end