class Services::AdminDashboard::CustomerRegistrations
    attr_reader :customer

    def self.invoke(customer_id)
        self.new(customer_id).execute
    end

    def initialize(customer_id)
        @customer = Customer.find_by(id: customer_id)
    end

    def execute
        initialize_all_invoices_and_registration_records
        single_day_recs = retrieve_single_day_recs
        before_and_after_school_revenue = retrieve_before_and_after_school_recs
        retrieve_year_round_recs
        retrieve_day_care_rec
        retrieve_vacation_camp_recs
        retrieve_summer_camp_recs
        {
            single_day: {
                count: single_day_recs.count,
                revenue: single_day_recs.pluck(:amount_paid).reduce(:+).to_i
            },
            before_and_after_school: {
                invoices_paid: "#{@paid_after_school_invoices.count}/#{@after_school_invoices.count}",
                outstanding_balance:  @outstanding_after_school_balance,
                revenue: before_and_after_school_revenue
            },
            year_round_single_day: {
                registration_count: @year_round_recs.count,
                revenue: @year_round_recs.map(&:amount_paid).map(&:to_i).reduce(:+).to_i
            },
            day_care: {
                monthly_due: @day_care_monthly_due,
                paid_invoices: "#{@paid_day_care_invoices.count}/#{@day_care_invoices.count}",
                outstanding_balance: @day_care_outstanding_balance,
                revenue: @day_care_invoice_revenue
            },
            vacation_camp: {
                count: @vacation_camp_recs.count,
                revenue: @vacation_camp_recs.pluck(:amount_paid).reduce(:+).to_i
            },
            summer_camp: {
                deposit_revenue: @summer_camp_deposit_revenue,
                paid_invoices: "#{@summer_camp_paid_invoices.count}/#{@summer_camp_invoices_this_year.count}",
                invoices_revenue: @summer_camp_invoice_revenue,
                outstanding: @summer_camp_outstanding_balance,
                total_revenue: @total_summer_camp_revenue
            }
        }
    end

    private

    def retrieve_single_day_recs
        @customer.registration_records
            .joins(:single_day_record)
            .where('extract (year from single_day_records.created_at) = ?', Date.today.year)
    end

    def retrieve_year_round_recs
        year_recs = @customer.registration_records
            .joins(:annual_single_day_promo_record)
            .where('extract (year from annual_single_day_promo_records.created_at) = ?', Date.today.year)
        @year_round_recs = year_recs - @afters_school_recs
    end

    def retrieve_before_and_after_school_recs
        @afters_school_recs = @all_registration_records.where('programs.name = ?', 'After School')
            .where(customer_id: @customer.id)
            .joins(:before_and_afterschool_record)
            .where('extract (year from before_and_afterschool_records.created_at) = ?', Date.today.year)

        @unpaid_after_school_invoices = @all_invoices.where('programs.name = ?', 'After School')
            .where('registration_records.customer_id = ?', @customer.id)
            .where('extract (year from due_date) = ?', Date.today.year)
            .where(paid: false)
        
        @outstanding_after_school_balance = @unpaid_after_school_invoices.map(&:amount_to_dollars).reduce(:+).to_i

        after_school_deposit_revenue = @afters_school_recs.pluck(:amount_paid).reduce(:+).to_i
        @after_school_invoices =
            @all_invoices.where('programs.name = ?', 'After School')
            .where('registration_records.customer_id in (?)', @customer.id)
            .where('extract (year from due_date) = ?', Date.today.year)
        
        @paid_after_school_invoices = @after_school_invoices
            .where(paid: true)
            .where('extract (year from invoices.updated_at) = ?', Date.today.year)
            
        after_school_invoice_revenue = @paid_after_school_invoices.map(&:amount_to_dollars).reduce(:+).to_i
        after_school_revenue = after_school_deposit_revenue + after_school_invoice_revenue
    end

    def retrieve_day_care_rec
        day_care_rec = @all_registration_records.joins(:day_care_record).where('registration_records.customer_id = ?', @customer.id).first
        @day_care_invoices = day_care_rec.present? ? day_care_rec.invoices.where('extract (year from due_date) = ?', Date.today.year) : []
        @paid_day_care_invoices = @day_care_invoices.present? ? @day_care_invoices.where(paid: true) : []
        @day_care_invoice_revenue = @paid_day_care_invoices.present? ? @paid_day_care_invoices.map(&:amount_to_dollars).reduce(:+).to_i : 0
        @day_care_outstanding_balance = @day_care_invoices.present? ? @day_care_invoices.where(paid: false).map(&:amount_to_dollars).reduce(:+).to_i : 0
        @day_care_monthly_due = day_care_rec.present? ? day_care_rec.calculate_day_care_charge / 100 : 0
    end

    def retrieve_vacation_camp_recs
        @vacation_camp_recs = @all_registration_records.where('programs.name = ?', 'Vacation Camp')
            .where('registration_records.customer_id = ?', @customer.id)
            .where('extract (year from registration_records.created_at) = ?', Date.today.year)
    end

    def retrieve_summer_camp_recs
        summer_camp_recs_this_year = 
            @all_registration_records.where('programs.name = ?', 'Summer Camp')
            .where('extract (year from registration_records.created_at) = ?', Date.today.year)
            .where('registration_records.customer_id = ?', @customer.id)

        @summer_camp_deposit_revenue = summer_camp_recs_this_year.present? ? summer_camp_recs_this_year.pluck(:amount_paid).reduce(:+).to_i : 0
        @summer_camp_invoices_this_year = @all_invoices.where('programs.name = ?', 'Summer Camp')
            .where('extract (year from due_date) = ?', Date.today.year)
            .joins(:registration_record)
            .where('registration_records.customer_id = ?', @customer.id)
        @summer_camp_paid_invoices = @summer_camp_invoices_this_year.present? ? @summer_camp_invoices_this_year.where(paid: true) : []
        @summer_camp_invoice_revenue = @summer_camp_paid_invoices.present? ? @summer_camp_paid_invoices.map(&:amount_to_dollars).reduce(:+).to_i : 0
        @summer_camp_outstanding_balance = @summer_camp_invoices_this_year.present? ? @summer_camp_invoices_this_year.where(paid: false)
            .map(&:amount_to_dollars).reduce(:+).to_i : 0
        @total_summer_camp_revenue = @summer_camp_deposit_revenue + @summer_camp_invoice_revenue
    end

    def initialize_all_invoices_and_registration_records
        @all_invoices = Invoice.includes(:program).references(:programs)
        @all_registration_records = RegistrationRecord.includes(:program).references(:programs)
        @all_year_round_single_day_records = RegistrationRecord.joins(:annual_single_day_promo_record)
    end
end