class Services::AdminDashboard::UpdateReceipt
    attr_reader :receipt_id, :payment_for, :child_information
    def self.invoke(receipt_id, payment_for, child_information)
        self.new(receipt_id, payment_for, child_information).execute
    end

    def initialize(receipt_id, payment_for, child_information)
        @receipt_id = receipt_id
        @payment_for = payment_for
        @child_information = child_information
    end

    def execute
        receipt = Receipt.find_by(id: receipt_id)
        return {
            message: "Unable to update receipt. It does not exist",
            status: 'failure',
            status_code: 404
        } if !receipt.present?
        receipt.update({
            payment_for: payment_for
        })
        child_information.each do |child|
            ChildRecord.find_by(id: child[:id]).update({
                first_name: child[:first_name],
                last_name: child[:last_name],
                dob: child[:date_of_birth]
            })
        end
        {
            message: 'The receipt has been updated succesfully',
            status: 'success',
            status_code: 200
        }
    end
end