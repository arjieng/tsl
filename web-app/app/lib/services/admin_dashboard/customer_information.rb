class Services::AdminDashboard::CustomerInformation
    attr_reader :customer_id

    def self.invoke(customer_id)
        self.new(customer_id).execute
    end

    def initialize(customer_id)
        @customer_id = customer_id
    end

    def execute
        customer = Customer.find_by(id: customer_id)
        result = {
            account_information: {
                first_name: customer.first_name.capitalize,
                last_name: customer.last_name,
                email: customer.email,
                address: customer.address,
                phone_number: customer.phone_number,
                emergency_phone_number: customer.emergency_phone_number,
                card_brand: customer.card_brand.present? ? customer.card_brand : 'N/A',
                bank_name: customer.bank_name.present? ? customer.bank_name : 'N/A'
            },
            registrations_information: Services::AdminDashboard::CustomerRegistrations.invoke(customer_id)
        }
    end
end