class Services::AdminDashboard::MonthlySummary
    include ActionView::Helpers::NumberHelper
    attr_reader :year, :month

    def self.invoke(year = Date.today.year, month = Date.today.month)
        self.new(year.to_i, month.to_i).execute
    end

    def initialize(year, month)
        @year = year
        @month = month
    end

    def execute
        initialize_all_invoices_and_registration_records
        set_weeks_for_summary
        result = calculate_result
    end

    private

    def initialize_all_invoices_and_registration_records
        @all_invoices = Invoice.includes(:program).references(:programs)
        @all_registration_records = RegistrationRecord.includes(:program).references(:programs)
        @all_year_round_single_day_records = RegistrationRecord.joins(:annual_single_day_promo_record)
    end

    def set_weeks_for_summary
        date = Date.new(year, month, 1)
        beginning_of_month = date.beginning_of_month.day
        end_of_month = date.end_of_month.day
        @weeks_array = []
        edge_counter = 0
        
        loop do
            container = []
            7.times do
                edge_counter += 1
                (container << edge_counter) if edge_counter <= end_of_month
            end
            @weeks_array << container unless container.empty?
            break if edge_counter > end_of_month
        end
    end

    def calculate_result
        the_month = Date.new(year, month, 1).strftime('%B')
        edge_counter = 0
        result = @weeks_array.inject({}) do |acc, curr|
            edge_counter += 1
            weekly_revenue = calculate_weekly_revenue(curr.first, curr.last)
            acc.store("week_#{edge_counter}", 
                {
                    "weekly_total": number_to_currency(weekly_revenue, precision: 0, delimiter: ', '),
                    "week_range": "#{the_month} #{curr.first} - #{curr.last}",
                    "weekly_total_numeric_form": weekly_revenue,
                    "single_day_weekly_revenue": summarize_single_day_records(curr.first, curr.last),
                    "year_round_single_day_weekly_revenue": summarize_year_round_single_day_records(curr.first, curr.last),
                    "after_school_weekly_revenue": summarize_after_school_records(curr.first, curr.last),
                    "day_care_weekly_revenue": summarize_day_care_records(curr.first, curr.last),
                    "vacation_camp_weekly_revenue": summarize_vacation_camp_records(curr.first, curr.last),
                    "summer_camp_weekly_revenue": summarize_summer_camp_records(curr.first, curr.last)
                }
            )
            acc
        end
    end

    def calculate_weekly_revenue(start_day, end_day)
        single_day_revenue = summarize_single_day_records(start_day, end_day)
        year_round_single_day_revenue = summarize_year_round_single_day_records(start_day, end_day)
        after_school_revenue = summarize_after_school_records(start_day, end_day)
        day_care_revenue = summarize_day_care_records(start_day, end_day)
        vacation_camp_revenue = summarize_vacation_camp_records(start_day, end_day)
        summer_camp_revenue =  summarize_summer_camp_records(start_day, end_day)
        result = 
            single_day_revenue.to_i +
            year_round_single_day_revenue.to_i +
            after_school_revenue.to_i +
            day_care_revenue.to_i +
            vacation_camp_revenue.to_i +
            summer_camp_revenue.to_i
    end

    def summarize_single_day_records(start_day, end_day)
        single_day_records = @all_registration_records.where('programs.name = ?', 'Single Day')
            .joins(:single_day_record)
            .where(created_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)

        single_day_revenue = single_day_records.pluck(:amount_paid).reduce(:+).to_i
    end

    def summarize_year_round_single_day_records(start_day, end_day) 
        year_round_recs = @all_year_round_single_day_records
            .where(created_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
        before_and_after_school_recs = @all_registration_records.where('programs.name = ?', 'After School')
            .joins(:before_and_afterschool_record)
            .where(created_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
        filtered = year_round_recs - before_and_after_school_recs
        year_round_revenue = filtered.map(&:amount_paid).map(&:to_i).reduce(:+).to_i
    end

    def summarize_after_school_records(start_day, end_day)
        before_and_after_school_recs = @all_registration_records.where('programs.name = ?', 'After School')
            .joins(:before_and_afterschool_record)
            .where(created_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
        after_school_deposit_revenue = before_and_after_school_recs.pluck(:amount_paid).reduce(:+).to_i
        after_school_invoice_revenue = 
            @all_invoices.where('programs.name = ?', 'After School')
                .where('extract (year from due_date) = ?', year)
                .where('extract (year from invoices.updated_at) = ? ', year)
                .where('extract (month from invoices.updated_at) = ? ', month)
                .where(paid: true, updated_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
                .map(&:amount_to_dollars).reduce(:+).to_i
        after_school_revenue = after_school_deposit_revenue + after_school_invoice_revenue
    end

    def summarize_day_care_records(start_day, end_day)
        day_care_revenue = 
            @all_invoices.where('programs.name = ?', 'Day Care')
            .where('extract (year from due_date) = ?', year)
            .where('extract (month from invoices.updated_at) = ? ', month)
            .where('extract (year from invoices.updated_at) = ? ', year)
            .where(paid: true, updated_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
            .map(&:amount_to_dollars).reduce(:+).to_i
    end

    def summarize_vacation_camp_records(start_day, end_day)
        vacation_camp_revenue =
            @all_registration_records.where('programs.name = ?', 'Vacation Camp')
            .where(created_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
            .pluck(:amount_paid).reduce(:+).to_i
    end

    def summarize_summer_camp_records(start_day, end_day)
        summer_camp_recs =
            @all_registration_records.where('programs.name = ?', 'Summer Camp')
                .where(created_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
        summer_camp_deposit_revenue = summer_camp_recs.pluck(:amount_paid).reduce(:+).to_i
        summer_camp_invoice_revenue = 
            @all_invoices.where('programs.name = ?', 'Summer Camp')
                .where('extract (year from due_date) = ?', year)
                .where('extract (month from invoices.updated_at) = ? ', month)
                .where('extract (year from invoices.updated_at) = ? ', year)
                .where(paid: true, updated_at: Date.new(year, month, start_day).beginning_of_day..Date.new(year, month, end_day).end_of_day)
                .map(&:amount_to_dollars).reduce(:+).to_i
        summer_camp_revenue = summer_camp_deposit_revenue + summer_camp_invoice_revenue
    end
end