class Services::AdminDashboard::ProgramBreakdown
    attr_reader :year, :month, :program

    def self.invoke(year, month, program)
        self.new(year, month, program).execute
    end

    def initialize(year, month, program)
        @year = year
        @month = month
        @program = program
        @monthly_summary = Services::AdminDashboard::MonthlySummary.invoke(year, month)
    end

    def execute
        case program
            when 'single_day'
                breakdown = @monthly_summary.inject([]) do |acc, curr|
                    acc << [curr.second[:single_day_weekly_revenue]]
                end
            when 'year_round_single_day'
                breakdown = @monthly_summary.inject([]) do |acc, curr|
                    acc << [curr.second[:year_round_single_day_weekly_revenue]]
                end
            when 'before_and_after_school'
                breakdown = @monthly_summary.inject([]) do |acc, curr|
                    acc << [curr.second[:after_school_weekly_revenue]]
                end
            when 'day_care'
                breakdown = @monthly_summary.inject([]) do |acc, curr|
                    acc << [curr.second[:day_care_weekly_revenue]]
                end
            when 'vacation_camp'
                breakdown = @monthly_summary.inject([]) do |acc, curr|
                    acc << [curr.second[:vacation_camp_weekly_revenue]]
                end
            when 'summer_camp'
                breakdown = @monthly_summary.inject([]) do |acc, curr|
                    acc << [curr.second[:summer_camp_weekly_revenue]]
                end
        end
        summary = {
            breakdown: breakdown,
            total: breakdown.flatten.reduce(:+)
        }
        return summary
    end
end