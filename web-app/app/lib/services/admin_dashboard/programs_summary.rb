class Services::AdminDashboard::ProgramsSummary
    attr_reader :year
    def self.invoke(year)
        self.new(year).execute
    end

    def initialize(year)
        @year = year.to_i
    end

    def execute
        yearly_summary = Services::AdminDashboard::YearlySummary.invoke(year)
        summarize_summer_camp
        {
            'single_day': {
                'monthly_average': yearly_summary[:program_breakdown][:single_day].reduce(:+) / 3,
                'yearly_total':  yearly_summary[:program_breakdown][:single_day].reduce(:+)
            },
            'year_round_single_day': {
                'monthly_average': yearly_summary[:program_breakdown][:year_round_single_day].reduce(:+) / 3,
                'yearly_total': yearly_summary[:program_breakdown][:year_round_single_day].reduce(:+)
            },
            'after_school': {
                'monthly_average': yearly_summary[:program_breakdown][:after_school].reduce(:+) / 3,
                'yearly_total':  yearly_summary[:program_breakdown][:after_school].reduce(:+)
            },
            'day_care': {
                'weekly_average': (yearly_summary[:program_breakdown][:day_care].reduce(:+) / 3) / 5,
                'yearly_total': yearly_summary[:program_breakdown][:day_care].reduce(:+)
            },
            'vacation_camp': {
                'monthly_average': yearly_summary[:program_breakdown][:vacation_camp].reduce(:+) / 3,
                'yearly_total': yearly_summary[:program_breakdown][:vacation_camp].reduce(:+)
            },
            'summer_camp': {
                'year_to_date_deposits_revenue': @summer_camp_deposit_revenue,
                'year_to_date_invoices_revenue': @summer_camp_invoice_revenue,
                'outstanding_balance': @outstanding_balance,
                'total_year_to_date_earnings': @summer_camp_total_revenue
            }
        }
    end

    private

    def summarize_summer_camp
        all_invoices = Invoice.includes(:program).references(:programs)
        summer_camp_recs = 
            RegistrationRecord.includes(:program).references(:programs).where('programs.name = ?', 'Summer Camp')
            .where(created_at: 
                Date.new(year, 1, 1).beginning_of_month.beginning_of_day..Date.new(year, Date.today.month, 1).end_of_month.end_of_day
            )
        @summer_camp_deposit_revenue = summer_camp_recs.pluck(:amount_paid).reduce(:+).to_i
        @summer_camp_invoice_revenue = 
                all_invoices.where('programs.name = ?', 'Summer Camp')
                .where('extract (year from due_date) = ?', Date.today.year)
                .where('extract (year from invoices.updated_at) = ?', Date.today.year)
                .where(paid: true)
                .map(&:amount_to_dollars).reduce(:+).to_i
        @outstanding_balance = all_invoices.where('programs.name = ?', 'Summer Camp')
            .where('extract (year from due_date) = ?', Date.today.year)
            .where(paid: false)
            .map(&:amount_to_dollars).reduce(:+).to_i
        @summer_camp_total_revenue = @summer_camp_deposit_revenue + @summer_camp_invoice_revenue
    end
end