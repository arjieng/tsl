class Services::AdminDashboard::RetrieveWeekRange
    attr_reader :week_range_array, :day

    def self.invoke(week_range_array, day)
        self.new(week_range_array, day).execute
    end

    def initialize(week_range_array, day)
        @week_range_array = week_range_array
        @day = day
    end

    def execute
        result = week_range_array.select do |week_range|
            week_range.include? day
        end
        result.flatten
    end
end