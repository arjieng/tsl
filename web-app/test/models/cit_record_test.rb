require 'test_helper'

class CitRecordTest < ActiveSupport::TestCase

    def setup
        @cit_record = CitRecord.new(
            id: 1,
            first_name: "Finn",
            last_name: "Mertens",
            dob: Date.parse("2010/10/10"),
            email: "cit_application@gmail.com",
            address_line_one: "221B Baker Street",
            city: "London",
            state: "England",
            zip_code: "1222",
            location: "Guilderland"
        )
    end

    test "may have many emergency contacts" do
        assert_respond_to @cit_record, :emergency_contacts
    end

    test "first name must be present" do
        @cit_record.first_name = ' '
        assert_not @cit_record.valid?
    end

    test 'last name must be present' do
        @cit_record.last_name = ' '
        assert_not @cit_record.valid?
    end

    test 'phone number must be present' do
        @cit_record.phone_number = ''
        assert_not @cit_record.valid?
    end

    test 'email must be present' do
        @cit_record.email = ' '
        assert_not @cit_record.valid?
    end

    test 'address line one must be present' do
        @cit_record.address_line_one = ' '
        assert_not @cit_record.valid?
    end

    test 'city must be present' do
        @cit_record.city = ' '
        assert_not @cit_record.valid?
    end

    test 'state must be present' do
        @cit_record.state = ' '
        assert_not @cit_record.valid?
    end

    test 'zip code must be present' do
        @cit_record.zip_code = ' '
        assert_not @cit_record.valid?
    end

    test 'how_did_you_hear_about_us must be present' do
        @cit_record.how_did_you_hear_about_us = ' '
        assert_not @cit_record.valid?
    end

    test 'location must be present' do
        @cit_record.location = ' '
        assert_not @cit_record.valid?
    end

    test 'previous experience must be present' do
        @cit_record.previous_experience = ' '
        assert_not @cit_record.valid?
    end

    test 'why_are_you_interested must be present' do
        @cit_record.why_are_you_interested = ' '
        assert_not @cit_record.valid?
    end

    test 'action when completed tasks must be present' do
        @cit_record.action_when_completed_tasks = ''
        assert_not @cit_record.valid?
    end

    test 'arguing_action must be present' do
        @cit_record.arguing_action = ' '
        assert_not @cit_record.valid?
    end

    test 'transition_help must be present' do
        @cit_record.transition_help = ' '
        assert_not @cit_record.valid? 
    end

    test 'interests_and_talents must be present' do
        @cit_record.interests_and_talents = ' '
        assert_not @cit_record.valid?
    end

    test 'proven_responsibility must be present' do
        @cit_record.proven_responsibility = ' '
        assert_not @cit_record.valid? 
    end

    test 'what_you_will_bring must be present' do
        @cit_record.what_you_will_bring = ' '
        assert_not @cit_record.valid?
    end

    test 'dob must be present' do
        @cit_record.dob = ' '
        assert_not @cit_record.valid?
    end

    test 'water_play_action must be present' do
        @cit_record.water_play_action = ' '
        assert_not @cit_record.valid? 
    end

    test 'four_square_action must be present' do
        @cit_record.four_square_action = ' '
        assert_not @cit_record.valid?
    end

    test 'uninteresting_activity_action' do
        @cit_record.uninteresting_activity_action = ' '
        assert_not @cit_record.valid?
    end

    test 'made_a_difference must be present' do
        @cit_record.made_a_difference = ' '
        assert_not @cit_record.valid?
    end

    test 'attended tsl before, cit at tsl before, willing to follow directions, ' \
        'willing to meet vision must be present and be either true or false' do
        @cit_record.attended_tsl_before = true
        @cit_record.cit_at_tsl_before = true
        @cit_record.willing_to_follow_directions = true
        @cit_record.willing_to_meet_vision = true
        assert_kind_of TrueClass || FalseClass, @cit_record.attended_tsl_before
        assert_kind_of TrueClass || FalseClass, @cit_record.cit_at_tsl_before
        assert_kind_of TrueClass || FalseClass, @cit_record.willing_to_follow_directions
        assert_kind_of TrueClass || FalseClass, @cit_record.willing_to_meet_vision
    end

    test 'full name function must return concatenation of first name and last name' do
        assert_equal @cit_record.full_name, 
        @cit_record.first_name + ' ' + @cit_record.last_name
    end

    test 'sent an email to tom for cit application' do
        assert_difference 'ActionMailer::Base.deliveries.count' do
            @cit_record.send_tom_cit_email_notification
        end
        mail = ActionMailer::Base.deliveries.last
        assert_equal 'hello@tsladventures.net', mail['from'].to_s
        assert_equal "New CIT Application for #{@cit_record.location}", mail['subject'].to_s
        assert_equal 'tsladventures@gmail.com, ktbro.tsl@gmail.com', mail['to'].to_s
    end

    test 'agree to behavior policy must be true' do
        @cit_record.save
        assert_includes @cit_record.errors.full_messages, "Agree to behavior policy must be checked"
        assert_not @cit_record.valid?
    end
end 