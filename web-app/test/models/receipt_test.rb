require 'test_helper'

class ReceiptTest < ActiveSupport::TestCase
    
    def setup
        @receipt = receipts(:receipt_one)
    end

    test "may belong to a registration record" do
        if @receipt.registration_record.present?
            assert_kind_of RegistrationRecord, @receipt.registration_record
        else
            assert_nil @receipt.registration_record
        end
    end

    test "may belong to an invoice" do
        if @receipt.invoice.present?
            assert_kind_of Invoice, @receipt.invoice
        else
            assert_nil @receipt.invoice
        end
    end

    test "must belong to a customer" do
        assert_kind_of Customer, @receipt.customer
    end
end
