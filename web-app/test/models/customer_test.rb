require 'test_helper'

class CustomerTest < ActiveSupport::TestCase
    def setup
        @customer = customers(:customer_one)
    end

    test "may have many registration records" do
        assert_respond_to @customer, :registration_records
    end

    test "may have many invoices" do
        assert_respond_to @customer, :invoices
    end

    test "must have one customer user" do
        assert_kind_of CustomerUser, @customer.customer_user
    end

    test "must return count of registration records" do
        assert_operator @customer.registration_records_count, :>=, 0
        assert_kind_of Fixnum, @customer.registration_records_count
    end

    test "must return full name" do
        assert_equal @customer.name, @customer.first_name + ' ' + @customer.last_name
    end

    test "must return either true or false for has complete information" do
        assert_includes [true, false], @customer.has_complete_information?
    end

    test "may have many receipts" do
        assert_respond_to @customer, :receipts
    end
end
