require 'test_helper'

class DayCareRecordTest < ActiveSupport::TestCase
    def setup
        @daycare = DayCareRecord.new(
            start_date: Date.parse("2018/10/10")
        )
    end

    test "start date must be present" do
        @daycare.start_date = ' '
        assert_not @daycare.valid?
    end
end