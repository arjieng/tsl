require 'test_helper'

class CustomerUserTest < ActiveSupport::TestCase

    def setup
        @customer_user = customer_users(:customer_user_one)
    end

    test "must belong to a customer" do
        assert_kind_of Customer, @customer_user.customer
    end

    test 'must respond to has_basic_information' do
        assert_respond_to @customer_user, :has_basic_information?
        assert_kind_of FalseClass || TrueClass, @customer_user.has_basic_information?
    end

    test 'must respond to has partial information' do
        assert_respond_to @customer_user, :has_basic_information?
        assert @customer_user.present?
        @customer_user.customer = nil
        assert_not @customer_user.has_basic_information?
    end
end