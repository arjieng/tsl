require 'test_helper'

class UserTest < ActiveSupport::TestCase

	def setup
		@user = User.new(
			name: "Finn Mertens",
			email: "finn.tsl@gmail.com",
			password: "password"
		)
	end

	test "must accept valid user credentials" do
		assert @user.valid?
	end

	test "may have many locations" do
		assert_equal 2, users(:user_one).locations.count
	end

	test "may have many programs" do
		assert_operator users(:user_one).programs.count, :>=, 1
	end

	test "email must be present" do
		@user.email = ' '
		assert_not @user.valid?
	end

	test "must reject invalid email addresses" do
		@user.email = 'test@gmail.com'
		assert_not @user.valid?
	end
end
