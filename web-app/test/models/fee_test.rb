require 'test_helper'

class FeeTest < ActiveSupport::TestCase

    def setup
        @fee = Fee.new(
            invoice_id: 1,
            amount: 50
        )
    end

    test "must belong to an invoice" do
        assert_kind_of Invoice, @fee.invoice
    end

    test "must return amount in cents" do
        assert_equal @fee.amount_in_cents, @fee.amount.to_f * 100.0
    end
end