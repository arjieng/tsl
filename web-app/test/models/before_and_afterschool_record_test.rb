require 'test_helper'
class BeforeAndAfterschoolRecordTest < ActiveSupport::TestCase
    
    def setup
        @rec = before_and_afterschool_records(:before_and_aftershool_record_one) 
    end
    test "must belong to a registration record" do
        assert @rec.registration_record_id.present?
    end

    test "must have a start date" do
        assert @rec.start_date.present?
    end

    test "must have at least one day selected" do
        assert_nil @rec.send(:one_day_chosen)
    end

    test "must return count of before days per week" do
        assert_operator @rec.before_days_per_week, :>=, 0
        assert_kind_of Fixnum, @rec.before_days_per_week
    end

    test "must return count of after days per week" do
        assert_operator @rec.after_days_per_week, :>=, 0
        assert_kind_of Fixnum, @rec.after_days_per_week
    end

    test "must return count of days per week" do
        assert_operator @rec.days_per_week, :>=, 0
        assert_kind_of Fixnum, @rec.days_per_week
    end
end