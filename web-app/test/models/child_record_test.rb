require 'test_helper'

class ChildRecordTest < ActiveSupport::TestCase

    def setup
        @child_record = child_records(:child_record_one)
    end

    test "must belong to a registration record" do
        assert @child_record.registration_record.present?
    end

    test "must have a first name" do
        assert @child_record.first_name.present?
        @child_record.first_name = ' '
        assert_not @child_record.valid?
    end

    test "must have a last name" do
        assert @child_record.last_name.present?
        @child_record.last_name = ' '
        assert_not @child_record.valid?
    end

    test "must have a valid date of birth format" do
        assert_raise do
            @child_record.dob = Date.parse("2010/10/0")
        end
    end

    test "must return age as integer" do
        assert_kind_of Fixnum, @child_record.age
    end
end