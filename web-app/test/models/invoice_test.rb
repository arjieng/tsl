require 'test_helper'

class InvoiceTest < ActiveSupport::TestCase

    def setup
        @invoice = invoices(:invoice_one)
    end

    # test for scopes start here #

    test "scope due_today must return invoices that are due today" do
        @due_today = Invoice.due_today
        assert_kind_of Invoice::ActiveRecord_Relation, @due_today
        assert_equal 10, @due_today.count
    end

    test "scope overdue_by_one_day must return invoices that are overdue by one day" do
        @overdue_by_one_day = Invoice.overdue_by_one_day
        assert_kind_of Invoice::ActiveRecord_Relation, @overdue_by_one_day
        assert_equal 20, @overdue_by_one_day.count
    end

    test "scope overdue by one week must return invoices overdue by one week" do
        @overdue_one_week = Invoice.overdue_by_one_week
        assert_kind_of Invoice::ActiveRecord_Relation, @overdue_one_week
        assert_equal 10, @overdue_one_week.count
    end

    test "scope overdue by two weeks must return invoices overdue by two weeks" do
        @overdue_two_weeks = Invoice.overdue_by_two_weeks
        assert_kind_of Invoice::ActiveRecord_Relation, @overdue_two_weeks
        assert_equal 10, @overdue_two_weeks.count
    end

    test "scope overdue this month must return invoices overdue this month" do
        @overdue_this_month = Invoice.overdue_this_month
        assert_kind_of Invoice::ActiveRecord_Relation, @overdue_this_month
        assert_operator @overdue_this_month.count, :>=, 0
    end

    test "scope `unpaid afterschool this month` must return invoices unpaid this mont" do
        unpaid_this_month = Invoice.unpaid_afterschool_this_month
        assert_kind_of Invoice::ActiveRecord_Relation, unpaid_this_month
        assert_operator unpaid_this_month.count, :>=, 0
    end

    # test for scopes ends here #

    test "overdue last month must return invoices overdue last month" do
        @overdue_last_month = Invoice.overdue_last_month
        assert_kind_of Invoice::ActiveRecord_Relation, @overdue_last_month
        assert_operator @overdue_last_month.count, :>=, 0
    end

    test "must belong to registration_record" do
        assert_kind_of RegistrationRecord, @invoice.registration_record
        assert_respond_to @invoice, :discounts
        assert_respond_to @invoice, :fees
    end

    test "memo and due_date must be present" do
        invoice = Invoice.new(
            registration_record_id: 1
        )
        invoice.save
        assert_includes invoice.errors.full_messages, "Due date can't be blank"
        assert_includes invoice.errors.full_messages, "Memo can't be blank"
    end

    test "invoice must be divided by 100 with amount to dollars" do
        assert_equal @invoice.amount_to_dollars, @invoice.amount / 100
    end

    test "invoice must return amount paid to dollars" do
        assert_equal @invoice.amount_paid_to_dollars, @invoice.amount_paid / 100
    end

    test "discounts total to dollars" do
        assert_equal @invoice.discounts_total_to_dollars,
            @invoice.discounts_total / 100
    end

    test "fees total to dollars" do
        assert_equal @invoice.fees_total, @invoice.fees_total / 100
    end

    test "amount after discounts and fees" do
        assert_equal @invoice.amount_after_discounts_and_fees,
            @invoice.amount - @invoice.discounts_total + @invoice.fees_total
    end

    test "amount after discounts and fees to dollars" do
        assert_equal @invoice.amount_after_discounts_and_fees_to_dollars,
            @invoice.amount_after_discounts_and_fees / 100
    end

    test "amount remaining" do
        assert_equal @invoice.amount_remaining,
            @invoice.amount_after_discounts_and_fees - @invoice.amount_paid
    end

    test "amount remaining to dollars" do
        assert_equal @invoice.amount_remaining_to_dollars,
            @invoice.amount_remaining / 100
    end

    test "discounts total" do
        assert_equal @invoice.discounts_total, @invoice.discounts.to_a.sum(&:amount_in_cents)
    end

    test "fees total" do
        assert_equal @invoice.fees_total, @invoice.fees.to_a.sum(&:amount_in_cents)
    end

    test "successful customer autopay charge" do
       # test invoice.autopay_charge customer here
    #    @invoice.autopay_charge_customer
    
    end

    test "invoice may have many receipts" do
        assert_respond_to @invoice, :receipts
    end
end