require 'test_helper'

class DiscountTest < ActiveSupport::TestCase
    
    def setup
        @discount = Discount.new(
            invoice_id: 1,
            registration_record_id: 1,
            program_id: 1,
            amount: 50.0
        )
    end

    test "must belong to an invoice" do
        assert_kind_of Invoice, @discount.invoice
    end

    test "must belong to a registration record" do
        assert_kind_of RegistrationRecord, @discount.registration_record
    end

    test "must belong to a program" do
        assert_kind_of Program, @discount.program     
    end

    test "must return amount in cents" do
        assert_equal @discount.amount_in_cents, @discount.amount.to_f * 100.0
        assert_kind_of Float, @discount.amount_in_cents
    end

end