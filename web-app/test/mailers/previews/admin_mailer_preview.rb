class AdminMailerPreview < ActionMailer::Preview
    def preview_notify_tom_of_record_update
        params = define_mailer_params
        groupon = RegistrationRecord.find(5727)
        registration_record_params = define_params
        AdminMailer.notify_tom_of_record_update(
            groupon,
            User.last
        )
    end

    def notify_tom_of_shop_purchase
        AdminMailer.notify_tom_of_shop_purchase(Purchase.first)
    end

    private

    def define_mailer_params
        {
            "utf8"=>"✓",
            "authenticity_token"=>"/OJxyYwKLrX3jYFQbPo5ZP8Cw4S7PpfE68nrFtyrtX4s/jinB55Ko1zsSugVcymK6O5AudT1x647xcomAWmiGw==",
            :registration_record=> {"location"=>"Clifton Park",
            :child_records_attributes=>{"0"=>{"first_name"=>"Finn", "last_name"=>"Mertens", "dob"=>"2010-10-10", "school_attending"=>"AT", "_destroy"=>"false"}},
            :attendance=>{"february"=>"2", "april"=>"3"},
            "coupon_code"=>"",
            "agree_to_tos"=>"1",
            "customer_id"=>"2168",
            "referrer"=>"vacation_camp"},
            "commit"=>"Submit And Pay With Card on File",
            "controller"=>"registration_records",
            "action"=>"create"
        }
    end

    def define_params
        {
            "location"=>"Clifton Park",
            "coupon_code"=>"",
            "referrer"=>"vacation_camp",
            "customer_id"=>"2168",
            "child_records_attributes"=>{"0"=>{"_destroy"=>"false", "first_name"=>"Finn", "last_name"=>"Mertens", "dob"=>"2010-10-10", "school_attending"=>"AT"}}
        }
    end
end