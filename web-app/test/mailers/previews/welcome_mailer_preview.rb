class WelcomeMailerPreview < ActionMailer::Preview
    def send_tom_email_notification
        single_day = RegistrationRecord.find(6418)
        vacation_camp = RegistrationRecord.find(5695)
        nine_week_promo_summer_camp = RegistrationRecord.find(5697)
        registration_record_params = define_params
        before_and_after_school = RegistrationRecord.find(6268)
        cit = RegistrationRecord.find(5709)
        groupon = RegistrationRecord.find(5727)
        params = define_mailer_params
        day_care = RegistrationRecord.find(5898)
        WelcomeMailer.send_tom_email_notification(
            # day_care,
            # registration_record_params,
            single_day,
            # day_care_params,
            params,
            day_care_mailer_params
        )
    end

    def day_care_welcome_email
        rec = RegistrationRecord.find(6736)
        WelcomeMailer.day_care_welcome_email(nil, rec)
    end

    private

    def define_params
        {
            "location"=>"Clifton Park",
            "coupon_code"=>"",
            "referrer"=>"vacation_camp",
            "customer_id"=>"2168",
            "child_records_attributes"=>{"0"=>{"_destroy"=>"false", "first_name"=>"Finn", "last_name"=>"Mertens", "dob"=>"2010-10-10", "school_attending"=>"AT"}}
        }
    end

    def define_mailer_params
        {
            "utf8"=>"✓",
            "authenticity_token"=>"/OJxyYwKLrX3jYFQbPo5ZP8Cw4S7PpfE68nrFtyrtX4s/jinB55Ko1zsSugVcymK6O5AudT1x647xcomAWmiGw==",
            :registration_record=> {"location"=>"Clifton Park",
            :child_records_attributes=>{"0"=>{"first_name"=>"Finn", "last_name"=>"Mertens", "dob"=>"2010-10-10", "school_attending"=>"AT", "_destroy"=>"false"}},
            :attendance=>{"february"=>"2", "april"=>"3"},
            "coupon_code"=>"",
            "agree_to_tos"=>"1",
            "customer_id"=>"2168",
            "referrer"=>"vacation_camp"},
            "commit"=>"Submit And Pay With Card on File",
            "controller"=>"registration_records",
            "action"=>"create"
        }
    end

    def day_care_params
        {
            "location"=>"Troy daycare",
            "agree_to_tos"=>"1",
            "referrer"=>"day_care",
            "customer_id"=>"2168",
            "day_care_record_attributes"=>{"start_date"=>"2019-01-25"},
            "child_records_attributes"=>{"0"=>{
                "_destroy"=>"false", "first_name"=>"Isaac", "last_name"=>"Clarke", "dob"=>"2010-10-10", "child_classification"=>"pre_schooler"}
            }
        }
    end

    def day_care_mailer_params
        {
            "utf8"=>"✓",
            "authenticity_token"=>"d82TJIGK6tAnIzTqRpTHDR5GjU9ogcUQVMGmrRdfYG3vNf/M/J1FmJNCjxqcYDH2Gf48w8ufdbXxTpRix3sXSQ==",
            "registration_record"=>
            {"location"=>"Troy daycare",
            "child_records_attributes"=>{"0"=>{"first_name"=>"Isaac", "last_name"=>"Clarke", "dob"=>"2010-10-10", "child_classification"=>"pre_schooler", "_destroy"=>"false"}},
            "day_care_record_attributes"=>{"start_date"=>"2019-01-25"},
            "agree_to_tos"=>"1",
            "customer_id"=>"2168",
            "referrer"=>"day_care"},
            "commit"=>"Submit",
            "controller"=>"registration_records",
            "action"=>"create"
        }
    end
end