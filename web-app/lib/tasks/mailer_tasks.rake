
# lib/tasks/mailer_tasks.rake
namespace :mail do
	desc "Sends reminders reminders for unpaid invoices"
	task :invoice_reminders => :environment do
		puts "Sending emails..."

		Invoice.due_today.each do |invoice|
			unless invoice.registration_record.program.name == "After School"
				InvoicesMailer.due_today(invoice.id).deliver!
			end
		end

		Invoice.overdue_by_one_day.each do |invoice|
			if invoice.registration_record.program.name == "Day Care"
				InvoicesMailer.overdue_by_one_day(invoice.id).deliver!
				InvoicesMailer.notify_tom_overdue_by_one_day(invoice.id).deliver!
				# notify Jaime for overdue daycare invoices
				InvoicesMailer.notify_jaime_overdue_daycare_by_one_day(invoice.id).deliver!
			end
		end

		puts "Complete!"
	end
end
