namespace :pay do
	task :autopay_invoices => :environment do
		Invoice.includes(registration_record: :customer).where('customers.autopay_enabled' => true).where(paid: false).where('registration_records.deleted = ?', false).where('invoices.due_date <= ?', Date.today).each do |invoice|
			puts "Invoice found! Charging customer..."
			invoice.autopay_charge_customer
		end
	end
end

namespace :add do
	desc 'Adds $5 Late Fee To Invoice for Every Day That It\'s Late'
	task :late_fees_to_overdue_invoices => :environment do
		puts "Adding Late Fees..."

		invoices = Invoice.includes(:registration_record, :program).references(:registration_records, :programs)
			.where('programs.name = ?', 'Day Care')
			.where('extract (year from invoices.created_at) = ?', Date.today.year)
			.where(paid: false)
			.where('registration_records.deleted = ?', false)
		invoices.each do |invoice|
			if invoice.due_date + 5.days <= Date.today
				invoice.fees.create!(amount: 5)
			end
		end
	end
end

namespace :generate do
	desc "Generates Daily Invoices..."
	task :invoices => :environment do
		puts 'Creating Day Care Invoices'
		start_date = Date.parse('14-03-2017') # March 14th, 2017

		day_care_program_ids = Program.where(name: "Day Care").map{|p| p.id }
		RegistrationRecord.where(enrolled: true, program_id: day_care_program_ids).where(deleted: false).where('created_at > ?', start_date.beginning_of_day).order(:id).each do |registration_record|
			puts "Checking Invoices for Registration Record: #{registration_record.id}"
			registration_record.create_daycare_invoices
			sleep(0.1)
		end
		puts 'Done With Day Care Invoices'
	end

	desc "Generate Yesterday Invoices..."
	task :yesterday_invoices => :environment do
		start_date = Date.parse('14-03-2017') # March 14th, 2017

		day_care_program_ids = Program.where(name: "Day Care").map{|p| p.id }
		RegistrationRecord.where(enrolled: true, program_id: day_care_program_ids).where(deleted: false).where('created_at > ?', start_date.beginning_of_day).each do |registration_record|
			puts "Checking Invoices for Registration Record: #{registration_record.id}"
			registration_record.create_yesterday_invoices
			sleep(0.1)
		end
	end

	desc "Summer Camp Backlog Invoices..."
	task :summer_camp_backlog_invoices => :environment do
		start_date = Date.parse('14-03-2017') # March 14th, 2017

		RegistrationRecord.where(enrolled: true).where(deleted: false).where('created_at > ?', start_date.beginning_of_day).each do |registration_record|
			registration_record.create_summer_camp_invoices
			sleep(0.1)
		end
	end
end
