namespace :format do
	task :child_record_dobs => :environment do
		# Update all records that are just strings of numbers
		ChildRecord.where.not(dob: nil).select{|c| !c.dob.include?("/") && !c.dob.include?("-") && !c.dob.include?(",") && !c.dob.include?("y") && !c.dob.include?("M") && !c.dob.include?("\\") && !c.dob.include?(".")}.each do |c|
			if c.dob.strip.length == 8
				puts "8: #{c.dob}"

				begin
					c.update_attributes(dob: Date.strptime(c.dob.strip, "%m%d%Y"))
				rescue
					c.update_attributes(dob: Date.strptime(c.dob.strip, "%Y%m%d"))
				end
			elsif c.dob.strip.length == 6
				puts "6: #{c.dob}"

				c.update_attributes(dob: Date.strptime(c.dob.strip, '%m%d%y'))
			elsif c.dob.strip.length == 7
				dob = "#{0}#{c.dob.strip}"
				puts "7: #{dob}"

				begin
					c.update_attributes(dob: Date.strptime(dob, '%m%d%Y'))
				rescue
					puts "Can't format: #{dob}"
					c.update_attributes(dob: nil)
				end
			elsif c.dob.strip.length == 5
				dob = "#{0}#{c.dob.strip}"
				puts "5: #{dob}"

				c.update_attributes(dob: Date.strptime(dob, '%m%d%y'))
			else
				puts "Unknown: #{c.dob}"

				c.update_attributes(dob: nil)
			end
		end

		# Update all records with "-" in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?("-")}.each do |c|
			begin
				Date.strptime(c.dob.strip, "%Y-%m-%d")

				c.update_attributes(dob: Date.strptime(c.dob.strip, "%Y-%m-%d"))
			rescue
				if c.dob.length == 10
					puts "10: #{c.dob}"

					begin
						c.update_attributes(dob: Date.strptime(c.dob, "%m-%d-%Y"))
					rescue
						c.update_attributes(dob: Date.strptime(c.dob, "%Y-%d-%m"))
					end
				elsif c.dob.length == 7
					dob = "0#{c.dob}"
					puts "7: #{dob}"

					c.update_attributes(dob: Date.strptime(dob, '%m-%d-%y'))
				elsif c.dob.length == 9
					dob = "0#{c.dob}"
					puts "9: #{c.dob}"

					begin
						c.update_attributes(dob: Date.strptime(dob, '%m-%d-%Y'))
					rescue
						c.update_attributes(dob: Date.strptime(c.dob, '%Y-%d-%m'))
					end
				elsif c.dob.length == 8
					puts "8: #{c.dob}"

					c.update_attributes(dob: Date.strptime(c.dob, '%m-%d-%y'))
				elsif c.dob == "12-15-20111"
					puts "Other: #{c.dob}"
					c.update_attributes(dob: Date.strptime("12-15-2011", "%m-%d-%Y"))
				else
					puts "Other: #{c.dob}"
				end
			end
		end

		# Update all records with "/" in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?("/")}.each do |c|
			if c.dob.length == 10
				puts "10: #{c.dob}"

				if c.dob.include?("EDD")
					dob = c.dob.gsub('EDD ', '')
					c.update_attributes(dob: Date.strptime(dob, '%m/%d/%y'))
				else
					c.update_attributes(dob: Date.strptime(c.dob, '%m/%d/%Y'))
				end
			elsif c.dob.length == 9
				puts "9: #{c.dob}"

				c.update_attributes(dob: Date.strptime(c.dob, '%m/%d/%Y'))
			elsif c.dob.length == 8
				puts "8: #{c.dob}"

				c.update_attributes(dob: Date.strptime(c.dob, '%m/%d/%y'))
			elsif c.dob.length == 7
				puts "7: #{c.dob}"

				c.update_attributes(dob: Date.strptime(c.dob, '%m/%d/%y'))
			elsif c.dob.length == 6
				puts "6: #{c.dob}"

				c.update_attributes(dob: Date.strptime(c.dob, '%m/%d/%y'))
			else
				puts "Other: #{c.dob}"
			end
		end

		# Update all records with "." in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?(".")}.each do |c|
			puts c.dob

			c.update_attributes(dob: Date.strptime(c.dob, '%m.%d.%Y'))
		end

		# Update all records with "Oct" in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?("Oct")}.each do |c|
			puts c.dob

			c.update_attributes(dob: Date.strptime(c.dob, '%b %d, %Y'))
		end

		# Update all records with "September" in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?("September")}.each do |c|
			puts c.dob

			c.update_attributes(dob: Date.strptime(c.dob, '%B %d, %Y'))
		end

		# Update all records with "March" in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?("March")}.each do |c|
			puts c.dob

			c.update_attributes(dob: Date.strptime(c.dob, '%B %d %Y'))
		end

		# Update all records with "May" in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?("May")}.each do |c|
			puts c.dob

			c.update_attributes(dob: Date.strptime(c.dob, '%B %d %Y'))
		end

		# Update all records with "July" in them
		ChildRecord.where.not(dob: nil).select{|c| c.dob.include?("July")}.each do |c|
			puts c.dob

			c.update_attributes(dob: Date.strptime(c.dob, '%B %d %Y'))
		end

		ChildRecord.where.not(dob: nil).all.each do |c|
			begin
				Date.strptime(c.dob.strip, "%Y-%m-%d")
			rescue
				if c.dob == "11\\06\\2006"
					c.update_attributes(dob: Date.strptime(c.dob, "%m\\%d\\%Y"))
				else
					puts "Other: #{c.dob}"
				end
			end
		end
	end
end

namespace :update do
	task :summer_camp_weeks => :environment do
		SummerCampWeek.where(name: "Friendzone").update_all(name: "Week One: Friendzone")
		SummerCampWeek.where(name: "Wacky Holiday").update_all(name: "Week Two: Wacky Holiday")
		SummerCampWeek.where(name: "Luau").update_all(name: "Week Three: Luau")
		SummerCampWeek.where(name: "Game Show").update_all(name: "Week Four: Game Show")
		SummerCampWeek.where(name: "Heroes vs Villains").update_all(name: "Week Five: Heroes vs Villains")
		SummerCampWeek.where(name: "Mash-Up").update_all(name: "Week Six: Mash-Up")
		SummerCampWeek.where(name: "Innovation Station").update_all(name: "Week Seven: Innovation Station")
		SummerCampWeek.where(name: "Under the Moon").update_all(name: "Week Eight: Under the Moon")
		SummerCampWeek.where(name: "Blockbuster").update_all(name: "Week Nine: Blockbuster")
		SummerCampWeek.where(name: "TSL Rewind").update_all(name: "Week Ten: TSL Rewind")
	end

	task :stripe_emails => :environment do
		Customer.where.not(stripe_customer_identifier: nil).each do |customer|
			puts "#{customer.email} - #{customer.stripe_customer_identifier}"
			stripe_customer = Stripe::Customer.retrieve(customer.stripe_customer_identifier)
			stripe_customer.email = customer.email
			stripe_customer.save
			puts "Done"
		end
	end
end

namespace :generate do
	task :before_and_afterschool_invoices => :environment do
		RegistrationRecord.before_and_afterschool_records_from_2018_and_onwards.each do |registration_record|
			# only send invoices for months October, November, and December on year 2018
			# hence invoices will be sent for registrations that have a start day on September 
			if registration_record.created_at.year == 2018
				if registration_record.before_and_afterschool_record.start_date.month >= 10
					puts "Generating invoice for #{registration_record.id}"
					registration_record.create_before_and_afterschool_invoice
					puts "Finished generating invoice for #{registration_record.id}"
				end
			else
				puts "Generating invoice for #{registration_record.id}"
				registration_record.create_before_and_afterschool_invoice
				puts "Finished generating invoice for #{registration_record.id}"
			end
		end
	end
end

namespace :create do
	task :single_day_dates => :environment do
		Program.where(id: [38, 42, 46, 51, 55, 74, 60]).each do |program|
			SingleDayDate.create!(
				name: "Winter Break",
				date: Date.parse("2017-12-26"),
				capacity: 100,
				program_id: program.id
			)
			SingleDayDate.create!(
				name: "Winter Break",
				date: Date.parse("2017-12-27"),
				capacity: 100,
				program_id: program.id
			)
			SingleDayDate.create!(
				name: "Winter Break",
				date: Date.parse("2017-12-28"),
				capacity: 100,
				program_id: program.id
			)
			SingleDayDate.create!(
				name: "Winter Break",
				date: Date.parse("2017-12-29"),
				capacity: 100,
				program_id: program.id
			)
			SingleDayDate.create!(
				name: "Martin Luther King Jr. Day",
				date: Date.parse("2018-01-15"),
				capacity: 100,
				program_id: program.id
			)
		end
	end

	task :summer_camp_weeks => :environment do
		Program.where(name: "Summer Camp").each do |program|
			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-06-25")).present?
				program.summer_camp_weeks.create!(
					name: "Friendzone",
					start_date: Date.parse("2018-06-25"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-07-02")).present?
				program.summer_camp_weeks.create!(
					name: "Wacky Holiday",
					start_date: Date.parse("2018-07-02"),
					capacity: 1000,
					notes: "Closed on 7/4"
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-07-09")).present?
				program.summer_camp_weeks.create!(
					name: "Luau",
					start_date: Date.parse("2018-07-09"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-07-16")).present?
				program.summer_camp_weeks.create!(
					name: "Game Show",
					start_date: Date.parse("2018-07-16"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-07-23")).present?
				program.summer_camp_weeks.create!(
					name: "Heroes vs Villains",
					start_date: Date.parse("2018-07-23"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-07-30")).present?
				program.summer_camp_weeks.create!(
					name: "Mash-Up",
					start_date: Date.parse("2018-07-30"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-08-06")).present?
				program.summer_camp_weeks.create!(
					name: "Innovation Station",
					start_date: Date.parse("2018-08-06"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-08-13")).present?
				program.summer_camp_weeks.create!(
					name: "Under the Moon",
					start_date: Date.parse("2018-08-13"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-08-20")).present?
				program.summer_camp_weeks.create!(
					name: "Blockbuster",
					start_date: Date.parse("2018-08-20"),
					capacity: 1000
				)
			end

			if !program.summer_camp_weeks.where(start_date: Date.parse("2018-08-27")).present?
				program.summer_camp_weeks.create!(
					name: "TSL Rewind",
					start_date: Date.parse("2018-08-27"),
					capacity: 1000,
					notes: "Closed on 08/31"
				)
			end
		end
	end
end
