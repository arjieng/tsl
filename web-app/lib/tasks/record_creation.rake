namespace :database do
    desc "create product records"
    task :create_product_records => :environment do
        puts "creating product records.."
        Product.create!(name: 'TSL Summer Tee', description: 'Get Your TSL Summer Tee Today!', image: 'summer-tee.png')
        puts "finished creating product records!"
    end

    desc "create size records"
    task :create_size_records => :environment do
        puts "creating size records.."
        Size.create!(
            [
                {name: "Youth Small", quantity: 50, price: 8.00, product_id: Product.find_by(name: 'TSL Summer Tee').id},
                {name: "Youth Medium", quantity: 50, price: 8.00, product_id: Product.find_by(name: 'TSL Summer Tee').id},
                {name: "Youth Large", quantity: 50, price: 8.00, product_id: Product.find_by(name: 'TSL Summer Tee').id},
                {name: "Adult Small", quantity: 50, price: 8.00, product_id: Product.find_by(name: 'TSL Summer Tee').id},
                {name: "Adult Large", quantity: 50, price: 8.00, product_id: Product.find_by(name: 'TSL Summer Tee').id},
                {name: "Adult Extra Large", quantity: 50, price: 8.00, product_id: Product.find_by(name: 'TSL Summer Tee').id},
                {name: "Adult Extra Extra Large", quantity: 50, price: 8.00, product_id: Product.find_by(name: 'TSL Summer Tee').id}
            ]
        )
        puts "finished creating size records!"
    end
end