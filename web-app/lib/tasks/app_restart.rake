task :restart do
  require 'platform-api'
  app_name = ENV.fetch('APP_NAME')
  key = ENV.fetch('RESTART_API_KEY')
  connection = PlatformAPI.connect_oauth(key)
  connection.dyno.list(app_name).map do |info|
    if info['type'] == 'web' && info['state'] == 'up'
      puts "Restarting #{info.inspect}"
      connection.dyno.restart(app_name, info['name'])
    else
      puts "Skipping #{info.inspect}"
    end
  end
end