# Preview all emails at http://localhost:3000/rails/mailers/invoices_mailer
class InvoicesMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/invoices_mailer/create
  def create
    InvoicesMailer.create
  end

end
