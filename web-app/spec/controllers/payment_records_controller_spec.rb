require 'rails_helper'

RSpec.describe PaymentRecordsController, type: :controller do

  describe "GET #single_day" do
    it "returns http success" do
      get :single_day
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #early_childhood_care" do
    it "returns http success" do
      get :early_childhood_care
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #before_and_afterschool" do
    it "returns http success" do
      get :before_and_afterschool
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #summer_camp" do
    it "returns http success" do
      get :summer_camp
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #vacation_camp" do
    it "returns http success" do
      get :vacation_camp
      expect(response).to have_http_status(:success)
    end
  end

end
