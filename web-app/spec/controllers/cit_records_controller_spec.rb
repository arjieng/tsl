require 'rails_helper'

RSpec.describe CitRecordsController, type: :controller do

  describe "GET #registration" do
    it "returns http success" do
      get :registration
      expect(response).to have_http_status(:success)
    end
  end

end
