require 'rails_helper'

RSpec.describe CustomerUsersController, type: :controller do

  describe "GET #edit_personal_information" do
    it "returns http success" do
      get :edit_personal_information
      expect(response).to have_http_status(:success)
    end
  end

end
