id: '92d6Bv0HSjClVvHsaylr-w'
frequency: every first of the month; 8:15 am
description: after school invoice generation
url: '/api/v1/invoices?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'zOQaqevJSQSdKKzi9q2yaQ'
frequency: every 4th of July
description: renders after school records obsolete
url: '/api/v1/records/obsolete/afterschool?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'ou2DtgCMQ0aeUBea1bTcEw'
frequency: every first of the month 5:15 am
description: background generate invoices through resque
url: '/api/v1/invoices/background-generate?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'JrWdaicESESVY5Eo2M6Lnw'
frequency: any day at the 27th - 30th of the month; the function will check whether today is the end of the month
description: send email notifications to admins and users 
url: '/api/v1/invoices/overdue/end-of-month-afterschool-notification?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: '9ozB46SqTWOVVmkMpZM2Wg'
frequency: every second of the month 
description: autopay invoices
url: 'api/v1/invoices/autopay?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'zR6cup3SRCmF8aQKA-aLGQ'
frequency: At 07:30 AM, only on Thursday
description: renders summer camp records overdue
url: 'api/v1/invoices/summer-camp/overdue?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'YgELqT3ES26NCab7GFov7Q'
frequency: Every 15th at 4:15 am
description: email notifications for overdue invoices; reminds clients that late fee generation commences tomorrow
url: '/api/v1/invoices/unpaid/after-school?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'jTGIl4w5TAu-E1iMlsAt2A'
frequency: Every 6th of October at 3:00 am
description: render summer camp records obsolete
url: 'api/v1/records/obsolete/summer-camp?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'kMTikV_wQkexvI8S4bbqMg'
frequency: Every 5th of the month at 6:00 am
description: dispatch email notifications for unpaid invoices
url: '/api/v1/invoices/overdue/one-week?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'ZsDLbaT8TzCvqs7iLi0Nqw'
frequency: Every first of the month at 7:15 am
description: generate afeterschool invoices
url: '/api/v1/invoices?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'BBJp8YP8Sp2xDw8twf3EmA'
frequency: At 10:00 AM, only on Monday
description: sending email notifications for overdue summer camp invoices
url: 'api/v1/invoices/summer-camp/due?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'Ouqr0qS5ROOD0iFnlUk9YQ'
frequency: Every 2nd of the month at 8:00 am
description: autopay overdue after school invoices
url: '/api/v1/invoices/autopay?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>
id: 'kz_T4AyMS1CtJ9Bu5AGODg'
frequency: Every monday at 10:30 am
description: autopay every monday summercamp
url: '/api/v1/invoices/summer-camp/autopay?access_key=kz_T4AyMS1CtJ9Bu5AGODg'

# ==============================================================>

id: 'jR2rC0-nQ_mbuAHEnv8AMA'
frequency: Every 7th of July
description: render vacation camp records obsolete
url: 'api/v1/records/obsolete/vacation-camp?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'pIfy12sTQT-HSE9TQ6i7Kg'
frequency: Every first of the month at 6:15 pm
description: invoice generation
url: 'api/v1/invoices?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'pEvHfSIqTL-Ioab8Z3TOsA'
frequency: At 06:00 AM, only on Friday
description: notifications for unpaid daycare invoices
url: '/api/v1/invoices/unpaid/daycare?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'li6NOCGxQQS97P4f1f-OHQ'
frequency: At 08:00 AM, on day 16 - 31 of the month
description: late fee generation for unpaid invoices
url: '/api/v1/invoices/overdue/fee?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'm3QBURMZRwSam4GSzLcWWA'
frequency: every fifth of July at 3:00
description: render single day records obsolete
url: 'api/v1/records/obsolete/single-day?access_key=g5OrKEEjnzN07j66vV8ngw'

# ==============================================================>

id: 'rtTjxCtGQHix77PsERQc_g'
frequency: every 27 - 31 of the month, 4:10 am
description: sends notifications to administrators for unpaid after school invoices for the month
url: '/api/v1/invoices/overdue/end-of-month-after-school?access_key=g5OrKEEjnzN07j66vV8ngw'