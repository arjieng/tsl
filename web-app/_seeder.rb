require_relative 'config/environment.rb'

b = ["East Greenbush-FUMC", "Guilderland - Christ Lutheran Church", "Guilderland - School House Road", "Niskayuna", "Rotterdam", "Troy- Sacred Heart"]
b.each do |loc|
	@program = Location.find_by(name: loc.to_s).programs.find_by(name: "Vacation Camp")
	if loc.to_s.eql?("East Greenbush-FUMC")
		@program.vacation_camp_schedules.create(name: "December Camp (Dec. 23-Jan. 3)", year: 2019, month: "december", day_twenty_three: true, day_twenty_four: true, day_twenty_five: true, day_twenty_six: true, day_twenty_seven: true, day_one: true, day_two: true, day_three: true, day_thirty: true, day_thirty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "February Camp (Feb. 17-21)", year: 2020, month: "february", day_seventeen: true, day_eighteen: true, day_nineteen: true, day_twenty: true, day_twenty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "April Camp (Apr. 10-17)", year: 2020, month: "april", day_ten: true, day_eleven: true, day_twelve: true, day_thirteen: true, day_fourteen: true, day_fifteen: true, day_sixteen: true, day_seventeen: true, capacity: 50)
	end

	if loc.to_s.eql?("Guilderland - Christ Lutheran Church")
		@program.vacation_camp_schedules.create(name: "December Camp (Dec. 23-Jan. 3)", year: 2019, month: "december", day_twenty_three: true, day_twenty_four: true, day_twenty_five: true, day_twenty_six: true, day_twenty_seven: true, day_one: true, day_two: true, day_three: true, day_thirty: true, day_thirty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "February Camp (Feb. 17-21)", year: 2020, month: "february", day_seventeen: true, day_eighteen: true, day_nineteen: true, day_twenty: true, day_twenty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "April Camp (Apr. 10-17)", year: 2020, month: "april", day_ten: true, day_eleven: true, day_twelve: true, day_thirteen: true, day_fourteen: true, day_fifteen: true, day_sixteen: true, day_seventeen: true, capacity: 50)
	end

	if loc.to_s.eql?("Niskayuna")
		@program.vacation_camp_schedules.create(name: "December Camp (Dec. 23-Jan. 3)", year: 2019, month: "december", day_twenty_three: true, day_twenty_four: true, day_twenty_five: true, day_twenty_six: true, day_twenty_seven: true, day_one: true, day_two: true, day_three: true, day_thirty: true, day_thirty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "February Camp (Feb. 17-21)", year: 2020, month: "february", day_seventeen: true, day_eighteen: true, day_nineteen: true, day_twenty: true, day_twenty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "April Camp (Apr. 10-17)", year: 2020, month: "april", day_ten: true, day_eleven: true, day_twelve: true, day_thirteen: true, day_fourteen: true, day_fifteen: true, day_sixteen: true, day_seventeen: true, capacity: 50)
	end

	if loc.to_s.eql?("Rotterdam")
		@program.vacation_camp_schedules.create(name: "December Camp (Dec. 23-Jan. 3)", year: 2019, month: "december", day_twenty_three: true, day_twenty_four: true, day_twenty_five: true, day_twenty_six: true, day_twenty_seven: true, day_one: true, day_two: true, day_three: true, day_thirty: true, day_thirty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "February Camp (Feb. 17-21)", year: 2020, month: "february", day_seventeen: true, day_eighteen: true, day_nineteen: true, day_twenty: true, day_twenty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "April Camp (Apr. 10-17)", year: 2020, month: "april", day_ten: true, day_eleven: true, day_twelve: true, day_thirteen: true, day_fourteen: true, day_fifteen: true, day_sixteen: true, day_seventeen: true, capacity: 50)
	end

	if loc.to_s.eql?("Guilderland - School House Road")
		@program.vacation_camp_schedules.create(name: "February Camp (Feb. 17-21)", year: 2020, month: "february", day_seventeen: true, day_eighteen: true, day_nineteen: true, day_twenty: true, day_twenty_one: true, capacity: 50)

		@program.vacation_camp_schedules.create(name: "April Camp (Apr. 10-17)", year: 2020, month: "april", day_ten: true, day_eleven: true, day_twelve: true, day_thirteen: true, day_fourteen: true, day_fifteen: true, day_sixteen: true, day_seventeen: true, capacity: 50)
	end

	if loc.to_s.eql?("Troy- Sacred Heart")
		@program.vacation_camp_schedules.create(name: "April Camp (Apr. 10-17)", year: 2020, month: "april", day_ten: true, day_eleven: true, day_twelve: true, day_thirteen: true, day_fourteen: true, day_fifteen: true, day_sixteen: true, day_seventeen: true, capacity: 50)
	end
end



# id_array = []
# b.each do |loc|
# 	@program = Location.find_by(name: loc.to_s).programs.find_by(name: "Vacation Camp")
# 	a = @program.vacation_camp_schedules.where(name: ["December Camp (Dec. 23-27)", "December Camp (Dec. 30-Jan. 3)", "February 17-21", "April 10-17"])
# 	a.each do |vsc|
# 		id_array.push vsc.id
# 		puts "#{vsc.id} #{vsc.name} #{vsc.month}"
# 	end
# end




# id_array = []
# b.each do |loc|
# 	@program = Location.find_by(name: loc.to_s).programs.find_by(name: "Vacation Camp")
# 	a = @program.vacation_camp_schedules.where(name: ["December Camp (Dec. 23-Jan. 3)", "February Camp (Feb. 17-21)", "April Camp (Apr. 10-17)"])
# 	a.each do |vsc|
# 		id_array.push vsc.id
# 		puts "#{vsc.id} #{vsc.name} #{vsc.month}"
# 	end
# end





# vcs = VacationCampSchedule.where(id: id_array)

# puts id_array