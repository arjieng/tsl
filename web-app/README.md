# TSL Information System

## Getting Started

TSLIS is running on Rails 4.2.1

```
$ git clone git@github.com:delightconsulting/tslis.git
$ cd tslis/
$ bundle install
```

The database that's being used is Postgresql. The configuration must be manually implemented by you but an example file is provided for you.

```
$ cp config/example_database.yml config/database.yml
```

The configuration in the `example_database.yml` may work for you out of the box, though you may need to tweak it for your local Postgresql configuration.

TSLIS uses [Figaro](https://github.com/laserlemon/figaro) to handle environment variables that are sensitive and don't belong in the repository. You'll need to create a file for these variables.

```
$ touch config/application.yml
```

You'll need to configure this file on your own as well. To get the values that you'll need for it please email [devwiz.sh@gmail.com](mailto:devwiz.sh@gmail.com).


If you configured your `database.yml` file correctly in the previous steps you may now create your database and tables.

```
$ rake db:create
$ rake db:schema:load
```

At this point you should be ready to boot up your Rails Server and test things out in the browser at [localhost:3000](http://localhost:3000).

```
$ rails s
```

## Deployment

TSLIS is run on Heroku. To deploy, first commit your changes and then...

```
$ git push heroku master
$ heroku run rake db:migrate
```

## Cloning Production Data To Development

```
$ rake db:drop
$ rake db:create
$ rake db:migrate
$ heroku pg:backups:capture
$ heroku pg:backups:download
$ pg_restore -d tslis_development -a latest.dump
```

## Testing

to run the test suite execute: ```rake test```

## Annual Single Day Package Record

# Nested within the before and after school record view

## The app uses the ```Temporize``` heroku add on for scheduled events

# See the temporize_schedules file for refence on the events