﻿using System;
using System.Collections.Generic;
using TSLAdventures.Utilities;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class HomePage : RootViewPage
    {
        public HomePage()
        {
            DataClass dataClass = DataClass.GetInstance;
            InitializeComponent();
            SetNavigationBar();

            System.Diagnostics.Debug.WriteLine(Newtonsoft.Json.JsonConvert.SerializeObject(dataClass.CustomerUser));
        }

        void SetNavigationBar()
        {
            this.LeftIcon = "Hamburger.png";
            this.LeftButtonCommand = new Command((x) => ((MasterDetailPage)Application.Current.MainPage).IsPresented = true);
        }

        void SingleDayCareButton_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new SingleDayCarePage());
        }

        void DayCareButton_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new DayCarePage());
        }

        void DayCareTourButton_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new DayCareTourPage());
        }

        void BeforeAfterSchoolButton_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new BeforeAfterSchoolPage());
        }

        void SummerCampButton_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new SummerCampPage());
        }

        void VacationCampButton_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new VacationCampPage());
        }

        void CITApplicationButton_Clicked(object sender, System.EventArgs e)
        {
            Navigation.PushAsync(new CITApplicationPage());
        }
    }
}
