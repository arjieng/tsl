﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using Xamarin.Forms;

namespace TSLAdventures.Views.PopupPages
{
    public partial class MoreOptionsPage : PopupPage
    {
        public MoreOptionsPage()
        {
            InitializeComponent();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            PopupNavigation.PopAsync();
        }
    }
}
