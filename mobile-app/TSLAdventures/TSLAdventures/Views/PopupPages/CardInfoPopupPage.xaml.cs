﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace TSLAdventures.Views.PopupPages
{
    public partial class CardInfoPopupPage : PopupPage, IRestConnector
    {
        RestService restService;
        CancellationTokenSource cts;
        Action<string> createAction;
        public CardInfoPopupPage(Action<string> createAction)
        {
            this.createAction = createAction;
            restService = new RestService { RestServiceDelegate = this };
            InitializeComponent();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            PopupNavigation.PopAsync();
        }

        async void CreateCard_Clicked(object sender, System.EventArgs e)
        {
            await Task.Run(async () =>
            {
                cts = new CancellationTokenSource();
                string[] expiry = expiryDate.Text.Split('/');
                await restService.RequestCardToken(cardNumber.Text, expiry[0], expiry[1], cvc.Text, cts.Token);
            });
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine("=========");
            System.Diagnostics.Debug.WriteLine(jsonData);
            System.Diagnostics.Debug.WriteLine("=========");
            createAction?.Invoke(jsonData["id"].ToString());
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            var obj = JObject.Parse(error);
            DisplayAlert("Card Failed", obj["error"]["message"].ToString(), "Okay");
        }
    }
}
//{
//    "error": {
//        "code": "incorrect_number",
//        "doc_url": "https://stripe.com/docs/error-codes/incorrect-number",
//        "message": "Your card number is incorrect.",
//        "param": "number",
//        "type": "card_error"
//    }
//}