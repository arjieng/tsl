﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Pages;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using Xamarin.Forms;

namespace TSLAdventures.Views.PopupPages
{
    public partial class EnrollmentinfoPage : PopupPage
    {
        public EnrollmentinfoPage()
        {
            InitializeComponent();
        }

        void Handle_Clicked(object sender, System.EventArgs e)
        {
            PopupNavigation.PopAsync();
        }
    }
}
