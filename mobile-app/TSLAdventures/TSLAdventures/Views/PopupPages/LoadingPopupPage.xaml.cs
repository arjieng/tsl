﻿using System;
using System.Collections.Generic;
using Rg.Plugins.Popup.Pages;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using Xamarin.Forms;

namespace TSLAdventures.Views.PopupPages
{
    public partial class LoadingPopupPage : PopupPage
    {
        public LoadingPopupPage()
        {
            InitializeComponent();
            var src = "loading.gif";
            int imageWidth = 60.scale();
            int imageHeight = 60.scale();
            var html = $"<body ><img src=\"{src}\" alt=\"A Gif file\" width=\"{imageWidth}\" height=\"{imageHeight}\" style=\"width: 100%; height: auto;\"/></body>";
            webView.Source = new HtmlWebViewSource
            {
                Html = html
            };
        }
    }
}
