﻿using System;
using System.Collections.Generic;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class ViewReceiptPage : RootViewPage
    {
        public ViewReceiptPage(Receipts receipts)
        {
            this.BindingContext = new ViewReceiptPageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }
    }
}
