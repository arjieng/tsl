﻿using System;
using System.Collections.Generic;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class UpdatePaymentInfoPage : RootViewPage
    {
        bool isFirst = true;
        public UpdatePaymentInfoPage()
        {
            System.Diagnostics.Debug.WriteLine(isFirst);
            this.BindingContext = new UpdatePaymentInfoPageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "Hamburger.png";
            this.LeftButtonCommand = new Command((x) => ((MasterDetailPage)Application.Current.MainPage).IsPresented = true);
            var context = this.BindingContext as UpdatePaymentInfoPageViewModel;
            if (!context.AutoPayEnabled)
            {
                isFirst = false;
            }
            System.Diagnostics.Debug.WriteLine(isFirst);
        }

        void AutoPay_Toggled(object sender, Xamarin.Forms.ToggledEventArgs e)
        {
            var context = this.BindingContext as UpdatePaymentInfoPageViewModel;

            if (!isFirst)
            {
                context.AutoPayToggledAction();
            }
            else
            {
                isFirst = false;
            }
        }
    }
}
