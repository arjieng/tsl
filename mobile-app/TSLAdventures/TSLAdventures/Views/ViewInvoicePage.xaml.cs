﻿using System;
using System.Collections.Generic;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class ViewInvoicePage : RootViewPage
    {
        public int id;
        public ViewInvoicePage(Invoices invoices)
        {
            this.id = invoices.id;
            this.BindingContext = new ViewInvoicePageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());

            var context = this.BindingContext as ViewInvoicePageViewModel;
            context.Invoice = invoices;
            context.Init();
        }
    }
}
