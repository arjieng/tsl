﻿using TSLAdventures.ViewModels;

namespace TSLAdventures.Views
{
    public partial class SigninPage : RootViewPage
    {
        public SigninPage()
        {
            this.BindingContext = new SigninPageViewModel(this);
            InitializeComponent();
        }
    }
}