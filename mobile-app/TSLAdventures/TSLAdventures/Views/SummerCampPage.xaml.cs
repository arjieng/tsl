﻿using System;
using System.Collections.Generic;
using Plugin.InputKit.Shared.Controls;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class SummerCampPage : RootViewPage
    {
        public SummerCampPage()
        {
            this.BindingContext = new SummerCampPageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }

        void Handle_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var context = this.BindingContext as SummerCampPageViewModel;
            context.SelectedLocationAddress = context.LocationAddress[context.SelectedIndex];

            FormattedString formatted = new FormattedString();
            Span span1 = new Span { Text = "Accepting children " };
            Span span2 = new Span { Text = context.LocationMinimumAge[context.SelectedIndex].ToString(), FontAttributes = FontAttributes.Bold, FontSize = 11.ScaleFont() };
            Span span3 = new Span { Text = " years old and up at this location." };
            formatted.Spans.Add(span1);
            formatted.Spans.Add(span2);
            formatted.Spans.Add(span3);
            minimumAgeIndicator.FormattedText = formatted;

            context.FetchSummerSchedules(context.LocationSource[context.SelectedIndex]);
        }

        void TOSLabel_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TOSPage(3));
        }
    }
}
