﻿using System;
using System.Collections.Generic;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class ForgotPasswordPage : RootViewPage
    {
        public ForgotPasswordPage()
        {
            this.BindingContext = new ForgotPasswordPageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }
    }
}
