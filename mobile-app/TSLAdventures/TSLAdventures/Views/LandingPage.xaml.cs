﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class LandingPage : MasterDetailPage
    {
        public LandingPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new HomePage())
            {
                BarTextColor = Color.White
            };
            ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
        }
    }
}
