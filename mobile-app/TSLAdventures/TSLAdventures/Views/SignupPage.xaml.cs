﻿using TSLAdventures.ViewModels;

namespace TSLAdventures.Views
{
    public partial class SignupPage : RootViewPage
    {
        public SignupPage()
        {
            this.BindingContext = new SignupPageViewModel(this);
            InitializeComponent();
        }
    }
}
