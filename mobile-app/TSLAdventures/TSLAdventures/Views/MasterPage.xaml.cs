﻿using System;
using System.Collections.Generic;
using TSLAdventures.Utilities;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class MasterPage : RootViewPage
    {
        DataClass dataClass;
        public MasterPage()
        {
            dataClass = DataClass.GetInstance;
            InitializeComponent();
            var customer_user = dataClass.CustomerUser;
            NameEntry.Text = string.Format("{0} {1}", customer_user.first_name, customer_user.last_name);
            EmailEntry.Text = customer_user.email;
        }

        void HomeButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetRow(BGBoxView, 1);
            ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new HomePage())
            {
                BarTextColor = Color.White
            };
            ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
        }

        void UpdatePaymentButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetRow(BGBoxView, 2);
            ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new UpdatePaymentInfoPage())
            {
                BarTextColor = Color.White
            };
            ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
        }

        void BillingButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetRow(BGBoxView, 3);
            ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new BillingPage())
            {
                BarTextColor = Color.White
            };
            ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
        }

        void AccountButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetRow(BGBoxView, 4);
            ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new AccountPage())
            {
                BarTextColor = Color.White
            };
            ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
        }

        void setColor(int index)
        {
            Label[] labels = { label1, label2, label3, label4 };
            foreach(var label in labels)
            {
                label.TextColor = Color.Black;
            }
            labels[index].TextColor = Color.FromHex("#357EDD");
        }

        void LogoutButton_Clicked(object sender, System.EventArgs e)
        {
            dataClass.CustomerUser = null;
            App.Current.MainPage = new NavigationPage(new SigninPage())
            {
                BarTextColor = Color.White
            };
        }
    }
}
