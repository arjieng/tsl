﻿using System;
using System.Collections.Generic;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class DayCareTourPage : RootViewPage
    {
        public DayCareTourPage()
        {
            this.BindingContext = new DayCareTourPageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }

        void Location_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var context = this.BindingContext as DayCareTourPageViewModel;
            context.SelectedLocationAddress = context.LocationAddress[context.SelectedIndex];
        }
    }
}
