﻿using System;
using System.Collections.Generic;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class SingleDayCarePage : RootViewPage
    {
        public SingleDayCarePage()
        {
            this.BindingContext = new SingleDayCarePageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }

        void Location_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var context = this.BindingContext as SingleDayCarePageViewModel;
            context.SelectedLocationAddress = context.LocationAddress[context.SelectedIndex];
            context.ChangeHeight();

            FormattedString formatted = new FormattedString();
            Span span1 = new Span { Text = "Accepting children " };
            Span span2 = new Span { Text = context.LocationMinimumAge[context.SelectedIndex].ToString(), FontAttributes = FontAttributes.Bold, FontSize = 11.ScaleFont() };
            Span span3 = new Span { Text = " years old and up at this location." };
            formatted.Spans.Add(span1);
            formatted.Spans.Add(span2);
            formatted.Spans.Add(span3);
            minimumAgeIndicator.FormattedText = formatted;
        }

        void OnLabel_Clicked(object sender, EventArgs e)
        {
            var context = this.BindingContext as SingleDayCarePageViewModel;
            var label = sender as Label;

            if (label.Text.Contains("Promotional"))
            {
                Grid.SetRow(CircleCheck, 1);
                Grid.SetRow(EmptyCircle, 0);
                label.TextColor = Color.White;
                Label1.TextColor = Color.Black;
                Boxview1.TranslateTo(-App.screenWidth, Boxview1.Y);
                Boxview2.TranslateTo(0, Boxview1.Y);
                PromotionalView.IsVisible = true;
                StandardView.IsVisible = false;
                context.RegistrationType = "promotional";
                context.CalculateAmount(context.RegistrationType);
            }
            else
            {
                Grid.SetRow(CircleCheck, 0);
                Grid.SetRow(EmptyCircle, 1);
                label.TextColor = Color.White;
                Label2.TextColor = Color.Black;
                Boxview2.TranslateTo(-App.screenWidth, Boxview1.Y);
                Boxview1.TranslateTo(0, Boxview1.Y);
                PromotionalView.IsVisible = false;
                StandardView.IsVisible = true;
                context.RegistrationType = "standard";
                context.CalculateAmount(context.RegistrationType);
            }
        }
    }
}
