﻿using System;
using System.Collections.Generic;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class CITApplicationPage : RootViewPage
    {

        public CITApplicationPage()
        {
            this.BindingContext = new CITApplicationPageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }
    }
}
