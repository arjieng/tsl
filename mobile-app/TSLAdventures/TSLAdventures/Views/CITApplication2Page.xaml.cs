﻿using System;
using System.Collections.Generic;
using System.Linq;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class CITApplication2Page : RootViewPage
    {

        public CITApplication2Page(Dictionary<string, string> objects)
        {
            this.BindingContext = new CITApplication2PageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());

            var context = this.BindingContext as CITApplication2PageViewModel;
            context.RegistreeInformations.LocationPickerSelectedItem = objects["location"];
            context.RegistreeInformations.FirstNameTextSource = objects["first_name"];
            context.RegistreeInformations.LastNameTextSource = objects["last_name"];
            context.RegistreeInformations.BirthdayTextSource = objects["dob"];
            context.RegistreeInformations.PhoneNumberTextSource = objects["phone_number"];
            context.RegistreeInformations.EmailAddressTextSource = objects["email"];
            context.RegistreeInformations.AddressOneTextSource = objects["address_line_one"];
            context.RegistreeInformations.AddressTwoTextSource = objects["address_line_two"];
            context.RegistreeInformations.CityTextSource = objects["city"];
            context.RegistreeInformations.StateTextSource = objects["state"];
            context.RegistreeInformations.ZipCodeTextSource = objects["zip_code"];
            context.RegistreeInformations.PickerSelectedItem = objects["how_did_you_hear_about_us"];



        }

        void TOSLabel_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TOSPage(5));
        }
    }
}
