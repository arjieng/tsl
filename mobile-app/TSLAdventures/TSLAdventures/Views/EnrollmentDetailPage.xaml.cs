﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TSLAdventures.Models;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class EnrollmentDetailPage : RootViewPage
    {
        public EnrollmentDetailPage(RegistrationInfo info)
        {
            this.BindingContext = new EnrollmentDetailPageViewModel(this, info);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }

        void ChildList_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            System.Diagnostics.Debug.WriteLine("tap");
            ChildInfoList.SelectedItem = null;
        }
    }
}
