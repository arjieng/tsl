﻿using System;
using System.Collections.Generic;
using TSLAdventures.Models;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class UserInfoPage : RootViewPage
    {
        public UserInfoPage(bool isNew, string email = null)
        {
            this.BindingContext = new UserInfoPageViewModel(this, isNew, email);
            InitializeComponent();
            if (!isNew)
            {
                this.LeftIcon = "WhiteBack";
                this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
            }
            else
            {
                this.LeftIcon = "WhiteClose";
                this.LeftButtonCommand = new Command((obj) =>
                {
                    App.Current.MainPage = new NavigationPage(new SigninPage())
                    {
                        BarTextColor = Color.White
                    };
                });
            }
        }
    }
}
