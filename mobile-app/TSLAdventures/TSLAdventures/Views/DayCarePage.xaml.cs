﻿using System;
using System.Collections.Generic;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class DayCarePage : RootViewPage
    {
        public DayCarePage()
        {
            this.BindingContext = new DayCarePageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }

        void Location_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var context = this.BindingContext as DayCarePageViewModel;
            context.SelectedLocationAddress = context.LocationAddress[context.SelectedIndex];
        }
    }
}
