﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.ViewModels;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class AccountPage : RootViewPage
    {
        public AccountPage()
        {
            this.BindingContext = new AccountPageViewModel(this);
            InitializeComponent();
            SetNavigationBar();
        }

        async void DisplayActionSheet()
        {
            var actionSheet = await DisplayActionSheet("Choose Action", "Cancel", "Delete Account", "Edit Personal Info", "View Billing Info");
            switch (actionSheet)
            {
                case "Cancel":

                    System.Diagnostics.Debug.WriteLine("Cancel");

                    break;

                case "Edit Personal Info":

                    await Navigation.PushAsync(new UserInfoPage(false));
                    break;

                case "View Billing Info":

                    ((MasterDetailPage)App.Current.MainPage).Detail = new NavigationPage(new UpdatePaymentInfoPage())
                    {
                        BarTextColor = Color.White
                    };
                    ((MasterDetailPage)App.Current.MainPage).IsPresented = false;
                    break;

                case "Delete Account":

                    var alert = await DisplayAlert("Delete Account?", "Deleting your account is permanent. Are you sure you want to delete your account?", "Delete Account", "Cancel");
                    break;
            }
        }

        void SetNavigationBar()
        {
            this.LeftIcon = "Hamburger.png";
            this.RightIcon2 = "More.png";
            this.LeftButtonCommand = new Command((x) => ((MasterDetailPage)Application.Current.MainPage).IsPresented = true);
            this.RightButton2Command = new Command((x) => DisplayActionSheet());
        }

        async void RegistrationInfoButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetColumn(BGBoxView, 0);
            AccountSettingsButton.TextColor = Color.FromHex("#000000");
            RegistrationInfoButton.TextColor = Color.FromHex("#357EDD");
            await AccountSettings.TranslateTo(App.screenWidth, AccountSettings.Y - 50.ScaleHeight());
            await RegistrationInfo.TranslateTo(0, RegistrationInfo.Y - 50.ScaleHeight());
        }

        async void AccountSettingsButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetColumn(BGBoxView, 1);
            AccountSettingsButton.TextColor = Color.FromHex("#357EDD");
            RegistrationInfoButton.TextColor = Color.FromHex("#000000");
            await RegistrationInfo.TranslateTo(-App.screenWidth, RegistrationInfo.Y - 50.ScaleHeight());
            await AccountSettings.TranslateTo(0, AccountSettings.Y - 50.ScaleHeight());
        }

        void Program_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            RegistrationInfo registrationInfo = (RegistrationInfo)RegistrationInfoList.SelectedItem;
            Navigation.PushAsync(new EnrollmentDetailPage(registrationInfo));
            RegistrationInfoList.SelectedItem = null;
        }
    }
}
