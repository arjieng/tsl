﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Plugin.InputKit.Shared.Controls;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class BeforeAfterSchoolPage : RootViewPage
    {
        public BeforeAfterSchoolPage()
        {
            this.BindingContext = new BeforeAfterSchoolPageViewModel(this);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());

        }

        void Location_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            var context = this.BindingContext as BeforeAfterSchoolPageViewModel;
            context.SelectedLocationAddress = context.LocationAddress[context.SelectedIndex];
        }

        void OnLabel1_Clicked(object sender, EventArgs e)
        {
            var label = sender as Label;
            Grid.SetRow(CircleCheck, 1);
            Grid.SetRow(EmptyCircle, 0);
            label.TextColor = Color.White;
            Label1.TextColor = Color.Black;
            Boxview1.TranslateTo(-App.screenWidth, Boxview1.Y);
            Boxview2.TranslateTo(0, Boxview1.Y);
            var context = this.BindingContext as BeforeAfterSchoolPageViewModel;
            context.SelectedIndex = 1;
            context.SelectedIndexXhanged();

            RadioGroup.Children[1].IsVisible = true;
            RadioGroup.Children[2].IsVisible = true;
        }

        void OnLabel2_Clicked(object sender, EventArgs e)
        {
            var label = sender as Label;
            Grid.SetRow(CircleCheck, 0);
            Grid.SetRow(EmptyCircle, 1);
            label.TextColor = Color.White;
            Label2.TextColor = Color.Black;
            Boxview2.TranslateTo(-App.screenWidth, Boxview1.Y);
            Boxview1.TranslateTo(0, Boxview1.Y);

            var context = this.BindingContext as BeforeAfterSchoolPageViewModel;
            context.SelectedIndex = 0;
            context.SelectedIndexXhanged();

            RadioGroup.Children[1].IsVisible = false;
            RadioGroup.Children[2].IsVisible = false;
        }

        void RegistrationType_SelectedItemChanged(object sender, System.EventArgs e)
        {
            var radio = sender as RadioButtonGroupView;
            //var context = this.BindingContext as BeforeAfterSchoolPageViewModel;

            if (radio.SelectedIndex == 0)
            {
                MainCheckboxes.IsVisible = true;
                RadioButtons.IsVisible = false;
            }
            else if(radio.SelectedIndex == 1)
            {
                RadioButtons.IsVisible = true;
                MainCheckboxes.IsVisible = true;
            }
            else
            {
                MainCheckboxes.IsVisible = false;
                RadioButtons.IsVisible = true;
            }
        }
    }
}
