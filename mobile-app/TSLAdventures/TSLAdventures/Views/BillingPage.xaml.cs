﻿using System;
using System.Collections.Generic;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class BillingPage : RootViewPage
    {
        bool isDone = true;
        public BillingPage()
        {
            this.BindingContext = new BillingPageVieModel(this);
            InitializeComponent();
            SetNavigationBar();
        }

        void SetNavigationBar()
        {
            this.LeftIcon = "Hamburger.png";
            this.LeftButtonCommand = new Command((x) => ((MasterDetailPage)Application.Current.MainPage).IsPresented = true);
        }

        async void InvoiceButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetColumn(BGBoxView, 0);
            ReceiptButton.TextColor = Color.FromHex("#000000");
            InvoiceButton.TextColor = Color.FromHex("#357EDD");
            await Receipts.TranslateTo(App.screenWidth, Receipts.Y - 50.ScaleHeight());
            await Invoice.TranslateTo(0, Invoice.Y - 50.ScaleHeight());
        }

        async void ReceiptButton_Clicked(object sender, System.EventArgs e)
        {
            Grid.SetColumn(BGBoxView, 1);
            ReceiptButton.TextColor = Color.FromHex("#357EDD");
            InvoiceButton.TextColor = Color.FromHex("#000000");
            await Invoice.TranslateTo(-App.screenWidth, Invoice.Y - 50.ScaleHeight());
            await Receipts.TranslateTo(0, Receipts.Y - 50.ScaleHeight());
            if(isDone)
            {
                isDone = false;
                var context = this.BindingContext as BillingPageVieModel;
                context.GetReceipts();
            }
        }

        void Invoice_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var context = this.BindingContext as BillingPageVieModel;
            context.InvoiceItemSelectedAction(e.Item as Invoices);
        }

        void Receipt_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var context = this.BindingContext as BillingPageVieModel;
            context.ReceiptItemSelectedAction(e.Item as Receipts);
        }
    }
}
