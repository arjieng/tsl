﻿using System;
using System.Collections.Generic;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Views
{
    public partial class TOSPage : RootViewPage
    {
        public TOSPage(int code)
        {
            this.BindingContext = new TOSViewModel(this, code);
            InitializeComponent();
            this.LeftIcon = "WhiteBack";
            this.LeftButtonCommand = new Command((x) => Navigation.PopAsync());
        }
    }
}
