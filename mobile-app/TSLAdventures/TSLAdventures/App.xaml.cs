﻿using System;
using TSLAdventures.Utilities;
using TSLAdventures.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace TSLAdventures
{
    public partial class App : Application
    {
        DataClass dataClass;
        public static float screenWidth { get; set; }
        public static float screenHeight { get; set; }
        public static float appScale { get; set; }
        public static int DeviceType { get; set; }
        public static double screenScale
        {
            get { return (screenWidth + screenHeight) / (320.0f + 568.0f); }
            //get {
            //  float tempWidth = 0;
            //  float tempHeight = 0;

            //  if (screenWidth < 320.0f) { tempWidth = (float)screenWidth; } else { tempWidth = 320.0f; }

            //  if (screenHeight < 568.0f) { tempHeight = (float)screenHeight; } else { tempHeight = 568.0f; }

            //  return (screenWidth + screenHeight) / (320.0f + tempHeight);
            //}
        }

        public App()
        {
            dataClass = DataClass.GetInstance;
            InitializeComponent();
            var customer_user = dataClass.CustomerUser;
            if(customer_user != null)
            {

                if(customer_user.has_basic_information && customer_user.confirmed_information)
                {
                    MainPage = new LandingPage();
                }
                else
                {
                    MainPage = new UserInfoPage(true, customer_user.email);
                }

            }
            else
            {
                MainPage = new NavigationPage(new SigninPage())
                {
                    BarTextColor = Color.White
                };
            }

            //MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
