﻿using System;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Renderers
{
    public class CustomButton : Button
    {
        public static readonly BindableProperty TextCaseProperty = BindableProperty.Create("TextCasing", typeof(string), typeof(CustomButton), "None");
        public static readonly BindableProperty TextPaddingProperty = BindableProperty.Create("TextPadding", typeof(string), typeof(CustomButton), "None");

        public string TextCasing
        {
            get { return (string)GetValue(TextCaseProperty); }
            set { SetValue(TextCaseProperty, value); }
        }

        public string TextPadding
        {
            get { return (string)GetValue(TextPaddingProperty); }
            set { SetValue(TextPaddingProperty, value); }
        }
    }
}
