﻿using System;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Renderers
{
    public class CustomEntry : Entry
    {
        public static readonly BindableProperty AutoCapitalizationProperty = BindableProperty.Create("AutoCapitalization", typeof(string), typeof(CustomEntry), "None");
        public static readonly BindableProperty TextAlignmentProperty = BindableProperty.Create("TextAlignment", typeof(string), typeof(CustomEntry), "None");
        public static readonly BindableProperty BorderRadiusProperty = BindableProperty.Create("BorderRadius", typeof(int), typeof(CustomEntry), 0);
        public static readonly BindableProperty BorderColorProperty = BindableProperty.Create("BorderColor", typeof(Color), typeof(CustomEntry), Color.FromHex("#E8E6E7"));

        public string AutoCapitalization
        {
            get { return (string)GetValue(AutoCapitalizationProperty); }
            set { SetValue(AutoCapitalizationProperty, value); }
        }

        public string TextAlignment
        {
            get { return (string)GetValue(TextAlignmentProperty); }
            set { SetValue(TextAlignmentProperty, value); }
        }

        public int BorderRadius
        {
            get { return (int)GetValue(BorderRadiusProperty); }
            set { SetValue(BorderRadiusProperty, value); }
        }

        public Color BorderColor
        {
            get { return (Color)GetValue(BorderColorProperty); }
            set { SetValue(BorderColorProperty, value); }
        }
    }
}
