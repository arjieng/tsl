﻿using Xamarin.Forms;

namespace TSLAdventures.Utilities.Renderers
{
    public class CustomListView : ListView
    {
        public static readonly BindableProperty IsScrollEnabledProperty = BindableProperty.Create(nameof(IsScrollEnabled), typeof(bool), typeof(CustomListView), true);
        public bool IsScrollEnabled
        {
            get { return (bool)GetValue(IsScrollEnabledProperty); }
            set { SetValue(IsScrollEnabledProperty, value); }
        }
    }
}
