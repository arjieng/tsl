﻿using System;
namespace TSLAdventures.Utilities
{
    public class Constants
    {
        public static string URL = "http://192.168.1.9";
        public static string VERSION = "/api/v1";
        public static string PORT = ":3000";
        public static string USER_URL = "";
        public static string REGISTER_URL = "/register";
        public static string SIGNIN_URL = "/sign-in";
        public static string FORGOTPASSWORD_URL = "/forgot_password";
        public static string CUSTOMERDETAILS_URL = "/customers";
        public static string RESET_PASSWORD = "/change_password";
        public static string ROOT_URL = URL + PORT + VERSION;
        public static string PAGINATION_URL = URL + PORT;
        public static int LOAD_MORE;

        public static string GET_LOCATIONS = "/data/get_locations";
        public static string GET_SUMMER_CAMP_WEEKS = "/data/get_summer_camp_weeks?location_name=";
        public static string GET_VACATION_CAMP_SCHEDULES = "/data/get_vacation_camp_schedules?location_name=";
        public static string GET_SINGLE_DAY_DATES = "/data/get_single_day_dates";
        public static string ADD_CARD = "/customer_users/add_card";
        public static string UNLINK_CARD = "/customer_users/unlink_card";
        public static string REGISTRATION_RECORDS = ROOT_URL + "/registration_records";
        public static string CIT_RECORDS = "/cit_records";
        public static string UPDATE_AUTO_PAY = "/customer_users/update_auto_pay";

        public static string GET_INVOICES = "/data/get_invoices";
        public static string RECEIPTS = "/receipts";
        public static string UPDATE_CONTACT_INFORMATIONS = "/customers/";
        public static string GET_CONTACT_INFORMATIONS = "/customers/fetch_contact_informations";
        public static string REGISTRATION_INFORMATION = "/dashboard/registration_information";
        public static string REGISTRATION_INFORMATION_SHOW = "/dashboard/registration_information_show?id=";
        //192.168.1.17:3000/api/v1/dashboard/registration_information_show?id=7712
        //192.168.1.17:3000/api/v1/dashboard/registration_information
    }
}
