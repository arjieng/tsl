﻿using System;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Behaviors
{
    public class AddChildButtonBehavior : Behavior<Button>
    {
        protected override void OnAttachedTo(Button bindable)
        {
            bindable.Clicked += Button_Clicked;
            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Button bindable)
        {
            bindable.Clicked += Button_Clicked;
            base.OnDetachingFrom(bindable);
        }

        public void Button_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = (Button)sender;

        }
    }
}
