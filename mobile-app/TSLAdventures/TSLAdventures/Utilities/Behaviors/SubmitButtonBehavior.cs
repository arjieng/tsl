﻿using System;
using TSLAdventures.Views;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Behaviors
{
    public class SubmitButtonBehavior : Behavior<Button>
    {
        public static readonly BindableProperty ClickBindableProperty = BindableProperty.Create("IsClicked", typeof(bool), typeof(UserInfoPage), false);
        public bool IsClicked
        {
            set { SetValue(ClickBindableProperty, value); }
            get { return (bool)GetValue(ClickBindableProperty); }
        }

        protected override void OnAttachedTo(Button bindable)
        {
            bindable.Clicked += Button_Clicked;
            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Button bindable)
        {
            bindable.Clicked += Button_Clicked;
            base.OnDetachingFrom(bindable);
        }

        public void Button_Clicked(object sender, EventArgs eventArgs)
        {
            Button button = (Button)sender;
            IsClicked = true;
        }
    }
}
