﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Behaviors
{
    public class EmailValidator : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.Unfocused += OnEntryUnfocused;
            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.Unfocused += OnEntryUnfocused;
            base.OnDetachingFrom(bindable);
        }

        void OnEntryUnfocused(object sender, FocusEventArgs e)
        {
            var entry = (Entry)sender;
            var entry2 = sender as Renderers.CustomEntry;
            if (entry.Text.Length != 0)
            {
                if (Regex.Match(((Entry)sender).Text, @"^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$").Success)
                {
                    //entry2.BackgroundColor = new Color(0, 255, 0, 0.4);
                    entry2.BorderColor = Color.FromHex("#E8E6E7");
                }
                else
                {
                    //entry2.BackgroundColor = new Color(255, 0, 0, 0.4);
                    entry2.BorderColor = Color.Red;
                }
            }
        }
    }
}
