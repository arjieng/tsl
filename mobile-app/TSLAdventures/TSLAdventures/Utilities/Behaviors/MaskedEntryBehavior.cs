﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Behaviors
{
    public class MaskedEntryBehavior : Behavior<Entry>
    {
        private string _mask = "";
        public string Mask
        {
            get => _mask;
            set
            {
                _mask = value;
                SetPositions();
            }
        }

        protected override void OnAttachedTo(Entry entry)
        {
            base.OnAttachedTo(entry);
            entry.TextChanged += OnEntryTextChanged;
            //entry.Focused += Bindable_Focused;
            //entry.Unfocused += Bindable_Unfocused;
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            base.OnDetachingFrom(entry);
            entry.TextChanged -= OnEntryTextChanged;
            //entry.Focused += Bindable_Focused;
            //entry.Unfocused += Bindable_Unfocused;
        }

        IDictionary<int, char> _positions;

        void SetPositions()
        {
            if (string.IsNullOrEmpty(Mask))
            {
                _positions = null;
                return;
            }

            var list = new Dictionary<int, char>();
            for (var i = 0; i < Mask.Length; i++)
                if (Mask[i] != 'X')
                    list.Add(i, Mask[i]);

            _positions = list;
        }

        private void OnEntryTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = sender as Entry;

            var text = entry.Text;

            if (string.IsNullOrWhiteSpace(text) || _positions == null)
                return;

            if (text.Length > _mask.Length)
            {
                entry.Text = text.Remove(text.Length - 1);
                return;
            }

            foreach (var position in _positions)
                if (text.Length >= position.Key + 1)
                {
                    var value = position.Value.ToString();
                    if (text.Substring(position.Key, 1) != value)
                        text = text.Insert(position.Key, value);
                }

            if (entry.Text != text)
                entry.Text = text;
        }

        async void Bindable_Focused(object sender, FocusEventArgs e)
        {
            Entry entryField = (Entry)sender;

            entryField.Placeholder = "yyyy-mm-dd";
        }

        async void Bindable_Unfocused(object sender, FocusEventArgs e)
        {
            Entry entryField = (Entry)sender;

            entryField.Placeholder = "Date of Birth";
        }
    }
}
