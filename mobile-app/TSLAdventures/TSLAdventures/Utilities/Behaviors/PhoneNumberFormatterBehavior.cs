﻿using System;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Behaviors
{
    public class PhoneNumberFormatterBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            bindable.TextChanged += onTextChanged;

            base.OnAttachedTo(bindable);
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            bindable.TextChanged -= onTextChanged;

            base.OnDetachingFrom(bindable);
        }

        void onTextChanged(object sender, TextChangedEventArgs args)
        {
            var entry = (Entry)sender;

            switch (entry.StyleId)
            {
                case "1":
                    if (entry.Text.Length > 19)
                    {
                        entry.Text = args.OldTextValue;
                    }
                    if (entry.Text.Length < 20)
                        entry.Text = formatCardNumber(entry.Text);
                    break;
                case "2":
                    if (entry.Text.Length > 7)
                    {
                        entry.Text = args.OldTextValue;
                    }

                    if (entry.Text.Length < 8)
                        entry.Text = formatPhoneNumber(entry.Text);
                    break;
                case "3":
                    break;
            }

        }

        private string formatCardNumber(string input)
        {
            var digitsRegex = new Regex(@"[^\d]");
            var digits = digitsRegex.Replace(input, "");
            System.Diagnostics.Debug.WriteLine(digits.Length);
            if (digits.Length <= 4)
                return digits;

            if (digits.Length <= 8)
                return $"{digits.Substring(0, 4)} {digits.Substring(4)}";

            if (digits.Length <= 12)
                return $"{digits.Substring(0, 4)} {digits.Substring(4, 4)} {digits.Substring(8)}";

            return $"{digits.Substring(0, 4)} {digits.Substring(4, 4)} {digits.Substring(8, 4)} {digits.Substring(12)}";
        }

        private string formatPhoneNumber(string input)
        {
            var digitsRegex = new Regex(@"[^\d]");
            var digits = digitsRegex.Replace(input, "");

            if (digits.Length <= 1)
                return digits;

            //if (digits.Length <= 7)
            //return $"{digits.Substring(0, 3)}-{digits.Substring(3)}";

            return $"{digits.Substring(0, 2)}/{digits.Substring(2)}";
        }
    }
}
