﻿using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TSLAdventures.Models;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Utilities
{
    public class DataClass : BaseModel
    {
        static DataClass dataClass;

        public static DataClass GetInstance
        {
            get
            {
                if(dataClass == null)
                {
                    dataClass = new DataClass();
                }
                return dataClass;
            }
        }

        CustomerUser _cu;
        public CustomerUser CustomerUser
        {
            get
            {
                if (Application.Current.Properties.ContainsKey("customer_user"))
                {
                    JObject json = JObject.Parse(Application.Current.Properties["customer_user"].ToString());
                    _cu = JsonConvert.DeserializeObject<CustomerUser>(json.ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                }
                return _cu;
            }

            set
            {
                if (value == null)
                {
                    Application.Current.Properties.Remove("customer_user");
                }
                else
                {
                    _cu = value;
                    Application.Current.Properties["customer_user"] = JsonConvert.SerializeObject(_cu);
                }
                Application.Current.SavePropertiesAsync();
                onPropertyChanged("CustomerUser");
            }
        }



    }
}
