﻿using System;
using Xamarin.Forms;

namespace TSLAdventures.Utilities.Helpers.ScaleHelper
{
    public static class ScaleCodedHelper
    {
        /// <summary>
        /// Scales the height.
        /// </summary>
        /// <returns>The height.</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static float ScaleHeight(this int number, int? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (float)(number * (App.screenHeight / 568.0));
        }

        /// <summary>
        /// Scales the height.
        /// </summary>
        /// <returns>The height.</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static float ScaleHeight(this double number, double? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (float)(number * (App.screenHeight / 568.0));
        }

        /// <summary>
        /// Scales the height.
        /// </summary>
        /// <returns>The height.</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static float ScaleHeight(this float number, float? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (float)(number * (App.screenHeight / 568.0));
        }

        //public static float ScaleHeight(this double number)
        //{
        //    return (float)(number * (App.screenHeight / 568.0));
        //}

        //public static float ScaleHeight(this float number)
        //{
        //    return (float)(number * (App.screenHeight / 568.0));
        //}

        /// <summary>
        /// Scales the width.
        /// </summary>
        /// <returns>The width.</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static float ScaleWidth(this int number, int? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (float)(number * (App.screenWidth / 320.0));
        }

        /// <summary>
        /// Scales the width.
        /// </summary>
        /// <returns>The width.</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static float ScaleWidth(this double number, double? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (float)(number * (App.screenWidth / 320.0));
        }

        /// <summary>
        /// Scales the width.
        /// </summary>
        /// <returns>The width.</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static float ScaleWidth(this float number, float? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (float)(number * (App.screenWidth / 320.0));
        }

        /// <summary>
        /// Scales the font.
        /// </summary>
        /// <returns>The font.</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static double ScaleFont(this int number, int? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (number * (App.screenHeight / 568.0) - (Device.RuntimePlatform == Device.iOS ? 0.5 : 0));
        }

        /// <summary>
        /// </summary>
        /// <returns>Formatted chips</returns>
        /// <param name="number">Number.</param>
        /// <param name="iOS">Value for iOS.</param>
        public static double ScaleFont(this double number, double? iOS = null)
        {
            if (iOS.HasValue && Device.RuntimePlatform == Device.iOS)
                number = iOS.Value;

            return (number * (App.screenHeight / 568.0) - (Device.RuntimePlatform == Device.iOS ? 0.5 : 0));
        }

        public static double scale(this double number)
        {
            return number * App.screenScale;
        }

        public static float scale(this float number)
        {
            return (float)(number * App.screenScale);
        }

        public static int scale(this int number)
        {
            return (int)(number * App.screenScale);
        }

    }
}