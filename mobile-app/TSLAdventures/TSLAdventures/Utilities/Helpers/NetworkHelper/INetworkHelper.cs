﻿using System;
using System.Threading.Tasks;

namespace TSLAdventures
{
    public interface INetworkHelper
    {
        bool HasInternet();
        Task<bool> IsHostReachable();
    }
}
