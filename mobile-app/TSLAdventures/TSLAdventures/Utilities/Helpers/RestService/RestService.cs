﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace TSLAdventures.Utilities.Helpers.RestService
{
    public class RestService : IRestService
    {
        NetworkHelper networkHelper = NetworkHelper.GetInstance;
        HttpClient client;
        DataClass dataClass = DataClass.GetInstance;

        WeakReference<IRestConnector> _restServiceDelegate;
        public IRestConnector RestServiceDelegate
        {
            get
            {
                IRestConnector restServiceDelegate;
                return _restServiceDelegate.TryGetTarget(out restServiceDelegate) ? restServiceDelegate : null;
            }

            set
            {
                _restServiceDelegate = new WeakReference<IRestConnector>(value);
            }
        }

        public RestService()
        {
            var auth = dataClass.CustomerUser != null ? dataClass.CustomerUser.access_token : "";
            client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", auth);
        }

        public async Task RequestCardToken(string number, string exp_month, string exp_year, string cvc, CancellationToken ct)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable())
                {
                    try
                    {
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", "sk_test_RKvVRyKHQ1JupX1pjfVaDmxt00HLsq9xmy");
                        var uri = new Uri("https://api.stripe.com/v1/tokens");

                        HttpContent content = new FormUrlEncodedContent(new[] {
                            new KeyValuePair<string, string>("card[number]", number),
                            new KeyValuePair<string, string>("card[exp_month]", exp_month),
                            new KeyValuePair<string, string>("card[exp_year]", exp_year),
                            new KeyValuePair<string, string>("card[cvc]", cvc)
                        });

                        content.Headers.ContentType = new MediaTypeHeaderValue("application/x-www-form-urlencoded");
                        HttpResponseMessage response = await client.PostAsync(uri, content, ct);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }
                        else
                        {
                            RestServiceDelegate?.ReceiveTimeoutError("Network error", await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                        }
                    }
                    catch (Exception e)
                    {
                        RestServiceDelegate?.ReceiveTimeoutError("Network error", "Request timeout error");
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for TSL Adventures App cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }
        }

        public async Task PostRequestAsync(string url, object dictionary, CancellationToken ct)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(url);
                        var json = JsonConvert.SerializeObject(dictionary);
                        System.Diagnostics.Debug.WriteLine(json);
                        var content = new StringContent(json, Encoding.UTF8, "application/json");

                        HttpResponseMessage response = await client.PostAsync(uri, content, ct);
                        if (response.IsSuccessStatusCode)
                        {
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }
                        else
                        {
                            var error_results = JObject.Parse(await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                            var error = string.Empty;
                            if(error_results["error"] != null)
                            {
                                var list = JsonConvert.DeserializeObject<List<string>>(error_results["error"].ToString());
                                error = string.Join("\n", list);
                            }
                            RestServiceDelegate?.ReceiveTimeoutError("Network error", error);
                        }
                    }
                    catch (Exception e)
                    {
                        RestServiceDelegate?.ReceiveTimeoutError("Network error", "Request timeout error");
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for TSL Adventures App cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }
        }

        public async Task GetRequest(string url, CancellationToken ct)
        {
            if(networkHelper.HasInternet())
            {
                if(await networkHelper.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(url);

                        HttpResponseMessage response = await client.GetAsync(uri, ct);

                        if(response.IsSuccessStatusCode)
                        {
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }
                    }
                    catch (Exception e)
                    {
                        RestServiceDelegate?.ReceiveTimeoutError("Network Error", "Request timeout error");
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for TSL Adventures App cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }
        }

        public async Task PutRequestAsync(string url, object dictionary, CancellationToken ct)
        {
            if(networkHelper.HasInternet())
            {
                if(await networkHelper.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(url);
                        var json = JsonConvert.SerializeObject(dictionary);
                        var content = new StringContent(json, Encoding.UTF8, "application/json");
                        System.Diagnostics.Debug.WriteLine(url);
                        System.Diagnostics.Debug.WriteLine(json);
                        HttpResponseMessage response = await client.PutAsync(uri, content, ct);
                        if(response.IsSuccessStatusCode)
                        {
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }
                        else
                        {
                            RestServiceDelegate?.ReceiveTimeoutError("Error", await response.Content.ReadAsStringAsync().ConfigureAwait(false));
                        }
                    }
                    catch (Exception e)
                    {
                        RestServiceDelegate?.ReceiveTimeoutError("Network error", "Request timeout error");                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for TSL Adventures App cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }
        }

        public async Task DeleteRequestAsync(string url, CancellationToken ct)
        {
            if (networkHelper.HasInternet())
            {
                if (await networkHelper.IsHostReachable())
                {
                    try
                    {
                        var uri = new Uri(url);
                        var response = await client.DeleteAsync(uri);

                        if (response.IsSuccessStatusCode)
                        {
                            var responseMsg = await response.Content.ReadAsStringAsync().ConfigureAwait(false);
                            RestServiceDelegate?.ReceiveJSONData(JObject.Parse(responseMsg), ct);
                        }

                    }
                    catch (Exception e)
                    {
                        RestServiceDelegate?.ReceiveTimeoutError("Network error", "Request timeout error");
                    }
                }
                else
                {
                    RestServiceDelegate?.ReceiveTimeoutError("Host Unreachable!", "The URL host for TSL Adventures App cannot be reached and seems to be unavailable. Please try again later!");
                }
            }
            else
            {
                RestServiceDelegate?.ReceiveTimeoutError("No Internet Connection!", "Please check your internet connection, and try again!");
            }
        }
    }
}
