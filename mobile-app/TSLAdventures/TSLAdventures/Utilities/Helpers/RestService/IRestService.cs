﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TSLAdventures.Utilities.Helpers.RestService
{
    public interface IRestService
    {
        Task PostRequestAsync(string url, object dictionary, CancellationToken ct);
        Task GetRequest(string url, CancellationToken ct);
        Task PutRequestAsync(string url, object dictionary, CancellationToken ct);
        Task DeleteRequestAsync(string url, CancellationToken ct);
    }
}
