﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using TSLAdventures.Models;
using TSLAdventures.ViewModels;
using Xamarin.Forms;

namespace TSLAdventures.Utilities
{
    public class AlternateColorDataTemplateSelector : DataTemplateSelector
    {
        public DataTemplate EvenTemplate { get; set; }
        public DataTemplate UnevenTemplate { get; set; }
        public string KeyModel { get; set; }

        protected override DataTemplate OnSelectTemplate(object item, BindableObject container)
        {

            if (KeyModel.Equals("RegistrationInfo"))
            {
                return ((ObservableCollection<RegistrationInfo>)((ListView)container).ItemsSource).IndexOf(item as RegistrationInfo) % 2 == 0 ? EvenTemplate : UnevenTemplate;
            } else if (KeyModel.Equals("InvoicesInfo"))
            {
                return ((ObservableCollection<Invoices>)((ListView)container).ItemsSource).IndexOf(item as Invoices) % 2 == 0 ? EvenTemplate : UnevenTemplate;
            }
            else if (KeyModel.Equals("ReceiptInfo"))
            {
                return ((ObservableCollection<Receipts>)((ListView)container).ItemsSource).IndexOf(item as Receipts) % 2 == 0 ? EvenTemplate : UnevenTemplate;
            }
            else
            {
                return ((ObservableCollection<Student>)((ListView)container).ItemsSource).IndexOf(item as Student) % 2 == 0 ? EvenTemplate : UnevenTemplate; ;
            }
        }
    }
}