﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Plugin.InputKit.Shared.Controls;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class SummerCampPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        Page page;
        int reqType = 0;

        private List<string> _LocationSource;
        public List<string> LocationSource
        {
            get { return _LocationSource; }
            set
            {
                if(_LocationSource != value)
                {
                    _LocationSource = value;
                    onPropertyChanged(nameof(LocationSource));
                }
            }
        }

        private ChildRecord _ChildOne;
        public ChildRecord ChildOne
        {
            get { return _ChildOne; }
            set
            {
                if (_ChildOne != value)
                {
                    _ChildOne = value;
                    onPropertyChanged(nameof(ChildOne));
                }
            }
        }

        private ChildRecord _ChildTwo;
        public ChildRecord ChildTwo
        {
            get { return _ChildTwo; }
            set
            {
                if (_ChildTwo != value)
                {
                    _ChildTwo = value;
                    onPropertyChanged(nameof(ChildTwo));
                }
            }
        }

        private ChildRecord _ChildThree;
        public ChildRecord ChildThree
        {
            get { return _ChildThree; }
            set
            {
                if (_ChildThree != value)
                {
                    _ChildThree = value;
                    onPropertyChanged(nameof(ChildThree));
                }
            }
        }

        private List<string> _LocationAddress;
        public List<string> LocationAddress
        {
            get { return _LocationAddress; }
            set
            {
                if(_LocationAddress != value)
                {
                    _LocationAddress = value;
                    onPropertyChanged(nameof(LocationAddress));
                }
            }
        }

        private List<int> _LocationMinimumAge;
        public List<int> LocationMinimumAge
        {
            get { return _LocationMinimumAge; }
            set
            {
                if (_LocationMinimumAge != value)
                {
                    _LocationMinimumAge = value;
                    onPropertyChanged(nameof(LocationMinimumAge));
                }
            }
        }

        private string _SelectedLocationAddress;
        public string SelectedLocationAddress
        {
            get { return _SelectedLocationAddress; }
            set
            {
                if (_SelectedLocationAddress != value)
                {
                    _SelectedLocationAddress = value;
                    onPropertyChanged(nameof(SelectedLocationAddress));
                }
            }
        }

        private int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                if (_SelectedIndex != value)
                {
                    _SelectedIndex = value;
                    onPropertyChanged(nameof(SelectedIndex));
                }
            }
        }

        private string _SelectedItem;
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                if (_SelectedItem != value)
                {
                    _SelectedItem = value;
                    onPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        private ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set
            {
                if (_SubmitCommand != value)
                {
                    _SubmitCommand = value;
                    onPropertyChanged(nameof(SubmitCommand));
                }
            }
        }

        private ICommand _GotoTOSCommand;
        public ICommand GotoTOSCommand
        {
            get { return _GotoTOSCommand; }
            set
            {
                if (_GotoTOSCommand != value)
                {
                    _GotoTOSCommand = value;
                    onPropertyChanged(nameof(GotoTOSCommand));
                }
            }
        }

        private ICommand _RemoveFirstChildCommand;
        public ICommand RemoveFirstChildCommand
        {
            get { return _RemoveFirstChildCommand; }
            set
            {
                if (_RemoveFirstChildCommand != value)
                {
                    _RemoveFirstChildCommand = value;
                    onPropertyChanged(nameof(RemoveFirstChildCommand));
                }
            }
        }

        private ICommand _RemoveSecondChildCommand;
        public ICommand RemoveSecondChildCommand
        {
            get { return _RemoveSecondChildCommand; }
            set
            {
                if (_RemoveSecondChildCommand != value)
                {
                    _RemoveSecondChildCommand = value;
                    onPropertyChanged(nameof(RemoveSecondChildCommand));
                }
            }
        }

        private ICommand _RemoveThirdChildCommand;
        public ICommand RemoveThirdChildCommand
        {
            get { return _RemoveThirdChildCommand; }
            set
            {
                if (_RemoveThirdChildCommand != value)
                {
                    _RemoveThirdChildCommand = value;
                    onPropertyChanged(nameof(RemoveThirdChildCommand));
                }
            }
        }

        private ICommand _RegistrationTypeSelectedItemChangedCommand;
        public ICommand RegistrationTypeSelectedItemChangedCommand
        {
            get { return _RegistrationTypeSelectedItemChangedCommand; }
            set
            {
                _RegistrationTypeSelectedItemChangedCommand = value;
                onPropertyChanged(nameof(RegistrationTypeSelectedItemChangedCommand));
            }
        }

        ICommand _PromoPackageRegistrationTypeSelectedItemChangedCommand;
        public ICommand PromoPackageRegistrationTypeSelectedItemChangedCommand
        {
            get { return _PromoPackageRegistrationTypeSelectedItemChangedCommand; }
            set
            {
                _PromoPackageRegistrationTypeSelectedItemChangedCommand = value;
                onPropertyChanged(nameof(PromoPackageRegistrationTypeSelectedItemChangedCommand));
            }
        }

        private ICommand _AddChildCommand;
        public ICommand AddChildCommand
        {
            get { return _AddChildCommand; }
            set
            {
                if (_AddChildCommand != value)
                {
                    _AddChildCommand = value;
                    onPropertyChanged(nameof(AddChildCommand));
                }
            }
        }

        private bool _PromoViewVisibility;
        public bool PromoViewVisibility
        {
            get { return _PromoViewVisibility; }
            set
            {
                _PromoViewVisibility = value;
                onPropertyChanged(nameof(PromoViewVisibility));
            }
        }

        private bool _RegularLabelVisibility;
        public bool RegularLabelVisibility
        {
            get { return _RegularLabelVisibility; }
            set
            {
                _RegularLabelVisibility = value;
                onPropertyChanged(nameof(RegularLabelVisibility));
            }
        }



        private bool _FirstChildVisibility;
        public bool FirstChildVisibility
        {
            get { return _FirstChildVisibility; }
            set
            {
                if (_FirstChildVisibility != value)
                {
                    _FirstChildVisibility = value;
                    onPropertyChanged(nameof(FirstChildVisibility));
                }
            }
        }

        private bool _SecondChildVisibility;
        public bool SecondChildVisibility
        {
            get { return _SecondChildVisibility; }
            set
            {
                if (_SecondChildVisibility != value)
                {
                    _SecondChildVisibility = value;
                    onPropertyChanged(nameof(SecondChildVisibility));
                }
            }
        }

        private bool _ThirdChildVisibility;
        public bool ThirdChildVisibility
        {
            get { return _ThirdChildVisibility; }
            set
            {
                if (_ThirdChildVisibility != value)
                {
                    _ThirdChildVisibility = value;
                    onPropertyChanged(nameof(ThirdChildVisibility));
                }
            }
        }

        bool _WeekCheckBoxesVisibility;
        public bool WeekCheckBoxesVisibility
        {
            get { return _WeekCheckBoxesVisibility; }
            set
            {
                if (_WeekCheckBoxesVisibility != value)
                {
                    _WeekCheckBoxesVisibility = value;
                    onPropertyChanged(nameof(WeekCheckBoxesVisibility));
                }
            }
        }

        public SummerCampPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;


            InitiateValues();
        }

        private string _SummerCampRegistrationOption;
        public string SummerCampRegistrationOption
        {
            get { return _SummerCampRegistrationOption; }
            set
            {
                if (_SummerCampRegistrationOption != value)
                {
                    _SummerCampRegistrationOption = value;
                    onPropertyChanged(nameof(SummerCampRegistrationOption));
                }
            }
        }

        private string _SummerCampRegularRegistrationChosen;
        public string SummerCampRegularRegistrationChosen
        {
            get { return _SummerCampRegularRegistrationChosen; }
            set
            {
                if (_SummerCampRegularRegistrationChosen != value)
                {
                    _SummerCampRegularRegistrationChosen = value;
                    onPropertyChanged(nameof(SummerCampRegularRegistrationChosen));
                }
            }
        }

        private string _SummerCampPromoRegistrationChosen;
        public string SummerCampPromoRegistrationChosen
        {
            get { return _SummerCampPromoRegistrationChosen; }
            set
            {
                if (_SummerCampPromoRegistrationChosen != value)
                {
                    _SummerCampPromoRegistrationChosen = value;
                    onPropertyChanged(nameof(SummerCampPromoRegistrationChosen));
                }
            }
        }

        SummerCampAttributes _SummerCamp;
        public SummerCampAttributes SummerCamp
        {
            get { return _SummerCamp; }
            set
            {
                if(_SummerCamp != value)
                {
                    _SummerCamp = value;
                    onPropertyChanged(nameof(SummerCamp));
                }
            }
        }

        private string _CouponCode;
        public string CouponCode
        {
            get { return _CouponCode; }
            set
            {
                if (_CouponCode != value)
                {
                    _CouponCode = value;
                    onPropertyChanged(nameof(CouponCode));
                }
            }
        }

        private bool _HasAgreed;
        public bool HasAgreed
        {
            get { return _HasAgreed; }
            set
            {
                _HasAgreed = value;
                onPropertyChanged(nameof(HasAgreed));
            }
        }

        private string _PromoName;
        public string PromoName
        {
            get { return _PromoName; }
            set
            {
                if (_PromoName != value)
                {
                    _PromoName = value;
                    onPropertyChanged(nameof(PromoName));
                }
            }
        }

        private List<SummerCampRegularRegistration> _RegularRegistration;
        public List<SummerCampRegularRegistration> RegularRegistration
        {
            get { return _RegularRegistration; }
            set
            {
                if (_RegularRegistration != value)
                {
                    _RegularRegistration = value;
                    onPropertyChanged(nameof(RegularRegistration));
                }
            }
        }

        private double _ListViewHeight;
        public double ListViewHeight
        {
            get { return _ListViewHeight; }
            set
            {
                if (_ListViewHeight != value)
                {
                    _ListViewHeight = value;
                    onPropertyChanged(nameof(ListViewHeight));
                }
            }
        }

        async void InitiateValues() // this should be on ReceiveJSONData because locations will be fetch from server
        {
            SummerCampRegistrationOption = "summer_camp_promo_registration";
            SummerCampRegularRegistrationChosen = "false";
            SummerCampPromoRegistrationChosen = "true";
            CouponCode = "";
            HasAgreed = false;
            SummerCamp = new SummerCampAttributes { week_one = true, week_two = true, week_three = true, week_four = true, week_five = true, week_six = true, week_seven = true, week_eight = true, week_nine = true };
            WeekCheckBoxesVisibility = false;
            PromoName = "one_child_nine_weeks";

            PromoViewVisibility = true;
            RegularLabelVisibility = false;
            FirstChildVisibility = true;
            SecondChildVisibility = false;
            ThirdChildVisibility = false;
            ChildOne = new ChildRecord();
            ChildTwo = new ChildRecord();
            ChildThree = new ChildRecord();
            AddChildCommand = new Command(AddChildCommandAction);
            SubmitCommand = new Command(SubmitCommandAction);
            GotoTOSCommand = new Command((x) => page.Navigation.PushAsync(new TOSPage(3)));
            RegistrationTypeSelectedItemChangedCommand = new Command(RegistrationTypeSelectedItemChangedAction);
            PromoPackageRegistrationTypeSelectedItemChangedCommand = new Command(PromoPackageRegistrationTypeSelectedItemChangedAction);
            RemoveFirstChildCommand = new Command((x) => FirstChildVisibility = false);
            RemoveSecondChildCommand = new Command((x) => SecondChildVisibility = false);
            RemoveThirdChildCommand = new Command((x) => ThirdChildVisibility = false);

            await Task.Run(async () =>
            {
                reqType = 1;
                string url = string.Format("{0}{1}?program_name=Single Day", Constants.ROOT_URL, Constants.GET_LOCATIONS);
                System.Diagnostics.Debug.WriteLine(url);
                await restService.GetRequest(url, cts.Token);
            });
        }

        void PromoPackageRegistrationTypeSelectedItemChangedAction(object obj)
        {
            var radio = obj as RadioButtonGroupView;
            switch (radio.SelectedIndex)
            {
                case 0:
                    PromoName = "one_child_nine_weeks";
                    WeekCheckBoxesVisibility = false;
                    SummerCamp = new SummerCampAttributes { week_one = true, week_two = true, week_three = true, week_four = true, week_five = true, week_six = true, week_seven = true, week_eight = true, week_nine = true };
                    break;
                case 1:
                    PromoName = "two_children_nine_weeks";
                    WeekCheckBoxesVisibility = false;
                    SummerCamp = new SummerCampAttributes { week_one = true, week_two = true, week_three = true, week_four = true, week_five = true, week_six = true, week_seven = true, week_eight = true, week_nine = true };
                    break;
                case 2:
                    PromoName = "three_children_nine_weeks";
                    WeekCheckBoxesVisibility = false;
                    SummerCamp = new SummerCampAttributes { week_one = true, week_two = true, week_three = true, week_four = true, week_five = true, week_six = true, week_seven = true, week_eight = true, week_nine = true };
                    break;
                case 3:
                    PromoName = "one_child_five_weeks";
                    WeekCheckBoxesVisibility = true;
                    SummerCamp = new SummerCampAttributes();
                    break;
                case 4:
                    PromoName = "two_children_five_weeks";
                    WeekCheckBoxesVisibility = true;
                    SummerCamp = new SummerCampAttributes();
                    break;
                case 5:
                    PromoName = "three_children_five_weeks";
                    WeekCheckBoxesVisibility = true;
                    SummerCamp = new SummerCampAttributes();
                    break;
            }
        }

        void RegistrationTypeSelectedItemChangedAction(object obj)
        {
            var radio = obj as RadioButtonGroupView;
            System.Diagnostics.Debug.WriteLine(radio.SelectedIndex);

            if (radio.SelectedIndex == 0)
            {
                PromoViewVisibility = true;
                RegularLabelVisibility = false;

                SummerCampRegistrationOption = "summer_camp_promo_registration";
                SummerCampRegularRegistrationChosen = "false";
                SummerCampPromoRegistrationChosen = "true";
            }
            else
            {
                PromoViewVisibility = false;
                RegularLabelVisibility = true;

                SummerCampRegistrationOption = "summer_camp_regular_registration";
                SummerCampRegularRegistrationChosen = "true";
                SummerCampPromoRegistrationChosen = "false";
                //SummerCamp = new SummerCampAttributes { week_one = true };
            }
        }


        void AddChildCommandAction()
        {
            if (!FirstChildVisibility)
            {
                ChildOne = new ChildRecord();
                FirstChildVisibility = true;
            }
            else if (!SecondChildVisibility)
            {
                ChildTwo = new ChildRecord();
                SecondChildVisibility = true;
            }
            else if (!ThirdChildVisibility)
            {
                ChildThree = new ChildRecord();
                ThirdChildVisibility = true;
            }
        }

        async void SubmitCommandAction()
        {
            bool datasCompleted = true, invokeChilMissingFields = false;
            List<ChildRecord> child_list = new List<ChildRecord>();
            object newJson = new { };

            if (FirstChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildOne.first_name) && !string.IsNullOrEmpty(ChildOne.last_name) && !string.IsNullOrEmpty(ChildOne.dob) && !string.IsNullOrEmpty(ChildOne.school_attending))
                {
                    child_list.Add(ChildOne);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (SecondChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildTwo.first_name) && !string.IsNullOrEmpty(ChildTwo.last_name) && !string.IsNullOrEmpty(ChildTwo.dob) && !string.IsNullOrEmpty(ChildTwo.school_attending))
                {
                    child_list.Add(ChildTwo);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (ThirdChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildThree.first_name) && !string.IsNullOrEmpty(ChildThree.last_name) && !string.IsNullOrEmpty(ChildThree.dob) && !string.IsNullOrEmpty(ChildThree.school_attending))
                {
                    child_list.Add(ChildThree);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (invokeChilMissingFields)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "Please fill all fields in childs form.", "Okay");
                });
            }

            if (SummerCampRegistrationOption.Equals("summer_camp_promo_registration"))
            {

                if (SummerCamp.checkifhasselected)
                {
                    if (PromoName.Equals("one_child_five_weeks") || PromoName.Equals("two_children_five_weeks") || PromoName.Equals("three_children_five_weeks"))
                    {
                        if (SummerCamp.getcheckcount.Equals(5))
                        {
                            newJson = new
                            {
                                week_one = SummerCamp.week_one ? "1" : "0",
                                week_two = SummerCamp.week_two ? "1" : "0",
                                week_three = SummerCamp.week_three ? "1" : "0",
                                week_four = SummerCamp.week_four ? "1" : "0",
                                week_five = SummerCamp.week_five ? "1" : "0",
                                week_six = SummerCamp.week_six ? "1" : "0",
                                week_seven = SummerCamp.week_seven ? "1" : "0",
                                week_eight = SummerCamp.week_eight ? "1" : "0",
                                week_nine = SummerCamp.week_nine ? "1" : "0",
                                week_ten = SummerCamp.week_ten ? "1" : "0"

                            };
                        }
                        else
                        {
                            Device.BeginInvokeOnMainThread(() =>
                            {
                                datasCompleted = false;
                                page.DisplayAlert("Invalid Input", "You must select five weeks in promo package registration.", "Okay");
                            });
                        }
                    }
                    else
                    {
                        newJson = new
                        {
                            week_one = SummerCamp.week_one ? "1" : "0",
                            week_two = SummerCamp.week_two ? "1" : "0",
                            week_three = SummerCamp.week_three ? "1" : "0",
                            week_four = SummerCamp.week_four ? "1" : "0",
                            week_five = SummerCamp.week_five ? "1" : "0",
                            week_six = SummerCamp.week_six ? "1" : "0",
                            week_seven = SummerCamp.week_seven ? "1" : "0",
                            week_eight = SummerCamp.week_eight ? "1" : "0",
                            week_nine = SummerCamp.week_nine ? "1" : "0",
                            week_ten = SummerCamp.week_ten ? "1" : "0"
                        };
                    }


                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        datasCompleted = false;
                        page.DisplayAlert("Missing Fields", "Please fill all fields in promo package registration form.", "Okay");
                    });
                }
            }

            if (SummerCampRegistrationOption.Equals("summer_camp_regular_registration"))
            {
                List<SummerCampRegularRegistration> checkedDays = new List<SummerCampRegularRegistration>(RegularRegistration.Where(x => x.IsChecked.Equals(true)));
                System.Diagnostics.Debug.WriteLine("=============================");
                System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(checkedDays));
                System.Diagnostics.Debug.WriteLine("=============================");
                if (!checkedDays.Any())
                {
                    datasCompleted = false;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        page.DisplayAlert("Missing Fields", "Please select the weeks you are registering.", "Okay");
                    });
                }
                else
                {
                    newJson = new
                    {
                        week_one = checkedDays.Any(x => x.name[5].Equals('1')) ? "1" : "0",
                        week_two = checkedDays.Any(x => x.name[5].Equals('2')) ? "1" : "0",
                        week_three = checkedDays.Any(x => x.name[5].Equals('3')) ? "1" : "0",
                        week_four = checkedDays.Any(x => x.name[5].Equals('4')) ? "1" : "0",
                        week_five = checkedDays.Any(x => x.name[5].Equals('5')) ? "1" : "0",
                        week_six = checkedDays.Any(x => x.name[5].Equals('6')) ? "1" : "0",
                        week_seven = checkedDays.Any(x => x.name[5].Equals('7')) ? "1" : "0",
                        week_eight = checkedDays.Any(x => x.name[5].Equals('8')) ? "1" : "0",
                        week_nine = checkedDays.Any(x => x.name[5].Equals('9')) ? "1" : "0",
                        week_ten = checkedDays.Any(x => x.name.Substring(5,2).Equals("10")) ? "1" : "0"
                    };
                }
            }

            if (!HasAgreed)
            {
                datasCompleted = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "You must accept terms of service before registering.", "Okay");
                });
            }

            if (datasCompleted)
            {
                await Task.Run(async () =>
                {
                    await PopupNavigation.PushAsync(new LoadingPopupPage(), false);
                    object json = new { };
                    if (SummerCampRegistrationOption.Equals("summer_camp_promo_registration"))
                    {
                        json = new
                        {
                            registration_record = new
                            {
                                location = LocationSource[SelectedIndex],
                                child_records_attributes = child_list,
                                summer_camp_record_attributes = newJson,
                                coupon_code = CouponCode,
                                agree_to_tos = HasAgreed ? "1" : "0",
                                customer_id = dataClass.CustomerUser.customer_id,
                                referrer = "summer_camp"

                            },
                            summer_camp_registration_option = SummerCampRegistrationOption,
                            summer_camp_regular_registration_chosen = SummerCampRegularRegistrationChosen,
                            summer_camp_promo_registration_chosen = SummerCampPromoRegistrationChosen,
                            promo_name = PromoName,
                            commit = "Submit And Pay With Card on File"
                        };
                    }
                    else
                    {
                        json = new
                        {
                            registration_record = new
                            {
                                location = LocationSource[SelectedIndex],
                                child_records_attributes = child_list,
                                summer_camp_record_attributes = newJson,
                                coupon_code = CouponCode,
                                agree_to_tos = HasAgreed ? "1" : "0",
                                customer_id = dataClass.CustomerUser.customer_id,
                                referrer = "summer_camp"

                            },
                            summer_camp_registration_option = SummerCampRegistrationOption,
                            summer_camp_regular_registration_chosen = SummerCampRegularRegistrationChosen,
                            summer_camp_promo_registration_chosen = SummerCampPromoRegistrationChosen,
                            promo_name = PromoName,
                            commit = "Submit And Pay With Card on File"
                        };
                    }
                    reqType = 2;
                    await restService.PostRequestAsync(Constants.REGISTRATION_RECORDS, json, cts.Token);
                });
            }
            //System.Diagnostics.Debug.WriteLine("============================="); 
            //System.Diagnostics.Debug.WriteLine(LocationSource[SelectedIndex]);
            //System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(child_list));
            //System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(newJson));
            //System.Diagnostics.Debug.WriteLine(HasAgreed ? "1" : "0");
            //System.Diagnostics.Debug.WriteLine(dataClass.CustomerUser.customer_id);
            //System.Diagnostics.Debug.WriteLine(SummerCampRegistrationOption);
            //System.Diagnostics.Debug.WriteLine(SummerCampRegularRegistrationChosen);
            //System.Diagnostics.Debug.WriteLine(SummerCampPromoRegistrationChosen);
            //System.Diagnostics.Debug.WriteLine(PromoName);
            //System.Diagnostics.Debug.WriteLine("=============================");

        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            Device.BeginInvokeOnMainThread(async () =>
            {

                switch (reqType)
                {
                    case 1:
                        List<string> _locationSource = new List<string>(), _locationAddresSource = new List<string>();
                        List<int> _locationMinimumAge = new List<int>();
                        List<LocationsModel> locations = JsonConvert.DeserializeObject<List<LocationsModel>>(jsonData["locations"].ToString());
                        foreach (LocationsModel location in locations)
                        {
                            _locationSource.Add(location.name);
                            _locationAddresSource.Add(location.address);
                            _locationMinimumAge.Add(location.minimum_age);
                        }
                        LocationSource = _locationSource;
                        LocationAddress = _locationAddresSource;
                        LocationMinimumAge = _locationMinimumAge;
                        SelectedIndex = 0;
                        SelectedItem = LocationSource[0];
                        reqType = 0;
                        break;
                    case 2:
                        await PopupNavigation.PopAllAsync();
                        await page.DisplayAlert("Success", "Registration Complete", "Okay");
                        break;
                    case 3:
                        var height = App.DeviceType == 1 ? 35 : 30;
                        RegularRegistration = JsonConvert.DeserializeObject<List<SummerCampRegularRegistration>>(jsonData["summer_camp_weeks"].ToString());
                        ListViewHeight = (height * RegularRegistration.Count).ScaleHeight();
                        System.Diagnostics.Debug.WriteLine("=============================");
                        System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(RegularRegistration));
                        System.Diagnostics.Debug.WriteLine("=============================");
                        break;
                }

            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await page.DisplayAlert(title, error, "okay");
            });
        }

        CancellationTokenSource locationCTS;
        public async void FetchSummerSchedules(string location)
        {
            //GET_SUMMER_CAMP_WEEKS
            System.Diagnostics.Debug.WriteLine(location);
            await Task.Run(async () =>
            {
                if(locationCTS != null)
                {
                    locationCTS.Cancel();
                }
                locationCTS = null;
                locationCTS = new CancellationTokenSource();
                reqType = 3;
                await restService.GetRequest(Constants.ROOT_URL + Constants.GET_SUMMER_CAMP_WEEKS + location, locationCTS.Token);
            });
        }
    }

    public class SummerCampAttributes
    {
        bool _week_one = false;
        public bool week_one
        {
            get { return _week_one; }
            set { _week_one = value; }
        }

        bool _week_two = false;
        public bool week_two
        {
            get { return _week_two; }
            set { _week_two = value; }
        }

        bool _week_three = false;
        public bool week_three
        {
            get { return _week_three; }
            set { _week_three = value; }
        }

        bool _week_four = false;
        public bool week_four
        {
            get { return _week_four; }
            set { _week_four = value; }
        }

        bool _week_five = false;
        public bool week_five
        {
            get { return _week_five; }
            set { _week_five = value; }
        }

        bool _week_six = false;
        public bool week_six
        {
            get { return _week_six; }
            set { _week_six = value; }
        }

        bool _week_seven = false;
        public bool week_seven
        {
            get { return _week_seven; }
            set { _week_seven = value; }
        }

        bool _week_eight = false;
        public bool week_eight
        {
            get { return _week_eight; }
            set { _week_eight = value; }
        }

        bool _week_nine = false;
        public bool week_nine
        {
            get { return _week_nine; }
            set { _week_nine = value; }
        }

        bool _week_ten = false;
        public bool week_ten
        {
            get { return _week_ten; }
            set { _week_ten = value; }
        }

        public int getcheckcount
        {
            get
            {
                int count = 0;
                if (week_one)
                {
                    count += 1;
                }
                if (week_two)
                {
                    count += 1;
                }
                if (week_three)
                {
                    count += 1;
                }
                if (week_four)
                {
                    count += 1;
                }
                if (week_five)
                {
                    count += 1;
                }
                if (week_six)
                {
                    count += 1;
                }
                if (week_seven)
                {
                    count += 1;
                }
                if (week_eight)
                {
                    count += 1;
                }
                if (week_nine)
                {
                    count += 1;
                }
                if (week_ten)
                {
                    count += 1;
                }

                return count;
            }
        }

        public bool checkifhasselected
        {
            get
            {
                if (week_one)
                {
                    return true;
                }
                if (week_two)
                {
                    return true;
                }
                if (week_three)
                {
                    return true;
                }
                if (week_four)
                {
                    return true;
                }
                if (week_five)
                {
                    return true;
                }
                if (week_six)
                {
                    return true;
                }
                if (week_seven)
                {
                    return true;
                }
                if (week_eight)
                {
                    return true;
                }
                if (week_nine)
                {
                    return true;
                }
                if (week_ten)
                {
                    return true;
                }

                return false;
            }
        }
    }
}
