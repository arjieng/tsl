﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class SingleDayCarePageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        CancellationTokenSource cts;
        Page page;

        private List<string> _LocationSource;
        public List<string> LocationSource
        {
            get { return _LocationSource; }
            set
            {
                if(_LocationSource != value)
                {
                    _LocationSource = value;
                    onPropertyChanged(nameof(LocationSource));
                }
            }
        }

        private List<string> _LocationAddress;
        public List<string> LocationAddress
        {
            get { return _LocationAddress; }
            set
            {
                if (_LocationAddress != value)
                {
                    _LocationAddress = value;
                    onPropertyChanged(nameof(LocationAddress));
                }
            }
        }

        private List<int> _LocationMinimumAge;
        public List<int> LocationMinimumAge
        {
            get { return _LocationMinimumAge; }
            set
            {
                if(_LocationMinimumAge != value)
                {
                    _LocationMinimumAge = value;
                    onPropertyChanged(nameof(LocationMinimumAge));
                }
            }
        }

        private List<DayCheckBoxes> _StandardRegistration;
        public List<DayCheckBoxes> StandardRegistration
        {
            get { return _StandardRegistration; }
            set
            {
                if (_StandardRegistration != value)
                {
                    _StandardRegistration = value;
                    onPropertyChanged(nameof(StandardRegistration));
                }
            }
        }

        private ChildRecord _ChildOne;
        public ChildRecord ChildOne
        {
            get { return _ChildOne; }
            set
            {
                if (_ChildOne != value)
                {
                    _ChildOne = value;
                    onPropertyChanged(nameof(ChildOne));
                }
            }
        }

        private ChildRecord _ChildTwo;
        public ChildRecord ChildTwo
        {
            get { return _ChildTwo; }
            set
            {
                if (_ChildTwo != value)
                {
                    _ChildTwo = value;
                    onPropertyChanged(nameof(ChildTwo));
                }
            }
        }

        private ChildRecord _ChildThree;
        public ChildRecord ChildThree
        {
            get { return _ChildThree; }
            set
            {
                if (_ChildThree != value)
                {
                    _ChildThree = value;
                    onPropertyChanged(nameof(ChildThree));
                }
            }
        }

        private string _SelectedLocationAddress;
        public string SelectedLocationAddress
        {
            get { return _SelectedLocationAddress; }
            set
            {
                if(_SelectedLocationAddress != value)
                {
                    _SelectedLocationAddress = value;
                    onPropertyChanged(nameof(SelectedLocationAddress));
                }
            }
        }

        private int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                if(_SelectedIndex != value)
                {
                    _SelectedIndex = value;
                    onPropertyChanged(nameof(SelectedIndex));
                }
            }
        }

        private string _SelectedItem;
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                if(_SelectedItem != value)
                {
                    _SelectedItem = value;
                    onPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        private ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set
            {
                if(_SubmitCommand != value)
                {
                    _SubmitCommand = value;
                    onPropertyChanged(nameof(SubmitCommand));
                }
            }
        }

        private ICommand _GotoTOSCommand;
        public ICommand GotoTOSCommand
        {
            get { return _GotoTOSCommand; }
            set
            {
                if(_GotoTOSCommand != value)
                {
                    _GotoTOSCommand = value;
                    onPropertyChanged(nameof(GotoTOSCommand));
                }
            }
        }

        private ICommand _RemoveFirstChildCommand;
        public ICommand RemoveFirstChildCommand
        {
            get { return _RemoveFirstChildCommand; }
            set
            {
                if (_RemoveFirstChildCommand != value)
                {
                    _RemoveFirstChildCommand = value;
                    onPropertyChanged(nameof(RemoveFirstChildCommand));
                }
            }
        }

        private ICommand _RemoveSecondChildCommand;
        public ICommand RemoveSecondChildCommand
        {
            get { return _RemoveSecondChildCommand; }
            set
            {
                if (_RemoveSecondChildCommand != value)
                {
                    _RemoveSecondChildCommand = value;
                    onPropertyChanged(nameof(RemoveSecondChildCommand));
                }
            }
        }

        private ICommand _RemoveThirdChildCommand;
        public ICommand RemoveThirdChildCommand
        {
            get { return _RemoveThirdChildCommand; }
            set
            {
                if (_RemoveThirdChildCommand != value)
                {
                    _RemoveThirdChildCommand = value;
                    onPropertyChanged(nameof(RemoveThirdChildCommand));
                }
            }
        }

        private ICommand _AddChildCommand;
        public ICommand AddChildCommand
        {
            get { return _AddChildCommand; }
            set
            {
                if(_AddChildCommand != value)
                {
                    _AddChildCommand = value;
                    onPropertyChanged(nameof(AddChildCommand));
                }
            }
        }

        private string _CouponCode;
        public string CouponCode
        {
            get { return _CouponCode; }
            set
            {
                if(_CouponCode != value)
                {
                    _CouponCode = value;
                    onPropertyChanged(nameof(CouponCode));
                }
            }
        }

        private bool _HasAgreed;
        public bool HasAgreed
        {
            get { return _HasAgreed; }
            set
            {
                _HasAgreed = value;
                onPropertyChanged(nameof(HasAgreed));
            }
        }

        private bool _FirstChildVisibility;
        public bool FirstChildVisibility
        {
            get { return _FirstChildVisibility; }
            set
            {
                if(_FirstChildVisibility != value)
                {
                    _FirstChildVisibility = value;
                    onPropertyChanged(nameof(FirstChildVisibility));
                }
            }
        }

        private bool _SecondChildVisibility;
        public bool SecondChildVisibility
        {
            get { return _SecondChildVisibility; }
            set
            {
                if (_SecondChildVisibility != value)
                {
                    _SecondChildVisibility = value;
                    onPropertyChanged(nameof(SecondChildVisibility));
                }
            }
        }

        private bool _ThirdChildVisibility;
        public bool ThirdChildVisibility
        {
            get { return _ThirdChildVisibility; }
            set
            {
                if (_ThirdChildVisibility != value)
                {
                    _ThirdChildVisibility = value;
                    onPropertyChanged(nameof(ThirdChildVisibility));
                }
            }
        }

        private double _ListViewHeight;
        public double ListViewHeight
        {
            get { return _ListViewHeight; }
            set
            {
                if(_ListViewHeight != value)
                {
                    _ListViewHeight = value;
                    onPropertyChanged(nameof(ListViewHeight));
                }
            }
        }

        private int _PaymentAmount;
        public int PaymentAmount
        {
            get { return _PaymentAmount; }
            set
            {
                if(_PaymentAmount != value)
                {
                    _PaymentAmount = value;
                    onPropertyChanged(nameof(PaymentAmount));
                }
            }
        }

        private string _RegistrationType;
        public string RegistrationType
        {
            get { return _RegistrationType; }
            set
            {
                if(_RegistrationType != value)
                {
                    _RegistrationType = value;
                    onPropertyChanged(nameof(RegistrationType));
                }
            }
        }

        public SingleDayCarePageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            InitiateValues();
        }
        int reqType = 0;
        async void InitiateValues() 
        {
            FirstChildVisibility = true;
            SecondChildVisibility = false;
            ThirdChildVisibility = false;
            RegistrationType = "standard";
            CouponCode = "";
            PaymentAmount = 50;
            ChildOne = new ChildRecord();
            ChildTwo = new ChildRecord();
            ChildThree = new ChildRecord();
            HasAgreed = false;
            AddChildCommand = new Command(AddChildCommandAction);
            SubmitCommand = new Command(SubmitCommandAction);
            GotoTOSCommand = new Command((x) => page.Navigation.PushAsync(new TOSPage(0)));
            RemoveFirstChildCommand = new Command((x) => { FirstChildVisibility = false; ChildOne = null; CalculateAmount(RegistrationType); });
            RemoveSecondChildCommand = new Command((x) => { SecondChildVisibility = false; ChildTwo = null; CalculateAmount(RegistrationType); });
            RemoveThirdChildCommand = new Command((x) => { ThirdChildVisibility = false; ChildThree = null; CalculateAmount(RegistrationType); });

            await Task.Run(async () =>
            {
                reqType = 1;
                string url = string.Format("{0}{1}?program_name=Single Day", Constants.ROOT_URL, Constants.GET_LOCATIONS);
                System.Diagnostics.Debug.WriteLine(url);
                await restService.GetRequest(url, cts.Token);
            });
        }

        public void CalculateAmount(string type)
        {
            if((FirstChildVisibility && !SecondChildVisibility && !ThirdChildVisibility) || (!FirstChildVisibility && SecondChildVisibility && !ThirdChildVisibility) || (!FirstChildVisibility && !SecondChildVisibility && ThirdChildVisibility))
            {
                PaymentAmount = type == "standard" ? 50 : 400;
            }
            else if((FirstChildVisibility && SecondChildVisibility && !ThirdChildVisibility) || (SecondChildVisibility && ThirdChildVisibility && !FirstChildVisibility) || (FirstChildVisibility && ThirdChildVisibility && !SecondChildVisibility))
            {
                PaymentAmount = type == "standard" ? 85 : 700;
            }
            else
            {
                PaymentAmount = type == "standard" ? 100 : 900;
            }
        }

        void AddChildCommandAction()
        {
            if (!FirstChildVisibility)
            {
                ChildOne = new ChildRecord();
                FirstChildVisibility = true;
                CalculateAmount(RegistrationType);
            }
            else if (!SecondChildVisibility)
            {
                ChildTwo = new ChildRecord();
                SecondChildVisibility = true;
                CalculateAmount(RegistrationType);
            }
            else if (!ThirdChildVisibility)
            {
                ChildThree = new ChildRecord();
                ThirdChildVisibility = true;
                CalculateAmount(RegistrationType);
            }
        }

        async void SubmitCommandAction()
        {
            bool datasCompleted = true, invokeChilMissingFields = false;
            List<ChildRecord> child_list = new List<ChildRecord>();
            if (FirstChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildOne.first_name) && !string.IsNullOrEmpty(ChildOne.last_name) && !string.IsNullOrEmpty(ChildOne.dob) && !string.IsNullOrEmpty(ChildOne.school_attending))
                {
                    child_list.Add(ChildOne);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (SecondChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildTwo.first_name) && !string.IsNullOrEmpty(ChildTwo.last_name) && !string.IsNullOrEmpty(ChildTwo.dob) && !string.IsNullOrEmpty(ChildTwo.school_attending))
                {
                    child_list.Add(ChildTwo);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (ThirdChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildThree.first_name) && !string.IsNullOrEmpty(ChildThree.last_name) && !string.IsNullOrEmpty(ChildThree.dob) && !string.IsNullOrEmpty(ChildThree.school_attending))
                {
                    child_list.Add(ChildThree);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (invokeChilMissingFields)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "Please fill all fields in childs form.", "Okay");
                });
            }


            var checkedDays = StandardRegistration.Where(x => x.IsChecked.Equals(true));
            var ids = checkedDays.Select(p => p.id);
            if (checkedDays.Count() == 0)
            {
                datasCompleted = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "Please select what day are you registering.", "Okay");
                });
            }

            if (!HasAgreed)
            {
                datasCompleted = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "You must accept terms of service before registering.", "Okay");
                });
            }

            if (datasCompleted)
            {
                await Task.Run(async () =>
                {
                    await PopupNavigation.PushAsync(new LoadingPopupPage(), false);
                    reqType = 3;
                    if (RegistrationType.Equals("standard"))
                    {
                        var json = new
                        {
                            registration_record = new
                            {
                                location = LocationSource[SelectedIndex],
                                child_records_attributes = child_list,
                                single_day_record_attributes = new
                                {
                                    registration_type = "standard_single_day_registration", // RegistrationType.Equals("standard") ? "" : "promo_registration",
                                    single_day_date_id = ""
                                },
                                single_day_date_ids = ids,
                                annual_single_day_promo_record_attributes = new { },
                                coupon_code = CouponCode,
                                agree_to_tos = HasAgreed ? "1" : "0",
                                customer_id = DataClass.GetInstance.CustomerUser.customer_id,
                                referrer = "single_day"
                            },
                            commit = "Submit And Pay With Card on File"
                        };
                        System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(json) + Constants.REGISTRATION_RECORDS);
                        await restService.PostRequestAsync(Constants.REGISTRATION_RECORDS, json, cts.Token);
                    }
                    else
                    {
                        var json = new
                        {
                            registration_record = new
                            {
                                location = LocationSource[SelectedIndex],
                                child_records_attributes = child_list,
                                single_day_record_attributes = new
                                {
                                    registration_type = "promo_registration",
                                    single_day_date_id = ""
                                },
                                single_day_date_ids = ids,
                                annual_single_day_promo_record_attributes = new {
                                    promo_name = "one_child_annual_single_day"
                                },
                                coupon_code = CouponCode,
                                agree_to_tos = HasAgreed ? "1" : "0",
                                customer_id = DataClass.GetInstance.CustomerUser.customer_id,
                                referrer = "single_day"
                            },
                            commit = "Submit And Pay With Card on File"
                        };
                        System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(json) + Constants.REGISTRATION_RECORDS);
                        //await restService.PostRequestAsync(Constants.REGISTRATION_RECORDS, json, cts.Token);
                    }



                });
            }
        }

        public async void ChangeHeight()
        {
            //var height = App.DeviceType == 1 ? 35 : 30;
            //if (SelectedIndex == 2)
            //{
            //    StandardRegistration = new List<DayCheckBoxes>();
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 7", IsChecked = false });
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 8", IsChecked = false });
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 9", IsChecked = false });
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 10", IsChecked = false });
            //    ListViewHeight = (height * StandardRegistration.Count).ScaleHeight();
            //}
            //else if(SelectedIndex == 1)
            //{
            //    StandardRegistration = new List<DayCheckBoxes>();
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 4", IsChecked = false });
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 5", IsChecked = false });
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 6", IsChecked = false });
            //    ListViewHeight = (height * StandardRegistration.Count).ScaleHeight();
            //}
            //else if(SelectedIndex == 0)
            //{
            //    StandardRegistration = new List<DayCheckBoxes>();
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 1", IsChecked = false });
            //    StandardRegistration.Add(new DayCheckBoxes { Text = "Option 2", IsChecked = false });
            //    ListViewHeight = (height * StandardRegistration.Count).ScaleHeight();
            //}
            await Task.Run(async () =>
            {
                reqType = 2;
                string url = string.Format("{0}{1}?location_name={2}", Constants.ROOT_URL, Constants.GET_SINGLE_DAY_DATES, LocationSource[SelectedIndex]);
                System.Diagnostics.Debug.WriteLine(url);
                await restService.GetRequest(url, cts.Token);
            });
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            Device.BeginInvokeOnMainThread(async () =>
            {

                switch (reqType)
                {
                    case 1:
                        List<string> _locationSource = new List<string>(), _locationAddresSource = new List<string>();
                        List<int> _locationMinimumAge = new List<int>();
                        List<LocationsModel> locations = JsonConvert.DeserializeObject<List<LocationsModel>>(jsonData["locations"].ToString());
                        foreach(LocationsModel location in locations)
                        {
                            _locationSource.Add(location.name);
                            _locationAddresSource.Add(location.address);
                            _locationMinimumAge.Add(location.minimum_age);
                        }
                        LocationSource = _locationSource;
                        LocationAddress = _locationAddresSource;
                        LocationMinimumAge = _locationMinimumAge;
                        SelectedIndex = 0;
                        SelectedItem = LocationSource[0];
                        reqType = 0;
                        break;
                    case 2:
                        var height = App.DeviceType == 1 ? 35 : 30;
                        //single_day_datesList
                        List<SingleDayDatesModel> _singleDates = JsonConvert.DeserializeObject<List<SingleDayDatesModel>>(jsonData["single_day_dates"].ToString());
                        StandardRegistration = new List<DayCheckBoxes>();
                        foreach (SingleDayDatesModel singleDate in _singleDates)
                        {
                            StandardRegistration.Add(new DayCheckBoxes { id = singleDate.id, Text = singleDate.name, IsChecked = false });
                        }
                        ListViewHeight = (height * StandardRegistration.Count).ScaleHeight();
                        reqType = 0;
                        break;
                    case 3:
                        await PopupNavigation.PopAllAsync();
                        await page.DisplayAlert("Success", "Registration Complete", "Okay");
                        break;
                }

            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await PopupNavigation.PopAllAsync();
                await page.DisplayAlert(title, error, "okay");
            });
        }
    }

    public class ChildRecord
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string dob { get; set; }
        public string school_attending { get; set; }
        private string destroy = "false";
        public string _destroy { get { return destroy; } set { destroy = value; } }
    }
}
