﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class SignupPageViewModel : BaseModel, IRestConnector
    {
        CancellationTokenSource cts;
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        Page page;
        bool isValid;

        private string _Email;
        public string Email
        {
            get { return _Email; }
            set
            {
                if (_Email != value)
                {
                    _Email = value;
                    onPropertyChanged(nameof(Email));
                }
            }
        }

        private string _Password;
        public string Password
        {
            get { return _Password; }
            set
            {
                if (_Password != value)
                {
                    _Password = value;
                    onPropertyChanged(nameof(Password));
                }
            }
        }

        private string _ConfirmPassword;
        public string ConfirmPassword
        {
            get { return _ConfirmPassword; }
            set
            {
                if (_ConfirmPassword != value)
                {
                    _ConfirmPassword = value;
                    onPropertyChanged(nameof(ConfirmPassword));
                }
            }
        }

        private ICommand _SignupCommand;
        public ICommand SignupCommand
        {
            get { return _SignupCommand; }
            set
            {
                if (_SignupCommand != value)
                {
                    _SignupCommand = value;
                    onPropertyChanged(nameof(SignupCommand));
                }
            }
        }

        private ICommand _GotoSigninCommand;
        public ICommand GotoSigninCommand
        {
            get { return _GotoSigninCommand; }
            set
            {
                if (_GotoSigninCommand != value)
                {
                    _GotoSigninCommand = value;
                    onPropertyChanged(nameof(GotoSigninCommand));
                }
            }
        }

        private bool _PasswordLabelVisibility;
        public bool PasswordLabelVisibility
        {
            get { return _PasswordLabelVisibility; }
            set
            {
                if (_PasswordLabelVisibility != value)
                {
                    _PasswordLabelVisibility = value;
                    onPropertyChanged(nameof(PasswordLabelVisibility));
                }
            }
        }

        public SignupPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            PasswordLabelVisibility = false;
            page = owner;

            Email = "";
            Password = "";
            ConfirmPassword = "";
            SignupCommand = new Command(SignupAction);
            GotoSigninCommand = new Command(GoToSignin);
        }

        async void SignupAction()
        {
            isValid = true;
            if (string.IsNullOrEmpty(Email) || string.IsNullOrEmpty(Password) || string.IsNullOrEmpty(ConfirmPassword))
            {
                PasswordLabelVisibility = false;
                isValid = false;
            }

            if (!Password.Equals(ConfirmPassword))
            {
                PasswordLabelVisibility = true;
                isValid = false;
            }

            if (isValid)
            {
                PasswordLabelVisibility = false;
                await Task.Run(async () =>
                {
                    PopupNavigation.PushAsync(new LoadingPopupPage());
                    string url = Constants.ROOT_URL + Constants.REGISTER_URL;
                    await restService.PostRequestAsync(url, new { customer_user = new { email = Email, password = Password } }, cts.Token);
                });
            }
        }

        void GoToSignin()
        {
            page.Navigation.PopAsync();
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if ((int.Parse(jsonData["status"].ToString())) == 200)
                {
                    System.Diagnostics.Debug.WriteLine(jsonData);
                    var registeredUser = JsonConvert.DeserializeObject<CustomerUser>((jsonData["user"].ToString()));
                    registeredUser.has_basic_information = Boolean.Parse(jsonData["has_basic_information"].ToString());
                    registeredUser.confirmed_information = Boolean.Parse(jsonData["confirmed_information"].ToString());
                    dataClass.CustomerUser = registeredUser;

                    await PopupNavigation.PopAllAsync();
                    //await page.Navigation.PushAsync(new UserInfoPage(true, Email));
                    App.Current.MainPage = new UserInfoPage(true, Email);
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await PopupNavigation.PopAllAsync();
                await page.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
