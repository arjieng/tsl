﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class EnrollmentDetailPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        CancellationTokenSource cts;
        Page page;

        private ObservableCollection<Student> _ChildInfoSource;
        public ObservableCollection<Student> ChildInfoSource
        {
            get { return _ChildInfoSource; }
            set
            {
                if(_ChildInfoSource != value)
                {
                    _ChildInfoSource = value;
                    onPropertyChanged(nameof(ChildInfoSource));
                }
            }
        }

        private bool _IsActive;
        public bool IsActive
        {
            get { return _IsActive; }
            set
            {
                if(_IsActive != value)
                {
                    _IsActive = value;
                    onPropertyChanged(nameof(IsActive));
                }
            }
        }

        private string _ProgramTextSource;
        public string ProgramTextSource
        {
            get { return _ProgramTextSource; }
            set
            {
                if(_ProgramTextSource != value)
                {
                    _ProgramTextSource = value;
                    onPropertyChanged(nameof(ProgramTextSource));
                }
            }
        }

        public EnrollmentDetailPageViewModel(Page owner, RegistrationInfo info)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            ProgramTextSource = info.program;
            IsActive = info.is_active;

            SetValues(info.id);

        }

        async void SetValues(int id) // this should be on ReceiveJSONData
        {
            ChildInfoSource = new ObservableCollection<Student>();
            //ChildInfoSource.Add(new Student() { first_name = "Finn", last_name = "Mertens", age = 8, dob = "2010-10-10", school_attending = "N/A" });
            //ChildInfoSource.Add(new Student() { first_name = "Jake", last_name = "Mertens", age = 8, dob = "2010-10-10", school_attending = "N/A" });

            await Task.Run(async () =>
            {
                //reqType = 2;
                string url = Constants.ROOT_URL + Constants.REGISTRATION_INFORMATION_SHOW + id;
                await restService.GetRequest(url, cts.Token);
            });
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                var list = JsonConvert.DeserializeObject<ObservableCollection<Student>>(jsonData["child_records"].ToString());
                var today = DateTime.Today;
                foreach (var l in list)
                {
                    var birthdate = DateTime.Parse(l.dob);
                    var age = today.Year - birthdate.Year;
                    if (birthdate.Date > today.AddYears(-age)) age--;
                    ChildInfoSource.Add(new Student() { first_name = l.first_name, last_name = l.last_name, age = age, dob = l.dob, school_attending = l.school_attending });
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {

        }
    }
}
