﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class BeforeAfterSchoolPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        Page page;
        int reqType = 0;

        private List<string> _LocationSource;
        public List<string> LocationSource
        {
            get { return _LocationSource; }
            set
            {
                if (_LocationSource != value)
                {
                    _LocationSource = value;
                    onPropertyChanged(nameof(LocationSource));
                }
            }
        }

        private List<string> _LocationAddress;
        public List<string> LocationAddress
        {
            get { return _LocationAddress; }
            set
            {
                if (_LocationAddress != value)
                {
                    _LocationAddress = value;
                    onPropertyChanged(nameof(LocationAddress));
                }
            }
        }

        private string _SelectedLocationAddress;
        public string SelectedLocationAddress
        {
            get { return _SelectedLocationAddress; }
            set
            {
                if (_SelectedLocationAddress != value)
                {
                    _SelectedLocationAddress = value;
                    onPropertyChanged(nameof(SelectedLocationAddress));
                }
            }
        }

        private int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                if (_SelectedIndex != value)
                {
                    _SelectedIndex = value;
                    onPropertyChanged(nameof(SelectedIndex));
                }
            }
        }

        private int _RegistrationTypeSelectionSelectedIndex;
        public int RegistrationTypeSelectionSelectedIndex
        {
            get { return _RegistrationTypeSelectionSelectedIndex; }
            set
            {
                if (_RegistrationTypeSelectionSelectedIndex != value)
                {
                    _RegistrationTypeSelectionSelectedIndex = value;
                    onPropertyChanged(nameof(RegistrationTypeSelectionSelectedIndex));
                }
            }
        }

        private int _PackageSelectedIndex;
        public int PackageSelectedIndex
        {
            get { return _PackageSelectedIndex; }
            set
            {
                if (_PackageSelectedIndex != value)
                {
                    _PackageSelectedIndex = value;
                    onPropertyChanged(nameof(PackageSelectedIndex));
                }
            }
        }

        private ChildRecord _ChildOne;
        public ChildRecord ChildOne
        {
            get { return _ChildOne; }
            set
            {
                if (_ChildOne != value)
                {
                    _ChildOne = value;
                    onPropertyChanged(nameof(ChildOne));
                }
            }
        }

        private ChildRecord _ChildTwo;
        public ChildRecord ChildTwo
        {
            get { return _ChildTwo; }
            set
            {
                if (_ChildTwo != value)
                {
                    _ChildTwo = value;
                    onPropertyChanged(nameof(ChildTwo));
                }
            }
        }

        private ChildRecord _ChildThree;
        public ChildRecord ChildThree
        {
            get { return _ChildThree; }
            set
            {
                if (_ChildThree != value)
                {
                    _ChildThree = value;
                    onPropertyChanged(nameof(ChildThree));
                }
            }
        }

        private SchoolWeekDaysScheds _BeforeSchool;
        public SchoolWeekDaysScheds BeforeSchool
        {
            get { return _BeforeSchool; }
            set
            {
                if (_BeforeSchool != value)
                {
                    _BeforeSchool = value;
                    onPropertyChanged(nameof(BeforeSchool));
                }
            }
        }

        private SchoolWeekDaysScheds _AfterSchool;
        public SchoolWeekDaysScheds AfterSchool
        {
            get { return _AfterSchool; }
            set
            {
                if (_AfterSchool != value)
                {
                    _AfterSchool = value;
                    onPropertyChanged(nameof(AfterSchool));
                }
            }
        }



        private ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set
            {
                if (_SubmitCommand != value)
                {
                    _SubmitCommand = value;
                    onPropertyChanged(nameof(SubmitCommand));
                }
            }
        }

        private ICommand _GotoTOSCommand;
        public ICommand GotoTOSCommand
        {
            get { return _GotoTOSCommand; }
            set
            {
                if (_GotoTOSCommand != value)
                {
                    _GotoTOSCommand = value;
                    onPropertyChanged(nameof(GotoTOSCommand));
                }
            }
        }

        private ICommand _RemoveFirstChildCommand;
        public ICommand RemoveFirstChildCommand
        {
            get { return _RemoveFirstChildCommand; }
            set
            {
                if (_RemoveFirstChildCommand != value)
                {
                    _RemoveFirstChildCommand = value;
                    onPropertyChanged(nameof(RemoveFirstChildCommand));
                }
            }
        }

        private ICommand _RemoveSecondChildCommand;
        public ICommand RemoveSecondChildCommand
        {
            get { return _RemoveSecondChildCommand; }
            set
            {
                if (_RemoveSecondChildCommand != value)
                {
                    _RemoveSecondChildCommand = value;
                    onPropertyChanged(nameof(RemoveSecondChildCommand));
                }
            }
        }

        private ICommand _RemoveThirdChildCommand;
        public ICommand RemoveThirdChildCommand
        {
            get { return _RemoveThirdChildCommand; }
            set
            {
                if (_RemoveThirdChildCommand != value)
                {
                    _RemoveThirdChildCommand = value;
                    onPropertyChanged(nameof(RemoveThirdChildCommand));
                }
            }
        }

        private ICommand _RegistrationTypeSelectionSelectedIndexChangedCommand;
        public ICommand RegistrationTypeSelectionSelectedIndexChangedCommand
        {
            get { return _RegistrationTypeSelectionSelectedIndexChangedCommand; }
            set { _RegistrationTypeSelectionSelectedIndexChangedCommand = value; onPropertyChanged(nameof(RegistrationTypeSelectionSelectedIndexChangedCommand)); }
        }

        ICommand _PackageSelectedIndexChanged;
        public ICommand PackageSelectedIndexChanged
        {
            get { return _PackageSelectedIndexChanged; }
            set { _PackageSelectedIndexChanged = value; onPropertyChanged(nameof(PackageSelectedIndexChanged)); }
        }

        private ICommand _AddChildCommand;
        public ICommand AddChildCommand
        {
            get { return _AddChildCommand; }
            set
            {
                if (_AddChildCommand != value)
                {
                    _AddChildCommand = value;
                    onPropertyChanged(nameof(AddChildCommand));
                }
            }
        }

        private bool _FirstChildVisibility;
        public bool FirstChildVisibility
        {
            get { return _FirstChildVisibility; }
            set
            {
                if (_FirstChildVisibility != value)
                {
                    _FirstChildVisibility = value;
                    onPropertyChanged(nameof(FirstChildVisibility));
                }
            }
        }

        private bool _SecondChildVisibility;
        public bool SecondChildVisibility
        {
            get { return _SecondChildVisibility; }
            set
            {
                if (_SecondChildVisibility != value)
                {
                    _SecondChildVisibility = value;
                    onPropertyChanged(nameof(SecondChildVisibility));
                }
            }
        }

        private bool _ThirdChildVisibility;
        public bool ThirdChildVisibility
        {
            get { return _ThirdChildVisibility; }
            set
            {
                if (_ThirdChildVisibility != value)
                {
                    _ThirdChildVisibility = value;
                    onPropertyChanged(nameof(ThirdChildVisibility));
                }
            }
        }

        private int _DepositFee;
        public int DepositFee
        {
            get { return _DepositFee; }
            set
            {
                if(_DepositFee != value)
                {
                    _DepositFee = value;
                    onPropertyChanged(nameof(DepositFee));
                }
            }
        }

        private int _RegistrationFee;
        public int RegistrationFee
        {
            get { return _RegistrationFee; }
            set
            {
                if(_RegistrationFee != value)
                {
                    _RegistrationFee = value;
                    onPropertyChanged(nameof(RegistrationFee));
                }
            }
        }

        private int _AnnualPackageFee;
        public int AnnualPackageFee
        {
            get { return _AnnualPackageFee; }
            set
            {
                if(_AnnualPackageFee != value)
                {
                    _AnnualPackageFee = value;
                    onPropertyChanged(nameof(AnnualPackageFee));
                }
            }
        }

        private int _TotalAmount;
        public int TotalAmount
        {
            get { return _TotalAmount; }
            set
            {
                if(_TotalAmount != value)
                {
                    _TotalAmount = value;
                    onPropertyChanged(nameof(TotalAmount));
                }
            }
        }

        private string _SelectedItem;
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                if (_SelectedItem != value)
                {
                    _SelectedItem = value;
                    onPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        private string _CouponCode;
        public string CouponCode
        {
            get { return _CouponCode; }
            set
            {
                if (_CouponCode != value)
                {
                    _CouponCode = value;
                    onPropertyChanged(nameof(CouponCode));
                }
            }
        }

        private List<int> _LocationMinimumAge;
        public List<int> LocationMinimumAge
        {
            get { return _LocationMinimumAge; }
            set
            {
                if (_LocationMinimumAge != value)
                {
                    _LocationMinimumAge = value;
                    onPropertyChanged(nameof(LocationMinimumAge));
                }
            }
        }

        private bool _HasAgreed;
        public bool HasAgreed
        {
            get { return _HasAgreed; }
            set
            {
                _HasAgreed = value;
                onPropertyChanged(nameof(HasAgreed));
            }
        }

        public BeforeAfterSchoolPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            InitiateValues();
        }

        async void InitiateValues() // this should be on ReceiveJSONData because locations will be fetch from server
        {
            //LocationSource = new List<string>
            //{
            //    "East Greenbush - FUMC",
            //    "Guilderland - Christ Lutheran Church",
            //    "Guilderland - School House Road",
            //    "Niskayuna",
            //    "Rotterdam",
            //    "Troy - Sacred Heart"
            //};

            //LocationAddress = new List<string>
            //{
            //    "4 Northcrest Drive Clifton Park, NY 12065", 
            //    "1500 Western Avenue Albany, NY 12203", 
            //    "183 Schoolhouse Road Albany, NY 12203",
            //    "3041 Troy Schenectady Road Niskayuna, NY, 12309",  
            //    "2617 Hamburg Street", 
            //    "625 7th Avenue", 
            //};
            DepositFee = 100;
            RegistrationFee = 25;
            AnnualPackageFee = 0;
            TotalAmount = DepositFee + RegistrationFee + AnnualPackageFee;

            FirstChildVisibility = true;
            SecondChildVisibility = false;
            ThirdChildVisibility = false;

            PackageSelectedIndex = 0;
            SelectedIndex = 0;
            RegistrationTypeSelectionSelectedIndex = 0;
            AfterSchool = new SchoolWeekDaysScheds();
            BeforeSchool = new SchoolWeekDaysScheds();
            ChildOne = new ChildRecord();
            ChildTwo = new ChildRecord();
            ChildThree = new ChildRecord();
            AddChildCommand = new Command(AddChildCommandAction);
            SubmitCommand = new Command(SubmitCommandAction);
            GotoTOSCommand = new Command((x) => page.Navigation.PushAsync(new TOSPage(2)));
            RemoveFirstChildCommand = new Command((x) => FirstChildVisibility = false);
            RemoveSecondChildCommand = new Command((x) => SecondChildVisibility = false);
            RemoveThirdChildCommand = new Command((x) => ThirdChildVisibility = false);
            RegistrationTypeSelectionSelectedIndexChangedCommand = new Command(RegistrationTypeSelectionSelectedIndexChangedAction);
            PackageSelectedIndexChanged = new Command(PackageSelectedIndexAction);
            CouponCode = "";
            await Task.Run(async () =>
            {
                reqType = 1;
                string url = string.Format("{0}{1}?program_name=After School", Constants.ROOT_URL, Constants.GET_LOCATIONS);
                System.Diagnostics.Debug.WriteLine(url);
                await restService.GetRequest(url, cts.Token);
            });
        }

        private void PackageSelectedIndexAction(object obj)
        {
            switch (PackageSelectedIndex)
            {
                case 0:
                    AnnualPackageFee = 400;
                    break;
                case 1:
                    AnnualPackageFee = 700;
                    break;
                case 2:
                    AnnualPackageFee = 900;
                    break;
            }
            TotalAmount = DepositFee + RegistrationFee + AnnualPackageFee;
        }

        public void SelectedIndexXhanged()
        {
            if (SelectedItem.Equals(1))
            {
                AnnualPackageFee = 400;
                TotalAmount = DepositFee + RegistrationFee + AnnualPackageFee;
            }
            else
            {
                RegistrationTypeSelectionSelectedIndex = 0;
                DepositFee = 100;
                RegistrationFee = 25;
                AnnualPackageFee = 0;
                TotalAmount = DepositFee + RegistrationFee + AnnualPackageFee;
            }
        }

        private void RegistrationTypeSelectionSelectedIndexChangedAction(object obj)
        {
            if (SelectedIndex.Equals(1))
            {
                switch (RegistrationTypeSelectionSelectedIndex)
                {
                    case 0:
                        DepositFee = 100;
                        RegistrationFee = 25;
                        AnnualPackageFee = 0;
                        break;
                    case 1:
                        DepositFee = 100;
                        RegistrationFee = 25;
                        switch (PackageSelectedIndex)
                        {
                            case 0:
                                AnnualPackageFee = 400;
                                break;
                            case 1:
                                AnnualPackageFee = 700;
                                break;
                            case 2:
                                AnnualPackageFee = 900;
                                break;
                        }
                        break;
                    case 2:
                        DepositFee = 0;
                        RegistrationFee = 0;
                        switch (PackageSelectedIndex)
                        {
                            case 0:
                                AnnualPackageFee = 400;
                                break;
                            case 1:
                                AnnualPackageFee = 700;
                                break;
                            case 2:
                                AnnualPackageFee = 900;
                                break;
                        }
                        break;
                }
                TotalAmount = DepositFee + RegistrationFee + AnnualPackageFee;
            }
        }

        void AddChildCommandAction()
        {
            if (!FirstChildVisibility)
            {
                FirstChildVisibility = true;
            }
            else if (!SecondChildVisibility)
            {
                SecondChildVisibility = true;
            }
            else if (!ThirdChildVisibility)
            {
                ThirdChildVisibility = true;
            }
        }

        async void SubmitCommandAction()
        {
            bool datasCompleted = true, invokeChilMissingFields = false;
            string registration_type = "standard_after_school_registration";
            List<ChildRecord> child_list = new List<ChildRecord>();
            Dictionary<string, string> days = new Dictionary<string, string>();

            if (FirstChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildOne.first_name) && !string.IsNullOrEmpty(ChildOne.last_name) && !string.IsNullOrEmpty(ChildOne.dob) && !string.IsNullOrEmpty(ChildOne.school_attending))
                {
                    child_list.Add(ChildOne);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (SecondChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildTwo.first_name) && !string.IsNullOrEmpty(ChildTwo.last_name) && !string.IsNullOrEmpty(ChildTwo.dob) && !string.IsNullOrEmpty(ChildTwo.school_attending))
                {
                    child_list.Add(ChildTwo);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (ThirdChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildThree.first_name) && !string.IsNullOrEmpty(ChildThree.last_name) && !string.IsNullOrEmpty(ChildThree.dob) && !string.IsNullOrEmpty(ChildThree.school_attending))
                {
                    child_list.Add(ChildThree);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (invokeChilMissingFields)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "Please fill all fields in childs form.", "Okay");
                });
            }

            switch (RegistrationTypeSelectionSelectedIndex)
            {
                case 0:
                    registration_type = "standard_after_school_registration";
                    break;
                case 1:
                    registration_type = "promo_bundle_after_school_registration";
                    break;
                case 2:
                    registration_type = "promo_package_after_school_registration";
                    break;
            }

            if (RegistrationTypeSelectionSelectedIndex != 2)
            {
                if (!AfterSchool.hasChecked)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        datasCompleted = false;
                        page.DisplayAlert("Missing Fields", "Please select days after school.", "Okay");
                    });
                }

                if (!BeforeSchool.hasChecked)
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        datasCompleted = false;
                        page.DisplayAlert("Missing Fields", "Please select days before school.", "Okay");
                    });
                }
            }

            if (!HasAgreed)
            {
                datasCompleted = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "You must accept terms of service before registering.", "Okay");
                });
            }

            if (datasCompleted)
            {
                await Task.Run(async () =>
                {
                    var json = new
                    {
                        registration_record = new
                        {
                            location = LocationSource[SelectedIndex],
                            child_records_attributes = child_list,
                            before_and_afterschool_record_attributes = new
                            {
                                year_cycle = "2019-2020",
                                registration_type = registration_type,
                                start_date = "2019-09-03",
                                before_school_monday = (RegistrationTypeSelectionSelectedIndex != 2) ? (BeforeSchool.monday ? "1" : "0") : "1",
                                before_school_tuesday = (RegistrationTypeSelectionSelectedIndex != 2) ? (BeforeSchool.tuesday ? "1" : "0") : "1",
                                before_school_wednesday = (RegistrationTypeSelectionSelectedIndex != 2) ? (BeforeSchool.wednesday ? "1" : "0") : "1",
                                before_school_thursday = (RegistrationTypeSelectionSelectedIndex != 2) ? (BeforeSchool.thursday ? "1" : "0") : "1",
                                before_school_friday = (RegistrationTypeSelectionSelectedIndex != 2) ? (BeforeSchool.friday ? "1" : "0") : "1",
                                after_school_monday = (RegistrationTypeSelectionSelectedIndex != 2) ? (AfterSchool.monday ? "1" : "0") : "1",
                                after_school_tuesday = (RegistrationTypeSelectionSelectedIndex != 2) ? (AfterSchool.tuesday ? "1" : "0") : "1",
                                after_school_wednesday = (RegistrationTypeSelectionSelectedIndex != 2) ? (AfterSchool.wednesday ? "1" : "0") : "1",
                                after_school_thursday = (RegistrationTypeSelectionSelectedIndex != 2) ? (AfterSchool.thursday ? "1" : "0") : "1",
                                after_school_friday = (RegistrationTypeSelectionSelectedIndex != 2) ? (AfterSchool.friday ? "1" : "0") : "1"
                            },
                            coupon_code = CouponCode,
                            agree_to_tos = HasAgreed ? "1" : "0",
                            customer_id = dataClass.CustomerUser.customer_id,
                            referrer = "after_school"
                        },
                        commit = "Submit And Pay With Card on File"
                    };

                    System.Diagnostics.Debug.WriteLine("=======================");
                    System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(json));
                    System.Diagnostics.Debug.WriteLine(Constants.REGISTRATION_RECORDS);
                    System.Diagnostics.Debug.WriteLine("=======================");
                    await PopupNavigation.PushAsync(new LoadingPopupPage(), false);
                    reqType = 2;
                    await restService.PostRequestAsync(Constants.REGISTRATION_RECORDS, json, cts.Token);
                });
            }

        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            Device.BeginInvokeOnMainThread(async () =>
            {

                switch (reqType)
                {
                    case 1:
                        List<string> _locationSource = new List<string>(), _locationAddresSource = new List<string>();
                        List<int> _locationMinimumAge = new List<int>();
                        List<LocationsModel> locations = JsonConvert.DeserializeObject<List<LocationsModel>>(jsonData["locations"].ToString());
                        foreach (LocationsModel location in locations)
                        {
                            _locationSource.Add(location.name);
                            _locationAddresSource.Add(location.address);
                            _locationMinimumAge.Add(location.minimum_age);
                        }
                        LocationSource = _locationSource;
                        LocationAddress = _locationAddresSource;
                        LocationMinimumAge = _locationMinimumAge;
                        SelectedIndex = 0;
                        SelectedItem = LocationSource[0];
                        reqType = 0;
                        break;
                    case 2:
                        await PopupNavigation.PopAllAsync();
                        await page.DisplayAlert("Success", "Registration Complete", "Okay");
                        break;
                    case 3:
                        break;
                }

            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await page.DisplayAlert(title, error, "okay");
            });
        }
    }

    public class SchoolWeekDaysScheds
    {
        public bool monday { get; set; }
        public bool tuesday { get; set; }
        public bool wednesday { get; set; }
        public bool thursday { get; set; }
        public bool friday { get; set; }

        public bool hasChecked
        {
            get
            {
                if (monday)
                    return true;
                if (tuesday)
                    return true;
                if (wednesday)
                    return true;
                if (thursday)
                    return true;
                if (friday)
                    return true;

                return false;
            }
        }
    }
}
