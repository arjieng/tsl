﻿using System;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class UserInfoPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        string email;
        Page page;
        bool IsNew;

        private string _FirstName;
        public string FirstName
        {
            get { return _FirstName; }
            set
            {
                if(_FirstName != value)
                {
                    _FirstName = value;
                    onPropertyChanged(nameof(FirstName));
                }
            }
        }

        private string _LastName;
        public string LastName
        {
            get { return _LastName; }
            set
            {
                if(_LastName != value)
                {
                    _LastName = value;
                    onPropertyChanged(nameof(LastName));
                }
            }
        }

        private string _EmergencyNumber;
        public string EmergencyNumber
        {
            get { return _EmergencyNumber; }
            set
            {
                if(_EmergencyNumber != value)
                {
                    _EmergencyNumber = value;
                    onPropertyChanged(nameof(EmergencyNumber));
                }
            }
        }

        private string _PhoneNumber;
        public string PhoneNumber
        {
            get { return _PhoneNumber; }
            set
            {
                if(_PhoneNumber != value)
                {
                    _PhoneNumber = value;
                    onPropertyChanged(nameof(PhoneNumber));
                }
            }
        }

        private string _AddressLineOne;
        public string AddressLineOne
        {
            get { return _AddressLineOne; }
            set
            {
                if(_AddressLineOne != value)
                {
                    _AddressLineOne = value;
                    onPropertyChanged(nameof(AddressLineOne));
                }
            }
        }

        private string _AddressLineTwo;
        public string AddressLineTwo
        {
            get { return _AddressLineTwo; }
            set
            {
                if(_AddressLineTwo != value)
                {
                    _AddressLineTwo = value;
                    onPropertyChanged(nameof(AddressLineTwo));
                }
            }
        }

        private string _City;
        public string City
        {
            get { return _City; }
            set
            {
                if (_City != value)
                {
                    _City = value;
                    onPropertyChanged(nameof(City));
                }
            }
        }

        private string _State;
        public string State
        {
            get { return _State; }
            set
            {
                if (_State != value)
                {
                    _State = value;
                    onPropertyChanged(nameof(State));
                }
            }
        }

        private int _ZipCode;
        public int ZipCode
        {
            get { return _ZipCode; }
            set
            {
                if(_ZipCode != value)
                {
                    _ZipCode = value;
                    onPropertyChanged(nameof(ZipCode));
                }
            }
        }

        private string _PickerSelectedItem;
        public string PickerSelectedItem
        { 
            get { return _PickerSelectedItem; }
            set
            {
                if(_PickerSelectedItem != value)
                {
                    _PickerSelectedItem = value;
                    onPropertyChanged(nameof(PickerSelectedItem));
                }
            }
        }

        private ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set
            {
                if(_SubmitCommand != value)
                {
                    _SubmitCommand = value;
                    onPropertyChanged(nameof(SubmitCommand));
                }
            }
        }

        public UserInfoPageViewModel(Page owner, bool isNew, string email = null)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            this.email = email;
            page = owner;
            IsNew = isNew;

            SubmitCommand = new Command(SubmitAction);
            InitializeValues();
        }

        void InitializeValues()
        {
            CustomerUser cu = dataClass.CustomerUser;
            FirstName = cu.first_name;
            LastName = cu.last_name;
            EmergencyNumber = cu.emergency_phone_number;
            PhoneNumber = cu.phone_number;
            AddressLineOne = cu.address_line_one;
            AddressLineTwo = cu.address_line_two;
            City = cu.city;
            State = cu.state;
            ZipCode = cu.zip_code;
        }

        async void SubmitAction()
        {
            if(string.IsNullOrEmpty(FirstName) || string.IsNullOrEmpty(LastName) || string.IsNullOrEmpty(EmergencyNumber) || string.IsNullOrEmpty(PhoneNumber) ||
                string.IsNullOrEmpty(AddressLineOne) || string.IsNullOrEmpty(City) || string.IsNullOrEmpty(State) || string.IsNullOrEmpty(ZipCode.ToString()))
            {
                System.Diagnostics.Debug.WriteLine("null");
            }
            else
            {
                await Task.Run(async () =>
                {
                    PopupNavigation.PushAsync(new LoadingPopupPage());
                    //var tempPhoneNumber = Regex.Replace(PhoneNumber, "[^0-9]", "");
                    //var tempEmergencyNumber = Regex.Replace(EmergencyNumber, "[^0-9]", "");
                    string url = Constants.ROOT_URL + Constants.CUSTOMERDETAILS_URL;
                    await restService.PostRequestAsync(url, new { customer = new { email = email, first_name = FirstName, last_name = LastName, phone_number = PhoneNumber, emergency_phone_number = EmergencyNumber
                            , address_line_one = AddressLineOne, address_line_two = AddressLineTwo, city = City, state = State, zip_code = ZipCode, how_did_you_hear_about_us = PickerSelectedItem } }, cts.Token);
                });
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                if ((int.Parse(jsonData["status"].ToString())) == 200)
                {
                    if (IsNew)
                    {
                        //System.Diagnostics.Debug.WriteLine(jsonData);
                        var registeredUser = JsonConvert.DeserializeObject<CustomerUser>(jsonData["customer"].ToString());
                        registeredUser.has_basic_information = Boolean.Parse(jsonData["has_basic_information"].ToString());
                        registeredUser.confirmed_information = Boolean.Parse(jsonData["confirmed_information"].ToString());
                        dataClass.CustomerUser = registeredUser;
                        await PopupNavigation.PopAllAsync();
                        App.Current.MainPage = new LandingPage();
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await page.DisplayAlert(title, error, "Okay");
                await PopupNavigation.PopAllAsync();
            });
        }
    }
}
