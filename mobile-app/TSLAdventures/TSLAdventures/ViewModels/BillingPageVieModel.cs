﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class BillingPageVieModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        CancellationTokenSource cts;
        Page page;
        int reqType = 0;

        private ObservableCollection<Invoices> _InvoiceSource;
        public ObservableCollection<Invoices> InvoiceSource
        {
            get { return _InvoiceSource; }
            set
            {
                if(_InvoiceSource != value)
                {
                    _InvoiceSource = value;
                    onPropertyChanged(nameof(InvoiceSource));
                }
            }
        }

        private ObservableCollection<Receipts> _ReceiptSource;
        public ObservableCollection<Receipts> ReceiptSource
        {
            get { return _ReceiptSource; }
            set
            {
                if(_ReceiptSource != value)
                {
                    _ReceiptSource = value;
                    onPropertyChanged(nameof(ReceiptSource));
                }
            }
        }

        private bool _HasReceiptRecords;
        public bool HasReceiptRecords
        {
            get { return _HasReceiptRecords; }
            set
            {
                if(_HasReceiptRecords != value)
                {
                    _HasReceiptRecords = value;
                    onPropertyChanged(nameof(HasReceiptRecords));
                }
            }
        }

        private bool _HasInvoiceRecords;
        public bool HasInvoiceRecords
        {
            get { return _HasInvoiceRecords; }
            set
            {
                if (_HasInvoiceRecords != value)
                {
                    _HasInvoiceRecords = value;
                    onPropertyChanged(nameof(HasInvoiceRecords));
                }
            }
        }

        public BillingPageVieModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            HasInvoiceRecords = true;
            HasReceiptRecords = true;
            Init();
        }

        async void Init()
        {
            await Task.Run(async () =>
            {
                reqType = 1;
                await restService.GetRequest(Constants.ROOT_URL + Constants.GET_INVOICES, cts.Token);
            });
        }

        public async void GetReceipts()
        {
            await Task.Run(async () =>
            {
                reqType = 2;
                await restService.GetRequest(Constants.ROOT_URL + Constants.RECEIPTS, cts.Token);
            });
        }

        public void InvoiceItemSelectedAction(Invoices i)
        {
            page.Navigation.PushAsync(new ViewInvoicePage(i));
        }

        public void ReceiptItemSelectedAction(Receipts r)
        {
            page.Navigation.PushAsync(new ViewReceiptPage(r));
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            Device.BeginInvokeOnMainThread(async () =>
            {
                switch (reqType)
                {
                    case 1:
                        InvoiceSource = JsonConvert.DeserializeObject<ObservableCollection<Invoices>>(jsonData["invoice"].ToString());
                        break;
                    case 2:
                        ReceiptSource = JsonConvert.DeserializeObject<ObservableCollection<Receipts>>(jsonData["receipts"].ToString());
                        break;
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {

        }
    }

    public class Invoices
    {
        public int id { get; set; }
        public int registration_record_id { get; set; }
        public int amount { get; set; }
        public bool paid { get; set; }
        public string memo { get; set; }
        public string due_date { get; set; }

        public string invoice
        {
            get { return $"Invoice {id} / {memo}"; }
        }
    }

    public class Receipts
    {
        public int id { get; set; }
        public int registration_record_id { get; set; }
        public int customer_id { get; set; }
        public string amount { get; set; }
        public string program_name { get; set; }
        public string payment_for { get; set; }
        public DateTime created_at { get; set; }

        public string date_issued
        {
            get { return created_at.ToString("MMMM dd, yyyy"); }
        }
        public string to_display
        {
            get { return $"{program_name} / {payment_for}"; }
        }
    }

    //"id": 8249,
    //"registration_record_id": 7728,
    //"invoice_id": null,
    //"customer_id": 3144,
    //"amount": "150.0",
    //"program_name": "Vacation Camp",
    //"payment_for": "Vacation Camp Child Care Service",
    //"created_at": "2019-08-01T07:43:10.535Z",
    //"updated_at": "2019-08-01T07:43:10.535Z",
    //"purchase_id": null
}
