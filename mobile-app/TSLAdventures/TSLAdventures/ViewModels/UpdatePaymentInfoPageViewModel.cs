﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class UpdatePaymentInfoPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        Page page;
        int reqType = 0;

        private ICommand _AddCardCommand;
        public ICommand AddCardCommand
        {
            get { return _AddCardCommand; }
            set
            {
                if (_AddCardCommand != value)
                {
                    _AddCardCommand = value;
                    onPropertyChanged(nameof(AddCardCommand));
                }
            }
        }

        private ICommand _SubmitBankDetailsCommand;
        public ICommand SubmitBankDetailsCommand
        {
            get { return _SubmitBankDetailsCommand; }
            set
            {
                if (_SubmitBankDetailsCommand != value)
                {
                    _SubmitBankDetailsCommand = value;
                    onPropertyChanged(nameof(SubmitBankDetailsCommand));
                }
            }
        }

        private ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set
            {
                if (_SubmitCommand != value)
                {
                    _SubmitCommand = value;
                    onPropertyChanged(nameof(SubmitCommand));
                }
            }
        }

        private ICommand _UnlinkCommand;
        public ICommand UnlinkCommand
        {
            get { return _UnlinkCommand; }
            set
            {
                if (_UnlinkCommand != value)
                {
                    _UnlinkCommand = value;
                    onPropertyChanged(nameof(UnlinkCommand));
                }
            }
        }

        private ICommand _DeleteFirstEmailCommand;
        public ICommand DeleteFirstEmailCommand
        {
            get { return _DeleteFirstEmailCommand; }
            set
            {
                if (_DeleteFirstEmailCommand != value)
                {
                    _DeleteFirstEmailCommand = value;
                    onPropertyChanged(nameof(DeleteFirstEmailCommand));
                }
            }
        }

        private ICommand _DeleteSecondEmailCommand;
        public ICommand DeleteSecondEmailCommand
        {
            get { return _DeleteSecondEmailCommand; }
            set
            {
                if (_DeleteSecondEmailCommand != value)
                {
                    _DeleteSecondEmailCommand = value;
                    onPropertyChanged(nameof(DeleteSecondEmailCommand));
                }
            }
        }

        private ICommand _DeleteThirdEmailCommand;
        public ICommand DeleteThirdEmailCommand
        {
            get { return _DeleteThirdEmailCommand; }
            set
            {
                if (_DeleteThirdEmailCommand != value)
                {
                    _DeleteThirdEmailCommand = value;
                    onPropertyChanged(nameof(DeleteThirdEmailCommand));
                }
            }
        }

        //ICommand _AutoPayToggledCommand;
        //public ICommand AutoPayToggledCommand
        //{
        //    get { return _AutoPayToggledCommand; }
        //    set { _AutoPayToggledCommand = value; onPropertyChanged(nameof(AutoPayToggledCommand)); }
        //}

        private bool _NullIndicator;
        public bool NullIndicator
        {
            get { return _NullIndicator; }
            set
            {
                if (_NullIndicator != value)
                {
                    _NullIndicator = value;
                    onPropertyChanged(nameof(NullIndicator));
                }
            }
        }

        private string _FirstEmailText;
        public string FirstEmailText
        {
            get { return _FirstEmailText; }
            set
            {
                if (_FirstEmailText != value)
                {
                    _FirstEmailText = value;
                    onPropertyChanged(nameof(FirstEmailText));
                }
            }
        }

        private string _SecondEmailText;
        public string SecondEmailText
        {
            get { return _SecondEmailText; }
            set
            {
                if (_SecondEmailText != value)
                {
                    _SecondEmailText = value;
                    onPropertyChanged(nameof(SecondEmailText));
                }
            }
        }

        private string _ThirdEmailText;
        public string ThirdEmailText
        {
            get { return _ThirdEmailText; }
            set
            {
                if (_ThirdEmailText != value)
                {
                    _ThirdEmailText = value;
                    onPropertyChanged(nameof(ThirdEmailText));
                }
            }
        }

        bool _AutoPayEnabled;
        public bool AutoPayEnabled
        {
            get { return _AutoPayEnabled; }
            set { _AutoPayEnabled = value; onPropertyChanged(nameof(AutoPayEnabled)); }
        }

        ContactInformations _Contact1;
        public ContactInformations Contact1
        {
            get { return _Contact1; }
            set { _Contact1 = value; onPropertyChanged(nameof(Contact1)); }
        }

        ContactInformations _Contact2;
        public ContactInformations Contact2
        {
            get { return _Contact2; }
            set { _Contact2 = value; onPropertyChanged(nameof(Contact2)); }
        }

        ContactInformations _Contact3;
        public ContactInformations Contact3
        {
            get { return _Contact3; }
            set { _Contact3 = value; onPropertyChanged(nameof(Contact3)); }
        }

        bool _Contact1Visibility;
        public bool Contact1Visibility
        {
            get { return _Contact1Visibility; }
            set { _Contact1Visibility = value; onPropertyChanged(nameof(Contact1Visibility)); }
        }

        bool _Contact2Visibility;
        public bool Contact2Visibility
        {
            get { return _Contact2Visibility; }
            set { _Contact2Visibility = value; onPropertyChanged(nameof(Contact2Visibility)); }
        }

        bool _Contact3Visibility;
        public bool Contact3Visibility
        {
            get { return _Contact3Visibility; }
            set { _Contact3Visibility = value; onPropertyChanged(nameof(Contact3Visibility)); }
        }

        public UpdatePaymentInfoPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            //EmailValues();

            NullIndicator = true;
            if (!string.IsNullOrEmpty(dataClass.CustomerUser.card_brand) && !string.IsNullOrEmpty(dataClass.CustomerUser.card_last_four))
            {
                NullIndicator = false;
            }

            Contact1 = new ContactInformations();
            Contact2 = new ContactInformations();
            Contact3 = new ContactInformations();
            Contact1Visibility = true;
            Contact2Visibility = true;
            Contact3Visibility = true;

            AutoPayEnabled = dataClass.CustomerUser.autopay_enabled;
            UnlinkCommand = new Command(UnlinkAction);
            SubmitCommand = new Command(SubmitAction);
            AddCardCommand = new Command(AddCardAction);
            SubmitBankDetailsCommand = new Command(SubmitBankDetailsAction);
            DeleteFirstEmailCommand = new Command(DeleteFirstEmailAction);
            DeleteSecondEmailCommand = new Command(DeleteSecondEmailAction);
            DeleteThirdEmailCommand = new Command(DeleteThirdEmailAction);
            //AutoPayToggledCommand = new Command(AutoPayToggledAction);
            Init();
        }

        public async void Init()
        {
            await Task.Run(async () =>
            {
                reqType = 5;
                await restService.GetRequest(Constants.ROOT_URL + Constants.GET_CONTACT_INFORMATIONS, cts.Token);
            });
        }

        public async void AutoPayToggledAction()
        {
            await Task.Run(async () =>
            {
                var json = new
                {
                    customer_user = new
                    {
                        auto_pay_user_id = dataClass.CustomerUser.customer_id,
                        autopay_enabled = AutoPayEnabled
                    }
                };
                reqType = 3;
                await restService.PutRequestAsync(Constants.ROOT_URL + Constants.UPDATE_AUTO_PAY, json, cts.Token);
            });
        }

        private async void UnlinkAction(object obj)
        {
            bool unlinkCard = await page.DisplayAlert("Unlink Card", "Do you really want to unlink this card?", "Yes", "No");
            if (unlinkCard)
            {
                await Task.Run(async () =>
                {
                    reqType = 2;
                    string url = string.Format("{0}{1}", Constants.ROOT_URL, Constants.UNLINK_CARD);
                    System.Diagnostics.Debug.WriteLine(url);
                    await restService.PutRequestAsync(url, new { }, cts.Token);
                });
            }
        }

        //get value from server
        void EmailValues()
        {
            FirstEmailText = Contact1.email;
            SecondEmailText = Contact2.email;
            ThirdEmailText = Contact3.email;
        }

        void AddCardAction()
        {
            PopupNavigation.PushAsync(new CardInfoPopupPage(CreateCardAction));
        }

        async void CreateCardAction(string token)
        {
            await Task.Run(async () =>
            {
                reqType = 1;
                var json = new { stripeToken = token, stripeEmail = dataClass.CustomerUser.email };
                await restService.PostRequestAsync(string.Format("{0}{1}", Constants.ROOT_URL, Constants.ADD_CARD), json, cts.Token);
            });

            System.Diagnostics.Debug.WriteLine(token);
        }

        void SubmitBankDetailsAction()
        {

        }

        async void SubmitAction()
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++");
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(Contact1));
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(Contact2));
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(Contact3));
            System.Diagnostics.Debug.WriteLine("+++++++++++++");
            ObservableCollection<ContactInformations> list = new ObservableCollection<ContactInformations>();
            if (!string.IsNullOrEmpty(Contact1.email))
                list.Add(Contact1);
            if (!string.IsNullOrEmpty(Contact2.email))
                list.Add(Contact2);
            if (!string.IsNullOrEmpty(Contact3.email))
                list.Add(Contact3);

            await Task.Run(async () =>
            {
                var json = new { customer = new { contact_informations_attributes = list } };
                reqType = 6;
                await restService.PutRequestAsync(Constants.ROOT_URL + Constants.UPDATE_CONTACT_INFORMATIONS + dataClass.CustomerUser.customer_id, json, cts.Token);
            });
        }

        async void DeleteFirstEmailAction()
        {
            if(!Contact1.id.Equals(0) && !string.IsNullOrEmpty(Contact1.email))
            {
                var c = await page.DisplayAlert("Delete Contact", "Do you really want to delete this contact?", "Yes", "No");
                if (c)
                {
                    Contact1._destroy = true;
                    var json = new { customer = new { contact_informations_attributes = new ObservableCollection<ContactInformations> { Contact1 } } };
                    reqType = 6;
                    await restService.PutRequestAsync(Constants.ROOT_URL + Constants.UPDATE_CONTACT_INFORMATIONS + dataClass.CustomerUser.customer_id, json, cts.Token);

                }
            }
        }

        async void DeleteSecondEmailAction()
        {
            if (!Contact2.id.Equals(0) && !string.IsNullOrEmpty(Contact2.email))
            {
                var c = await page.DisplayAlert("Delete Contact", "Do you really want to delete this contact?", "Yes", "No");
                if (c)
                {
                    Contact2._destroy = true;
                    var json = new { customer = new { contact_informations_attributes = new ObservableCollection<ContactInformations> { Contact2 } } };
                    reqType = 6;
                    await restService.PutRequestAsync(Constants.ROOT_URL + Constants.UPDATE_CONTACT_INFORMATIONS + dataClass.CustomerUser.customer_id, json, cts.Token);

                }
            }
        }

        async void DeleteThirdEmailAction()
        {
            if (!Contact3.id.Equals(0) && !string.IsNullOrEmpty(Contact3.email))
            {
                var c = await page.DisplayAlert("Delete Contact", "Do you really want to delete this contact?", "Yes", "No");
                if (c)
                {
                    Contact3._destroy = true;
                    var json = new { customer = new { contact_informations_attributes = new ObservableCollection<ContactInformations> { Contact3 } } };
                    reqType = 6;
                    await restService.PutRequestAsync(Constants.ROOT_URL + Constants.UPDATE_CONTACT_INFORMATIONS + dataClass.CustomerUser.customer_id, json, cts.Token);

                }
            }
        }

        void UpdateEmails(ObservableCollection<ContactInformations> list)
        {
            if (list.Any())
            {
                if (list.Count.Equals(1))
                {
                    Contact1 = list[0];
                    Contact2 = new ContactInformations();
                    Contact3 = new ContactInformations();
                }

                if (list.Count.Equals(2))
                {
                    Contact1 = list[0];
                    Contact2 = list[1];
                    Contact3 = new ContactInformations();
                }

                if (list.Count.Equals(3))
                {
                    Contact1 = list[0];
                    Contact2 = list[1];
                    Contact3 = list[2];
                }

            }
            else
            {
                Contact1 = new ContactInformations();
                Contact2 = new ContactInformations();
                Contact3 = new ContactInformations();
            }
        }
        async void UpdateCustomerUser(JObject jsonData, int reqType = 0)
        {
            var newCustomer = JsonConvert.DeserializeObject<CustomerUser>(jsonData["customer_user"].ToString());
            newCustomer.access_token = dataClass.CustomerUser.access_token;
            newCustomer.has_basic_information = dataClass.CustomerUser.has_basic_information;
            newCustomer.confirmed_information = dataClass.CustomerUser.confirmed_information;
            dataClass.CustomerUser = newCustomer;
            if (reqType.Equals(3))
                await page.DisplayAlert($"Autopay {(newCustomer.autopay_enabled ? "Enabled" : "Disabled")}", jsonData["message"].ToString(), "Okay");
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine("+++++++++++++");
            System.Diagnostics.Debug.WriteLine(jsonData);
            System.Diagnostics.Debug.WriteLine("+++++++++++++");

            Device.BeginInvokeOnMainThread(async () =>
            {
                switch (reqType)
                {
                    case 1:
                        UpdateCustomerUser(jsonData);
                        PopupNavigation.PopAsync();
                        await page.DisplayAlert("Card Created", jsonData["message"].ToString(), "Okay");
                        break;
                    case 2:
                        UpdateCustomerUser(jsonData);
                        await page.DisplayAlert("Card Unlinked", jsonData["message"].ToString(), "Okay");
                        break;
                    case 3:
                        UpdateCustomerUser(jsonData, 3);
                        break;
                    case 5:
                        ObservableCollection<ContactInformations> list = JsonConvert.DeserializeObject<ObservableCollection<ContactInformations>>(jsonData["contact_informations"].ToString());
                        UpdateEmails(list);
                        break;
                    case 6:
                        ObservableCollection<ContactInformations> list2 = JsonConvert.DeserializeObject<ObservableCollection<ContactInformations>>(jsonData["contact_informations"].ToString());
                        UpdateEmails(list2);
                        await page.DisplayAlert("", "Contact Information Updated", "Okay");
                        break;
                }


                reqType = 0;
                if (!string.IsNullOrEmpty(dataClass.CustomerUser.card_brand) && !string.IsNullOrEmpty(dataClass.CustomerUser.card_last_four))
                {
                    NullIndicator = false;
                }
                else
                {
                    NullIndicator = true;
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            System.Diagnostics.Debug.WriteLine("---------------");
            System.Diagnostics.Debug.WriteLine(error);
            System.Diagnostics.Debug.WriteLine("---------------");
        }
    }
    public class ContactInformations
    {
        int? _id = null;
        public int? id
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        string _email = "";
        public string email
        {
            get { return _email; }
            set
            {
                _email = value;
            }
        }

        bool __destroy = false;
        public bool _destroy
        {
            get { return __destroy; }
            set { __destroy = value; }
        }
    }
}
