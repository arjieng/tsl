﻿using System;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class AccountPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        Page page;
        int reqType = 0;

        private ObservableCollection<RegistrationInfo> _RegistrationInfoSource;
        public ObservableCollection<RegistrationInfo> RegistrationInfoSource
        {
            get { return _RegistrationInfoSource; }
            set
            {
                if(_RegistrationInfoSource != value)
                {
                    _RegistrationInfoSource = value;
                    onPropertyChanged(nameof(RegistrationInfoSource));
                }
            }
        }

        private string _EmailText;
        public string EmailText
        {
            get { return _EmailText; }
            set
            {
                if(_EmailText != value)
                {
                    _EmailText = value;
                    onPropertyChanged(nameof(EmailText));
                }
            }
        }

        private string _PasswordText;
        public string PasswordText
        {
            get { return _PasswordText; }
            set
            {
                if(_PasswordText != value)
                {
                    _PasswordText = value;
                    onPropertyChanged(nameof(PasswordText));
                }
            }
        }

        private string _ConfirmPasswordText;
        public string ConfirmPasswordText
        {
            get { return _ConfirmPasswordText; }
            set
            {
                if(_ConfirmPasswordText != value)
                {
                    _ConfirmPasswordText = value;
                    onPropertyChanged(nameof(ConfirmPasswordText));
                }
            }
        }

        private bool _IsPasswordLabelVisible;
        public bool IsPasswordLabelVisible
        {
            get { return _IsPasswordLabelVisible; }
            set
            {
                if(_IsPasswordLabelVisible != value)
                {
                    _IsPasswordLabelVisible = value;
                    onPropertyChanged(nameof(IsPasswordLabelVisible));                
                }
            }
        }


        private ICommand _UpdateCommand;
        public ICommand UpdateCommand
        {
            get { return _UpdateCommand; }
            set
            {
                if(_UpdateCommand != value)
                {
                    _UpdateCommand = value;
                    onPropertyChanged(nameof(UpdateCommand));
                }
            }
        }

        public AccountPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            SetValues();
            IsPasswordLabelVisible = false;
            UpdateCommand = new Command(UpdateAction);
        }

        async void SetValues() //THIS SHOULD BE ON RECEIVEJSONDATA 
        {
            RegistrationInfoSource = new ObservableCollection<RegistrationInfo>();
            //RegistrationInfoSource.Add(new RegistrationInfo { date = DateTime.Now, program = "Guilderland - School House Road Day Care Tour", is_active = true });
            //RegistrationInfoSource.Add(new RegistrationInfo { date = DateTime.Now, program = "Guilderland - School House Road Day Care Tour", is_active = false });
            //RegistrationInfoSource.Add(new RegistrationInfo { date = DateTime.Now, program = "Guilderland - School House Road Day Care Tour", is_active = true });

            EmailText = dataClass.CustomerUser.email; // "hello@gmail.com";
            PasswordText = "";
            ConfirmPasswordText = "";

            await Task.Run(async () =>
            {
                reqType = 2;
                string url = Constants.ROOT_URL + Constants.REGISTRATION_INFORMATION;
                await restService.GetRequest(url, cts.Token);
            });
        }

        async void UpdateAction()
        {
            bool infoIsComplete = true;

            if (string.IsNullOrEmpty(PasswordText) && string.IsNullOrEmpty(ConfirmPasswordText))
            {
                page.DisplayAlert("Empty fields", "Please fill the empty fields", "Okay");
                infoIsComplete = false;
            }
            else
            {
                if (!PasswordText.Equals(ConfirmPasswordText))
                {
                    IsPasswordLabelVisible = true;
                    infoIsComplete = false;
                }
            }

            if (infoIsComplete)
            {
                await Task.Run(async () =>
                {
                    reqType = 1;
                    string url = Constants.ROOT_URL + Constants.RESET_PASSWORD;

                    await restService.PutRequestAsync(url, new { customer_user = new { password = PasswordText, password_confirmation = ConfirmPasswordText }, access_token = dataClass.CustomerUser.access_token }, cts.Token);
                });
            }
            //if (string.IsNullOrEmpty(PasswordText) && string.IsNullOrEmpty(ConfirmPasswordText))
            //{

            //}
            //else
            //{
            //    if (PasswordText.Equals(ConfirmPasswordText))
            //    {
            //        IsPasswordLabelVisible = false;
            //    }
            //    else
            //    {
            //        IsPasswordLabelVisible = true;
            //    }
            //}
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                System.Diagnostics.Debug.WriteLine("++++++++++++++");
                System.Diagnostics.Debug.WriteLine(jsonData);
                System.Diagnostics.Debug.WriteLine("++++++++++++++");
                if (reqType.Equals(2))
                {
                    var another_list = JsonConvert.DeserializeObject<ObservableCollection<RegistrationInformation>>(jsonData["registration_information"].ToString());
                    foreach(var c in another_list)
                    {
                        RegistrationInfoSource.Add(new RegistrationInfo { id = c.id, date = c.registration_date, program = c.name, is_active = c.enrollment_active.Equals("Yes") ? true : false });
                    }
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                var obj = JObject.Parse(error);
                page.DisplayAlert((obj["message"].ToString().Equals("Failed to update password!") ? "Update failed" : "Upddate successful"), obj["message"].ToString(), "Okay");
            });
        }
    }

    public class RegistrationInformation
    {
        public int id { get; set; }
        public string name { get; set; }
        public DateTime registration_date { get; set; }
        public string enrollment_active { get; set; }
    }
}
