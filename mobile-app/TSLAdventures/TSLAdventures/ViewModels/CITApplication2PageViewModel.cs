﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class CITApplication2PageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        CancellationTokenSource cts;
        Page page;

        ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set { _SubmitCommand = value; onPropertyChanged(nameof(SubmitCommand)); }
        }

        private RegistreeInfo _RegistreeInformations;
        public RegistreeInfo RegistreeInformations
        {
            get { return _RegistreeInformations; }
            set { _RegistreeInformations = value;  onPropertyChanged(nameof(RegistreeInformations)); }
        }

        private Questionaires _Questions;
        public Questionaires Questions
        {
            get { return _Questions; }
            set { _Questions = value; onPropertyChanged(nameof(Questions)); }
        }

        public CITApplication2PageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            SubmitCommand = new Command(SubmitAction);
            Questions = new Questionaires();
            RegistreeInformations = new RegistreeInfo();

        }

        private bool _HasAgreed;
        public bool HasAgreed
        {
            get { return _HasAgreed; }
            set
            {
                _HasAgreed = value;
                onPropertyChanged(nameof(HasAgreed));
            }
        }

        private async void SubmitAction(object obj)
        {
            System.Diagnostics.Debug.WriteLine("==========================");
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(RegistreeInformations));
            System.Diagnostics.Debug.WriteLine(JsonConvert.SerializeObject(Questions));
            System.Diagnostics.Debug.WriteLine("==========================");
            bool datasComplete = true;
            if (Questions.Question1SelectedIndex.Equals(0))
            {
                if (string.IsNullOrEmpty(Questions.Question1YesSelectedItem))
                {
                    datasComplete = false;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        page.DisplayAlert("Data incomplete", "Please select location for the first question", "Okay");
                    });
                }
            }

            if (Questions.Question2SelectedIndex.Equals(0))
            {
                if (string.IsNullOrEmpty(Questions.Question2YesSelectedItem))
                {
                    datasComplete = false;
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        page.DisplayAlert("Data incomplete", "Please select location for the second question", "Okay");
                    });
                }
            }


            if (!HasAgreed)
            {
                datasComplete = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "You must accept terms of service before registering.", "Okay");
                });
            }

            if (datasComplete)
            {
                if (!string.IsNullOrEmpty(Questions.Question1Text) || !string.IsNullOrEmpty(Questions.Question2Text) || !string.IsNullOrEmpty(Questions.Question3Text) ||
                !string.IsNullOrEmpty(Questions.Question4Text) || !string.IsNullOrEmpty(Questions.Question5Text) || !string.IsNullOrEmpty(Questions.Question6Text) ||
                !string.IsNullOrEmpty(Questions.Question7Text) || !string.IsNullOrEmpty(Questions.Question8Text) || !string.IsNullOrEmpty(Questions.Question9Text) ||
                !string.IsNullOrEmpty(Questions.Question10Text) || !string.IsNullOrEmpty(Questions.Question11Text) || !string.IsNullOrEmpty(Questions.Question12Text))
                {
                    await Task.Run(async () =>
                    {
                        var json = new
                        {
                            cit_record = new
                            {
                                location = RegistreeInformations.LocationPickerSelectedItem,
                                first_name = RegistreeInformations.FirstNameTextSource,
                                last_name = RegistreeInformations.LastNameTextSource,
                                dob = RegistreeInformations.BirthdayTextSource,
                                phone_number = RegistreeInformations.PhoneNumberTextSource,
                                email = RegistreeInformations.EmailAddressTextSource,
                                address_line_one = RegistreeInformations.AddressOneTextSource,
                                address_line_two = RegistreeInformations.AddressTwoTextSource,
                                city = RegistreeInformations.CityTextSource,
                                state = RegistreeInformations.StateTextSource,
                                zip_code = RegistreeInformations.ZipCodeTextSource,
                                how_did_you_hear_about_us = RegistreeInformations.PickerSelectedItem,

                                attended_tsl_before = Questions.Question1SelectedIndex.Equals(0) ? "true" : "false",
                                attended_location = Questions.Question1YesSelectedItem,
                                cit_at_tsl_before = Questions.Question2SelectedIndex.Equals(0) ? "true" : "false",
                                cit_at_tsl_location = Questions.Question2YesSelectedItem,
                                previous_experience = Questions.Question1Text,
                                why_are_you_interested = Questions.Question2Text,
                                willing_to_follow_directions = Questions.Question3SelectedIndex.Equals(0) ? "true" : "false",
                                action_when_completed_tasks = Questions.Question3Text,
                                water_play_action = Questions.Question4Text,
                                four_square_action = Questions.Question5Text,
                                arguing_action = Questions.Question6Text,
                                transition_help = Questions.Question7Text,
                                uninteresting_activity_action = Questions.Question8Text,
                                interests_and_talents = Questions.Question9Text,
                                proven_responsibility = Questions.Question10Text,
                                made_a_difference = Questions.Question11Text,
                                what_you_will_bring = Questions.Question12Text,
                                willing_to_meet_vision = Questions.Question4SelectedIndex.Equals(0) ? "true" : "false",
                                agree_to_behavior_policy = HasAgreed ? "1" : "0"
                            },
                            commit = "Submit"
                        };

                        await PopupNavigation.PushAsync(new LoadingPopupPage(), false);
                        await restService.PostRequestAsync(Constants.ROOT_URL + Constants.CIT_RECORDS, json, cts.Token);
                    });
                }
                else
                {
                    Device.BeginInvokeOnMainThread(() =>
                    {
                        page.DisplayAlert("Data incomplete", "Please fill up all questions.", "Okay");
                    });
                }
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await PopupNavigation.PopAllAsync();
                await page.DisplayAlert("Success", "Registration Complete", "Okay");
                await page.Navigation.PopToRootAsync();
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {

        }
    }

    public class Questionaires
    {
        public int Question1SelectedIndex { get; set; }
        public int Question2SelectedIndex { get; set; }
        public int Question3SelectedIndex { get; set; }
        public int Question4SelectedIndex { get; set; }

        public string Question1YesSelectedItem { get; set; }
        public string Question2YesSelectedItem { get; set; }

        public string Question1Text { get; set; }
        public string Question2Text { get; set; }
        public string Question3Text { get; set; }
        public string Question4Text { get; set; }
        public string Question5Text { get; set; }
        public string Question6Text { get; set; }
        public string Question7Text { get; set; }
        public string Question8Text { get; set; }
        public string Question9Text { get; set; }
        public string Question10Text { get; set; }
        public string Question11Text { get; set; }
        public string Question12Text { get; set; }

    }

    public class RegistreeInfo
    {
        public string LocationPickerSelectedItem = "", FirstNameTextSource = "", LastNameTextSource = "", BirthdayTextSource = "", PhoneNumberTextSource = "", EmailAddressTextSource = "",
            AddressOneTextSource = "", AddressTwoTextSource = "", CityTextSource = "", StateTextSource = "", ZipCodeTextSource = "", PickerSelectedItem = "";
    }
}
