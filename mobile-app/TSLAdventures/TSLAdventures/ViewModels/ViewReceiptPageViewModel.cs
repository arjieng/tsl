﻿using System;
using System.Threading;
using Newtonsoft.Json.Linq;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class ViewReceiptPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts; Page page;
        int reqType = 0;

        public ViewReceiptPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            throw new NotImplementedException();
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            throw new NotImplementedException();
        }
    }
}
