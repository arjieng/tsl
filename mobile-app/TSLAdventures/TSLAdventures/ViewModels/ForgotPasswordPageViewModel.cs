﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class ForgotPasswordPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        CancellationTokenSource cts;
        Page page;

        private string _EmailTextSource;
        public string EmailTextSource
        {
            get { return _EmailTextSource; }
            set
            {
                if(_EmailTextSource != value)
                {
                    _EmailTextSource = value;
                    onPropertyChanged(nameof(EmailTextSource));
                }
            }
        }

        private ICommand _ResetPasswordCommand;
        public ICommand ResetPasswordCommand
        {
            get { return _ResetPasswordCommand; }
            set
            {
                if(_ResetPasswordCommand != value)
                {
                    _ResetPasswordCommand = value;
                    onPropertyChanged(nameof(ResetPasswordCommand));
                }
            }
        }

        public ForgotPasswordPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            ResetPasswordCommand = new Command(ResetPasswordAction);
            EmailTextSource = "";
        }

        async void ResetPasswordAction()
        {
            await Task.Run(async () =>
            {
                if (!string.IsNullOrEmpty(EmailTextSource))
                {
                    PopupNavigation.PushAsync(new LoadingPopupPage());
                    string url = Constants.ROOT_URL + Constants.FORGOTPASSWORD_URL;
                    await restService.PostRequestAsync(url, new { email = EmailTextSource }, cts.Token);
                }
            });
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                System.Diagnostics.Debug.WriteLine(jsonData);
                if ((int.Parse(jsonData["status"].ToString())) == 200)
                {
                    await PopupNavigation.PopAllAsync();
                    await page.DisplayAlert("Success", "You'll receive an email from TSL Adventures with a link to reset password. Please check your email.", "Okay");
                }
            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await PopupNavigation.PopAllAsync();
                await page.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
