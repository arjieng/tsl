﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class DayCareTourPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        Page page;
        int reqType = 0;

        private List<string> _LocationSource;
        public List<string> LocationSource
        {
            get { return _LocationSource; }
            set
            {
                if(_LocationSource != value)
                {
                    _LocationSource = value;
                    onPropertyChanged(nameof(LocationSource));
                }
            }
        }

        private List<string> _LocationAddress;
        public List<string> LocationAddress
        {
            get { return _LocationAddress; }
            set
            {
                if(_LocationAddress != value)
                {
                    _LocationAddress = value;
                    onPropertyChanged(nameof(LocationAddress));
                }
            }
        }

        private string _SelectedLocationAddress;
        public string SelectedLocationAddress
        {
            get { return _SelectedLocationAddress; }
            set
            {
                if (_SelectedLocationAddress != value)
                {
                    _SelectedLocationAddress = value;
                    onPropertyChanged(nameof(SelectedLocationAddress));
                }
            }
        }

        private int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                if (_SelectedIndex != value)
                {
                    _SelectedIndex = value;
                    onPropertyChanged(nameof(SelectedIndex));
                }
            }
        }

        private ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set
            {
                if (_SubmitCommand != value)
                {
                    _SubmitCommand = value;
                    onPropertyChanged(nameof(SubmitCommand));
                }
            }
        }

        private ICommand _GotoTOSCommand;
        public ICommand GotoTOSCommand
        {
            get { return _GotoTOSCommand; }
            set
            {
                if (_GotoTOSCommand != value)
                {
                    _GotoTOSCommand = value;
                    onPropertyChanged(nameof(GotoTOSCommand));
                }
            }
        }

        private ICommand _RemoveFirstChildCommand;
        public ICommand RemoveFirstChildCommand
        {
            get { return _RemoveFirstChildCommand; }
            set
            {
                if (_RemoveFirstChildCommand != value)
                {
                    _RemoveFirstChildCommand = value;
                    onPropertyChanged(nameof(RemoveFirstChildCommand));
                }
            }
        }

        private ICommand _RemoveSecondChildCommand;
        public ICommand RemoveSecondChildCommand
        {
            get { return _RemoveSecondChildCommand; }
            set
            {
                if (_RemoveSecondChildCommand != value)
                {
                    _RemoveSecondChildCommand = value;
                    onPropertyChanged(nameof(RemoveSecondChildCommand));
                }
            }
        }

        private ICommand _RemoveThirdChildCommand;
        public ICommand RemoveThirdChildCommand
        {
            get { return _RemoveThirdChildCommand; }
            set
            {
                if (_RemoveThirdChildCommand != value)
                {
                    _RemoveThirdChildCommand = value;
                    onPropertyChanged(nameof(RemoveThirdChildCommand));
                }
            }
        }

        private ICommand _AddChildCommand;
        public ICommand AddChildCommand
        {
            get { return _AddChildCommand; }
            set
            {
                if (_AddChildCommand != value)
                {
                    _AddChildCommand = value;
                    onPropertyChanged(nameof(AddChildCommand));
                }
            }
        }

        private DayCareChildRecord _ChildOne;
        public DayCareChildRecord ChildOne
        {
            get { return _ChildOne; }
            set
            {
                if (_ChildOne != value)
                {
                    _ChildOne = value;
                    onPropertyChanged(nameof(ChildOne));
                }
            }
        }

        private DayCareChildRecord _ChildTwo;
        public DayCareChildRecord ChildTwo
        {
            get { return _ChildTwo; }
            set
            {
                if (_ChildTwo != value)
                {
                    _ChildTwo = value;
                    onPropertyChanged(nameof(ChildTwo));
                }
            }
        }

        private DayCareChildRecord _ChildThree;
        public DayCareChildRecord ChildThree
        {
            get { return _ChildThree; }
            set
            {
                if (_ChildThree != value)
                {
                    _ChildThree = value;
                    onPropertyChanged(nameof(ChildThree));
                }
            }
        }

        private string _SelectedItem;
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                if (_SelectedItem != value)
                {
                    _SelectedItem = value;
                    onPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        private bool _FirstChildVisibility;
        public bool FirstChildVisibility
        {
            get { return _FirstChildVisibility; }
            set
            {
                if (_FirstChildVisibility != value)
                {
                    _FirstChildVisibility = value;
                    onPropertyChanged(nameof(FirstChildVisibility));
                }
            }
        }

        private bool _SecondChildVisibility;
        public bool SecondChildVisibility
        {
            get { return _SecondChildVisibility; }
            set
            {
                if (_SecondChildVisibility != value)
                {
                    _SecondChildVisibility = value;
                    onPropertyChanged(nameof(SecondChildVisibility));
                }
            }
        }

        private bool _ThirdChildVisibility;
        public bool ThirdChildVisibility
        {
            get { return _ThirdChildVisibility; }
            set
            {
                if (_ThirdChildVisibility != value)
                {
                    _ThirdChildVisibility = value;
                    onPropertyChanged(nameof(ThirdChildVisibility));
                }
            }
        }

        private bool _HasAgreed;
        public bool HasAgreed
        {
            get { return _HasAgreed; }
            set
            {
                _HasAgreed = value;
                onPropertyChanged(nameof(HasAgreed));
            }
        }

        public DayCareTourPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            InitiateValues();
        }

        async void InitiateValues() // this should be on ReceiveJSONData because locations will be fetch from server
        {
            //LocationSource = new List<string>
            //{
            //    "Guilderland - School House Road",
            //    "Rotterdam",
            //    "Troy daycare"
            //};

            //LocationAddress = new List<string>
            //{
            //    "183 Schoolhouse Road Albany, NY 12203", 
            //    "2617 Hamburg Street", 
            //    "1st street"
            //};

            FirstChildVisibility = true;
            SecondChildVisibility = false;
            ThirdChildVisibility = false;
            ChildOne = new DayCareChildRecord();
            ChildTwo = new DayCareChildRecord();
            ChildThree = new DayCareChildRecord();

            AddChildCommand = new Command(AddChildCommandAction);
            SubmitCommand = new Command(SubmitCommandAction);
            GotoTOSCommand = new Command((x) => page.Navigation.PushAsync(new TOSPage(1)));
            RemoveFirstChildCommand = new Command((x) => FirstChildVisibility = false);
            RemoveSecondChildCommand = new Command((x) => SecondChildVisibility = false);
            RemoveThirdChildCommand = new Command((x) => ThirdChildVisibility = false);

            await Task.Run(async () =>
            {
                reqType = 1;
                string url = string.Format("{0}{1}?program_name=Day Care Tour", Constants.ROOT_URL, Constants.GET_LOCATIONS);
                System.Diagnostics.Debug.WriteLine(url);
                await restService.GetRequest(url, cts.Token);
            });
        }

        void AddChildCommandAction()
        {
            if (!FirstChildVisibility)
            {
                FirstChildVisibility = true;
            }
            else if (!SecondChildVisibility)
            {
                SecondChildVisibility = true;
            }
            else if (!ThirdChildVisibility)
            {
                ThirdChildVisibility = true;
            }
        }

        async void SubmitCommandAction()
        {
            //validations first
            List<DayCareChildRecord> child_list = new List<DayCareChildRecord>();

            bool datasCompleted = true, invokeChilMissingFields = false;
            if (FirstChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildOne.first_name) && !string.IsNullOrEmpty(ChildOne.last_name) && !string.IsNullOrEmpty(ChildOne.dob) && !string.IsNullOrEmpty(ChildOne.child_classification))
                {
                    child_list.Add(ChildOne);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (SecondChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildTwo.first_name) && !string.IsNullOrEmpty(ChildTwo.last_name) && !string.IsNullOrEmpty(ChildTwo.dob) && !string.IsNullOrEmpty(ChildTwo.child_classification))
                {
                    child_list.Add(ChildTwo);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (ThirdChildVisibility)
            {
                if (!string.IsNullOrEmpty(ChildThree.first_name) && !string.IsNullOrEmpty(ChildThree.last_name) && !string.IsNullOrEmpty(ChildThree.dob) && !string.IsNullOrEmpty(ChildThree.child_classification))
                {
                    child_list.Add(ChildThree);
                }
                else
                {
                    invokeChilMissingFields = true;
                    datasCompleted = false;
                }
            }

            if (invokeChilMissingFields)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "Please fill all fields in childs form.", "Okay");
                });
            }

            if (!HasAgreed)
            {
                datasCompleted = false;
                Device.BeginInvokeOnMainThread(() =>
                {
                    page.DisplayAlert("Missing Fields", "You must accept terms of service before registering.", "Okay");
                });
            }

            if (datasCompleted)
            {
                await Task.Run(async () =>
                {
                    await PopupNavigation.PushAsync(new LoadingPopupPage(), false);
                    reqType = 2;
                    var json = new
                    {
                        registration_record = new
                        {
                            location = LocationSource[SelectedIndex],
                            child_records_attributes = child_list,
                            agree_to_tos = HasAgreed ? "1" : "0",
                            customer_id = dataClass.CustomerUser.customer_id,
                            referrer = "day_care_tour"
                        },
                        commit = "Submit"
                    };
                    await restService.PostRequestAsync(Constants.REGISTRATION_RECORDS, json, cts.Token);
                });
            }


        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {

                switch (reqType)
                {
                    case 1:
                        List<string> _locationSource = new List<string>(), _locationAddresSource = new List<string>();
                        List<int> _locationMinimumAge = new List<int>();
                        List<LocationsModel> locations = JsonConvert.DeserializeObject<List<LocationsModel>>(jsonData["locations"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                        foreach (LocationsModel location in locations)
                        {
                            _locationSource.Add(location.name);
                            _locationAddresSource.Add(location.address);
                            _locationMinimumAge.Add(location.minimum_age);
                        }
                        LocationSource = _locationSource;
                        LocationAddress = _locationAddresSource;
                        SelectedIndex = 0;
                        SelectedItem = LocationSource[0];
                        reqType = 0;
                        break;
                    case 2:
                        await PopupNavigation.PopAllAsync();
                        await page.DisplayAlert("Success", "Day Care Tour Registration Complete", "Okay");
                        break;
                }

            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await PopupNavigation.PopAllAsync();
                await page.DisplayAlert(title, error, "okay");
            });
        }
    }

    public class DayCareChildRecord
    {
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string dob { get; set; }
        public string child_classification { get; set; }
        private string destroy = "false";
        public string _destroy { get { return destroy; } set { destroy = value; } }
    }
}
