﻿using System;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class TOSViewModel : BaseModel
        {

            private string _textSource;
            public string textSource
            {
                get { return _textSource; }
                set
                {
                    if (_textSource != value)
                    {
                        _textSource = value;
                        onPropertyChanged(nameof(textSource));
                    }
                }
            }

            private string _textTitle;
            public string textTitle
            {
                get { return _textTitle; }
                set
                {
                    if (_textTitle != value)
                    {
                        _textTitle = value;
                        onPropertyChanged(nameof(textTitle));
                    }
                }
            }

            string[] title = new string[] { "Single Day Program", "Day Care Program", "Before & After School Program", "Summer Camp Program", "Vacation Camp Program", "CIT Program Behavior Policy" };
            string[] terms = new string[6];

            public TOSViewModel(Page owner, int TOSCode)
            {
                SetTOS();
                textSource = terms[TOSCode];
                textTitle = title[TOSCode];
            }

            void SetTOS()
            {
                terms[0] = "Payment for full days must be paid in full at the time of registration.\n\n" +
                    "If an enrollment is cancelled outside of 7 days prior to the program start date that payment will be credited.\n\n" +
                    "If an enrollment is cancelled within 7 days of the program start no credits or refunds will apply.";

                terms[1] = "ENROLLMENT (APPLIES YEAR ROUND)\n\n" +
                    "All families will pay a deposit in the amount of two weeks of your child’s tuition, which will be held as a security deposit against your last two weeks in the TSL program. All families must pay their deposit immediately upon enrollment.\n\n" +
                    "Families with more than one child are entitled to a 10% discount on the lesser of the two tuition charges. (In the case of school-age children being the additional sibling enrollees, discounts will only apply to the flat monthly after school care bill and NOT for summer, vacation, or random full days of care.\n\n" +
                    "All families agree to pay the weekly tuition by Thursday of each week. Families who are two weeks arrears will be sent notification of pending dismissal from program.\n\n" +
                    "Families paying on a bi-weekly or other irregular and approved pay schedule are advised to keep track of the calendar and note any months that contain a 5th week to avoid getting behind.\n\n" +
                    "No pro-rates or discounts will be offered if the center closes for inclement weather or any other unexpected emergency or issue that makes closure necessary.";

                terms[2] = "A $25 non-refundable registration fee will be charged at the time of enrollment. This will not be applied to monthly tuition. Likewise, a $100 deposit will be required, which will be secured against your first month of fees.\n\n" +
                    "Those who opt to purchase a 10 month before and after school package at a discount will receive a limited refund equivalent to the amount outlined here in the Terms of Service if the need to cancel that package arises: 50% refund issued if package is cancelled prior to October 31st. 25% refund issued if package is cancelled after December 31st. Full refunds will be offered only within one week of the package purchase.\n\n" +
                    "Those of you who opt to purchase the single day annual package may send your child(ren) to any full day of care not included in vacation camp, summer camp, or any other camp that is being offered as a full week. Days that are included in the full day of care package include and are not limited to: conference days, Veteran’s day, Election Day, Jewish holidays, Columbus Day, Thanksgiving Eve, Christmas or New Year’s Eve, Martin Luther King Day. Once purchased, there will be no refunds on the single day annual package after 30 days. You will not need to register for any single day on the website and will be entitled to simply send your child to any full day of care needed during the year given a courtesy email to your respective site director.\n\n" +
                    "The billing cycle for regular before and after school is from the 1st-30th of each month, with payment deadlines established for the 1st of each month. Those of you opting to use the automatic payment feature will note deductions made on the 2nd of each month.\n\n" +
                    "A $5 per day late fee will be applied to any unpaid balances after the 15th of each month. These late fees will not be ignored if you opt to pay later in the month. They must be paid if you incur those fees.\n\n" +
                    "Payments should be made through your online account where possible, but we will accept personal checks or bill pay to the following address and by the due date. TSL 183 Schoolhouse Road, Albany 12203.\n\n" +
                    "Any changes in enrollment must be submitted in writing to tsladventures@gmail.com at least two weeks in advance of the next billing cycle. We will not lessen the amount of your monthly fee, if you inform us after the 1st of the respective month of withdraw. You must inform us prior to the 1st of the month you wish to withdraw to cancel future payments.\n\n" +
                    "Monthly fees may be pro-rated for the month enrolled, if that enrollment is submitted after the first week of the month.\n\n" +
                    "Account holders will receive notices of delivery, past due notices if applicable, and notices related to suspension of services at the end of each month if payment is not received. These notices are automatically generated and serve to prompt you of the importance of paying for services timely if you wish to continue services with our company. TSL does not, unless under special circumstances, cater to families who are in arrears more than 30 days on their account.\n\n" +
                    "Clients who depart program with an open balance are subject to any and all collection attempts made by TSL Kids Crew in pursuit of monies owed.";

                terms[3] = "All families will pay a non-refundable registration fee of $25 when enrolling and regardless of when enrollment is conducted. This fee is separate from your summer tuition fees. There is also a $150 deposit required to secure your spot, which is nonrefundable after May 15 or with registrations made after May 15, nonrefundable after 5 business days of filing your registration. The registration fee is not refundable at any point subsequent to the filing of the registration.\n\n" +
                    "No changes to summer registration invoices whereas the deletion of days or weeks are concerned will be made after June 1 of the given year.\n\n" +
                    "A family can opt to add days or weeks at any time as long as there is available space.\n\n" +
                    "Families who purchase a discounted package will not have any portion of that package price refunded to them at any time if they opt for lesser weeks. You can at any time request a full refund of your package cost by May 15th if you opt to register week to week instead.\n\n" +
                    "When you register to secure a spot, you enter a contract with TSL. TSL honors this contract by hiring staff, purchasing materials, managing your enrollment from the point it is filed through the end of summer, communicating with you efficiently with any issues, and most importantly offering sound and safe programming. When parents do not hold up their end of the contract (i.e not paying, dropping enrollment outside of guidelines etc) you put our company at financial risk and block other families from enrolling. If our clients fail to fulfill the contract based on our Terms of Service, we will seek restitution through our partner collection agency. Every number, address and point of contact used to file your registration will be submitted to said agency as part of their collections attempt. We likewise authorize this agency to report uncollected monies to the major credit credit-reporting bureaus and can and will affect your credit rating.\n\n" +
                    "TSL Adventures will not refund money, pro-rate invoices, or revise its enrollment policies in any way subsequent to the deadlines outlined above with the two exceptions: If the child is severely injured or is medically unable to attend for a long duration resulting from sickness or injury. In these cases, a doctor’s note will be required. Or, if the child is put out of program by TSL personnel for any reason.\n\n" +
                    "TSL does NOT forgive the contractual balance owed for ANY of the following reasons: responsible party’s loss of employment, responsible party moves out of town unexpectedly, responsible party becomes injured, responsible party realizes cost is higher than expected, responsible party wants to try another program, responsible party perceives program to be unfulfilling or not meeting expectations or for any other reason outside of those stated above as accepted situations that warrant a contract break without penalty.\n\n" +
                    "Balances should be paid by the deadlines reflected in your parent accounts.\n\n" +
                    "A $5 per day late fee will be applied to your unpaid balance for each day it is not paid beyond the payment deadline.\n\n" +
                    "All summer balances must be paid in full by the last session of the summer, regardless of any specialized payment plan a family has made with TSL Adventures.";

                terms[4] = "Payment for the program will be made in full at the time of registration for the selections made.\n\n" +
                    "Cancellation requests can be honored outside of seven days to the program start date with a credit only to a family's account.\n\n" +
                    "Cancellation requests within seven days of the program start date cannot be honored and no credits or refunds will apply.";

                terms[5] = "Adventures Counselor in Training (CIT) program is aimed at giving children ages 12-15 who are interested in pursuing a career in childcare relative work experience in the field. Our program focuses on developing interpersonal skills, responsibility, and leadership all while participating in our unique, community centered summer camp program. CITs are responsible for engaging children in games and crafts, assisting staff in daily responsibilities, and setting a positive example while leading children in small group activities.\n\n" +
                    "Children, aged 12-15, interested in the program must submit the application below, in order to be considered for the CIT Program. All applicants are subject to site director approval as there are limited spaces available at each summer camp location. Tuition to the program is $150 per week with a giveback of $35 per week of service. The program runs for 10 weeks over summer and CITs may attend as few or as many weeks as they wish. Children selected to participate in the CIT program will also be required to attend a Summer Camp Training session on June 22 at 5:30pm at a location to be announced. Applications will be reviewed with a week turnaround. If approved, parents will have to then register them using the summer registration form on our website.";

            }
        }
    }
