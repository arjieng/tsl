﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class SigninPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts;
        Page page;

        private string _Username;
        public string Username
        {
            get { return _Username; }
            set
            {
                if (_Username != value)
                {
                    _Username = value;
                    onPropertyChanged(nameof(Username));
                }
            }
        }

        private string _Password;
        public string Password
        {
            get { return _Password; }
            set
            {
                if (_Password != value)
                {
                    _Password = value;
                    onPropertyChanged(nameof(Password));
                }
            }
        }

        private ICommand _LoginCommand;
        public ICommand LoginCommand
        {
            get { return _LoginCommand; }
            set
            {
                if (_LoginCommand != value)
                {
                    _LoginCommand = value;
                    onPropertyChanged(nameof(LoginCommand));
                }
            }
        }

        private ICommand _ForgotPasswordCommand;
        public ICommand ForgotPasswordCommand
        {
            get { return _ForgotPasswordCommand; }
            set
            {
                if (_ForgotPasswordCommand != value)
                {
                    _ForgotPasswordCommand = value;
                    onPropertyChanged(nameof(ForgotPasswordCommand));
                }
            }
        }

        private ICommand _GotoSignupPageCommand;
        public ICommand GotoSignupPageCommand
        {
            get { return _GotoSignupPageCommand; }
            set
            {
                if (_GotoSignupPageCommand != value)
                {
                    _GotoSignupPageCommand = value;
                    onPropertyChanged(nameof(GotoSignupPageCommand));
                }
            }
        }

        private Color _UsernameBorderColor;
        public Color UsernameBorderColor
        {
            get { return _UsernameBorderColor; }
            set
            {
                if (_UsernameBorderColor != value)
                {
                    _UsernameBorderColor = value;
                    onPropertyChanged(nameof(UsernameBorderColor));
                }
            }
        }

        private Color _PasswordBorderColor;
        public Color PasswordBorderColor
        {
            get { return _PasswordBorderColor; }
            set
            {
                if (_PasswordBorderColor != value)
                {
                    _PasswordBorderColor = value;
                    onPropertyChanged(nameof(PasswordBorderColor));
                }
            }
        }

        private int _IsValidEmailOpacity;
        public int IsValidEmailOpacity
        {
            get { return _IsValidEmailOpacity; }
            set
            {
                if(_IsValidEmailOpacity != value)
                {
                    _IsValidEmailOpacity = value;
                    onPropertyChanged(nameof(IsValidEmailOpacity));
                }
            }
        }

        public SigninPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            Initialize();
        }

        void Initialize()
        {
            Username = "";
            Password = "";
            IsValidEmailOpacity = 0;
            UsernameBorderColor = Color.FromHex("#E8E6E7");
            PasswordBorderColor = Color.FromHex("#E8E6E7");
            LoginCommand = new Command(LoginAction);
            GotoSignupPageCommand = new Command(GoToSignup);
            ForgotPasswordCommand = new Command(GoToForgotPassword);
        }

        async void LoginAction()
        {
            //App.Current.MainPage = new LandingPage();
            bool IsValid = true;
            UsernameBorderColor = Color.FromHex("#E8E6E7");
            PasswordBorderColor = Color.FromHex("#E8E6E7");

            if (string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                UsernameBorderColor = Color.Red;
                PasswordBorderColor = Color.Red;
                IsValid = false;
            }

            if (UsernameBorderColor == Color.Red || PasswordBorderColor == Color.Red)
            {
                IsValid = false;
            }

            if(IsValidEmailOpacity == 1)
            {
                IsValid = false;
            }

            if (IsValid)
            {
                await Task.Run(async () =>
                {
                    PopupNavigation.PushAsync(new LoadingPopupPage());
                    string url = Constants.ROOT_URL + Constants.SIGNIN_URL;
                    await restService.PostRequestAsync(url, new { customer_user = new { email = Username, password = Password } } , cts.Token);
                });
            }
        }

        void GoToForgotPassword()
        {
            page.Navigation.PushAsync(new ForgotPasswordPage());
        }

        void GoToSignup()
        {
            page.Navigation.PushAsync(new SignupPage());
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
            Device.BeginInvokeOnMainThread(async ()  =>
            {
                if ((int.Parse(jsonData["status"].ToString())) == 200)
                {
                    var registeredUser = JsonConvert.DeserializeObject<CustomerUser>((jsonData["customer_user"].ToString()));
                    registeredUser.has_basic_information = Boolean.Parse(jsonData["has_basic_information"].ToString());
                    registeredUser.confirmed_information = Boolean.Parse(jsonData["confirmed_information"].ToString());
                    dataClass.CustomerUser = registeredUser;

                    await PopupNavigation.PopAllAsync();
                    if (registeredUser.has_basic_information && registeredUser.confirmed_information)
                        App.Current.MainPage = new LandingPage();
                    else
                        App.Current.MainPage = new UserInfoPage(true, registeredUser.email);
                }

                if((int.Parse(jsonData["status"].ToString())) == 300)
                {
                    await PopupNavigation.PopAllAsync();
                    await page.DisplayAlert("Error", "Invalid email or password", "Okay");
                }
            });
        }

        public async void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                await PopupNavigation.PopAllAsync();
                await page.DisplayAlert(title, error, "Okay");
            });
        }
    }
}
