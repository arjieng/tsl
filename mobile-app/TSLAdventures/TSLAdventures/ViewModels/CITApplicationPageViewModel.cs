﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows.Input;
using Newtonsoft.Json.Linq;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class CITApplicationPageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        CancellationTokenSource cts;
        Page page;

        private string _FirstNameTextSource;
        public string FirstNameTextSource
        {
            get { return _FirstNameTextSource; }
            set
            {
                if (_FirstNameTextSource != value)
                {
                    _FirstNameTextSource = value;
                    onPropertyChanged(nameof(FirstNameTextSource));
                }
            }
        }

        private string _LastNameTextSource;
        public string LastNameTextSource
        {
            get { return _LastNameTextSource; }
            set
            {
                if (_LastNameTextSource != value)
                {
                    _LastNameTextSource = value;
                    onPropertyChanged(nameof(LastNameTextSource));
                }
            }
        }

        private string _BirthdayTextSource;
        public string BirthdayTextSource
        {
            get { return _BirthdayTextSource; }
            set
            {
                if (_BirthdayTextSource != value)
                {
                    _BirthdayTextSource = value;
                    onPropertyChanged(nameof(BirthdayTextSource));
                }
            }
        }

        private string _PhoneNumberTextSource;
        public string PhoneNumberTextSource
        {
            get { return _PhoneNumberTextSource; }
            set
            {
                if (_PhoneNumberTextSource != value)
                {
                    _PhoneNumberTextSource = value;
                    onPropertyChanged(nameof(PhoneNumberTextSource));
                }
            }
        }

        private string _EmailAddressTextSource;
        public string EmailAddressTextSource
        {
            get { return _EmailAddressTextSource; }
            set
            {
                if (_EmailAddressTextSource != value)
                {
                    _EmailAddressTextSource = value;
                    onPropertyChanged(nameof(EmailAddressTextSource));
                }
            }
        }

        private string _AddressOneTextSource;
        public string AddressOneTextSource
        {
            get { return _AddressOneTextSource; }
            set
            {
                if (_AddressOneTextSource != value)
                {
                    _AddressOneTextSource = value;
                    onPropertyChanged(nameof(AddressOneTextSource));
                }
            }
        }

        private string _AddressTwoTextSource;
        public string AddressTwoTextSource
        {
            get { return _AddressTwoTextSource; }
            set
            {
                if (_AddressTwoTextSource != value)
                {
                    _AddressTwoTextSource = value;
                    onPropertyChanged(nameof(AddressTwoTextSource));
                }
            }
        }

        private string _CityTextSource;
        public string CityTextSource
        {
            get { return _CityTextSource; }
            set
            {
                if (_CityTextSource != value)
                {
                    _CityTextSource = value;
                    onPropertyChanged(nameof(CityTextSource));
                }
            }
        }

        private string _StateTextSource;
        public string StateTextSource
        {
            get { return _StateTextSource; }
            set
            {
                if (_StateTextSource != value)
                {
                    _StateTextSource = value;
                    onPropertyChanged(nameof(StateTextSource));
                }
            }
        }

        private string _ZipCodeTextSource;
        public string ZipCodeTextSource
        {
            get { return _ZipCodeTextSource; }
            set
            {
                if (_ZipCodeTextSource != value)
                {
                    _ZipCodeTextSource = value;
                    onPropertyChanged(nameof(ZipCodeTextSource));
                }
            }
        }

        private string _LocationPickerSelectedItem;
        public string LocationPickerSelectedItem
        {
            get { return _LocationPickerSelectedItem; }
            set
            {
                if (_LocationPickerSelectedItem != value)
                {
                    _LocationPickerSelectedItem = value;
                    onPropertyChanged(nameof(LocationPickerSelectedItem));
                }
            }
        }

        private string _PickerSelectedItem;
        public string PickerSelectedItem
        {
            get { return _PickerSelectedItem; }
            set
            {
                if (_PickerSelectedItem != value)
                {
                    _PickerSelectedItem = value;
                    onPropertyChanged(nameof(PickerSelectedItem));
                }
            }
        }

        private ICommand _GoToStep2Command;
        public ICommand GoToStep2Command
        {
            get { return _GoToStep2Command; }
            set
            {
                if (_GoToStep2Command != value)
                {
                    _GoToStep2Command = value;
                    onPropertyChanged(nameof(GoToStep2Command));
                }
            }
        }

        public CITApplicationPageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            Initialize();
        }

        void Initialize()
        {
            GoToStep2Command = new Command(GotoStep2Action);
            FirstNameTextSource = "";
            LastNameTextSource = "";
            BirthdayTextSource = "";
            PhoneNumberTextSource = "";
            EmailAddressTextSource = "";
            AddressOneTextSource = "";
            AddressTwoTextSource = "";
            CityTextSource = "";
            StateTextSource = "";
            PickerSelectedItem = "";
            ZipCodeTextSource = "";
            LocationPickerSelectedItem = "Clifton Park";
        }

        void GotoStep2Action()
        {
            if (!string.IsNullOrEmpty(FirstNameTextSource) || !string.IsNullOrEmpty(LastNameTextSource) || !string.IsNullOrEmpty(BirthdayTextSource) || !string.IsNullOrEmpty(PhoneNumberTextSource) ||
                !string.IsNullOrEmpty(EmailAddressTextSource) || !string.IsNullOrEmpty(AddressOneTextSource) || !string.IsNullOrEmpty(CityTextSource) || !string.IsNullOrEmpty(StateTextSource) || !string.IsNullOrEmpty(ZipCodeTextSource))
            {
                Dictionary<string, string> objects = new Dictionary<string, string> { };
                objects.Add("location", LocationPickerSelectedItem);
                objects.Add("first_name", FirstNameTextSource);
                objects.Add("last_name", LastNameTextSource);
                objects.Add("dob", BirthdayTextSource);
                objects.Add("phone_number", PhoneNumberTextSource);
                objects.Add("email", EmailAddressTextSource);
                objects.Add("address_line_one", AddressOneTextSource);
                objects.Add("address_line_two", AddressTwoTextSource);
                objects.Add("city", CityTextSource);
                objects.Add("state", StateTextSource);
                objects.Add("zip_code", ZipCodeTextSource);
                objects.Add("how_did_you_hear_about_us", PickerSelectedItem);

                page.Navigation.PushAsync(new CITApplication2Page(objects));
            }
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {

        }

        public void ReceiveTimeoutError(string title, string error)
        {
        
        }
    }
}
