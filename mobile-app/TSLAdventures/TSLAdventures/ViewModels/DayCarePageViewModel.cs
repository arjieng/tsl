﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TSLAdventures.Models;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class DayCarePageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        CancellationTokenSource cts;
        Page page;
        int reqType = 0;

        private List<string> _LocationSource;
        public List<string> LocationSource
        {
            get { return _LocationSource; }
            set
            {
                if (_LocationSource != value)
                {
                    _LocationSource = value;
                    onPropertyChanged(nameof(LocationSource));
                }
            }
        }

        private List<string> _LocationAddress;
        public List<string> LocationAddress
        {
            get { return _LocationAddress; }
            set
            {
                if (_LocationAddress != value)
                {
                    _LocationAddress = value;
                    onPropertyChanged(nameof(LocationAddress));
                }
            }
        }

        private string _SelectedLocationAddress;
        public string SelectedLocationAddress
        {
            get { return _SelectedLocationAddress; }
            set
            {
                if (_SelectedLocationAddress != value)
                {
                    _SelectedLocationAddress = value;
                    onPropertyChanged(nameof(SelectedLocationAddress));
                }
            }
        }

        private DayCareChildRecord _ChildOne;
        public DayCareChildRecord ChildOne
        {
            get { return _ChildOne; }
            set
            {
                if (_ChildOne != value)
                {
                    _ChildOne = value;
                    onPropertyChanged(nameof(ChildOne));
                }
            }
        }

        private DayCareChildRecord _ChildTwo;
        public DayCareChildRecord ChildTwo
        {
            get { return _ChildTwo; }
            set
            {
                if (_ChildTwo != value)
                {
                    _ChildTwo = value;
                    onPropertyChanged(nameof(ChildTwo));
                }
            }
        }

        private DayCareChildRecord _ChildThree;
        public DayCareChildRecord ChildThree
        {
            get { return _ChildThree; }
            set
            {
                if (_ChildThree != value)
                {
                    _ChildThree = value;
                    onPropertyChanged(nameof(ChildThree));
                }
            }
        }

        private string _SelectedItem;
        public string SelectedItem
        {
            get { return _SelectedItem; }
            set
            {
                if (_SelectedItem != value)
                {
                    _SelectedItem = value;
                    onPropertyChanged(nameof(SelectedItem));
                }
            }
        }

        private bool _FirstChildVisibility;
        public bool FirstChildVisibility
        {
            get { return _FirstChildVisibility; }
            set
            {
                if (_FirstChildVisibility != value)
                {
                    _FirstChildVisibility = value;
                    onPropertyChanged(nameof(FirstChildVisibility));
                }
            }
        }

        private bool _SecondChildVisibility;
        public bool SecondChildVisibility
        {
            get { return _SecondChildVisibility; }
            set
            {
                if (_SecondChildVisibility != value)
                {
                    _SecondChildVisibility = value;
                    onPropertyChanged(nameof(SecondChildVisibility));
                }
            }
        }

        private bool _ThirdChildVisibility;
        public bool ThirdChildVisibility
        {
            get { return _ThirdChildVisibility; }
            set
            {
                if (_ThirdChildVisibility != value)
                {
                    _ThirdChildVisibility = value;
                    onPropertyChanged(nameof(ThirdChildVisibility));
                }
            }
        }

        private bool _HasAgreed;
        public bool HasAgreed
        {
            get { return _HasAgreed; }
            set
            {
                _HasAgreed = value;
                onPropertyChanged(nameof(HasAgreed));
            }
        }

        private int _SelectedIndex;
        public int SelectedIndex
        {
            get { return _SelectedIndex; }
            set
            {
                if (_SelectedIndex != value)
                {
                    _SelectedIndex = value;
                    onPropertyChanged(nameof(SelectedIndex));
                }
            }
        }

        private ICommand _SubmitCommand;
        public ICommand SubmitCommand
        {
            get { return _SubmitCommand; }
            set
            {
                if (_SubmitCommand != value)
                {
                    _SubmitCommand = value;
                    onPropertyChanged(nameof(SubmitCommand));
                }
            }
        }

        private ICommand _SubmitPasscodeCommand;
        public ICommand SubmitPasscodeCommand
        {
            get { return _SubmitPasscodeCommand; }
            set
            {
                if (_SubmitPasscodeCommand != value)
                {
                    _SubmitPasscodeCommand = value;
                    onPropertyChanged(nameof(SubmitPasscodeCommand));
                }
            }
        }

        private ICommand _GotoTOSCommand;
        public ICommand GotoTOSCommand
        {
            get { return _GotoTOSCommand; }
            set
            {
                if (_GotoTOSCommand != value)
                {
                    _GotoTOSCommand = value;
                    onPropertyChanged(nameof(GotoTOSCommand));
                }
            }
        }

        private ICommand _RemoveFirstChildCommand;
        public ICommand RemoveFirstChildCommand
        {
            get { return _RemoveFirstChildCommand; }
            set
            {
                if (_RemoveFirstChildCommand != value)
                {
                    _RemoveFirstChildCommand = value;
                    onPropertyChanged(nameof(RemoveFirstChildCommand));
                }
            }
        }

        private ICommand _RemoveSecondChildCommand;
        public ICommand RemoveSecondChildCommand
        {
            get { return _RemoveSecondChildCommand; }
            set
            {
                if (_RemoveSecondChildCommand != value)
                {
                    _RemoveSecondChildCommand = value;
                    onPropertyChanged(nameof(RemoveSecondChildCommand));
                }
            }
        }

        private ICommand _RemoveThirdChildCommand;
        public ICommand RemoveThirdChildCommand
        {
            get { return _RemoveThirdChildCommand; }
            set
            {
                if (_RemoveThirdChildCommand != value)
                {
                    _RemoveThirdChildCommand = value;
                    onPropertyChanged(nameof(RemoveThirdChildCommand));
                }
            }
        }

        private ICommand _AddChildCommand;
        public ICommand AddChildCommand
        {
            get { return _AddChildCommand; }
            set
            {
                if (_AddChildCommand != value)
                {
                    _AddChildCommand = value;
                    onPropertyChanged(nameof(AddChildCommand));
                }
            }
        }

        private string _DayCareCodeText;
        public string DayCareCodeText
        {
            get { return _DayCareCodeText; }
            set
            {
                if(_DayCareCodeText != value)
                {
                    _DayCareCodeText = value;
                    onPropertyChanged(nameof(DayCareCodeText));
                }
            }
        }

        bool _PasscodeContainerVisibility;
        public bool PasscodeContainerVisibility
        {
            get { return _PasscodeContainerVisibility; }
            set { _PasscodeContainerVisibility = value; onPropertyChanged(nameof(PasscodeContainerVisibility)); }
        }

        bool _FormContainerVisibility;
        public bool FormContainerVisibility
        {
            get { return _FormContainerVisibility; }
            set { _FormContainerVisibility = value; onPropertyChanged(nameof(FormContainerVisibility)); }
        }

        public DayCarePageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            InitValues();
        }

        async void InitValues()
        {
            FormContainerVisibility = false;
            PasscodeContainerVisibility = true;
            DayCareCodeText = "";
            FirstChildVisibility = true;
            SecondChildVisibility = false;
            ThirdChildVisibility = false;
            ChildOne = new DayCareChildRecord();
            ChildTwo = new DayCareChildRecord();
            ChildThree = new DayCareChildRecord();
            SubmitCommand = new Command(SubmitAction);
            SubmitPasscodeCommand = new Command(SubmitPasscodeAction);
            GotoTOSCommand = new Command((x) => page.Navigation.PushAsync(new TOSPage(1)));
            RemoveFirstChildCommand = new Command((x) => FirstChildVisibility = false);
            RemoveSecondChildCommand = new Command((x) => SecondChildVisibility = false);
            RemoveThirdChildCommand = new Command((x) => ThirdChildVisibility = false);
            AddChildCommand = new Command(AddChildCommandAction);

            await Task.Run(async () =>
            {
                reqType = 1;
                string url = string.Format("{0}{1}?program_name=Day Care", Constants.ROOT_URL, Constants.GET_LOCATIONS);
                System.Diagnostics.Debug.WriteLine(url);
                await restService.GetRequest(url, cts.Token);
            });
        }

        void AddChildCommandAction()
        {
            if (!FirstChildVisibility)
            {
                FirstChildVisibility = true;
            }
            else if (!SecondChildVisibility)
            {
                SecondChildVisibility = true;
            }
            else if (!ThirdChildVisibility)
            {
                ThirdChildVisibility = true;
            }
        }

        private void SubmitPasscodeAction(object obj)
        {
            if (DayCareCodeText.Equals("adventure"))
            {
                PasscodeContainerVisibility = false;
                FormContainerVisibility = true;
            }

            if (string.IsNullOrEmpty(DayCareCodeText))
            {

            }
        }

        void SubmitAction()
        {

        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {

                switch (reqType)
                {
                    case 1:
                        List<string> _locationSource = new List<string>(), _locationAddresSource = new List<string>();
                        List<int> _locationMinimumAge = new List<int>();
                        List<LocationsModel> locations = JsonConvert.DeserializeObject<List<LocationsModel>>(jsonData["locations"].ToString(), new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
                        foreach (LocationsModel location in locations)
                        {
                            _locationSource.Add(location.name);
                            _locationAddresSource.Add(location.address);
                            _locationMinimumAge.Add(location.minimum_age);
                        }
                        LocationSource = _locationSource;
                        LocationAddress = _locationAddresSource;
                        SelectedIndex = 0;
                        SelectedItem = LocationSource[0];
                        reqType = 0;
                        break;
                    case 2:
                        //await PopupNavigation.PopAllAsync();
                        //await page.DisplayAlert("Success", "Day Care Tour Registration Complete", "Okay");
                        break;
                }

            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {

        }
    }
}
