﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using TSLAdventures.Utilities;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using TSLAdventures.Views;
using Xamarin.Forms;

namespace TSLAdventures.ViewModels
{
    public class ViewInvoicePageViewModel : BaseModel, IRestConnector
    {
        RestService restService = new RestService();
        DataClass dataClass = DataClass.GetInstance;
        CancellationTokenSource cts; Page page;
        int reqType = 0;

        public ViewInvoicePageViewModel(Page owner)
        {
            restService.RestServiceDelegate = this;
            cts = new CancellationTokenSource();
            page = owner;

            InvoiceAmountFormattedString = new FormattedString();
            AmountPaidFormattedString = new FormattedString();
            DiscountFormattedString = new FormattedString();
            FeesFormattedString = new FormattedString();
            DueFormattedString = new FormattedString();
        }

        FormattedString _InvoiceAmountFormattedString;
        public FormattedString InvoiceAmountFormattedString
        {
            get { return _InvoiceAmountFormattedString; }
            set { _InvoiceAmountFormattedString = value; onPropertyChanged(nameof(InvoiceAmountFormattedString)); }
        }

        FormattedString _AmountPaidFormattedString;
        public FormattedString AmountPaidFormattedString
        {
            get { return _AmountPaidFormattedString; }
            set { _AmountPaidFormattedString = value; onPropertyChanged(nameof(AmountPaidFormattedString)); }
        }

        FormattedString _DiscountFormattedString;
        public FormattedString DiscountFormattedString
        {
            get { return _DiscountFormattedString; }
            set { _DiscountFormattedString = value; onPropertyChanged(nameof(DiscountFormattedString)); }
        }

        FormattedString _FeesFormattedString;
        public FormattedString FeesFormattedString
        {
            get { return _FeesFormattedString; }
            set { _FeesFormattedString = value; onPropertyChanged(nameof(FeesFormattedString)); }
        }

        FormattedString _DueFormattedString;
        public FormattedString DueFormattedString
        {
            get { return _DueFormattedString; }
            set { _DueFormattedString = value; onPropertyChanged(nameof(DueFormattedString)); }
        }

        Invoices _Invoice;
        public Invoices Invoice
        {
            get { return _Invoice; }
            set { _Invoice = value; onPropertyChanged(nameof(Invoice)); }
        }

        public async void Init()
        {
            await Task.Run(async () =>
            {
                System.Diagnostics.Debug.WriteLine(Constants.ROOT_URL + $"/invoices/{(page as ViewInvoicePage).id}");
                await restService.GetRequest(Constants.ROOT_URL + $"/invoices/{(page as ViewInvoicePage).id}", cts.Token);
            });
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            Device.BeginInvokeOnMainThread(() =>
            {
                InvoiceAmountFormattedString = new FormattedString();
                InvoiceAmountFormattedString.Spans.Add(new Span() { Text = "Invoice Amount: $" });
                InvoiceAmountFormattedString.Spans.Add(new Span { Text = jsonData["invoice_amount"].ToString(), FontAttributes = FontAttributes.Bold, FontSize = 17.ScaleFont() });

                AmountPaidFormattedString = new FormattedString();
                AmountPaidFormattedString.Spans.Add(new Span() { Text = "Amount Paid: $" });
                AmountPaidFormattedString.Spans.Add(new Span { Text = jsonData["amount_paid"].ToString(), FontAttributes = FontAttributes.Bold, FontSize = 17.ScaleFont() });

                DiscountFormattedString = new FormattedString();
                DiscountFormattedString.Spans.Add(new Span() { Text = "Disounts: $" });
                DiscountFormattedString.Spans.Add(new Span { Text = jsonData["discount"].ToString(), FontAttributes = FontAttributes.Bold, FontSize = 17.ScaleFont() });

                FeesFormattedString = new FormattedString();
                FeesFormattedString.Spans.Add(new Span() { Text = "Fees: $" });
                FeesFormattedString.Spans.Add(new Span { Text = jsonData["fees"].ToString(), FontAttributes = FontAttributes.Bold, FontSize = 17.ScaleFont() });

                DueFormattedString = new FormattedString();
                DueFormattedString.Spans.Add(new Span() { Text = "Due: $" });
                DueFormattedString.Spans.Add(new Span { Text = jsonData["due"].ToString(), FontAttributes = FontAttributes.Bold, FontSize = 17.ScaleFont() });

            });
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            Device.BeginInvokeOnMainThread(async () =>
            {
                System.Diagnostics.Debug.WriteLine($"{title} {error}");
            });
        }
    }
}
