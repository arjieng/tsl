﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using TSLAdventures.Utilities.Helpers.RestService;
using TSLAdventures.Views.PopupPages;
using Xamarin.Forms;

namespace TSLAdventures
{
    public partial class MainPage : ContentPage, IRestConnector
    {
        RestService restService;
        public MainPage()
        {
            restService = new RestService { RestServiceDelegate = this };
            InitializeComponent();
        }

        public void ReceiveJSONData(JObject jsonData, CancellationToken ct)
        {
            System.Diagnostics.Debug.WriteLine(jsonData);
        }

        public void ReceiveTimeoutError(string title, string error)
        {
            System.Diagnostics.Debug.WriteLine(error);
        }

        async void Handle_Clicked(object sender, System.EventArgs e)
        {
            //await Task.Run(async () =>
            //{
            //    CancellationTokenSource cts = new CancellationTokenSource();
            //    await restService.RequestCardToken("4242424242424242", "07", "2020", "123", cts.Token);
            //});
            //await PopupNavigation.PushAsync(new CardInfoPopupPage());
        }
    }
}
