﻿using System;
namespace TSLAdventures.Models
{
    public class RegistrationInfo
    {
        public int id { get; set; }
        public string program { get; set; }
        public DateTime date { get; set; }
        public bool is_active { get; set; }
    }
}
