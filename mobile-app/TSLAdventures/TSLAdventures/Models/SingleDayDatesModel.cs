﻿using System;
namespace TSLAdventures.Models
{
    public class SingleDayDatesModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string date { get; set; }
        public int capacity { get; set; }
    }
}
