﻿using System;
namespace TSLAdventures.Models
{
    public class Student
    {
        public string id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public int age { get; set; }
        public string dob { get; set; }
        public string school_attending { get; set; }
    }
}
