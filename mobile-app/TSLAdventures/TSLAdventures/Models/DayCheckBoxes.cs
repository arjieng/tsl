﻿using System;
using TSLAdventures.ViewModels;

namespace TSLAdventures.Models
{
    public class DayCheckBoxes : BaseModel
    {
        int _id;
        public int id
        {
            get { return _id; }
            set
            {
                if(_id != value)
                {
                    _id = value;
                    onPropertyChanged(nameof(id));
                }
            }
        }

        private string _Text;
        public string Text
        {
            get { return _Text; }
            set
            {
                if (_Text != value)
                {
                    _Text = value;
                    onPropertyChanged(nameof(Text));
                }
            }
        }

        private bool _IsChecked;
        public bool IsChecked
        {
            get { return _IsChecked; }
            set
            {
                if (_IsChecked != value)
                {
                    _IsChecked = value;
                    onPropertyChanged(nameof(IsChecked));
                }
            }
        }
    }

    public class SummerCampRegularRegistration{
        public int id{ get; set; }
        public string name { get; set; }
        public string start_date { get; set; }
        public int capacity { get; set; }
        public bool IsChecked { get; set; }
    }
}
