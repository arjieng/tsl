﻿using System;
namespace TSLAdventures.Models
{
    public class CustomerUser
    {
        public int customer_id { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public string phone_number { get; set; }
        public string emergency_phone_number { get; set; }
        public string address_line_one { get; set; }
        public string address_line_two { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public int zip_code { get; set; }
        public string how_did_you_hear_about_us { get; set; }
        public string email { get; set; }
        public string access_token { get; set; }
        public bool has_basic_information { get; set; }
        public bool confirmed_information { get; set; }
        public string card_last_four { get; set; }
        public string card_brand { get; set; }
        public bool autopay_enabled { get; set; }
    }
}