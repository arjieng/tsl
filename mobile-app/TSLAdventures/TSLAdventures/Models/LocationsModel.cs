﻿using System;
using TSLAdventures.ViewModels;

namespace TSLAdventures.Models
{
    public class LocationsModel
    {
        public string name { get; set; }
        public string address { get; set; }
        public int minimum_age { get; set; }

    }
}
