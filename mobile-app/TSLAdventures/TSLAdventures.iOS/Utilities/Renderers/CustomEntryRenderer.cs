﻿using System;
using System.ComponentModel;
using TSLAdventures.iOS.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using TSLAdventures.Utilities.Helpers.ScaleHelper;

[assembly:ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace TSLAdventures.iOS.Utilities.Renderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        float animatedDistance;
        CustomEntry customEntry;
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            //Control.Layer.BorderWidth = 0;
            //Control.BorderStyle = UITextBorderStyle.None;
            Control.Layer.BorderColor = customEntry.BorderColor.ToCGColor();
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            var entry = Element as CustomEntry;
            if (e.NewElement != null)
            {
                customEntry = (CustomEntry)Element;
                Control.BorderStyle = UITextBorderStyle.RoundedRect;
                Control.Layer.BorderWidth = 1.ScaleHeight();
                Control.Layer.BorderColor = customEntry.BorderColor.ToCGColor();
                Control.Layer.CornerRadius = entry.BorderRadius;

                if (customEntry.TextAlignment == "Center")
                {
                    Control.TextAlignment = UITextAlignment.Center;
                }

                if (customEntry.TextAlignment == "Right")
                {
                    Control.TextAlignment = UITextAlignment.Right;
                }

                if (customEntry.AutoCapitalization == "Words")
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Words;
                }
                else if (customEntry.AutoCapitalization == "Sentences")
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.Sentences;
                }
                else
                {
                    Control.AutocapitalizationType = UITextAutocapitalizationType.None;
                }

                Control.EditingDidBegin += OnEntryDidEdit;
                Control.EditingDidEnd += OnEntryDidEnd;
            }
        }

        private void OnEntryDidEnd(object sender, EventArgs e)
        {
            UIView parentView = getParentView();
            var viewFrame = parentView.Bounds;

            viewFrame.Y = 0.0f;

            UIView.BeginAnimations(null, (IntPtr)null);
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(0.3);

            parentView.Frame = viewFrame;

            UIView.CommitAnimations();
        }

        private void OnEntryDidEdit(object sender, EventArgs e)
        {
            UIView parentWindow = getParentView();
            var textfieldRect = parentWindow.ConvertRectFromView(Control.Bounds, Control);
            var viewRect = parentWindow.ConvertRectFromView(parentWindow.Bounds, parentWindow);

            float midline = (float)(textfieldRect.Y + 0.5 * textfieldRect.Height);
            float numerator = (float)(midline - viewRect.Y - 0.2 * viewRect.Height);
            float denominator = (float)((1.0f - 0.2f) * viewRect.Height);
            float heightFraction = numerator / denominator;

            if (heightFraction < 0.0)
            {
                heightFraction = 0.0f;
            }
            else if (heightFraction > 1.0)
            {
                heightFraction = 1.0f;
            }

            UIInterfaceOrientation orientation = UIApplication.SharedApplication.StatusBarOrientation;
            if (orientation == UIInterfaceOrientation.Portrait || orientation == UIInterfaceOrientation.PortraitUpsideDown)
            {
                if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((216.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(216.0f * heightFraction);
                }
            }
            else
            {
                if (customEntry.Keyboard == Keyboard.Numeric || customEntry.Keyboard == Keyboard.Telephone)
                {
                    animatedDistance = (float)Math.Floor((162.0f + 44.0f) * heightFraction);
                }
                else
                {
                    animatedDistance = (float)Math.Floor(162.0f * heightFraction);
                }
            }

            var viewFrame = parentWindow.Frame;
            viewFrame.Y -= animatedDistance;

            UIView.BeginAnimations(null, (IntPtr)null);
            UIView.SetAnimationBeginsFromCurrentState(true);
            UIView.SetAnimationDuration(0.3);

            parentWindow.Frame = viewFrame;

            UIView.CommitAnimations();
        }

        UIView getParentView()
        {
            UIView view = Control.Superview;

            while (view != null && !(view is UIWindow))
            {
                view = view.Superview;
            }

            return view;
        }
    }
}
