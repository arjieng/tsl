﻿using System;
using System.ComponentModel;
using TSLAdventures.iOS.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly:ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace TSLAdventures.iOS.Utilities.Renderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            Control.Layer.BorderWidth = 0;
            Control.BorderStyle = UITextBorderStyle.None;
        }
    }
}
