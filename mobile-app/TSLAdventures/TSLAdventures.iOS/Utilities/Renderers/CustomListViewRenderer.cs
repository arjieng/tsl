﻿using TSLAdventures.iOS.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(CustomListView), typeof(CustomListViewRenderer))]
namespace TSLAdventures.iOS.Utilities.Renderers
{
    public class CustomListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;

            Control.ShowsVerticalScrollIndicator = false;
            Control.ScrollEnabled = (Element as CustomListView).IsScrollEnabled;
        }
    }
}
