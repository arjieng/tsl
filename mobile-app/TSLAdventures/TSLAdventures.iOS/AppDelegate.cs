﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarouselView.FormsPlugin.iOS;
using Foundation;
using SuaveControls.MaterialForms.iOS;
using UIKit;
using Xamarin.Forms;

namespace TSLAdventures.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            App.screenWidth = (float)UIScreen.MainScreen.Bounds.Width;
            App.screenHeight = (float)UIScreen.MainScreen.Bounds.Height;
            App.appScale = (float)UIScreen.MainScreen.Scale;
            App.DeviceType = 0;
            app.SetStatusBarStyle(UIStatusBarStyle.LightContent, true);
            RendererInitializer.Init();
            CarouselViewRenderer.Init();
            Rg.Plugins.Popup.Popup.Init();
            global::Xamarin.Forms.Forms.Init();
            Plugin.InputKit.Platforms.iOS.Config.Init();
            LoadApplication(new App());

            return base.FinishedLaunching(app, options);
        }
    }
}
