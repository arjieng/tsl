﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using TSLAdventures.Droid.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomEditor), typeof(CustomEditorRenderer))]
namespace TSLAdventures.Droid.Utilities.Renderers
{
    public class CustomEditorRenderer : EditorRenderer
    {
        public CustomEditorRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Editor> e)
        {
            base.OnElementChanged(e);
            if (Element == null)
                return;

            UpdateBorders();
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            UpdateBorders();
        }

        void UpdateBorders()
        {
            var entry = Element as CustomEditor;
            GradientDrawable shape = new GradientDrawable();
            shape.SetColor(Android.Graphics.Color.White);
            shape.SetShape(ShapeType.Rectangle);
            shape.SetCornerRadius(entry.BorderRadius);
            shape.SetStroke(2, entry.BorderColor.ToAndroid());
            this.Control.SetBackground(shape);
        }
    }
}
