﻿using System;
using Android.Content;
using TSLAdventures.Droid.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(WebView), typeof(CustomWebviewRenderer))]
namespace TSLAdventures.Droid.Utilities.Renderers
{
    public class CustomWebviewRenderer : WebViewRenderer
    {
        public CustomWebviewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<WebView> e)
        {
            base.OnElementChanged(e);

            if (Element == null)
                return;

            Control.SetBackgroundColor(Color.Transparent.ToAndroid());
        }
    }
}
