﻿using System;
using System.ComponentModel;
using Android.Content;
using Android.Graphics.Drawables;
using Android.Views;
using TSLAdventures.Droid.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using TSLAdventures.Utilities.Helpers.ScaleHelper;
using Android.Util;

[assembly: ExportRenderer(typeof(CustomEntry), typeof(CustomEntryRenderer))]
namespace TSLAdventures.Droid.Utilities.Renderers
{
    public class CustomEntryRenderer : EntryRenderer
    {
        public CustomEntryRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Entry> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;

            var customEntry = e.NewElement as CustomEntry;
            if (customEntry.TextAlignment == "Center")
            {
                Control.Gravity = GravityFlags.CenterHorizontal;
            }
            Control.Background = null;
            //Control.SetPadding(Control.PaddingLeft, Control.PaddingTop, Control.PaddingRight, (int)10.ScaleHeight());
            UpdateBorders();
         }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);
            UpdateBorders();
        }

        void UpdateBorders()
        {
            var entry = Element as CustomEntry;
            GradientDrawable shape = new GradientDrawable();
            shape.SetColor(Android.Graphics.Color.White);
            shape.SetShape(ShapeType.Rectangle);
            shape.SetCornerRadius(DpToPixels(this.Context,entry.BorderRadius));
            shape.SetStroke(2, entry.BorderColor.ToAndroid());
            this.Control.SetBackground(shape);
        }

        public static float DpToPixels(Context context, float valueInDp)
        {
            DisplayMetrics metrics = context.Resources.DisplayMetrics;
            return TypedValue.ApplyDimension(ComplexUnitType.Dip, valueInDp, metrics);
        }
    }
}
