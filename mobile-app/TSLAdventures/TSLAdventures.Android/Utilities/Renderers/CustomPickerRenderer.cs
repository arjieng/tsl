﻿using System;
using Android.Content;
using TSLAdventures.Droid.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly:ExportRenderer(typeof(CustomPicker), typeof(CustomPickerRenderer))]
namespace TSLAdventures.Droid.Utilities.Renderers
{
    public class CustomPickerRenderer : PickerRenderer
    {
        public CustomPickerRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Picker> e)
        {
            base.OnElementChanged(e);
            Control.Background = null;
        }
    }
}
