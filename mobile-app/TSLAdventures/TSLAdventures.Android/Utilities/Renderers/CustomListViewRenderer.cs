﻿using System;
using Android.Content;
using TSLAdventures.Droid.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomListView), typeof(CustomListViewRenderer))]
namespace TSLAdventures.Droid.Utilities.Renderers
{
    public class CustomListViewRenderer : ListViewRenderer
    {
        public CustomListViewRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);
            if (Element == null)
                return;

            Control.VerticalScrollBarEnabled = false;
        }
    }
}
