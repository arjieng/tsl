﻿using System;
using Android.Content;
using TSLAdventures.Droid.Utilities.Renderers;
using TSLAdventures.Utilities.Renderers;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(CustomButton), typeof(CustomButtonRenderer))]
namespace TSLAdventures.Droid.Utilities.Renderers
{
    public class CustomButtonRenderer : ButtonRenderer
    {
        public CustomButtonRenderer(Context context) : base(context)
        {
        }

        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (Control == null)
                return;

            var customButton = e.NewElement as CustomButton;
            if (customButton.TextCasing == "Pascal")
            {
                Control.SetAllCaps(false);
            }

            if (customButton.TextPadding == "Zero")
            {
                //Control.Gravity = Android.Views.GravityFlags.Left | Android.Views.GravityFlags.Top;
                Control.SetPadding(0, 0, 0, 0);
            }
        }
    }
}
